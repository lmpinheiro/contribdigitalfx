-- MySQL dump 10.13  Distrib 5.6.24, for Win32 (x86)
--
-- Host: localhost    Database: contdigital
-- ------------------------------------------------------
-- Server version	5.6.25-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_user`
--

DROP TABLE IF EXISTS `tb_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user` (
  `ID_USER` int(11) NOT NULL AUTO_INCREMENT,
  `X_NAME` varchar(254) NOT NULL,
  `X_SURNAME` varchar(254) NOT NULL,
  `X_EMAIL` varchar(45) NOT NULL,
  `X_PASS` varchar(45) NOT NULL,
  `DT_REGISTER` datetime NOT NULL,
  `X_QUESTION` varchar(256) NOT NULL,
  `X_ANSWER` varchar(256) NOT NULL,
  `IS_ENABLE` int(11) DEFAULT '0',
  PRIMARY KEY (`ID_USER`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user`
--

LOCK TABLES `tb_user` WRITE;
/*!40000 ALTER TABLE `tb_user` DISABLE KEYS */;
INSERT INTO `tb_user` VALUES (30,'Luan','Asbahr','luan.mackenzie@gmail.com','a35476b9052cb745c7937b9c7f3cbfd4','2015-07-10 00:00:00','asdasd','123',1),(34,'Luan','Mendes','luan.mackenzie@gmail.coma','a35476b9052cb745c7937b9c7f3cbfd4','2015-07-11 00:00:00','','',NULL),(35,'Kuasn','sdsad','sda@sdasd.com','287ba77fba533a7cba7b9538ac692ce4','2015-07-11 00:00:00','','',NULL),(36,'sdad','sdasdasd','luan.mackenzie@gmail.comaa','a35476b9052cb745c7937b9c7f3cbfd4','2015-07-11 00:00:00','','',NULL),(37,'Luan','Mendes ','luan.macke@gail.com','616343fc9cc1c92066d6c0a9bb9023ea','2015-07-11 00:00:00','','',NULL),(38,'sadasdasd','asdasd','asdasdasd@asda.asda','f63e972cb1bc78f6f62a51f1594fce07','2015-07-12 00:00:00','','',NULL),(39,'Luan','Asbahs','lausd@asdfas.com','2fe3600d4af0e1326f6a3b47a7cdd39a','2015-07-12 00:00:00','asdad','asdasdasd',NULL),(40,'Rebeca','Asbahr Silva Pinheiro','rebecaasbahr@yahoo.com.br','fce1f9f828179747ba3b1310ca4df76e','2015-07-16 00:00:00','Qual o homem que eu mais amo','luan',NULL),(41,'Lua ','OPinh','lasj@asd.com','a35476b9052cb745c7937b9c7f3cbfd4','2015-07-25 00:00:00','asdasd','asdad',1),(42,'luan','adas','asdasda@asdas.com','a35476b9052cb745c7937b9c7f3cbfd4','2015-07-25 00:00:00','asdas','asdasd',1),(43,'dasdasd','asdasdad','asd@adasd.com.br','a35476b9052cb745c7937b9c7f3cbfd4','2015-07-25 00:00:00','asdasdasd','asdasd',1),(44,'sddad','asdasda','sdasdasd@sdac.com','a35476b9052cb745c7937b9c7f3cbfd4','2015-07-26 00:00:00','asdadasd','asdasd',1);
/*!40000 ALTER TABLE `tb_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-20 16:40:30
