-- MySQL dump 10.13  Distrib 5.6.24, for Win32 (x86)
--
-- Host: localhost    Database: contdigital
-- ------------------------------------------------------
-- Server version	5.6.25-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_transmission_reg`
--

DROP TABLE IF EXISTS `tb_transmission_reg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_transmission_reg` (
  `ID_REGISTER` int(11) NOT NULL AUTO_INCREMENT,
  `ID_ISSUER` int(11) NOT NULL,
  `DT_TRANSMISSION` datetime NOT NULL,
  `NR_RECEIPT` int(11) DEFAULT NULL,
  `BL_XML_SEND` blob,
  `BL_XML_RETURN` blob,
  `X_RETURN_CODE` varchar(5) DEFAULT NULL,
  `X_RETURN_MSG` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID_REGISTER`),
  KEY `FK_TRANS_ISSUE_idx` (`ID_ISSUER`),
  CONSTRAINT `FK_TRANS_ISSUE` FOREIGN KEY (`ID_ISSUER`) REFERENCES `tb_issuer` (`ID_ISSUER`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_transmission_reg`
--

LOCK TABLES `tb_transmission_reg` WRITE;
/*!40000 ALTER TABLE `tb_transmission_reg` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_transmission_reg` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-20 16:40:35
