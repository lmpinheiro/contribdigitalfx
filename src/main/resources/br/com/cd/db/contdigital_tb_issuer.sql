-- MySQL dump 10.13  Distrib 5.6.24, for Win32 (x86)
--
-- Host: localhost    Database: contdigital
-- ------------------------------------------------------
-- Server version	5.6.25-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_issuer`
--

DROP TABLE IF EXISTS `tb_issuer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_issuer` (
  `ID_ISSUER` int(11) NOT NULL AUTO_INCREMENT,
  `ID_USER` int(11) NOT NULL,
  `ID_COUNTY` int(11) NOT NULL,
  `X_CNPJ` varchar(14) NOT NULL,
  `X_COMPANY_NAME` varchar(60) NOT NULL,
  `X_TRADE_NAME` varchar(60) DEFAULT NULL,
  `X_STREET` varchar(60) NOT NULL,
  `NR_NUMBER` varchar(60) NOT NULL,
  `X_COMPLEMENT` varchar(60) DEFAULT NULL,
  `X_DISTRICT` varchar(60) NOT NULL,
  `SG_UF` varchar(2) NOT NULL,
  `X_ZIP_CODE` varchar(8) DEFAULT NULL,
  `X_PHONE` varchar(15) DEFAULT NULL,
  `X_IE` varchar(14) NOT NULL,
  `X_IE_ST` varchar(14) DEFAULT NULL,
  `X_IM` varchar(15) DEFAULT NULL,
  `X_CNAE` varchar(7) DEFAULT NULL,
  `BL_LOGO` blob,
  `ST_REGIME_TRIB` varchar(4) NOT NULL,
  `X_EMAIL` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`ID_ISSUER`),
  KEY `FK_ISSUER_USER_idx` (`ID_USER`),
  KEY `FK_ISSUER_COUNTY_idx` (`ID_COUNTY`),
  CONSTRAINT `FK_ISSUER_COUNTY` FOREIGN KEY (`ID_COUNTY`) REFERENCES `tb_county` (`ID_COUNTY`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ISSUER_USER` FOREIGN KEY (`ID_USER`) REFERENCES `tb_user` (`ID_USER`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_issuer`
--

LOCK TABLES `tb_issuer` WRITE;
/*!40000 ALTER TABLE `tb_issuer` DISABLE KEYS */;
INSERT INTO `tb_issuer` VALUES (17,30,3550308,'99171171171115','Secretaria da Fazenda do Estado de São Paulo','SEFAZ SP','Av. Rangel Pestana','300','3º Andar - Ala D. Pedro','Sé','SP','01017911','11986466178','116734997114','','111111111111111','','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0�\0\0\0�\0\0\0��A>\0\0IDATx��]ٵ�:d(��p2�無9gRx�\nd>����k�E���v�}|��.�L�\n�Y���x�{	��\r����j̽47`z�Xn�܀�z8綿C7hn��YLtM��L�1M�M����\r�.낀����܀��<����\r��u္��\r�Һ�oVs[�0U�\"���0�C|p������:�s��0*$��V�KZk]�	�3�!���\0w0��1�ٓ�>7`��Bp@�>,sKP,��%�̛<�!+�<Mn��h8`\"XF�J���:����*�D7]HRx��p��Z�&���~�{����0����f\0����pnI����+���^��2��b�\Z�؄ܙ���d����V��W����n�d}�bl�\\�\"�����<L\0���n����`vE���\"�n��ﹶ0�����wQ�p\"/~�\\���,7�@��<��A�B˔<����xe��u�zo��	��Ǐ݀����AI;\r4�3J�>4w՛[@���h}*9	t3�|0h��Y���0��\"�n��{\0��\r�\r�`�!��������]�:W��x~ h�G�,\n���9���#�X$��\\�x�\\�׹�>0�1�ڽ[���\0�c�a\0IߟMa!$�h�F�%��͙%1`q�(���Z�R�k�yKKb���\0���s�k�]�Y�\Z`\"�8Ȥ�J���6���u��=�h�{Z������9����$��;[t\n*M;~���-�D��g`�\Z����A����x���y�2�������x\0H�c���\ra���HuY*P�נ� 5�f�L0�%��s	&��o瘷\0����\\�;S� �gx\0(0+����\ZG�O�P`�-	Og��g q� ��L�+�O�g�g7BȌ4w��i>T0���f�:I��k�_w?֓,KMfy&9��+S�si�73�MUۣ��r\0f8G����z���H#|^W�`2�X��������������D\n·�@S����,�������uY؎�y.R��]w�Օ��ח(���L���>`�l��N7{~\0ˍ/V+�=�A=J�k�r�$m\'�B����1�s�y�����x���g0�?{�3��\'��{��	,J����>�2�p6t7�X��~�OoY�\n��\"�c^fM$�\0�~�0B�M[�L�\nI6�^����7ۧ�;�F�{�u�����b�Y�G&�\0�I�߇�J`z��#����\n\Z�F��� 6�f��ʅpk�7����i2}�DL-}��?��������^H�OV��b�����0��+\\<���;�*���i��oP�z�L�s�}�5il+2�ҳ�Yj�A0��a�����\0d����	�\0B�\r��2��Bh�ULjƟ���\n�[�/��^�\0�I $�2�t�[2�Z#���M�mz\n��]�~�G�jEk`��y�R��*��V�;1Ɇce�c��^�((���vH�.�����^��V��{\0SˆF6���:��r��r],c.�.R�����n��\Z�H�!&�x�,eZ�P\"{=`pR�\"�Y�Fb}���\\˘�ܑR]�� `2�� h�]��%X܂���s;\0Ӻ�V�#enܥ���]�̥�to;��y0<%�!�j\\�U��0M����`����\\j[�S����\\/\r��T9	�w0�#����`��Ux�S)��X�y-`*�+�B�e�)p�\0��H	08$j��;����\\�\00�\nlV������*tV�G�����Ʈ�`�-&M�(�~��xh�j톎j��@�C�g������%�{u����H0�daX~v]���#E�%���]\0�b��&�\0���]#�%\\QG�F�hv��l�G�[�6�����\\RP�!Z�]�n<��<��Lo���P�gGl-١�sVNK�P�!�\0�5��Cz�H�+IU���@�L �ډ�q+�2��E4_i����w{��ZY@\Z���%�\0����>� ɘkrKPI}OG;(�).����؉��G%����M0[;쉀��0z�]��$�t��u$�Xq?N�xu061~^пLe����^�t���坷�ϕ�?�E\'sK�TY�_L�b���~��,f�c:�hx�l3\n�ލ�r�,#�l�l.`x�b��Eg Q��W�k1�.��\\��U��,i`F��M˹ʕw����7�#����F�I3���.@Ҍ�	3�jm�t�{@�5����߇\0f�ׅR�E��%!1%`6dK�:I(5+&r���4Vc�b7]�|~sV;�8�@Zk_�]��1�~�Ju�Uo���X\\���cd���j�����-�T�s%�斞ϊt��z\r�ea$��Қ�J?fa8w�;u0���\0f��]��jb\rn��5~�¬�1�����l�0R�nQ��b�����\0�M�z��WU3�ޜ�{/��0��.�*�h�\\Mw�ۢ�yLR�j���a$��ߌa�xf��K0hn���8`*��b���p0=�F�\Z<\n����Z�2���=�\r�� �:�����$�`tW���V͊�Ƭ;�6_{kI��!�8�������h��U�K��\'�\\���^��ٟNM�Yϵ0�Ƿ,�YL����ƞᔌ�������ʜ�E1��_�ɕ������90�o��	����b�#}JǊ���m�\Z�>���l��Y�g���I�ʒ^���[1r�uM\Zj�R��7��^���|,���\0^|C�0������&S2&�̜��;G�ЬT�Y|�_GM[T�?\r0��\\qW-o�jǆ�<��dL=H�r\rL����@�/�6�tX�b�������i�\Z���UO�P�g]�-.�װ:�\n��~��\Zh�H�7����N�Q�v���]C��I���e�4Y�<_�R�0�D�]��l������~��\"��k`��\"�q9[�M��#�Ja��,��2���g��[��m<߭z�\"s%`��b��� �\03��%�j�Db����q*u��Ҏ_�f]fv�Zx&|�>F�\r�\0C�K�0<�Q�sXՙ��Q:�ɿ0馯�ax�\'[��\0\0�t��h�ɢ�I���݈��,��r�\Z`$�Nبx�ݦ�t�,��E���YR��g��I8Lk�B��>�$���0x~^�Ƽ\0���\n�!��)�z�����0Z�	}�nt��A>�cV��n�۾.�%m�v�ۅ@��L�.��b,H��\Z?�R,%�V�˥t�T|,P����&d���y��tP��݉7�,����S�G�����&�:�t�=wG\\8��\rY<�C	���S��΀�m.�����[�Rk0�2`���2��̘c��ʹ�d!&}�&��&��^/ �^\r�&8p�d˚��6�0k��;�������}I�.��v�WfR#��>�/�����g�`�V��V�0�<�?eE�\"�r���1���B�*k]h�Y4�|F�_?u\'D�\\�^�Aޭ5�w&kJ?@%�mTNP�;�NB�3�{��x&�����M�f��$\nh]\n�\r>[v8j\r08i�ܐtd�/��;r��y�l��\ZtG^�k\nW�溶1�ͨ�5ۉ��Lv�\\Th�ω\'��6���),�t����0$O2IT+��݄1�Dnq������Ɯ���^�h�>�^d�y7��6��v�&�a%�&�>_�*�	�Fñ��>!�X���;��7���y�y->@`;\0̖	� ����S�	:sw|]���Y���Gl���q�8��hZ�%���+��_�$	�}&^H��Y�#mb���{/8�y/#�]T�!�Tg+%y%���d�QlĄ�g[aөiЏ���`6WY �<U�\r��}���u�0��f.t�A7>ފ�7�b6%�c1��p���Pɼ\'�p`�v[n!0�#�\\\nΰ����eH�$a�A������D6��f8z�!��8���PH�7��2[p+\0F���j���вLVP�� H���l(�T?[\r0?�	Q>0��t�p�m�Oa�xA.Wީ�������3iT��5	)����S�~/�4Jy�X��~�c%3�ff\n!��/Q(�kHK9�E��ֆ.r���Kk�Ţ�簩�`�Q��o�(E�`3I� o0�;q$���t�[	\0����+çM�ri&���x;�tge��a��ZN��,\"T\Z�k��	xq��Qz��\'�ngU����F�����S}\r0Ĺ�+���<\"���\0[-�h����Fӥ�y&�;�z0��bn	RP�ƀ�ױ��\'��y���lS�֓OP�n��W�)��G~^�˂��9��S�J7\re���ڑ���j� �)f��GA�p�>8�F@�E�E�����d��F�(�5�ͭ�i��\0S���\0�A(&�5S��y,\r�S%�Q�\\�+����}M�~�=���t�,u\Zp�<Z9\n�y*mjR�\0�����.��H�,8`�1{�KF�=�!�ҡ�s<���]��!�Y�]��f����:��ʓ��o\" i��v$ Ԑ����E�A/jg����d�^И#`�x\0�M���5���d�r\0�b�Yd)8�Zӝ�j?P����+��24ix9�S��\\��,L��}0\0���q�k2��/��P�1�KI����XlI�0��%p�[+j��RJ�%�d$قbɤ�����*�w(.�auV���˽r<m0��fd`3�G�`�?�>4�{��fJY\r�f)�IӬ�ja�@�oR������V��C,��	�z�L�ӖS����oDt���S�(^�j��x��0\n�j�m����&�Q�\0���,�㋥��v.7��-��m�~=\Z��5񖘡�(�\ZX܃��\n���JR��So�̤�P���84�=�\nV��ߵ�@ޥ�%�g*Ur�}q�&`�9h�r�A�x�l��6s��V��O�U~�+^�$h��S����i|���xkgFj�T�G��P#eC��C���1�8��	��ZC���;0�qz͋�|��J灑�i�!����#l0v�G����لO�2���3�����¤PY%�gkԑ�G�u�0b,��\r(�Nb�����\"J;�ϝ�J<SD�5���L�h�H��@�^ky�27 ?��S4���,�T���f�m,�Z\'�����a�C/x��s\r�����T�l�`�����~�f�+E�/��)��YSH\n2�FZ�Y�kZ��F�,��PfӘ�$�M�M}\r�6�*SK�4�VK���T{��J�{��A���خU�ύL��5�H���\0����ʜ�������,{��!�w��!��t!|���I���%�N�G\nu���O��d	}I�qW�U�`򸅀T�W����E�Y�3\"l-c�1�����B���@J�{Ƅ��x@S�Ԅ�1�Uj�󶬚kkz�}����E.�	�`�m�o7�p�c�4�r~�Rz�v���b�iߍ�\Z�Lݲ�����1�D@a�^-��#��+���b�|��v�ڪ�y��jY�b�9�,�ڡ1����Ȥk�����;�Y��U�v�]�}��if�r���t>R��x�B%հD���nQ�-����Xl《�e�b�#�ޜ�ȆV�(eMk!U�[3�����\Z�wV?N��4���� T�8�s^�.\'��:֩W�^&�k�����bB��A�y�b���1C��̸�J\\vd�˯Fu[^�V�`>�t�ԇ�n�S&\Z���Q-�%�2d��gH�U��Q�3�$/�z�Ny�!-vu�%	�(�>I@l3���3J��4��̴�A��6g���>0��Oh���N������N|EI�ce~.$J;��tp-T��6��L����=���Y�%I(�w�0r��]���%e\0���U?DT���$T�<@c<�{�AiRw�]�b��3��\\Y֖�dv*�����\Z`��k,L����,I���h(HN\Z`e~��\\��Y��BVD��C`Ńq��o��^��%`z9�\Zgc:��!5_\0���\0W\"&��h�\r�1̐�sP�LV�%E�����#s$�r�!\0A�����\"\'��a,����}�>��G������F�w��A���nD�]\"ަ�(�|*P�0-S�ˤ.�O��{�V�����&�w��V:Z=���HC�?��|ĕg���S�ޓn%YC�%�9��+�M����_	�l�:@���5���E|O`�?�e�=`������?�w�������������x�cC�\0\0\0\0IEND�B`�','0','lmpinheiro@prodesp.sefaz.sp.gov.br');
/*!40000 ALTER TABLE `tb_issuer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-20 16:40:31
