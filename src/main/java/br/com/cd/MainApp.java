package br.com.cd;

import java.net.InetAddress;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import br.com.cd.control.InjectionController;
import br.com.cd.control.ScreenController;

public class MainApp extends Application {
	
	private static final Logger logger = Logger.getLogger(MainApp.class);

    private Stage primaryStage;
    
    @Override
    public void start(Stage stage) throws Exception {

    	MDC.put("IP", InetAddress.getLocalHost().getHostAddress());
    	
    	logger.info("Starting App");
    	
    	primaryStage = stage;
    	try{
	    	FXMLLoader loader = new FXMLLoader();
	    	loader.setControllerFactory((clazz)-> {
					return InjectionController.getContext().getBean(clazz);
			});
	        loader.setLocation(getClass().getResource("view/fxml/app-intro.fxml"));
	        Parent root = (Parent) loader.load();
	        Scene scene = new Scene(root, Color.TRANSPARENT);
	        stage.setScene(scene);
	        stage.getIcons().add(new Image("/br/com/cd/view/img/app-icon.png"));
	        stage.initStyle(StageStyle.TRANSPARENT);
	        stage.show();
	        
	        ScreenController controller = loader.getController();
	        controller.setStageParent(stage);
    	}catch(Exception e){
    		System.out.print(e);
    	}
    }

    /**
     * Retorna o palco principal.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }
    

    public static void main(String[] args) {
        launch(args);
    }
}