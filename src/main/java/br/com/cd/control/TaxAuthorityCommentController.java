package br.com.cd.control;

import java.net.URL;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.hibernate.annotations.common.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.animations.FadeInTransition;
import br.com.cd.animations.FadeOutLeftTransition;
import br.com.cd.animations.FadeOutTransition;
import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.control.TransitionController.TransitionType;
import br.com.cd.model.IssuerModel;
import br.com.cd.nfe.TNFe.InfNFe.Cobr.Dup;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefECF;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNF;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNFP;
import br.com.cd.nfe.TNFe.InfNFe.InfAdic.ObsCont;
import br.com.cd.nfe.TNFe.InfNFe.InfAdic.ObsFisco;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.FormatterTextUtil;
import br.com.cd.util.MaskFieldUtil;
import br.com.cd.util.SecurityUtil;

/**
 * FXML Controller class for Menu
 *
 * @author lmpinheiro
 */
@Controller
public class TaxAuthorityCommentController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(TaxAuthorityCommentController.class);
	
	Stage stage;
	boolean exists = false;
	boolean editAction = false;
    
    @Autowired
    ScreenConfig screenHelper;
    
    @Autowired
    FiscalDocumentController fiscalDocumentController;
    
    @Autowired
    TransitionController transitionController;
    
    @FXML
    private AnchorPane pnlMain;
	@FXML
    private Button btnClose;
	@FXML
    private Label lblTitle;
	
	@FXML
    private TextField txtName;
	@FXML
    private TextField txtComment;
	
	@FXML
    private Button btnAdd;
	@FXML
    private Button btnUpdate;
	@FXML
    private Button btnCancel;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing MenuController");
    	
    	Platform.runLater(() -> {
            stage = (Stage) btnClose.getScene().getWindow();
            
            setMaskField();
            installListeners();
            if(getData()!=null){
            	editAction = true;
            	fillForm((ObsFisco)getData());
            	btnAdd.setVisible(false);
            	btnUpdate.setVisible(true);
            }
            
            transitionController.executeTransition(null, TransitionType.BOUNCE_IN, null, pnlMain);
        });
    }    
   

    /**
     * Add Object on table
     */
    @FXML
    private void actSave(ActionEvent event) {
    	logger.info("Calling actAdd");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	ObsFisco taxAuthoritiesComment = populateObject(null);
        	
        	exists = false;
        	fiscalDocumentController.listTaxAuthorities.forEach(item -> {
        		if(taxAuthoritiesComment!=null && item!=null && 
        				!StringHelper.isEmpty(taxAuthoritiesComment.getXCampo()) && taxAuthoritiesComment.getXCampo().equals(item.getXCampo()) &&
        				!StringHelper.isEmpty(taxAuthoritiesComment.getXTexto()) && taxAuthoritiesComment.getXTexto().equals(item.getXTexto())){
        			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Observação do Fisco"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
	        	fiscalDocumentController.listTaxAuthorities.add(taxAuthoritiesComment);
	        	fiscalDocumentController.tableTaxAuthorities.setItems(fiscalDocumentController.listTaxAuthorities);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_INSERTED, "Observação do Fisco"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    
    
    /**
     * Update object on table
     */
    @FXML
    private void actUpdate(ActionEvent event) {
    	logger.info("Calling actUpdate");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	ObsFisco taxAuthorityCommentOld = (ObsFisco)getData();
        	ObservableList<ObsFisco> listtaxAuthorityComment = FXCollections.observableArrayList(fiscalDocumentController.listTaxAuthorities);
        	listtaxAuthorityComment.remove(taxAuthorityCommentOld);
        	
        	ObsFisco taxAuthorityCommentCurrent = populateObject(null);
        	
        	exists = false;
        	listtaxAuthorityComment.forEach(item -> {
        		if(taxAuthorityCommentCurrent!=null && item!=null && 
        				!StringHelper.isEmpty(taxAuthorityCommentCurrent.getXCampo()) && taxAuthorityCommentCurrent.getXCampo().equals(item.getXCampo()) &&
        				!StringHelper.isEmpty(taxAuthorityCommentCurrent.getXTexto()) && taxAuthorityCommentCurrent.getXTexto().equals(item.getXTexto())){
        			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Observação do Fisco"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
        		
        		ObsFisco taxAuthorityComment = populateObject((ObsFisco)getData());
        		
        		fiscalDocumentController.listTaxAuthorities.set(fiscalDocumentController.tableTaxAuthorities.getSelectionModel().getSelectedIndex(), taxAuthorityComment);
	        	fiscalDocumentController.tableTaxAuthorities.setItems(fiscalDocumentController.listTaxAuthorities);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_UPDATED, "Observação do Fisco"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    

    /**
     * Close Pane
     */
    @FXML
    private void actClose(ActionEvent event) {
    	logger.info("Calling actClose");
    	
    	message.clear();
    	
    	addMessage(MessageConstant.EXIT_CONFIRMATION);
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
        	stage.close();
        }
    }


    /**
     * Listeners Implementation
     */
    private void installListeners(){
    	txtName.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtName, 1, 20, false));
    	txtComment.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtComment, 1, 60, false));
    }
    
    
    /**
     * Populate object form  
     */
    private ObsFisco populateObject(ObsFisco taxAuthorityComment){
    	logger.info("Calling populateObject");
    	
    	if(taxAuthorityComment==null)taxAuthorityComment = new ObsFisco();
    	
    	taxAuthorityComment.setXCampo(txtName.getText());
    	taxAuthorityComment.setXTexto(txtComment.getText());
    	
    	return taxAuthorityComment;
    }
    
    
    /**
     * Fill Form  
     */
    private void fillForm(ObsFisco taxAuthorityComment){
    	logger.info("Calling fillForm");
    	
    	txtComment.setText(taxAuthorityComment.getXTexto());
		txtName.setText(taxAuthorityComment.getXCampo());
    }
    
    
    /**
     * Define Field's Mask  
     */
    private void setMaskField(){
    	logger.info("Calling setFieldMask");
    	
    	MaskFieldUtil.maxTextField(txtName, 20);
    	MaskFieldUtil.maxTextField(txtComment, 60);
    }
    
    /**
     * Fields Validation 
     */
    private void validateFields(){
    	logger.info("Calling validateFields");
    	
    	error.clear();
    	
    	
		/**
         * Validate if is a required field 
         */
    	if (txtName.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Nome do Campo"));
        }
    	if (txtComment.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Observação"));
        }
		
		
		/**
         * Validate if is a valid content 
         */
		if (txtName.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Nome do Campo"));
        }
		if (txtComment.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Observação"));
        }
    }
}
