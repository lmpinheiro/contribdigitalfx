package br.com.cd.control;

import java.net.URL;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.hibernate.annotations.common.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.animations.FadeInTransition;
import br.com.cd.animations.FadeOutLeftTransition;
import br.com.cd.animations.FadeOutTransition;
import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.control.TransitionController.TransitionType;
import br.com.cd.model.IssuerModel;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNF;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.FormatterTextUtil;
import br.com.cd.util.MaskFieldUtil;
import br.com.cd.util.SecurityUtil;

/**
 * FXML Controller class for Menu
 *
 * @author lmpinheiro
 */
@Controller
public class ReferredDocumentController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(ReferredDocumentController.class);
	
	Stage stage;
	boolean exists = false;
	boolean editAction = false;
    
    @Autowired
    ScreenConfig screenHelper;
    
    @Autowired
    FiscalDocumentController fiscalDocumentController;
    
    @Autowired
    TransitionController transitionController;
    
    @FXML
    private AnchorPane pnlMain;
	@FXML
    private Button btnClose;
	@FXML
    private Label lblTitle;
	@FXML
    private ToggleGroup referredDocument;
	@FXML
    private RadioButton rdDocumentTypeNfe;
	@FXML
    private RadioButton rdDocumentTypeCte;
	@FXML
    private RadioButton rdDocumentTypeNfp;
	
	@FXML
    private Pane pnlDfe;
	@FXML
    private TextField txtAccessKey;
	
	@FXML
    private Pane pnlNfp;
	@FXML
    private TextField txtModel;
	@FXML
    private TextField txtSerie;
	@FXML
    private TextField txtNumber;
	@FXML
    private ComboBox<String> cmbUf;
	@FXML
    private TextField txtIssueDate;
	@FXML
    private TextField txtCnpj;
	
	@FXML
    private Button btnAdd;
	@FXML
    private Button btnUpdate;
	@FXML
    private Button btnCancel;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing MenuController");

    	Platform.runLater(() -> {
            stage = (Stage) btnClose.getScene().getWindow();
            
            setMaskField();
            populateCombos();
            installListeners();
            if(getData()!=null){
            	editAction = true;
            	fillForm((NFref)getData());
            	btnAdd.setVisible(false);
            	btnUpdate.setVisible(true);
            }
            
            transitionController.executeTransition(null, TransitionType.BOUNCE_IN, null, pnlMain);
        });
    }    
   

    /**
     * Add Object on table
     */
    @FXML
    private void actSave(ActionEvent event) {
    	logger.info("Calling actAdd");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	NFref nfRef = populateObject(null);
        	
        	exists = false;
        	fiscalDocumentController.listFdRefereced.forEach(item -> {
        		if((!StringHelper.isEmpty(nfRef.getRefCTe()) && nfRef.getRefCTe().equals(item.getRefCTe())) ||
    			   (!StringHelper.isEmpty(nfRef.getRefNFe()) && nfRef.getRefNFe().equals(item.getRefNFe())) ||
    			   (nfRef.getRefNF()!=null && item.getRefNF()!=null &&
    				!StringHelper.isEmpty(nfRef.getRefNF().getAAMM()) && nfRef.getRefNF().getAAMM().equals(item.getRefNF().getAAMM()) &&
    				!StringHelper.isEmpty(nfRef.getRefNF().getCNPJ()) && nfRef.getRefNF().getCNPJ().equals(item.getRefNF().getCNPJ()) &&
    				!StringHelper.isEmpty(nfRef.getRefNF().getCUF()) && nfRef.getRefNF().getCUF().equals(item.getRefNF().getCUF()) &&
    				!StringHelper.isEmpty(nfRef.getRefNF().getMod()) && nfRef.getRefNF().getMod().equals(item.getRefNF().getMod()) &&
    				!StringHelper.isEmpty(nfRef.getRefNF().getNNF()) && nfRef.getRefNF().getNNF().equals(item.getRefNF().getNNF()) &&
    				!StringHelper.isEmpty(nfRef.getRefNF().getSerie()) && nfRef.getRefNF().getSerie().equals(item.getRefNF().getSerie()))){
        			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Documento"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
	        	fiscalDocumentController.listFdRefereced.add(nfRef);
	        	fiscalDocumentController.tableFdRefereced.setItems(fiscalDocumentController.listFdRefereced);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_INSERTED, "Documento"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    
    
    /**
     * Update object on table
     */
    @FXML
    private void actUpdate(ActionEvent event) {
    	logger.info("Calling actUpdate");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	NFref nfRefOld = (NFref)getData();
        	ObservableList<NFref> listFdRefereced = FXCollections.observableArrayList(fiscalDocumentController.listFdRefereced);
        	listFdRefereced.remove(nfRefOld);
        	
        	NFref nfRefCurrent = populateObject(null);
        	
        	exists = false;
        	listFdRefereced.forEach(item -> {
        		if((!StringHelper.isEmpty(nfRefCurrent.getRefCTe()) && nfRefCurrent.getRefCTe().equals(item.getRefCTe())) ||
    			   (!StringHelper.isEmpty(nfRefCurrent.getRefNFe()) && nfRefCurrent.getRefNFe().equals(item.getRefNFe())) ||
    			   (nfRefCurrent.getRefNF()!=null && item.getRefNF()!=null &&
    				!StringHelper.isEmpty(nfRefCurrent.getRefNF().getAAMM()) && nfRefCurrent.getRefNF().getAAMM().equals(item.getRefNF().getAAMM()) &&
    				!StringHelper.isEmpty(nfRefCurrent.getRefNF().getCNPJ()) && nfRefCurrent.getRefNF().getCNPJ().equals(item.getRefNF().getCNPJ()) &&
    				!StringHelper.isEmpty(nfRefCurrent.getRefNF().getCUF()) && nfRefCurrent.getRefNF().getCUF().equals(item.getRefNF().getCUF()) &&
    				!StringHelper.isEmpty(nfRefCurrent.getRefNF().getMod()) && nfRefCurrent.getRefNF().getMod().equals(item.getRefNF().getMod()) &&
    				!StringHelper.isEmpty(nfRefCurrent.getRefNF().getNNF()) && nfRefCurrent.getRefNF().getNNF().equals(item.getRefNF().getNNF()) &&
    				!StringHelper.isEmpty(nfRefCurrent.getRefNF().getSerie()) && nfRefCurrent.getRefNF().getSerie().equals(item.getRefNF().getSerie()))){
        			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Documento"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
        		
        		NFref nfRef = populateObject((NFref)getData());
        		
//        		fiscalDocumentController.tableFdRefereced.getItems().clear();
        		fiscalDocumentController.listFdRefereced.set(fiscalDocumentController.tableFdRefereced.getSelectionModel().getSelectedIndex(), nfRef);
	        	fiscalDocumentController.tableFdRefereced.setItems(fiscalDocumentController.listFdRefereced);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_UPDATED, "Documento"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    

    /**
     * Close Pane
     */
    @FXML
    private void actClose(ActionEvent event) {
    	logger.info("Calling actClose");
    	
    	message.clear();
    	
    	addMessage(MessageConstant.EXIT_CONFIRMATION);
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
        	stage.close();
        }
    }

    /**
     * Logout Function
     */
    @FXML
    private void actLogout(ActionEvent event) {
    	logger.info("Calling actLogout");
    	
    	message.clear();
    	
        addMessage(MessageConstant.USER_LOGOUT);
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
        	SecurityUtil.userLogout();
        	screenHelper.newStage(stage, lblTitle, "login.fxml", "Contribuinte Digital - Login", true, StageStyle.TRANSPARENT, null, false, this, null, true);
        }
    }
    
    /**
     * Listeners Implementation
     */
    private void installListeners(){
    	rdDocumentTypeNfp.selectedProperty().addListener((observableValue,  wasPreviouslySelected, isNowSelected) ->{
	        if (isNowSelected) { 
	        	transitionController.executeTransition(TransitionType.FADE_OUT_DOWN, TransitionType.FADE_IN_DOWN, pnlDfe, pnlNfp);
	        }else{
	        	transitionController.executeTransition(TransitionType.FADE_OUT_DOWN, TransitionType.FADE_IN_DOWN, pnlNfp, pnlDfe);
	        }
    	});
    	
    	txtModel.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtModel, 1, 2, false));
    	txtSerie.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtSerie, 1, 3, false));
    	txtNumber.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNumber, 1, 9, false));
    	txtIssueDate.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIssueDate, 1, 7, false));
    	txtCnpj.setOnKeyReleased(e -> DataValidateUtil.isValidCnpj(txtCnpj, true));
    	txtAccessKey.setOnKeyReleased(e -> DataValidateUtil.isValidAccessKey(txtAccessKey, false));
    }
    
    
    /**
     * Populate combos on initialization   
     */
    private void populateCombos(){
    	logger.info("Calling populateCombos");
    	
    	cmbUf.getItems().addAll(getStateService().findUfs());
    }
    
    
    /**
     * Populate object form  
     */
    private NFref populateObject(NFref nfRef){
    	logger.info("Calling populateObject");
    	
    	if(nfRef==null)nfRef = new NFref();
    	
    	if(rdDocumentTypeNfe.isSelected()){
    		nfRef.setRefNFe(txtAccessKey.getText());
    		nfRef.setRefNF(null);
    	}else if(rdDocumentTypeCte.isSelected()){
    		nfRef.setRefCTe(txtAccessKey.getText());
    		nfRef.setRefNF(null);
    	}else{
    		nfRef.setRefNFe(null);
    		nfRef.setRefCTe(null);
    		
    		RefNF refNF = new RefNF();
    		refNF.setAAMM(MaskFieldUtil.removeMaskNumeral(txtIssueDate.getText()));
    		refNF.setCNPJ(MaskFieldUtil.removeMaskNumeral(txtCnpj.getText()));
    		refNF.setCUF(cmbUf.getSelectionModel().getSelectedItem());
    		refNF.setMod(txtModel.getText());
    		refNF.setNNF(txtNumber.getText());
    		refNF.setSerie(txtSerie.getText());
    		nfRef.setRefNF(refNF);
    	}
    	return nfRef;
    }
    
    
    /**
     * Fill Form  
     */
    private void fillForm(NFref nfRef){
    	logger.info("Calling fillForm");
    	
    	if(nfRef.getRefCTe()!=null){
    		rdDocumentTypeCte.setSelected(true);
    		txtAccessKey.setText(nfRef.getRefCTe());
    		
    	}else if(nfRef.getRefNFe()!=null){
    		rdDocumentTypeNfe.setSelected(true);
    		txtAccessKey.setText(nfRef.getRefNFe());
    		
    	}else{
    		rdDocumentTypeNfp.setSelected(true);
    		txtIssueDate.setText(nfRef.getRefNF().getAAMM());
    		txtCnpj.setText(nfRef.getRefNF().getCNPJ());
    		cmbUf.getSelectionModel().select(nfRef.getRefNF().getCUF());
    		txtModel.setText(nfRef.getRefNF().getMod());
    		txtNumber.setText(nfRef.getRefNF().getNNF());
    		txtSerie.setText(nfRef.getRefNF().getSerie());
    	}
    }
    
    
    /**
     * Define Field's Mask  
     */
    private void setMaskField(){
    	logger.info("Calling setFieldMask");
    	
    	MaskFieldUtil.maxTextField(txtAccessKey, 44);
    	MaskFieldUtil.numericField(txtAccessKey);
    	MaskFieldUtil.numericField(txtModel);
    	MaskFieldUtil.maxTextField(txtModel, 2);
    	MaskFieldUtil.numericField(txtSerie);
    	MaskFieldUtil.maxTextField(txtSerie, 3);
    	MaskFieldUtil.numericField(txtNumber);
    	MaskFieldUtil.maxTextField(txtNumber, 9);
    	MaskFieldUtil.numericField(txtIssueDate);
    	MaskFieldUtil.dateMonthYearField(txtIssueDate);
    	MaskFieldUtil.cnpjField(txtCnpj);
    }
    
    /**
     * Fields Validation 
     */
    private void validateFields(){
    	logger.info("Calling validateFields");
    	
    	error.clear();
    	
    	if(rdDocumentTypeCte.isSelected() || rdDocumentTypeNfe.isSelected()){
    		/**
             * Validate if is a required field 
             */
    		if (txtAccessKey.getText().isEmpty()) {
            	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Chave de Acesso"));
            }
    		
    		/**
             * Validate if is a valid content 
             */
        	if (txtAccessKey.getStyle().contains("red")) {
            	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Chave de Acesso"));
            }
    	}
    	
    	if(rdDocumentTypeNfp.isSelected()){
    		/**
             * Validate if is a required field 
             */
    		if (txtModel.getText().isEmpty()) {
            	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Modelo"));
            }
    		if (txtSerie.getText().isEmpty()) {
            	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Série"));
            }
    		if (txtNumber.getText().isEmpty()) {
            	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Número"));
            }
    		if (txtIssueDate.getText().isEmpty()) {
            	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Data de Emissão"));
            }
    		if (cmbUf.getSelectionModel().getSelectedIndex()<0) {
            	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "UF"));
            }
    		if (txtCnpj.getText().isEmpty()) {
            	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "CNPJ"));
            }
    		
    		/**
             * Validate if is a valid content 
             */
    		if (txtModel.getStyle().contains("red")) {
            	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Modelo"));
            }
    		if (txtSerie.getStyle().contains("red")) {
            	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Série"));
            }
    		if (txtNumber.getStyle().contains("red")) {
            	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Número"));
            }
    		if (txtIssueDate.getStyle().contains("red")) {
            	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Data de Emissão"));
            }
    		if (cmbUf.getStyle().contains("red")) {
            	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "UF"));
            }
    		if (txtCnpj.getStyle().contains("red")) {
            	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "CNPJ"));
            }
    	}
    	
    }
}
