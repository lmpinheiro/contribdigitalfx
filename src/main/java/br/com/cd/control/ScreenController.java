package br.com.cd.control;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;

import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref;

/**
 * (FXML) Extended Screen Controller
 *
 * @author lmpinheiro
 */
@Controller
public class ScreenController extends InjectionController {
	private static final Logger logger = Logger.getLogger(ScreenController.class);
	
	Stage stageParent;
    
	/***************************************************
    *                Drag and Drop Pane                *
    ****************************************************/
    private double X = 0;
    private double Y = 0;
    
    @FXML
    protected void onRectanglePressed(MouseEvent event) {
        X = stageParent.getX() - event.getScreenX();
        Y = stageParent.getY() - event.getScreenY();
    }

    @FXML
    protected void onRectangleDragged(MouseEvent event) {
    	stageParent.setX(event.getScreenX() + X);
    	stageParent.setY(event.getScreenY() + Y);
    }
    
    /* *************************
     *     Stage Parent        *
    **************************/
    public void setStageParent(Stage stage){
    	stageParent = stage;
    }

	public Stage getStageParent() {
		return stageParent;
	}
	
	
	
	/***************************************************
    *             Screen's Messages  List              *
    ****************************************************/
	
	protected List<String> message = new ArrayList<String>();
	
	public void addMessage(String message) {
		logger.info("Calling addMessage");
		
		this.message.add(message);
	}
	
	public boolean hasMessage() {
		logger.info("Calling hasMessage");
		
		return message.size()>0;
	}

	
	/***************************************************
    *               Screen's Error  List               *
    ****************************************************/
	
	protected List<String> error = new ArrayList<String>();
	
	public void addError(String error) {
		logger.info("Calling addError");
		
		this.error.add(error);
	}
	
	public boolean hasError() {
		logger.info("Calling hasError");
		
		return error.size()>0;
	}
	
	
	/***************************************************
    *      Object Transaction between controllers      *
    ****************************************************/
	
	private Object data = new Object();
	
	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	
	/***************************************************
    *             Hack Tooltip Start Timing            *
    ****************************************************/
	public static void hackTooltipStartTiming(Tooltip tooltip) {
		logger.info("Calling hackTooltipStartTiming");
        try {
        	tooltip.setAnchorX(10);
        	tooltip.setAnchorY(10);
        	
        	tooltip.setX(10);
        	tooltip.setY(10);
        	
        	tooltip.setAutoHide(false);
            Field fieldBehavior = tooltip.getClass().getDeclaredField("BEHAVIOR");
            fieldBehavior.setAccessible(true);
            Object objBehavior = fieldBehavior.get(tooltip);

            Field fieldTimer = objBehavior.getClass().getDeclaredField("activationTimer");
            fieldTimer.setAccessible(true);
            Timeline objTimer = (Timeline) fieldTimer.get(objBehavior);

            objTimer.getKeyFrames().clear();
            objTimer.getKeyFrames().add(new KeyFrame(new Duration(250)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
}
