package br.com.cd.control;

import java.net.URL;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.text.Normalizer.Form;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;

import org.apache.log4j.Logger;
import org.hibernate.annotations.common.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.control.TransitionController.TransitionType;
import br.com.cd.enumeration.FiscalDocumentEnum.AttendanceTypeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.CfopEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.DocumentTypeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.IssueTypeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.PaymentMethodsEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.PurposeIssueEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.ShippingModeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.SpecialTaxationEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.TargetOperationEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.TaxpayerTypeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.VehicleTypeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.VersionEnum;
import br.com.cd.enumeration.ProductServiceEnum.CalculationBaseModeEnum;
import br.com.cd.enumeration.ProductServiceEnum.CalculationBaseModeSTEnum;
import br.com.cd.enumeration.ProductServiceEnum.CofinsEnum;
import br.com.cd.enumeration.ProductServiceEnum.DesonerationReasonEnum;
import br.com.cd.enumeration.ProductServiceEnum.IpiEnum;
import br.com.cd.enumeration.ProductServiceEnum.IssMandatoryEnum;
import br.com.cd.enumeration.ProductServiceEnum.IssqnServicesEnum;
import br.com.cd.enumeration.ProductServiceEnum.NormalTaxationTaxSituationEnum;
import br.com.cd.enumeration.ProductServiceEnum.OriginEnum;
import br.com.cd.enumeration.ProductServiceEnum.PisEnum;
import br.com.cd.enumeration.ProductServiceEnum.RegimeTributarioEnum;
import br.com.cd.enumeration.ProductServiceEnum.SimpleNationalTaxSituationEnum;
import br.com.cd.enumeration.ProductServiceEnum.SpecificProductEnum;
import br.com.cd.enumeration.ProductServiceEnum.VehicleConditionEnum;
import br.com.cd.enumeration.ProductServiceEnum.VehicleOperationTypeEnum;
import br.com.cd.enumeration.ProductServiceEnum.VehicleRestrictionEnum;
import br.com.cd.model.CountyModel;
import br.com.cd.model.FiscalDocumentModel;
import br.com.cd.model.IssuerModel;
import br.com.cd.nfe.TNFe;
import br.com.cd.nfe.TNFe.InfNFe;
import br.com.cd.nfe.TNFe.InfNFe.AutXML;
import br.com.cd.nfe.TNFe.InfNFe.Cana.Deduc;
import br.com.cd.nfe.TNFe.InfNFe.Cana.ForDia;
import br.com.cd.nfe.TNFe.InfNFe.Cobr.Dup;
import br.com.cd.nfe.TNFe.InfNFe.Det.Prod;
import br.com.cd.nfe.TNFe.InfNFe.Det.Prod.Arma;
import br.com.cd.nfe.TNFe.InfNFe.Det.Prod.Comb;
import br.com.cd.nfe.TNFe.InfNFe.Det.Prod.DI;
import br.com.cd.nfe.TNFe.InfNFe.Det.Prod.DetExport;
import br.com.cd.nfe.TNFe.InfNFe.Det.Prod.Med;
import br.com.cd.nfe.TNFe.InfNFe.Exporta;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefECF;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNFP;
import br.com.cd.nfe.TNFe.InfNFe.InfAdic.ObsCont;
import br.com.cd.nfe.TNFe.InfNFe.InfAdic.ObsFisco;
import br.com.cd.nfe.TNFe.InfNFe.InfAdic.ProcRef;
import br.com.cd.nfe.TNFe.InfNFe.Transp.Vol;
import br.com.cd.nfe.TVeiculo;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.FormatterTextUtil;
import br.com.cd.util.MaskFieldUtil;

/**
 * FXML Controller for Issuer'd Management
 *
 * @author lmpinheiro
 */
@Controller
public class ProductController extends ScreenController implements Initializable {

	private static final Logger logger = Logger.getLogger(ProductController.class);
    
	Stage stage;
	
	@Autowired
    private ScreenConfig screenHelper;
	
	@Autowired
    private TransitionController transitionController;
	
	@Autowired
	MenuController menuController;
	
	/**
	 * Header
	 */
	@FXML
    private Label lblTitle;
	@FXML
    private Pane pnlMain;
	@FXML
    private Pane pnlProductService;
	@FXML
    private Text lblAccessKeyHeader;
	@FXML
    private ImageView imgLoad;
	@FXML
    private ProgressBar bar;
	
	/**
	 * Menu
	 */
	@FXML
	private Accordion acdnMenu;
	@FXML
    private TitledPane tpData;
	@FXML
    private TitledPane tpTaxDeclaration;
	@FXML
    private TitledPane tpExportation;
	@FXML
    private TitledPane tpTax;
	@FXML
    private TitledPane tpAdditionalInformation;
	@FXML
    private TitledPane tpNewVehicle;
	@FXML
    private TitledPane tpMedicine;
	@FXML
    private TitledPane tpArmament;
	@FXML
    private TitledPane tpFuel;
	@FXML
    private TitledPane tpImmuneRole;
	
	/**
	 * Data Menu
	 */
	@FXML
    private AnchorPane pnlData;
		@FXML
	    private Accordion acdnData;
		@FXML
	    private TitledPane tpProductData;
			@FXML
		    private TextField txtCode;
			@FXML
		    private TextField txtDescription;
			@FXML
		    private TextField txtNcm;
			@FXML
		    private TextField txtExTipi;
			@FXML
		    private ComboBox<CfopEnum> cmbCfop;
			@FXML
		    private TextField txtComercialUnit;
			@FXML
		    private TextField txtQuantityComercial;
			@FXML
		    private TextField txtComercialUnitValue;
			@FXML
		    private TextField txtTaxableUnit;
			@FXML
		    private TextField txtQuantityTaxable;
			@FXML
		    private TextField txtTaxableUnitValue;
			@FXML
		    private TextField txtInsuranceTotalValue;
			@FXML
		    private TextField txtDiscountTotalValue;
			@FXML
		    private TextField txtFreightValue;
			@FXML
		    private TextField txtEan;
			@FXML
		    private TextField txtTaxableEan;
			@FXML
		    private TextField txtOtherExpenses;
			@FXML
		    private TextField txtTotalGrossValue;
			@FXML
		    private CheckBox chkPartOfValue;
			@FXML
		    private TextField txtPurchaseRequest;
			@FXML
		    private TextField txtPurchaseItem;
			@FXML
		    private TextField txtFciNumber;
		
		@FXML
	    private TitledPane tpNve;
			public ObservableList<String> listNve;
			@FXML
		    public TableView<String> tableNve;
			@FXML
			private Button btnAddNve;
			@FXML
			private Button btnEditNve;
			@FXML
			private Button btnDeleteNve;
			@FXML
		    private TableColumn<String, String> colNve;
			
		@FXML
	    private TitledPane tpSpecificProduct;
			@FXML
		    private ComboBox<SpecificProductEnum> cmbSpecificProduct;
		
			
	/**
	 * Tax Declaration Menu
	 */
	@FXML
    private AnchorPane pnlTaxDeclaration;
		@FXML	
		public ObservableList<DI> listTaxDeclaration;
		@FXML
	    public TableView<DI> tableTaxDeclaration;
		@FXML
		private Button btnAddTaxDeclaration;
		@FXML
		private Button btnEditTaxDeclaration;
		@FXML
		private Button btnDeleteTaxDeclaration;
		@FXML
	    private TableColumn<DI, String> colTaxDeclarationNumber;
		@FXML
	    private TableColumn<DI, String> colTaxDeclarationDate;
		@FXML
	    private TableColumn<DI, String> colTaxDeclarationUf;
		@FXML
	    private TableColumn<DI, String> colTaxDeclarationLandingPlace;
		@FXML
	    private TableColumn<DI, String> colTaxDeclarationLandingDate;
		@FXML
	    private TableColumn<DI, String> colTaxDeclarationExporter;
		
		
	/**
	 * Export Menu
	 */
	@FXML
    private AnchorPane pnlExportation;
		@FXML	
		public ObservableList<DetExport> listExportation;
		@FXML
	    public TableView<DetExport> tableExportation;
		@FXML
		private Button btnAddExportation;
		@FXML
		private Button btnEditExportation;
		@FXML
		private Button btnDeleteExportation;
		@FXML
	    private TableColumn<DetExport, String> colExportationDrawback;
		@FXML
	    private TableColumn<DetExport, String> colExportationRecord;
		@FXML
	    private TableColumn<DetExport, String> colExportationKeyAccess;
		@FXML
	    private TableColumn<DetExport, String> colExportationExportedAmount;
		
		
	/**
	 * Tax Menu
	 */
	@FXML
    private AnchorPane pnlTax;
	 	@FXML
	    private TextField txtTotalAmountTaxes;
	 	
	 	@FXML
	    private TabPane tabPaneTax;
		 	@FXML
		    private Tab tabIcmsIssqn;
		 	 	@FXML
			    private ToggleGroup tgTaxationType;
				@FXML
			    private RadioButton rdTaxationTypeIcms;
				@FXML
			    private RadioButton rdTaxationTypeIssqn;
				
				@FXML
			    private AnchorPane icmsPanel;
					@FXML
				    private ComboBox<RegimeTributarioEnum> cmbIcmsRegimeTrib;
					
					@FXML
				    private AnchorPane simpleNationalPanel;
						@FXML
					    private ComboBox<SimpleNationalTaxSituationEnum> cmbIcmsSnTaxSituation;
						@FXML
					    private ComboBox<OriginEnum> cmbIcmsSnOrigin;
						@FXML
					    private TextField txtIcmSnsAliquotApplicable;
					    @FXML
					    private TextField txtIcmsSnCredit;
					    @FXML
					    private AnchorPane snIcmsPanel;
						    @FXML
						    private ComboBox<CalculationBaseModeEnum> cmbIcmsSnModDetBcIcms;
						    @FXML
						    private TextField txtIcmsSnBcIcms;
						    @FXML
						    private TextField txtIcmsSnAliquotIcms;
						    @FXML
						    private TextField txtIcmsSnReducPercentBcIcms;
						    @FXML
						    private TextField txtIcmsSnOperation;
						    
					    @FXML
					    private AnchorPane snIcmsStPanel;
						    @FXML
						    private ComboBox<CalculationBaseModeSTEnum> cmbIcmsSnModDetBcIcmsSt;
						    @FXML
						    private TextField txtIcmsSnBcIcmsSt;
						    @FXML
						    private TextField txtIcmsSnAliquotIcmsSt;
						    @FXML
						    private TextField txtIcmsSnSt;
						    @FXML
						    private TextField txtIcmsSnReducPercentBcIcmsSt;
						    @FXML
						    private TextField txtIcmsSnMarginPercentIcmsSt;
						    @FXML
						    private TextField txtIcmsSnBcIcmsStWithheldPreviously;
						    @FXML
						    private TextField txtIcmsSnStWithheldPreviously;
						    
				    @FXML
				    private AnchorPane normalTaxationPanel;
						@FXML
					    private ComboBox<NormalTaxationTaxSituationEnum> cmbIcmsTnTaxSituation;
						@FXML
					    private ComboBox<OriginEnum> cmbIcmsTnOrigin;
					    @FXML
					    private AnchorPane tnIcmsPanel;
						    @FXML
						    private ComboBox<CalculationBaseModeEnum> cmbIcmsTnModDetBcIcms;
						    @FXML
						    private TextField txtIcmsTnBcIcms;
						    @FXML
						    private TextField txtIcmsTnAliquotIcms;
						    @FXML
						    private TextField txtIcmsTnIcms;
						    @FXML
						    private TextField txtIcmsTnReducPercentBcIcms;
						    @FXML
						    private TextField txtIcmsTnOwnOperationBc;
						    @FXML
						    private TextField txtIcmsTnUnencumbered;
						    @FXML
						    private ComboBox<DesonerationReasonEnum> cmbIcmsTnReasonException;
						    @FXML
						    private TextField txtIcmsTnIcmsOperation;
						    @FXML
						    private TextField txtIcmsTnPercentDeferral;
						    @FXML
						    private TextField txtIcmsTnAmountDeferral;
						    
					    @FXML
					    private AnchorPane tnIcmsStPanel;
						    @FXML
						    private ComboBox<CalculationBaseModeSTEnum> cmbIcmsStTnModDetBcIcmsSt;
						    @FXML
						    private TextField txtIcmsStTnBcIcmsSt;
						    @FXML
						    private TextField txtIcmsStTnAliquotIcmsSt;
						    @FXML
						    private TextField txtIcmsStTnIcmsSt;
						    @FXML
						    private TextField txtIcmsStTnReducPercentBcIcmsSt;
						    @FXML
						    private TextField txtIcmsStTnAdditionalValueMargin;
						    @FXML
						    private ComboBox<String>  cmbIcmsStDueUf;
						    @FXML
						    private TextField txtIcmsStTnUnencumbered;
						    @FXML
						    private ComboBox<DesonerationReasonEnum>  cmbIcmsStTnReasonException;
						    @FXML
						    private TextField txtIcmsStBcWithholdingSenderUf;
						    @FXML
						    private TextField txtIcmsStWithholdingSenderUf;
						    @FXML
						    private TextField txtIcmsStBcRecipientUf;
						    @FXML
						    private TextField txtIcmsStRecipientUf;
						    @FXML
						    private TextField txtIcmsStBcWithholdingPreviously;
						    @FXML
						    private TextField txtIcmsStWithholdingPreviously;
						    
			    @FXML
			    private AnchorPane issqnPanel;	
				    @FXML
				    private TextField txtIssqnValue;
				    @FXML
				    private ComboBox<IssqnServicesEnum> cmbIssqnServiceList;
				    @FXML
				    private ComboBox<String> cmbIssqnUf;
				    @FXML
				    private ComboBox<CountyModel> cmbIssqnOccurrenceCounty;
//				    @FXML
//				    private ComboBox<Integer> cmbIssqnCountry;
				    @FXML
				    private TextField txtIssqnCalcBasis;
				    @FXML
				    private TextField txtIssqnAliquot;
				    @FXML
				    private ComboBox<IssMandatoryEnum> cmbIssqnIssMandatory;
				    @FXML
				    private ToggleGroup tgIssqnTaxIncentives;
					@FXML
				    private RadioButton rdIssqnTaxIncentivesYes;
					@FXML
				    private RadioButton rdIssqnTaxIncentivesNo;
					@FXML
				    private TextField txtIssqnUncondDiscount;
				    @FXML
				    private TextField txtIssqnOtherRetentionValue;
				    @FXML
				    private TextField txtIssqnUnconditionedDiscount;
				    @FXML
				    private TextField txtIssqnConditioningDiscount;
				    @FXML
				    private TextField txtIssqnIssRetentionValue;
				    @FXML
				    private TextField txtIssqnServiceCode;
				    @FXML
				    private ComboBox<String>  cmbIssqnIncidenceUF;
				    @FXML
				    private ComboBox<CountyModel> cmbIssqnIncidenceCounty;
				    @FXML
				    private TextField txtIssqnJudicialProcess;
				    
		 	@FXML
		    private Tab tabIpi;
		 	 	@FXML
			    private ComboBox<IpiEnum> cmbIpiTaxSituation;
		 	 	@FXML
			    private TextField txtIpiProducerCnpj;
			    @FXML
			    private TextField txtIpiFrameworkClass;
			    @FXML
			    private TextField txtIpiFrameworkCode;
			    @FXML
			    private TextField txtIpiLatchControlAmount;
			    @FXML
			    private TextField txtIpiLatchControlCode;
			    @FXML
			    private ToggleGroup ipiCalculationType;
				@FXML
			    private RadioButton rdIpiCalculationTypePercent;
				@FXML
			    private RadioButton rdIpiCalculationTypeOnValue;
				@FXML
			    private AnchorPane ipiPercentPanel;
					@FXML
				    private TextField txtIpiPercentCalcBasis;
				    @FXML
				    private TextField txtIpiPercentAliquot;
				    @FXML
				    private TextField txtIpiPercentIpiValue;
				@FXML
			    private AnchorPane ipiOnValuePanel;
					@FXML
				    private TextField txtIpiOnValueDefaultUnitAmount;
				    @FXML
				    private TextField txtIpiOnValueUnitValue;
				    @FXML
				    private TextField txtIpiOnValueIpiValue;
		 	
		 	@FXML
		    private Tab tabPis;
			 	@FXML
			    private ComboBox<PisEnum> cmbPisTaxSituation;
			    @FXML
			    private ToggleGroup pisCalculationType;
				@FXML
			    private RadioButton rdPisCalculationTypePercent;
				@FXML
			    private RadioButton rdPisCalculationTypeOnValue;
				@FXML
			    private AnchorPane pisPercentPanel;
					@FXML
				    private TextField txtPisPercentCalcBasis;
				    @FXML
				    private TextField txtPisPercentAliquot;
				    @FXML
				    private TextField txtPisPercentPisValue;
				@FXML
			    private AnchorPane pisOnValuePanel;
					@FXML
				    private TextField txtPisOnValueAliquot;
				    @FXML
				    private TextField txtPisOnValueSellAmount;
				    @FXML
				    private TextField txtPisOnValue;
				    
		 	@FXML
		    private Tab tabCofins;
			 	@FXML
			    private ComboBox<CofinsEnum> cmbCofinsTaxSituation;
			    @FXML
			    private ToggleGroup cofinsCalculationType;
				@FXML
			    private RadioButton rdCofinsCalculationTypePercent;
				@FXML
			    private RadioButton rdCofinsCalculationTypeOnValue;
				@FXML
			    private AnchorPane cofinsPercentPanel;
					@FXML
				    private TextField txtCofinsPercentCalcBasis;
				    @FXML
				    private TextField txtCofinsPercentAliquot;
				    @FXML
				    private TextField txtCofinsPercentCofinsValue;
				@FXML
			    private AnchorPane cofinsOnValuePanel;
					@FXML
				    private TextField txtCofinsOnValueAliquot;
				    @FXML
				    private TextField txtCofinsOnValueSellAmount;
				    @FXML
				    private TextField txtCofinsOnValue;
		 	
		 	@FXML
		    private Tab tabImport;
			 	@FXML
			    private TextField txtImportCalcBasis;
			    @FXML
			    private TextField txtImportCustomsExpenses;
			    @FXML
			    private TextField txtImportIofValue;
			    @FXML
			    private TextField txtImportTaxValue;
			    
		 	@FXML
		    private Tab tabReturnedIpi;
			 	@FXML
			    private TextField txtReturnedIpiReturnedCommodity;
			 	@FXML
			    private TextField txtReturnedIpiValue;

			 	
 	/**
	 * Additional Information Menu
	 */
	@FXML
    private AnchorPane pnlAdditionalInformation;
		@FXML
	    private TextArea txtProductAdditionalInformation;
		
		
	/**
	 * New Vehicle Menu
	 */
	@FXML
    private AnchorPane pnlNewVehicle;
		@FXML
	    private ComboBox<VehicleOperationTypeEnum> cmbVehicleOperationType;
		@FXML
	    private ComboBox<VehicleConditionEnum> cmbVehicleCondition;
		@FXML
	    private ComboBox<VehicleRestrictionEnum> cmbVehicleRestriction;
		@FXML
	    private TextField txtVehicleSerie;
	 	@FXML
	    private TextField txtVehicleFrame;
	 	@FXML
	    private CheckBox chkVehicleRebookedFrame;
	 	@FXML
	    private TextField txtVehicleMotorNumber;
	 	@FXML
	    private TextField txtVehiclePotency;
	 	@FXML
	    private TextField txtVehicleDisplacement;
	 	@FXML
	    private TextField txtVehicleNetWeight;
	 	@FXML
	    private TextField txtVehicleGrossWeight;
	 	@FXML
	    private TextField txtVehicleMaxCapTraction;
	 	@FXML
	    private TextField txtVehicleBrandModel;
	 	@FXML
	    private TextField txtVehicleYearManufacture;
	 	@FXML
	    private TextField txtVehicleYearModManufacture;
	 	@FXML
	    private TextField txtVehicleCapacity;
	 	@FXML
	    private TextField txtVehicleLengthAxis;
	 	@FXML
	    private TextField txtVehiclePaintType;
	 	@FXML
	    private TextField txtVehicleColorDescription;
	 	@FXML
	    private TextField txtVehicleColorCode;
	 	@FXML
	    private ComboBox<Integer> cmbVehicleColorDenatran;
		@FXML
	    private ComboBox<Integer> cmbVehicleFuelType;
		@FXML
	    private ComboBox<Integer> cmbVehicleType;
		@FXML
	    private ComboBox<Integer> cmbVehicleKind;
		
	
	/**
	 * Medicine Menu
	 */
	@FXML
    private AnchorPane pnlMedicine;
		@FXML	
		public ObservableList<Med> listMedicine;
		@FXML
	    public TableView<Med> tableMedicine;
		@FXML
		private Button btnAddMedicine;
		@FXML
		private Button btnEditMedicine;
		@FXML
		private Button btnDeleteMedicine;
		@FXML
	    private TableColumn<Med, String> colMedicineLotNumber;
		@FXML
	    private TableColumn<Med, String> colMedicineLotAmount;
		@FXML
	    private TableColumn<Med, String> colMedicineManufacturingDate;
		@FXML
	    private TableColumn<Med, String> colMedicineExpirationDate;
		@FXML
	    private TableColumn<Med, String> colMedicineMaxPrice;
		
		
	/**
	 * Armament Menu
	 */
	@FXML
    private AnchorPane pnlArmament;
		@FXML	
		public ObservableList<Arma> listArmament;
		@FXML
	    public TableView<Arma> tableArmament;
		@FXML
		private Button btnAddArmament;
		@FXML
		private Button btnEditArmament;
		@FXML
		private Button btnDeleteArmament;
		@FXML
	    private TableColumn<Arma, String> colArmamentSerieNumber;
		@FXML
	    private TableColumn<Arma, String> colArmamentBarrelSerieNumber;
		@FXML
	    private TableColumn<Arma, String> colArmamentDescription;
		
		
	/**
	 * Fuel Menu
	 */
	@FXML
    private AnchorPane pnlFuel;
		@FXML
	    private ComboBox<TcProdANP> cmbFuelAnpCode;
		@FXML
	    private TextField txtFuelCodif;
		@FXML
	    private ComboBox<String> cmbFuelUf;
		@FXML
	    private TextField txtFuelBilledAmount;
	 	@FXML
	    private TextField txtFuelNaturalGasQuantity;
	 	@FXML
	    private TextField txtFuelCalcBasis;
	 	@FXML
	    private TextField txtFuelAliquot;
	 	@FXML
	    private TextField txtFuelValue;
	 	
 	/**
	 * Immune Role Menu
	 */
	@FXML
    private AnchorPane pnlImmuneRole;
		@FXML
	    private TextField txtImmuneRoleRecopiNumber;
		
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Platform.runLater(() -> {
        	logger.info("Initializing IssuerController");
        	
            buttonsDisable();
            setModelColumn();
            setTooltips();
            setMaskField();
            populateCombos();
            installObservableList();
            installListeners();
            loadComponents();
        });
    } 
    
    /**
     * Add NVE
     */
    @FXML
    private void actAddNve(ActionEvent event) {
    	logger.info("Calling actAddNve");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblTitle, "nve.fxml", "NVE", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    /**
     * Edit NVE
     */
    @FXML
    private void actEditNve(ActionEvent event) {
    	logger.info("Calling actEditFdRefereced");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblTitle, "nve.fxml", "NVE", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableNve.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete NVE
     */
    @FXML
    private void actDeleteNve(ActionEvent event) {
    	logger.info("Calling actDeleteFdRefereced");
    	
    	message.clear();
    	
    	List<String> nveSelected = tableNve.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, nveSelected.size()+" NVE('s)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listNve.removeAll(nveSelected);
        }
    }
    
    /**
     * Add Tax Declaration
     */
    @FXML
    private void actAddTaxDeclaration(ActionEvent event) {
    	logger.info("Calling actAddTaxDeclaration");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblTitle, "tax-declaration.fxml", "Declaração de Importação", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    /**
     * Edit Tax Declaration
     */
    @FXML
    private void actEditTaxDeclaration(ActionEvent event) {
    	logger.info("Calling actEditTaxDeclaration");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblTitle, "tax-declaration.fxml", "Declaração de Importação", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableTaxDeclaration.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Tax Declaration
     */
    @FXML
    private void actDeleteTaxDeclaration(ActionEvent event) {
    	logger.info("Calling actDeleteTaxDeclaration");
    	
    	message.clear();
    	
    	List<DI> taxDeclarationSelected = tableTaxDeclaration.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, taxDeclarationSelected.size()+" Declaração(ões) de Importação"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listTaxDeclaration.removeAll(taxDeclarationSelected);
        }
    }
    
    
    /**
     * Add Export
     */
    @FXML
    private void actAddExportation(ActionEvent event) {
    	logger.info("Calling actAddExportation");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblTitle, "product-exportation.fxml", "Exportação", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    
    /**
     * Edit Export
     */
    @FXML
    private void actEditExportation(ActionEvent event) {
    	logger.info("Calling actEditExportation");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblTitle, "product-exportation.fxml", "Exportação", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableExportation.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Export
     */
    @FXML
    private void actDeleteExportation(ActionEvent event) {
    	logger.info("Calling actDeleteExportation");
    	
    	message.clear();
    	
    	List<DetExport> exportationSelected = tableExportation.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, exportationSelected.size()+" Exportação(ões)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listExportation.removeAll(exportationSelected);
        }
    }
    
    /**
     * Add Medicine
     */
    @FXML
    private void actAddMedicine(ActionEvent event) {
    	logger.info("Calling actAddMedicine");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblTitle, "product-medicine.fxml", "Medicamento", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    
    /**
     * Edit Medicine
     */
    @FXML
    private void actEditMedicine(ActionEvent event) {
    	logger.info("Calling actEditMedicine");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblTitle, "product-medicine.fxml", "Medicamento", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableMedicine.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Medicine
     */
    @FXML
    private void actDeleteMedicine(ActionEvent event) {
    	logger.info("Calling actDeleteMedicine");
    	
    	message.clear();
    	
    	List<Med> trailerSelected = tableMedicine.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, trailerSelected.size()+" Medicamento(s)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listMedicine.removeAll(trailerSelected);
        }
    }
    
    /**
     * Add Armament
     */
    @FXML
    private void actAddArmament(ActionEvent event) {
    	logger.info("Calling actAddArmament");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblTitle, "product-armament.fxml", "Armamento", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    
    /**
     * Edit Armament
     */
    @FXML
    private void actEditArmament(ActionEvent event) {
    	logger.info("Calling actEditArmament");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblTitle, "product-armament.fxml", "Armamento", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableArmament.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Armament
     */
    @FXML
    private void actDeleteArmament(ActionEvent event) {
    	logger.info("Calling actDeleteArmament");
    	
    	message.clear();
    	
    	List<Arma> armamentSelected = tableArmament.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, armamentSelected.size()+" volume(s)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listArmament.removeAll(armamentSelected);
        }
    }
    
    
    
    
    
    /**
     * Action to show the Issuer's Form to edit 
     */
    @FXML
    private void actEdit(ActionEvent e){
    	logger.info("Calling actEdit");
    	
    	clear();
    	
//    	issuerUpdate = tableIssuer.getSelectionModel().getSelectedItem();
//    	
//        txtCnpj.setText(issuerUpdate.getCnpj());
//        txtIe.setText(issuerUpdate.getIe());
//        txtCompanyName.setText(issuerUpdate.getCompanyName());
//        txtTradeName.setText(issuerUpdate.getTradeName());
//        txtIm.setText(issuerUpdate.getIm());
//        txtCnae.setText(issuerUpdate.getCnae());
//        txtIeSt.setText(issuerUpdate.getIeSt());
//        cmbRegimeTrib.getSelectionModel().select(issuerUpdate.getRegimeTrib());
//        cmbUF.getSelectionModel().select(issuerUpdate.getUf());
//        txtZipCode.setText(issuerUpdate.getZipCode());
//        txtStreet.setText(issuerUpdate.getStreet());
//        txtNumber.setText(issuerUpdate.getNumber());
//        txtComplement.setText(issuerUpdate.getComplement());
//        txtDistrict.setText(issuerUpdate.getDistrict());
//        txtEmail.setText(issuerUpdate.getEmail());
//        txtPhoneNumber.setText(issuerUpdate.getPhone());
//        if(issuerUpdate.getLogo()!=null)
//        	imgLogo.setImage(ImageUtil.getImageFromBytes(issuerUpdate.getLogo()));
//        
//        populateCountyCombo(issuerUpdate.getUf());
//        cmbCounty.getSelectionModel().select(issuerUpdate.getCountyModel());;
//        
//        btnSave.setVisible(false);
//        btnSave.setDefaultButton(false);
//        btnUpdate.setVisible(true);
//        btnUpdate.setDefaultButton(true);
//        
//        transitionController.executeTransition(TransitionType.FADE_OUT_UP, TransitionType.FADE_IN_UP, paneTable, paneIssuer);
    }
    
    
    /**
     * Update an Issuer
     */
    @FXML
    private void actUpdate(ActionEvent e){
    	logger.info("Calling actUpdate");
    	
//    	error.clear();
//    	message.clear();
//    	
//    	validateFields();
//    	
//    	if(hasError()){
//    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
//        }else{
//        	
//        	issuerUpdate = populateIssuer(issuerUpdate);
//            try {
//				getIssuerService().updateIssuer(issuerUpdate);
//				
//				message.clear();
//	        	addMessage(MessageFormat.format(MessageConstant.RECORD_UPDATED, "Emitente"));
//	        	screenHelper.dialogDefault("Yahooo...", message, DialogType.INFORMATION);
//	        	refreshDataTable();
//	        	
//	        	// Update Issuer Logged
//        		IssuerModel issuerSession = getIssuerService().getIssuerSession();
//	        	if(issuerSession!=null){
//    				if(issuerUpdate.getId()==issuerSession.getId()){
//    					MenuController menuController = (MenuController)getData();
//    					menuController.refreshIssuerLogged(issuerUpdate);;
//    				}
//        		}
//	        	
//	        	transitionController.executeTransition(TransitionType.FADE_OUT_UP, TransitionType.FADE_IN_UP, paneIssuer, paneTable);
//	        	clear();
//				
//			} catch (Exception e1) {
//				logger.error(e1);
//        		addError(e1.getMessage());
//        		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
//			}
//        }
    }
    
    
    /**
     * Save the Issuer on DB
     */
    @FXML
    private void actSave(ActionEvent event) {
    	logger.info("Calling actSave");
    	
    	error.clear();
    	message.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
            IssuerModel issuer = populateFiscalDocument(null);
            
            try {
				getIssuerService().saveIssuer(issuer);
				
				message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_INSERTED, "Emitente"));
	        	screenHelper.dialogDefault("Yahooo...", message, DialogType.INFORMATION);
	        	refreshDataTable();
//	        	transitionController.executeTransition(TransitionType.FADE_OUT_UP, TransitionType.FADE_IN_UP, paneIssuer, paneTable);
	        	clear();
				
			} catch (Exception e) {
				logger.error(e);
        		addError(e.getMessage());
        		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
			}
        }
    }

    
    /**
     * Populate Issuer Object 
     */
    private IssuerModel populateFiscalDocument(FiscalDocumentModel fiscalDocument){
    	logger.info("Calling populateFiscalDocument");
    	
    	if(fiscalDocument==null)
    		fiscalDocument = new FiscalDocumentModel();
    	
    	TNFe nfe = new TNFe();
    	InfNFe infNFe = new InfNFe();
    	
    	infNFe.setVersao(VersionEnum.SCHEMA_NFE.getDescription());
    	infNFe.setId("");//TODO 
    	
//        fiscalDocument.setCnpj(null);
//        fiscalDocument.setIe(null);
//        fiscalDocument.setCompanyName(null);
//        fiscalDocument.setTradeName(null);
//        fiscalDocument.setIm(null);
//        fiscalDocument.setCnae(null);
//        fiscalDocument.setIeSt(null);
//        fiscalDocument.setRegimeTrib(null);
//        fiscalDocument.setUf(null);
//        fiscalDocument.setZipCode(null));
//        fiscalDocument.setStreet(null);
//        fiscalDocument.setNumber(null);
//        fiscalDocument.setComplement(null);
//        fiscalDocument.setDistrict(null);
//        fiscalDocument.setEmail(null);
//        fiscalDocument.setPhone(null);
//        
//        		
//        CountyModel countyModel = cmbCounty.getValue();
//        fiscalDocument.setCountyModel(countyModel);
//        
//        UserModel userModel = SecurityUtil.getUserCurrent();
//        userModel.getIssuerModel().add(fiscalDocument);
//        
//        fiscalDocument.setUserModel(userModel);
//        
//        return fiscalDocument;
    	return null;
    }
    
    
    /**
     * Fields Validation 
     */
    private void validateFields(){
    	logger.info("Calling validateFields");
    	
    	error.clear();
    	
    	/**
         * Validate if is a required field 
         */
//    	if (txtCnpj.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "CNPJ"));
//        }
//    	if (txtIe.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Inscriï¿½ï¿½o Estadual"));
//        }
//    	if (txtCompanyName.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Razï¿½o Social"));
//        }
//    	if (txtIm.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Inscriï¿½ï¿½o Municipal"));
//        }
//    	if (cmbRegimeTrib.getSelectionModel().getSelectedIndex()<0) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Regime Tributï¿½rio (Subst. Trib.)"));
//        }
//    	if (cmbUF.getSelectionModel().getSelectedIndex()<0) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "UF"));
//        }else if(txtIe.getStyle().contains("green")){
//        	try {
//				DataValidateUtil.isValidaIe(txtIe.getText(), cmbUF.getSelectionModel().getSelectedItem());
//			} catch (Exception e) {
//				addError(e.getMessage());
//			}
//        }
//    	if (cmbCounty.getSelectionModel().getSelectedIndex()<0) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Municï¿½pio"));
//        }
//    	if (txtZipCode.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "CEP"));
//        }
//    	if (txtStreet.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Logradouro"));
//        }
//    	if (txtNumber.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Nï¿½mero"));
//        }
//    	if (txtDistrict.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Bairro"));
//        }
//    	
//    	
//    	/**
//         * Validate if is a valid content 
//         */
//    	if (txtCnpj.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "CNPJ"));
//        }
//    	if (txtIe.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Inscriï¿½ï¿½o Estadual"));
//        }
//    	if (txtIeSt.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Inscriï¿½ï¿½o Estadual (Subst. Trib.)"));
//        }
//    	if (txtCompanyName.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Razï¿½o Social"));
//        }
//    	if (txtTradeName.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Nome Fantasia"));
//        }
//    	if (txtIm.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Inscriï¿½ï¿½o Municipal"));
//        }
//    	if (txtCnae.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "CNAE"));
//        }
//    	if (txtZipCode.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "CEP"));
//        }
//    	if (txtStreet.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Logradouro"));
//        }
//    	if (txtNumber.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Nï¿½mero"));
//        }
//    	if (txtComplement.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Complemento"));
//        }
//    	if (txtDistrict.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Bairro"));
//        }
//    	if (txtPhoneNumber.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Telefone"));
//        }
//    	if (txtEmail.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "E-mail"));
//        }
    }
    
    
    /**
     * Initialize Table Screen with buttons disabled 
     */
    private void buttonsDisable(){
    	logger.info("Calling buttonsDisable");
    	
//    	btnSave.setVisible(true);
//        btnUpdate.setVisible(false);
//    	btnEnter.setDisable(true);
//    	btnAdd.setDisable(false);
//    	btnEdit.setDisable(true);
//    	btnDelete.setDisable(true);
    }
    
    
    /**
     * Install ObservableList on tables    
     */
    private void installObservableList(){
    	logger.info("Calling installObservableList");
    	
    	listFdRefereced = FXCollections.observableArrayList();
    	listFdProducerRefereced = FXCollections.observableArrayList();
    	listFdFiscalReceipt = FXCollections.observableArrayList();
    	listProduct = FXCollections.observableArrayList();
    	listTrailer = FXCollections.observableArrayList();
    	listVolume = FXCollections.observableArrayList();
    	listDuplicates = FXCollections.observableArrayList();
    	listTaxpayerComment = FXCollections.observableArrayList();
    	listTaxAuthorities = FXCollections.observableArrayList();
    	listProcessesReferenced = FXCollections.observableArrayList();
    	listDailySupplyCane = FXCollections.observableArrayList();
    	listDeductionCane = FXCollections.observableArrayList();
    	listDownloadAuthorization = FXCollections.observableArrayList();
    }
    
    
    /**
     * Populate combos on initialization   
     */
    private void populateCombos(){
    	logger.info("Calling populateCombos");
    	
    	//cmbCounty starts disabled and will be activated when cmbUF is selected
    	cmbCountyFiscalDocument.setDisable(true);
    	cmbCountyIssuer.setDisable(true);
    	cmbCountyReceiver.setDisable(true);
    	cmbCountyPickupLocation.setDisable(true);
    	cmbCountyDeliveryLocation.setDisable(true);
    	cmbCountyTransport.setDisable(true);
    	cmbCountyIcmsTransport.setDisable(true);
    	
    	cmbDocumentType.getItems().addAll(DocumentTypeEnum.values());
	    cmbPaymentMethods.getItems().addAll(PaymentMethodsEnum.values());
	    cmbIssueType.getItems().addAll(IssueTypeEnum.values());
	    cmbPurposeIssue.getItems().addAll(PurposeIssueEnum.values());
	    cmbAttendanceType.getItems().addAll(AttendanceTypeEnum.values());
	    cmbTargetOperation.getItems().addAll(TargetOperationEnum.values());
	    cmbUfFiscalDocument.getItems().addAll(getStateService().findUfs());
	    
	    cmbUFIssuer.getItems().addAll(getStateService().findUfs());
	    cmbRegimeTribIssuer.getItems().addAll(RegimeTributarioEnum.values());
	    
	    cmbTaxpayerIssuer.getItems().addAll(TaxpayerTypeEnum.values());
	    cmbUFReceiver.getItems().addAll(getStateService().findUfs());
	    cmbUFPickupLocation.getItems().addAll(getStateService().findUfs());
	    cmbUFDeliveryLocation.getItems().addAll(getStateService().findUfs());
	    
	    cmbSpecialTaxationTotals.getItems().addAll(SpecialTaxationEnum.values());
	    
	    cmbShippingModeTransport.getItems().addAll(ShippingModeEnum.values());
	    cmbUfTransport.getItems().addAll(getStateService().findUfs());
	    cmbCfopTransport.getItems().addAll(CfopEnum.values());
	    cmbUfIcmsTransport.getItems().addAll(getStateService().findUfs());
	    cmbUfCarTransport.getItems().addAll(getStateService().findUfs());
	    
	    cmbTypeVehicleTransport.getItems().addAll(VehicleTypeEnum.values());
	    
	    cmbUfExport.getItems().addAll(getStateService().findUfs());
    }
    
    
    /**
     * Populate cmbCounty after cmbUF is selected  
     */
    private void populateCountyCombo(ComboBox cmbCounty, String uf){
    	logger.info("Calling populateCountyCombo");
    	
    	cmbCounty.getItems().clear();
    	cmbCounty.setDisable(false);
    	cmbCounty.getItems().addAll(getStateService().findByUf(uf));
    }
    
    
    /**
     * Implements a tooltip alert on required fields  
     */
    private void setTooltips(){
    	logger.info("Calling setRequiredFields");
    	
    	/**
         * Tooltip subtitle buttons  
         */
//    	Tooltip tEnter = new Tooltip("Abrir Emitente");
//    	Tooltip tAdd = new Tooltip("Cadastrar Emitente");
//    	Tooltip tEdit = new Tooltip("Editar Emitente");
//    	Tooltip tDelete = new Tooltip("Excluir Emitente(s)");
//    	hackTooltipStartTiming(tEnter);
//    	hackTooltipStartTiming(tAdd);
//    	hackTooltipStartTiming(tEdit);
//    	hackTooltipStartTiming(tDelete);
//    	btnEnter.setTooltip(tEnter);
//    	btnAdd.setTooltip(tAdd);
//    	btnEdit.setTooltip(tEdit);
//    	btnDelete.setTooltip(tDelete);
    	
    	/**
         * Tooltip alert on required fields  
         */
    	Tooltip t = new Tooltip(MessageConstant.REQUIRED_FIELD);
    	hackTooltipStartTiming(t);
    	
//    	lblCnpj.setTooltip(t);
//    	lblCompanyName.setTooltip(t);
//    	lblIm.setTooltip(t);
//    	lblIe.setTooltip(t);
//    	lblUf.setTooltip(t);
//    	lblCounty.setTooltip(t);
//    	lblRegimeTrib.setTooltip(t);
//    	lblZipCode.setTooltip(t);
//    	lblStreet.setTooltip(t);
//    	lblNumber.setTooltip(t);
//    	lblDistrict.setTooltip(t);
    }
    
    
    /**
     * Listeners Implementation
     */
    private void installListeners(){
    	logger.info("Calling installListeners");
    	
    	/**
	     * NF-e Tab  
	     */
		/*Listeners for dynamic field validation*/
		txtSerie.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtSerie, DataValidateUtil.SERIE_PATTERN, false));
		txtNumber.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtNumber, DataValidateUtil.NUMBER_PATTERN, false));
		txtNumericCode.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNumericCode, 8, 8, false));
		dtIssue.setOnKeyReleased(e -> DataValidateUtil.isValidDateHour(dtIssue));
		dtInputOutput.setOnKeyReleased(e -> DataValidateUtil.isValidDateHour(dtInputOutput));
		txtOperationOrigins.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtOperationOrigins, 1, 60, false));
		
		/* Listener for dynamic comboBox between cmbUF and cmbCounty */
		cmbUfFiscalDocument.valueProperty().addListener((observableValue, oldValue, newValue) -> {
		      if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
		    	  populateCountyCombo(cmbCountyFiscalDocument, newValue);
		    	  
		      }
		});
		tableFdRefereced.setRowFactory( tv -> {
		    TableRow<NFref> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblTitle, "referred-document.fxml", "Documentos Referenciados", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		tableFdProducerRefereced.setRowFactory( tv -> {
		    TableRow<RefNFP> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblTitle, "producer-referred-document.fxml", "Documentos Referenciados do Produtor", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		tableFdFiscalReceipt.setRowFactory( tv -> {
		    TableRow<RefECF> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblTitle, "fiscal-receipt-referred.fxml", "Cupom Fiscal", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableTrailer.setRowFactory( tv -> {
		    TableRow<TVeiculo> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblTitle, "trailer-transport.fxml", "Reboque", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableVolume.setRowFactory( tv -> {
		    TableRow<Vol> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblTitle, "volume-transport.fxml", "Volume", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableDuplicates.setRowFactory( tv -> {
		    TableRow<Dup> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblTitle, "duplicate-invoice.fxml", "Duplicata", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableTaxpayerComment.setRowFactory( tv -> {
		    TableRow<ObsCont> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblTitle, "taxpayer-comment.fxml", "Observação do Contribuinte", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableProcessesReferenced.setRowFactory( tv -> {
		    TableRow<ProcRef> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblTitle, "referenced-process.fxml", "Processo Referenciado", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableDailySupplyCane.setRowFactory( tv -> {
		    TableRow<ForDia> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblTitle, "daily-supply-cane.fxml", "Fornecimento Diário", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableDeductionCane.setRowFactory( tv -> {
		    TableRow<Deduc> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblTitle, "deduction-cane.fxml", "Dedução", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableDownloadAuthorization.setRowFactory( tv -> {
		    TableRow<AutXML> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblTitle, "download-authorization.fxml", "Autorização de Download", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		/* Listener for dynamic button exhibition*/
		tableFdRefereced.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableFdRefereced.getSelectionModel().getSelectedItems().size()==0) {
		        btnAddFdRefereced.setDisable(false);
		        btnEditFdRefereced.setDisable(true);
		        btnDeleteFdRefereced.setDisable(true);
		    }else if (tableFdRefereced.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddFdRefereced.setDisable(false);
		    	btnEditFdRefereced.setDisable(false);
		    	btnDeleteFdRefereced.setDisable(false);
		    }else if (tableFdRefereced.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddFdRefereced.setDisable(false);
		    	btnEditFdRefereced.setDisable(true);
		    	btnDeleteFdRefereced.setDisable(false);
		    }
		});
		tableFdProducerRefereced.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableFdProducerRefereced.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddFdProducerRefereced.setDisable(false);
		    	btnEditFdProducerRefereced.setDisable(true);
		    	btnDeleteFdProducerRefereced.setDisable(true);
		    }else if (tableFdProducerRefereced.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddFdProducerRefereced.setDisable(false);
		    	btnEditFdProducerRefereced.setDisable(false);
		    	btnDeleteFdProducerRefereced.setDisable(false);
		    }else if (tableFdProducerRefereced.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddFdProducerRefereced.setDisable(false);
		    	btnEditFdProducerRefereced.setDisable(true);
		    	btnDeleteFdProducerRefereced.setDisable(false);
		    }
		});
		tableFdFiscalReceipt.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableFdFiscalReceipt.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddFdFiscalReceipt.setDisable(false);
		    	btnEditFdFiscalReceipt.setDisable(true);
		    	btnDeleteFdFiscalReceipt.setDisable(true);
		    }else if (tableFdFiscalReceipt.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddFdFiscalReceipt.setDisable(false);
		    	btnEditFdFiscalReceipt.setDisable(false);
		    	btnDeleteFdFiscalReceipt.setDisable(false);
		    }else if (tableFdFiscalReceipt.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddFdFiscalReceipt.setDisable(false);
		    	btnEditFdFiscalReceipt.setDisable(true);
		    	btnDeleteFdFiscalReceipt.setDisable(false);
		    }
		});
		tableProduct.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableProduct.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddProduct.setDisable(false);
		    	btnEditProduct.setDisable(true);
		    	btnDeleteProduct.setDisable(true);
		    }else if (tableProduct.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddProduct.setDisable(false);
		    	btnEditProduct.setDisable(false);
		    	btnDeleteProduct.setDisable(false);
		    }else if (tableProduct.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddProduct.setDisable(false);
		    	btnEditProduct.setDisable(true);
		    	btnDeleteProduct.setDisable(false);
		    }
		});
    	
		/**
	     * Issuer Tab  
	     */
		txtCnpjIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidCnpj(txtCnpjIssuer, true));
    	txtIeIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtIeIssuer, DataValidateUtil.IE_PATTERN, false));
    	txtCompanyNameIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCompanyNameIssuer, 2, 60, false));
    	txtTradeNameIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtTradeNameIssuer, 1, 60, false));
    	txtImIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtImIssuer, 1, 15, false));
    	txtCnaeIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtCnaeIssuer, DataValidateUtil.CNAE_PATTERN, false));
    	txtIeStIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtIeStIssuer, DataValidateUtil.IEST_PATTERN, false));
    	txtZipCodeIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtZipCodeIssuer, DataValidateUtil.ZIPCODE_PATTERN, true));
    	txtStreetIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtStreetIssuer, 2, 60, false));
    	txtNumberIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNumberIssuer, 1, 60, false));
    	txtComplementIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtComplementIssuer, 1, 60, false));
    	txtDistrictIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDistrictIssuer, 2, 60, false));
    	txtEmailIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtEmailIssuer, DataValidateUtil.EMAIL_PATTERN, false));
    	txtPhoneNumberIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtPhoneNumberIssuer, DataValidateUtil.PHONE_PATTERN, true));
    	/* Listener for dynamic comboBox between cmbUF and cmbCounty */
		cmbUFIssuer.valueProperty().addListener((observableValue, oldValue, newValue) -> {
		      if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
		    	  populateCountyCombo(cmbCountyIssuer, newValue);
		    	  
		      }
		});
		
		/**
	     * Receiver Tab  
	     */
		txtCnpjReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidCnpj(txtCnpjReceiver, true));
		txtIeReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtIeReceiver, DataValidateUtil.IE_PATTERN, false));
		txtSuframaReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtSuframaReceiver, DataValidateUtil.SUFRAMA_PATTERN, false));
		txtCompanyNameReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCompanyNameReceiver, 2, 60, false));
    	txtTradeNameIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtTradeNameIssuer, 1, 60, false));
    	txtImReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtImReceiver, 1, 15, false));
    	txtZipCodeReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtZipCodeReceiver, DataValidateUtil.ZIPCODE_PATTERN, true));
    	txtPhoneNumberReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtPhoneNumberReceiver, DataValidateUtil.PHONE_PATTERN, true));
    	txtStreetReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtStreetReceiver, 2, 60, false));
    	txtNumberReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNumberReceiver, 1, 60, false));
    	txtComplementReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtComplementReceiver, 1, 60, false));
    	txtDistrictReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDistrictReceiver, 2, 60, false));
    	txtEmailReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtEmailReceiver, DataValidateUtil.EMAIL_PATTERN, false));
	    /* Listener for dynamic comboBox between cmbUF and cmbCounty */
		cmbUFReceiver.valueProperty().addListener((observableValue, oldValue, newValue) -> {
		      if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
		    	  populateCountyCombo(cmbCountyReceiver, newValue);
		    	  
		      }
		});
		
		txtZipCodePickupLocation.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtZipCodePickupLocation, DataValidateUtil.ZIPCODE_PATTERN, true));
		txtPhoneNumberPickupLocation.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtPhoneNumberPickupLocation, DataValidateUtil.PHONE_PATTERN, true));
		txtStreetPickupLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtStreetPickupLocation, 2, 60, false));
		txtNumberPickupLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNumberPickupLocation, 1, 60, false));
		txtComplementPickupLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtComplementPickupLocation, 1, 60, false));
		txtDistrictPickupLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDistrictPickupLocation, 2, 60, false));
		txtCNPJPickupLocation.setOnKeyReleased(e -> DataValidateUtil.isValidCnpj(txtCNPJPickupLocation, true));
    	/* Listener for dynamic comboBox between cmbUF and cmbCounty */
    	cmbUFPickupLocation.valueProperty().addListener((observableValue, oldValue, newValue) -> {
		      if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
		    	  populateCountyCombo(cmbCountyPickupLocation, newValue);
		    	  
		      }
		});
    	
    	txtZipCodeDeliveryLocation.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtZipCodeDeliveryLocation, DataValidateUtil.ZIPCODE_PATTERN, true));
    	txtPhoneNumberDeliveryLocation.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtPhoneNumberDeliveryLocation, DataValidateUtil.PHONE_PATTERN, true));
    	txtStreetDeliveryLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtStreetDeliveryLocation, 2, 60, false));
    	txtNumberDeliveryLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNumberDeliveryLocation, 1, 60, false));
    	txtComplementDeliveryLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtComplementDeliveryLocation, 1, 60, false));
    	txtDistrictDeliveryLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDistrictDeliveryLocation, 2, 60, false));
    	txtCNPJDeliveryLocation.setOnKeyReleased(e -> DataValidateUtil.isValidCnpj(txtCNPJDeliveryLocation, true));
    	/* Listener for dynamic comboBox between cmbUF and cmbCounty */
    	cmbUFDeliveryLocation.valueProperty().addListener((observableValue, oldValue, newValue) -> {
		      if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
		    	  populateCountyCombo(cmbCountyDeliveryLocation, newValue);
		    	  
		      }
		});
    	
    	
    	/**
	     * Totals Tab  
	     */
    	txtCalcBasisIcmsTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCalcBasisIcmsTotals, 1, 20, false));
    	txtIcmsTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIcmsTotals, 1, 20, false));
    	txtCalcBasisIcmsStTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCalcBasisIcmsStTotals, 1, 20, false));
    	txtIcmsStTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIcmsStTotals, 1, 20, false));
    	txtProductTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtProductTotals, 1, 20, false));
    	txtFreightTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtFreightTotals, 1, 20, false));
    	txtInsuranceTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtInsuranceTotals, 1, 20, false));
    	txtDiscountTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDiscountTotals, 1, 20, false));
    	txtIiTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIiTotals, 1, 20, false));
    	txtIpiTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIpiTotals, 1, 20, false));
    	txtPisTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtPisTotals, 1, 20, false));
    	txtCofinsTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCofinsTotals, 1, 20, false));
    	txtOtherExpensesTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtOtherExpensesTotals, 1, 20, false));
    	txtIcmsFreeTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIcmsFreeTotals, 1, 20, false));
    	txtDocumentTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDocumentTotals, 1, 20, false));
    	
    	txtCalcBasisIssTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCalcBasisIssTotals, 1, 20, false));
    	txtIssTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIssTotals, 1, 20, false));
    	txtPisOnServiceTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtPisOnServiceTotals, 1, 20, false));
    	txtCofinsOnServiceTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCofinsOnServiceTotals, 1, 20, false));
    	txtNonTaxTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNonTaxTotals, 1, 20, false));
    	txtDeductionsTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDeductionsTotals, 1, 20, false));
    	txtConditionDiscountTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtConditionDiscountTotals, 1, 20, false));
    	txtUnconditionDiscountTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtUnconditionDiscountTotals, 1, 20, false));
    	txtIssWithholdingTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIssWithholdingTotals, 1, 20, false));
    	txtOtherWithholdingTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtOtherWithholdingTotals, 1, 20, false));
    	dtServiceTotals.setOnKeyReleased(e -> DataValidateUtil.isValidDate(dtServiceTotals));
    	
    	txtPisWithholdingTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtPisWithholdingTotals, 1, 20, false));
    	txtCofinsWithholdingTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCofinsWithholdingTotals, 1, 20, false));
    	txtCsllWithholdingTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCsllWithholdingTotals, 1, 20, false));
    	txtCalcBasisSocialSecurityTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCalcBasisSocialSecurityTotals, 1, 20, false));
    	txtSocialSecurityWithholdingTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtSocialSecurityWithholdingTotals, 1, 20, false));
    	txtCalcBasisIrrfTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCalcBasisIrrfTotals, 1, 20, false));
    	txtIrrfWithholdingTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIrrfWithholdingTotals, 1, 20, false));
    	
    	/**
	     * Transport Tab  
	     */
    	txtCpfNumberTransport.setOnKeyReleased(e -> DataValidateUtil.isValidCpf(txtCpfNumberTransport, true));
    	txtCnpjNumberTransport.setOnKeyReleased(e -> DataValidateUtil.isValidCnpj(txtCnpjNumberTransport, true));
    	txtIeTransport.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtIeTransport, DataValidateUtil.IE_PATTERN, false));
    	txtCompanyNameTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCompanyNameTransport, 2, 60, false));
    	txtStreetTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtStreetTransport, 2, 60, false));
    	cmbUfTransport.valueProperty().addListener((observableValue, oldValue, newValue) -> {
		      if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
		    	  populateCountyCombo(cmbCountyTransport, newValue);
		    	  
		      }
		});
    	rdDocumentTypeCpf.selectedProperty().addListener((observableValue,  wasPreviouslySelected, isNowSelected) ->{
    	        if (isNowSelected) { 
    	        	transitionController.executeTransition(TransitionType.FADE_OUT_DOWN, TransitionType.FADE_IN_DOWN, txtCnpjNumberTransport, txtCpfNumberTransport);
    	        	txtCpfNumberTransport.clear();
    	        	txtCpfNumberTransport.setStyle("-fx-text-fill:black");
    	        }else{
    	        	transitionController.executeTransition(TransitionType.FADE_OUT_DOWN, TransitionType.FADE_IN_DOWN, txtCpfNumberTransport, txtCnpjNumberTransport);
    	        	txtCnpjNumberTransport.clear();
    	        	txtCnpjNumberTransport.setStyle("-fx-text-fill:black");
    	        }
    	});
    	
    	txtCalcBasisIcmsTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCalcBasisIcmsTransport, 1, 20, false));
    	txtServiceValueTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtServiceValueTransport, 1, 20, false));
    	txtAliqTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtAliqTransport, 1, 10, false));
    	txtIcmsWithholdingTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIcmsWithholdingTransport, 1, 20, false));
    	cmbUfIcmsTransport.valueProperty().addListener((observableValue, oldValue, newValue) -> {
		      if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
		    	  populateCountyCombo(cmbCountyIcmsTransport, newValue);
		    	  
		      }
		});
    	
    	txtLicensePlateTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtLicensePlateTransport, 1, 7, true));
    	txtRntcCarTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtRntcCarTransport, 1, 20, false));
    	
    	tableTrailer.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableTrailer.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddTrailer.setDisable(false);
		    	btnEditTrailer.setDisable(true);
		    	btnDeleteTrailer.setDisable(true);
		    }else if (tableTrailer.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddTrailer.setDisable(false);
		    	btnEditTrailer.setDisable(false);
		    	btnDeleteTrailer.setDisable(false);
		    }else if (tableTrailer.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddTrailer.setDisable(false);
		    	btnEditTrailer.setDisable(true);
		    	btnDeleteTrailer.setDisable(false);
		    }
		});
    	txtFerryIdentificationTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtFerryIdentificationTransport, 1, 20, false));
    	txtWagonIdentificationTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtWagonIdentificationTransport, 1, 20, false));
    	
    	tableVolume.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableVolume.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddVolume.setDisable(false);
		    	btnEditVolume.setDisable(true);
		    	btnDeleteVolume.setDisable(true);
		    }else if (tableVolume.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddVolume.setDisable(false);
		    	btnEditVolume.setDisable(false);
		    	btnDeleteVolume.setDisable(false);
		    }else if (tableVolume.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddVolume.setDisable(false);
		    	btnEditVolume.setDisable(true);
		    	btnDeleteVolume.setDisable(false);
		    }
		});
    	
    	tableTrailer.setRowFactory( tv -> {
		    TableRow<TVeiculo> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblTitle, "trailer-transport.fxml", "Reboque", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
    	
    	
    	/**
	     * Invoice Tab  
	     */
	    txtInvoiceNumberCollection.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtInvoiceNumberCollection, 1, 60, false));
	    txtOriginalValueCollection.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtOriginalValueCollection, 1, 20, false));
	    txtDiscountValueCollection.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDiscountValueCollection, 1, 20, false));
	    txtNetValueCollection.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNetValueCollection, 1, 20, false));
	   
	    tableDuplicates.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableDuplicates.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddDuplicates.setDisable(false);
		    	btnEditDuplicates.setDisable(true);
		    	btnDeleteDuplicates.setDisable(true);
		    }else if (tableDuplicates.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddDuplicates.setDisable(false);
		    	btnEditDuplicates.setDisable(false);
		    	btnDeleteDuplicates.setDisable(false);
		    }else if (tableDuplicates.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddDuplicates.setDisable(false);
		    	btnEditDuplicates.setDisable(true);
		    	btnDeleteDuplicates.setDisable(false);
		    }
		});
	    
	    
	    /**
	     * Additional Information Tab  
	     */
	    txtFiscoAdditionalInformation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextArea(txtFiscoAdditionalInformation, 1, 2000, false));
	    txtTaxpayerAdditionalInformation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextArea(txtTaxpayerAdditionalInformation, 1, 5000, false));
	    
	    tableTaxpayerComment.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableTaxpayerComment.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddTaxpayerComment.setDisable(false);
		    	btnEditTaxpayerComment.setDisable(true);
		    	btnDeleteDuplicates.setDisable(true);
		    }else if (tableTaxpayerComment.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddTaxpayerComment.setDisable(false);
		    	btnEditTaxpayerComment.setDisable(false);
		    	btnDeleteTaxpayerComment.setDisable(false);
		    }else if (tableTaxpayerComment.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddTaxpayerComment.setDisable(false);
		    	btnEditTaxpayerComment.setDisable(true);
		    	btnDeleteTaxpayerComment.setDisable(false);
		    }
		});
	    
	    tableTaxAuthorities.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableTaxAuthorities.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddTaxAuthorities.setDisable(false);
		    	btnEditTaxAuthorities.setDisable(true);
		    	btnDeleteTaxAuthorities.setDisable(true);
		    }else if (tableTaxAuthorities.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddTaxAuthorities.setDisable(false);
		    	btnEditTaxAuthorities.setDisable(false);
		    	btnDeleteTaxAuthorities.setDisable(false);
		    }else if (tableTaxAuthorities.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddTaxAuthorities.setDisable(false);
		    	btnEditTaxAuthorities.setDisable(true);
		    	btnDeleteTaxAuthorities.setDisable(false);
		    }
		});
	    
	    tableProcessesReferenced.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableProcessesReferenced.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddProcessesReferenced.setDisable(false);
		    	btnEditProcessesReferenced.setDisable(true);
		    	btnDeleteProcessesReferenced.setDisable(true);
		    }else if (tableProcessesReferenced.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddProcessesReferenced.setDisable(false);
		    	btnEditProcessesReferenced.setDisable(false);
		    	btnDeleteProcessesReferenced.setDisable(false);
		    }else if (tableProcessesReferenced.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddProcessesReferenced.setDisable(false);
		    	btnEditProcessesReferenced.setDisable(true);
		    	btnDeleteProcessesReferenced.setDisable(false);
		    }
		});
	    
	    
	    /**
	     * Export Tab  
	     */
	    txtBoarding.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtBoarding, 1, 60, false));
	    txtDispatch.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDispatch, 1, 60, false));	
	    
	    
	    /**
	     * Purchase Tab  
	     */
	    txtPublicPurchase.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtPublicPurchase, 1, 22, false));	
	    txtRequestInfPurchase.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtRequestInfPurchase, 1, 60, false));
	    txtContractInfPurchase.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtContractInfPurchase, 1, 60, false));
	    
	    /**
		 * Cane Tab
		 */	
	    
	    txtHarvestIdentificationCane.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtHarvestIdentificationCane, DataValidateUtil.CANE_REF_PATTERN, false));
	    txtReferenceDateCane.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtReferenceDateCane, DataValidateUtil.CANE_PATTERN, false));
	    
	    tableDailySupplyCane.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableDailySupplyCane.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddDailySupplyCane.setDisable(false);
		    	btnEditDailySupplyCane.setDisable(true);
		    	btnDeleteDailySupplyCane.setDisable(true);
		    }else if (tableDailySupplyCane.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddDailySupplyCane.setDisable(false);
		    	btnEditDailySupplyCane.setDisable(false);
		    	btnDeleteDailySupplyCane.setDisable(false);
		    }else if (tableDailySupplyCane.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddDailySupplyCane.setDisable(false);
		    	btnEditDailySupplyCane.setDisable(true);
		    	btnDeleteDailySupplyCane.setDisable(false);
		    }
		});
	    
	    tableDeductionCane.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableDeductionCane.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddDeductionCane.setDisable(false);
		    	btnEditDeductionCane.setDisable(true);
		    	btnDeleteDeductionCane.setDisable(true);
		    }else if (tableDeductionCane.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddDeductionCane.setDisable(false);
		    	btnEditDeductionCane.setDisable(false);
		    	btnDeleteDeductionCane.setDisable(false);
		    }else if (tableDeductionCane.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddDeductionCane.setDisable(false);
		    	btnEditDeductionCane.setDisable(true);
		    	btnDeleteDeductionCane.setDisable(false);
		    }
		});
	    
	    txtMonthTotalSupplies.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtMonthTotalSupplies, 1, 26, false));
	    txtPreviousTotalSupplies.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtPreviousTotalSupplies, 1, 26, false));
	    txtGeralTotalSupplies.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtGeralTotalSupplies, 1, 26, false));
	    txtValueSupplies.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtValueSupplies, 1, 22, false));
	    txtValueTotalDeductionSupplies.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtValueTotalDeductionSupplies, 1, 22, false));
	    txtNetValueSupplies.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNetValueSupplies, 1, 22, false));
	    
	    
	    /**
		 * Download Autorization Tab
		 */
	    tableDownloadAuthorization.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableDownloadAuthorization.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddDownloadAuthorization.setDisable(false);
		    	btnEditDownloadAuthorization.setDisable(true);
		    	btnDeleteDownloadAuthorization.setDisable(true);
		    }else if (tableDownloadAuthorization.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddDownloadAuthorization.setDisable(false);
		    	btnEditDownloadAuthorization.setDisable(false);
		    	btnDeleteDownloadAuthorization.setDisable(false);
		    }else if (tableDownloadAuthorization.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddDownloadAuthorization.setDisable(false);
		    	btnEditDownloadAuthorization.setDisable(true);
		    	btnDeleteDownloadAuthorization.setDisable(false);
		    }
		});
	    
    }
    
    
    /**
     * Define Field's Mask  
     */
    private void setMaskField(){
    	logger.info("Calling setFieldMask");
    	
    	/**
	     * NF-e Tab  
	     */
    	MaskFieldUtil.numericField(txtSerie);
    	MaskFieldUtil.maxTextField(txtSerie,3);
    	MaskFieldUtil.numericField(txtNumber);
    	MaskFieldUtil.maxTextField(txtNumber,9);
    	MaskFieldUtil.numericField(txtNumericCode);
    	MaskFieldUtil.maxTextField(txtNumericCode,8);
    	MaskFieldUtil.dateHourField(dtIssue);
    	MaskFieldUtil.dateHourField(dtInputOutput);
    	MaskFieldUtil.maxTextField(txtOperationOrigins,60);
    	
    	/**
	     * Issuer Tab  
	     */
    	MaskFieldUtil.cnpjField(txtCnpjIssuer);
    	MaskFieldUtil.maxTextField(txtCompanyNameIssuer, 60);
    	MaskFieldUtil.maxTextField(txtTradeNameIssuer, 60);
    	MaskFieldUtil.maxTextField(txtImIssuer, 15);
    	MaskFieldUtil.numericField(txtImIssuer);
    	MaskFieldUtil.maxTextField(txtCnaeIssuer, 7);
    	MaskFieldUtil.numericField(txtCnaeIssuer);
    	MaskFieldUtil.maxTextField(txtIeStIssuer, 14);
    	MaskFieldUtil.numericField(txtIeStIssuer);
    	MaskFieldUtil.maxTextField(txtIeIssuer, 14);
    	MaskFieldUtil.numericField(txtIeIssuer);
    	MaskFieldUtil.zipCodeField(txtZipCodeIssuer);
    	MaskFieldUtil.maxTextField(txtStreetIssuer, 60);
    	MaskFieldUtil.maxTextField(txtNumberIssuer, 60);
    	MaskFieldUtil.maxTextField(txtComplementIssuer, 60);
    	MaskFieldUtil.maxTextField(txtDistrictIssuer, 60);
    	MaskFieldUtil.maxTextField(txtEmailIssuer, 256);
    	MaskFieldUtil.phoneField(txtPhoneNumberIssuer);
    	
    	/**
	     * Receiver Tab  
	     */
    	MaskFieldUtil.cnpjField(txtCnpjReceiver);
    	MaskFieldUtil.maxTextField(txtIeReceiver, 14);
    	MaskFieldUtil.numericField(txtIeReceiver);
    	MaskFieldUtil.maxTextField(txtSuframaReceiver, 9);
    	MaskFieldUtil.numericField(txtSuframaReceiver);
    	MaskFieldUtil.maxTextField(txtCompanyNameReceiver,60);
    	MaskFieldUtil.maxTextField(txtImReceiver,15);
    	MaskFieldUtil.numericField(txtImReceiver);
    	MaskFieldUtil.zipCodeField(txtZipCodeReceiver);
    	MaskFieldUtil.phoneField(txtPhoneNumberReceiver);	
    	MaskFieldUtil.maxTextField(txtStreetReceiver,60);
    	MaskFieldUtil.maxTextField(txtNumberReceiver,60);
    	MaskFieldUtil.maxTextField(txtComplementReceiver,60);
    	MaskFieldUtil.maxTextField(txtDistrictReceiver,60);
    	MaskFieldUtil.maxTextField(txtEmailReceiver, 256);
    	//Tab 2
	    MaskFieldUtil.zipCodeField(txtZipCodePickupLocation);
    	MaskFieldUtil.phoneField(txtPhoneNumberPickupLocation);	
    	MaskFieldUtil.maxTextField(txtStreetPickupLocation,60);
    	MaskFieldUtil.maxTextField(txtNumberPickupLocation,60);
    	MaskFieldUtil.maxTextField(txtComplementPickupLocation,60);
    	MaskFieldUtil.maxTextField(txtDistrictPickupLocation,60);
    	MaskFieldUtil.cnpjField(txtCNPJPickupLocation);
    	//Tab 3
    	MaskFieldUtil.zipCodeField(txtZipCodeDeliveryLocation);
    	MaskFieldUtil.phoneField(txtPhoneNumberDeliveryLocation);	
    	MaskFieldUtil.maxTextField(txtStreetDeliveryLocation,60);
    	MaskFieldUtil.maxTextField(txtNumberDeliveryLocation,60);
    	MaskFieldUtil.maxTextField(txtComplementDeliveryLocation,60);
    	MaskFieldUtil.maxTextField(txtDistrictDeliveryLocation,60);
    	MaskFieldUtil.cnpjField(txtCNPJDeliveryLocation);
    	
    	/**
	     * Totals Tab  
	     */
    	MaskFieldUtil.monetaryField(13, 2, txtCalcBasisIcmsTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIcmsTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtCalcBasisIcmsStTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIcmsStTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtProductTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtFreightTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtInsuranceTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtDiscountTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIiTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIpiTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtPisTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtCofinsTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtOtherExpensesTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIcmsFreeTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtDocumentTotals);
    	
    	MaskFieldUtil.monetaryField(13, 2, txtCalcBasisIssTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIssTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtPisOnServiceTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtCofinsOnServiceTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtNonTaxTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtDeductionsTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtConditionDiscountTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtUnconditionDiscountTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIssWithholdingTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtOtherWithholdingTotals);
    	MaskFieldUtil.dateField(dtServiceTotals);
    	
    	MaskFieldUtil.monetaryField(13, 2, txtPisWithholdingTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtCofinsWithholdingTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtCsllWithholdingTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtCalcBasisSocialSecurityTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtSocialSecurityWithholdingTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtCalcBasisIrrfTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIrrfWithholdingTotals);
    	
    	/**
	     * Transport Tab  
	     */
    	MaskFieldUtil.cpfField(txtCpfNumberTransport);
    	MaskFieldUtil.cnpjField(txtCnpjNumberTransport);
    	MaskFieldUtil.numericField(txtIeTransport);
    	MaskFieldUtil.maxTextField(txtIeTransport, 14);
    	MaskFieldUtil.maxTextField(txtCompanyNameTransport,60);
    	MaskFieldUtil.maxTextField(txtStreetTransport,60);
    	
    	MaskFieldUtil.monetaryField(13, 2, txtCalcBasisIcmsTransport);
    	MaskFieldUtil.monetaryField(13, 2, txtServiceValueTransport);
    	MaskFieldUtil.monetaryField(3, 4, txtAliqTransport);
    	MaskFieldUtil.monetaryField(13, 2, txtIcmsWithholdingTransport);
    	
    	MaskFieldUtil.licensePlateField(txtLicensePlateTransport);
    	MaskFieldUtil.maxTextField(txtRntcCarTransport, 20);
    	
    	MaskFieldUtil.maxTextField(txtFerryIdentificationTransport, 20);
    	MaskFieldUtil.maxTextField(txtWagonIdentificationTransport, 20);
    	
    	/**
	     * Invoice Tab  
	     */
    	MaskFieldUtil.maxTextField(txtInvoiceNumberCollection, 60);  
    	MaskFieldUtil.monetaryField(13, 2, txtOriginalValueCollection);    
    	MaskFieldUtil.monetaryField(13, 2, txtDiscountValueCollection);    
    	MaskFieldUtil.monetaryField(13, 2, txtNetValueCollection);    
    	
    	/**
	     * Additional Information Tab  
	     */
    	MaskFieldUtil.maxTextArea(txtFiscoAdditionalInformation, 2000);  
    	MaskFieldUtil.maxTextArea(txtTaxpayerAdditionalInformation, 5000);  
    	
    	/**
	     * Export Tab  
	     */
    	MaskFieldUtil.maxTextField(txtBoarding, 60);
    	MaskFieldUtil.maxTextField(txtDispatch, 60);
    	
    	/**
	     * Purchase Tab  
	     */
    	MaskFieldUtil.maxTextField(txtPublicPurchase, 22);	
    	MaskFieldUtil.maxTextField(txtRequestInfPurchase, 60);
    	MaskFieldUtil.maxTextField(txtContractInfPurchase, 60);
    	
    	/**
		 * Cane Tab
		 */	
    	MaskFieldUtil.numericField(txtHarvestIdentificationCane);
    	MaskFieldUtil.dateYearField(txtHarvestIdentificationCane);
    	MaskFieldUtil.numericField(txtReferenceDateCane);
    	MaskFieldUtil.dateMonthYearField(txtReferenceDateCane);
    	
    	MaskFieldUtil.monetaryField(11, 10, txtMonthTotalSupplies);
		MaskFieldUtil.monetaryField(11, 10, txtPreviousTotalSupplies);
		MaskFieldUtil.monetaryField(11, 10, txtGeralTotalSupplies);
		MaskFieldUtil.monetaryField(13, 2, txtValueSupplies);
		MaskFieldUtil.monetaryField(13, 2, txtValueTotalDeductionSupplies);
		MaskFieldUtil.monetaryField(13, 2, txtNetValueSupplies);
    }
    
    
    /**
     * Mapping columns table with issuer object
     */
    private void setModelColumn(){
    	logger.info("Calling setModelColumn");
    	
    	/**
		 * Referred Document Table
		 */	
    	tableFdRefereced.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colTypeFdRefereced.setCellValueFactory(nFref -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(nFref.getValue().getRefCTe()))
		    	  property.setValue("CT-e");
		      else if(!StringHelper.isEmpty(nFref.getValue().getRefNFe()))
		    	  property.setValue("NF-e");
		      else
		    	  property.setValue("NF");
		      return property;
    	});
    	colAccessKeyFdRefereced.setCellValueFactory(nFref -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(nFref.getValue().getRefCTe()))
		    	  property.setValue(nFref.getValue().getRefCTe());
		      else if(!StringHelper.isEmpty(nFref.getValue().getRefNFe()))
		    	  property.setValue(nFref.getValue().getRefNFe());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colUfFdRefereced.setCellValueFactory(nFref -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(nFref.getValue().getRefNF()!=null)
		    	  property.setValue(nFref.getValue().getRefNF().getCUF());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colCnpjFdRefereced.setCellValueFactory(nFref -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(nFref.getValue().getRefNF()!=null)
		    	  property.setValue(FormatterTextUtil.cnpjField(nFref.getValue().getRefNF().getCNPJ()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colModelFdRefereced.setCellValueFactory(nFref -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(nFref.getValue().getRefNF()!=null)
		    	  property.setValue(nFref.getValue().getRefNF().getMod());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colSerieFdRefereced.setCellValueFactory(nFref -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(nFref.getValue().getRefNF()!=null)
		    	  property.setValue(nFref.getValue().getRefNF().getSerie());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colNumberFdRefereced.setCellValueFactory(nFref -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(nFref.getValue().getRefNF()!=null)
		    	  property.setValue(nFref.getValue().getRefNF().getNNF());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	
    	/**
		 * ProducerReferred Document Table
		 */	
    	tableFdProducerRefereced.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colUfFdProducerRefereced.setCellValueFactory(refNfp -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refNfp.getValue().getCUF()))
		    	  property.setValue(refNfp.getValue().getCUF());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colCnpjFdProducerRefereced.setCellValueFactory(refNfp -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refNfp.getValue().getCNPJ()))
		    	  property.setValue(FormatterTextUtil.cnpjField(refNfp.getValue().getCNPJ()));
		      else if(!StringHelper.isEmpty(refNfp.getValue().getCPF()))
		    	  property.setValue(FormatterTextUtil.cpfField(refNfp.getValue().getCPF()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colModelFdProducerRefereced.setCellValueFactory(refNfp -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refNfp.getValue().getMod()))
		    	  property.setValue(refNfp.getValue().getMod());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colSerieFdProducerRefereced.setCellValueFactory(refNfp -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refNfp.getValue().getSerie()))
		    	  property.setValue(refNfp.getValue().getSerie());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colNumberFdProducerRefereced.setCellValueFactory(refNfp -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refNfp.getValue().getNNF()))
		    	  property.setValue(refNfp.getValue().getNNF());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colIeFdProducerRefereced.setCellValueFactory(refNfp -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refNfp.getValue().getIE()))
		    	  property.setValue(refNfp.getValue().getIE());
		      else
		    	  property.setValue("ISENTO");
		      return property;
    	});
    	
    	
    	/**
		 * Fiscal Receipt Table
		 */	
    	tableFdFiscalReceipt.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colModelFdFiscalReceipt.setCellValueFactory(refEcf -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refEcf.getValue().getMod()))
		    	  property.setValue(refEcf.getValue().getMod());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colCooFdFiscalReceipt.setCellValueFactory(refEcf -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refEcf.getValue().getNCOO()))
		    	  property.setValue(refEcf.getValue().getNCOO());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colEcfFdFiscalReceipt.setCellValueFactory(refEcf -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refEcf.getValue().getNECF()))
		    	  property.setValue(refEcf.getValue().getNECF());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	
    	/**
		 * Trailer Table
		 */	
    	tableTrailer.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colLicensePlateTrailer.setCellValueFactory(trailer -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(trailer.getValue().getPlaca()))
		    	  property.setValue(FormatterTextUtil.licensePlateField(trailer.getValue().getPlaca()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colUfTrailer.setCellValueFactory(trailer -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(trailer.getValue().getUF()!=null)
		    	  property.setValue(trailer.getValue().getUF().toString());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colRntcTrailer.setCellValueFactory(trailer -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(trailer.getValue().getRNTC()))
		    	  property.setValue(trailer.getValue().getRNTC());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	
    	/**
		 * Trailer Table
		 */	
    	tableVolume.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colAmountVolume.setCellValueFactory(volume -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(volume.getValue().getQVol()))
		    	  property.setValue(volume.getValue().getQVol());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colNetWeightVolume.setCellValueFactory(volume -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(volume.getValue().getPesoL()))
		    	  property.setValue(FormatterTextUtil.monetaryField(12, 3, volume.getValue().getPesoL().toString()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colGrossWeightVolume.setCellValueFactory(volume -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(volume.getValue().getPesoB()))
		    	  property.setValue(FormatterTextUtil.monetaryField(12, 3, volume.getValue().getPesoB().toString()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	colNumberVolume.setCellValueFactory(volume -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(volume.getValue().getNVol()))
		    	  property.setValue(volume.getValue().getNVol());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colBrandVolume.setCellValueFactory(volume -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(volume.getValue().getMarca()))
		    	  property.setValue(volume.getValue().getMarca());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colSpecieVolume.setCellValueFactory(volume -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(volume.getValue().getEsp()))
		    	  property.setValue(volume.getValue().getEsp());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colSealAmount.setCellValueFactory(volume -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(volume.getValue().getLacres()!=null)
		    	  property.setValue(Integer.toString(volume.getValue().getLacres().size()));
		      else
		    	  property.setValue("0");
		      return property;
    	});
    	
    	
    	/**
		 * Duplicate Table
		 */	
    	tableDuplicates.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colDueDateDuplicates.setCellValueFactory(duplicate -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(duplicate.getValue().getDVenc()))
		    	  property.setValue(duplicate.getValue().getDVenc());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colNumberDuplicates.setCellValueFactory(duplicate -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(duplicate.getValue().getNDup()))
		    	  property.setValue(duplicate.getValue().getNDup().toString());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colValueDuplicates.setCellValueFactory(duplicate -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(duplicate.getValue().getVDup()))
		    	  property.setValue(FormatterTextUtil.monetaryField(12, 3, duplicate.getValue().getVDup()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	
    	/**
		 * Taxpayer Comment Table
		 */	
    	tableTaxpayerComment.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colNameTaxpayerComment.setCellValueFactory(taxpayerComment -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(taxpayerComment.getValue().getXCampo()))
		    	  property.setValue(taxpayerComment.getValue().getXCampo());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colTaxpayerComment.setCellValueFactory(taxpayerComment -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(taxpayerComment.getValue().getXTexto()))
		    	  property.setValue(taxpayerComment.getValue().getXTexto().toString());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	
    	/**
		 * Tax Authority Comment Table
		 */	
    	tableTaxAuthorities.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colNameTaxAuthorities.setCellValueFactory(taxAuthorityComment -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(taxAuthorityComment.getValue().getXCampo()))
		    	  property.setValue(taxAuthorityComment.getValue().getXCampo());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colTaxAuthoritiesComment.setCellValueFactory(taxAuthorityComment -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(taxAuthorityComment.getValue().getXTexto()))
		    	  property.setValue(taxAuthorityComment.getValue().getXTexto().toString());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	
    	/**
		 * Process Referenced Table
		 */	
    	tableProcessesReferenced.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colIdentifierProcessesReferenced.setCellValueFactory(taxAuthorityComment -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(taxAuthorityComment.getValue().getNProc()))
		    	  property.setValue(taxAuthorityComment.getValue().getNProc());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colOriginProcessesReferenced.setCellValueFactory(taxAuthorityComment -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(taxAuthorityComment.getValue().getIndProc()))
		    	  property.setValue(taxAuthorityComment.getValue().getIndProc().toString());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	/**
		 * Daily Supply Table
		 */	
    	tableDailySupplyCane.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colDayDailySupplyCane.setCellValueFactory(dailySupply -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(dailySupply.getValue().getDia()))
		    	  property.setValue(dailySupply.getValue().getDia());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colNetWeightDailySupplyCane.setCellValueFactory(dailySupply -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(dailySupply.getValue().getQtde()))
		    	  property.setValue(FormatterTextUtil.monetaryField(11, 10, dailySupply.getValue().getQtde()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	/**
		 * Deduction Table
		 */	
    	tableDeductionCane.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colDescriptionDeductionCane.setCellValueFactory(deduction -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(deduction.getValue().getXDed()))
		    	  property.setValue(deduction.getValue().getXDed());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colValueDeductionCane.setCellValueFactory(deduction -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(deduction.getValue().getVDed()))
		    	  property.setValue(FormatterTextUtil.monetaryField(12, 3, deduction.getValue().getVDed()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	/**
		 * Download Authorization Table
		 */	
    	tableDownloadAuthorization.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colDocumentDownloadAuthorization.setCellValueFactory(downloadAuthorization -> {
    		 SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(downloadAuthorization.getValue().getCNPJ()))
		    	  property.setValue(FormatterTextUtil.cnpjField(downloadAuthorization.getValue().getCNPJ()));
		      else if(!StringHelper.isEmpty(downloadAuthorization.getValue().getCPF()))
		    	  property.setValue(FormatterTextUtil.cpfField(downloadAuthorization.getValue().getCPF()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    }
    
    
    /**
     * Clear Form
     */
    private void clear(){
    	logger.info("Calling clear");
    	
    	
//    	txtCnpj.clear();
//    	txtIe.clear();
//    	txtCompanyName.clear();
//    	txtTradeName.clear();
//    	txtIm.clear();
//    	txtCnae.clear();
//    	txtIeSt.clear();
//    	cmbRegimeTrib.getSelectionModel().clearSelection();
//    	cmbUF.getSelectionModel().clearSelection();
//    	cmbCounty.getSelectionModel().clearSelection();
//    	txtZipCode.clear();
//    	txtStreet.clear();
//    	txtNumber.clear();
//    	txtComplement.clear();
//    	txtDistrict.clear();
//    	txtEmail.clear();
//    	txtPhoneNumber.clear();
    }
    
    
    /**
     * Refresh Data Table
     */
    private void refreshDataTable(){
    	logger.info("Calling refreshDataTable");
    	
//        if(listIssuerObservable == null){
//            listIssuerObservable =  FXCollections.observableList(getIssuerService().findAllIssuers());
//        }else {
//            listIssuerObservable.clear();
//            listIssuerObservable.addAll(FXCollections.observableList(getIssuerService().findAllIssuers()));
//        }
//        tableIssuer.setItems(listIssuerObservable);
    }
    
    
    /**
     * Load data stream bar
     */
    private void loadComponents(){
    	logger.info("Calling selectWithService");
    	
        Service<Integer> service = new Service<Integer>() {
            @Override
            protected Task<Integer> createTask() {
                refreshDataTable();
                return new Task<Integer>() {           
                    @Override
                    protected Integer call() throws Exception {
                        Integer max = getIssuerService().findAllIssuers().size();
                        if (max > 35) {
                            max = 30;
                        }
                        updateProgress(0, max);
                        for (int k = 0; k < max; k++) {
                            Thread.sleep(40);
                            updateProgress(k+1, max);
                        }
                        return max;
                    }
                };
            }
        };
        service.start();
        bar.progressProperty().bind(service.progressProperty());
        service.setOnRunning((WorkerStateEvent event) -> {
            imgLoad.setVisible(true);
        });
        
        service.setOnSucceeded((WorkerStateEvent event) -> {
            imgLoad.setVisible(false);
            bar.setVisible(false);
//            transitionController.executeTransition(null, TransitionType.FADE_IN_UP, null, paneTable);
//            transitionController.executeTransition(null, TransitionType.FADE_IN_RIGHT, null, lblIssuer);
        });
    }
}
