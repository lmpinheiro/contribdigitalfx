package br.com.cd.control;

import java.net.URL;
import java.sql.Date;
import java.text.MessageFormat;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.util.Callback;

import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.control.TransitionController.TransitionType;
import br.com.cd.enumeration.FiscalDocumentEnum.IssueTypeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.SituationTypeEnum;
import br.com.cd.model.FiscalDocumentModel;
import br.com.cd.model.IssuerModel;
import br.com.cd.model.SearchDocumentModel;
import br.com.cd.util.MaskFieldUtil;

/**
 * FXML Controller for Search Documents
 *
 * @author lmpinheiro
 */
@Controller
public class SearchDocumentController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(SearchDocumentController.class);
    
//	IssuerModel issuerUpdate;
	private ObservableList<FiscalDocumentModel> listFiscalDocumentObservable;
	private SearchDocumentModel searchDocumentModel;
	
	@Autowired
    private ScreenConfig screenHelper;
	
	@Autowired
    private TransitionController transitionController;
	
	@Autowired
    private MenuController menuController;
	
	/**
	 * Header
	 */
	@FXML
    private Text lblSearchFiscalDocument;
	@FXML
    private ImageView imgLoad;
	@FXML
    private ProgressBar bar;
	
	/**
	 * Table Pane
	 */
	@FXML
    private AnchorPane paneMain;
	@FXML
    private AnchorPane paneTable;
	@FXML
    private HBox hbActionButtons;
	@FXML
    private Button btnAdd;
	@FXML
    private Button btnEdit;
	@FXML
    private Button btnDelete;
	
	@FXML
    private TableView<FiscalDocumentModel> tableFiscalDocument;
	@FXML
    private TableColumn<IssuerModel, String> colSerie;
	@FXML
    private TableColumn<IssuerModel, String> colNumber;
    @FXML
    private TableColumn <IssuerModel, String>colIssueDate;
    @FXML
    private TableColumn<IssuerModel, String> colCnpj;
    @FXML
    private TableColumn<IssuerModel, String> colUF;
    @FXML
    private TableColumn<IssuerModel, String> colType;
    @FXML
    private TableColumn<IssuerModel, String> colSituation;
    
    /**
	 * Search Forms
	 */
    @FXML
    private Label lblInitialIssue;
    @FXML
    private DatePicker dtInitialIssue;
    @FXML
    private Label lblFinalIssue;
    @FXML
    private DatePicker dtFinalIssue;
    @FXML
    private Label lblCnpj;
    @FXML
    private TextField txtCnpj;
    @FXML
    private Label lblCompanyName;
    @FXML
    private TextField txtCompanyName;
    @FXML
    private Label lblNumber;
    @FXML
    private TextField txtNumber;
    @FXML
    private Label lblSerie;
    @FXML
    private TextField txtSerie;
    @FXML
    private Label lblSituation;
    @FXML
    private ComboBox<SituationTypeEnum> cmbSituation;
    @FXML
    private Label lblTypeDocument;
    @FXML
    private ComboBox<IssueTypeEnum> cmbTypeDocument;
    @FXML
    private Button btnSearch;
    @FXML
    private Button btnClean;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Platform.runLater(() -> {
        	logger.info("Initializing SearchDocumentController");
        	
        	//Initialize Fiscal Documents observable List
            listFiscalDocumentObservable = FXCollections.observableArrayList();
            
            buttonsDisable();
            setModelColumn();
            setTooltips();
            setMaskField();
            populateCombos();
            installListeners();
            
            loadComponents();
        });
    } 
    
    
    /**
     * Action to search fiscal documents
     */
    @FXML
    private void actSearch(ActionEvent e){
    	logger.info("Calling actSearch");
    	refreshDataTable();
    }
    
    
    /**
     * Action clean search fields
     */
    @FXML
    private void actClean(ActionEvent e){
    	logger.info("Calling actClean");
    	clear();
    	refreshDataTable();
    }
    
    
    /**
     * Action to show the Issuer's Form
     */
    @FXML
    private void actAdd(ActionEvent e){
    	logger.info("Calling actAdd");
    	
    	menuController.updateRightFrame("fiscal-document.fxml");
    }
    
    
    /**
     * Action to show the Issuer's Form to edit 
     */
    @FXML
    private void actEdit(ActionEvent e){
    	logger.info("Calling actEdit");
    	
    	clear();
    	
//    	issuerUpdate = tableIssuer.getSelectionModel().getSelectedItem();
//    	
//        txtCnpj.setText(issuerUpdate.getCnpj());
//        txtIe.setText(issuerUpdate.getIe());
//        txtCompanyName.setText(issuerUpdate.getCompanyName());
//        txtTradeName.setText(issuerUpdate.getTradeName());
//        txtIm.setText(issuerUpdate.getIm());
//        txtCnae.setText(issuerUpdate.getCnae());
//        txtIeSt.setText(issuerUpdate.getIeSt());
//        cmbRegimeTrib.getSelectionModel().select(issuerUpdate.getRegimeTrib());
//        cmbUF.getSelectionModel().select(issuerUpdate.getUf());
//        txtZipCode.setText(issuerUpdate.getZipCode());
//        txtStreet.setText(issuerUpdate.getStreet());
//        txtNumber.setText(issuerUpdate.getNumber());
//        txtComplement.setText(issuerUpdate.getComplement());
//        txtDistrict.setText(issuerUpdate.getDistrict());
//        txtEmail.setText(issuerUpdate.getEmail());
//        txtPhoneNumber.setText(issuerUpdate.getPhone());
//        if(issuerUpdate.getLogo()!=null)
//        	imgLogo.setImage(ImageUtil.getImageFromBytes(issuerUpdate.getLogo()));
//        
//        populateCountyCombo(issuerUpdate.getUf());
//        cmbCounty.getSelectionModel().select(issuerUpdate.getCountyModel());;
//        
//        btnSave.setVisible(false);
//        btnUpdate.setVisible(true);
//        
//        transitionController.executeTransition(TransitionType.FADE_IN_UP, paneTable, paneIssuer);
    }
    
    
    /**
     * Delete an Issuer
     */
    @FXML
    private void actDelete(ActionEvent e){
    	logger.info("Calling actDelete");
    	
    	message.clear();
    	error.clear();
    	
    	List<FiscalDocumentModel> issuersSelected = tableFiscalDocument.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, issuersSelected.size()+" Emitente(s)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
        	try{
//        		getIssuerService().deleteIssuers(issuersSelected);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_DELETED, issuersSelected.size()+" Emitente(s)"));
	        	screenHelper.dialogDefault("Excusão de Emitente", message, DialogType.INFORMATION);
	        	// Remove Issuer Logged
        		IssuerModel issuerSession = getIssuerService().getIssuerSession();
        		if(issuerSession!=null){
        			issuersSelected.forEach(issuerSelected -> {
        				if(issuerSelected.getId()==issuerSession.getId()){
        					menuController.removeIssuerLogged();
        				}
        			});
        		}
	        	refreshDataTable();
        	}catch(Exception ex){
        		logger.error(ex);
        		addError(ex.getMessage());
        		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        	}
        }
    }
    
    
    /**
     * Populate Search Object 
     */
    private void populateSearch(){
    	logger.info("Calling populateSearch");
    	
    	if(searchDocumentModel==null)
    		searchDocumentModel = new SearchDocumentModel();
    	
    	searchDocumentModel.setCnpj(MaskFieldUtil.removeMaskNumeral(txtCnpj.getText()));
    	searchDocumentModel.setCompanyName(txtCompanyName.getText());
    	if(dtInitialIssue.getValue()!=null)
    		searchDocumentModel.setDtInitialIssue(new LocalDateTime(Date.valueOf(dtInitialIssue.getValue())));
    	else
    		searchDocumentModel.setDtInitialIssue(null);
    	if(dtFinalIssue.getValue()!=null)
    		searchDocumentModel.setDtFinalIssue(new LocalDateTime(Date.valueOf(dtFinalIssue.getValue().plusDays(1))));
    	else
    		searchDocumentModel.setDtFinalIssue(null);
    	searchDocumentModel.setIssue(cmbTypeDocument.getValue());
    	searchDocumentModel.setNumber(txtNumber.getText());
    	searchDocumentModel.setSerie(txtSerie.getText());
    	searchDocumentModel.setSituation(cmbSituation.getValue());
    }
    
 
    /**
     * Initialize Table Screen with buttons disabled 
     */
    private void buttonsDisable(){
    	logger.info("Calling buttonsDisable");
    	
    	btnAdd.setDisable(false);
    	btnEdit.setDisable(true);
    	btnDelete.setDisable(true);
    }
    
    
    /**
     * Listeners Implementation
     */
    private void installListeners(){
    	logger.info("Calling installListeners");
    	
    	/**
         * Listeners for dynamic field validation  
         */
    	final Callback<DatePicker, DateCell> dayCellFactoryIn = p -> {
    		return new DateCell() {
    			@Override
    			public void updateItem(java.time.LocalDate item, boolean empty) {
    				super.updateItem(item, empty);
    				if (dtFinalIssue.getValue()!=null && dtInitialIssue.getValue()!=null && dtInitialIssue.getValue().isAfter(dtFinalIssue.getValue())) {
    					dtFinalIssue.getEditor().clear();    				}
    				if (item.isAfter(java.time.LocalDate.now())) {
    					setDisable(true);
    					setStyle("-fx-background-color: #F5F5F5;");
    				}
    			}
            };
    	};
    	
    	
    	final Callback<DatePicker, DateCell> dayCellFactoryOut = q -> {
    		return new DateCell() {
    			@Override
    			public void updateItem(java.time.LocalDate item, boolean empty) {
    				super.updateItem(item, empty);
    				if (dtInitialIssue.getValue()!= null && item.isBefore(dtInitialIssue.getValue().plusDays(1))) {
    					setDisable(true);
    					setStyle("-fx-background-color: #F9ECEE;");
    				}
    				if (item.isAfter(java.time.LocalDate.now())) {
    					setDisable(true);
    					setStyle("-fx-background-color: #F5F5F5;");
    				}
    				if(item!=null && dtInitialIssue.getValue()!=null){
    					long p = ChronoUnit.DAYS.between(dtInitialIssue.getValue(), item);
    					setTooltip(new Tooltip("Você está selecionando o período de " + p + " dias"));
    				}
    			}
            };
    	};
    	
    	dtInitialIssue.setDayCellFactory(dayCellFactoryIn);  
        dtFinalIssue.setDayCellFactory(dayCellFactoryOut);  
        
    	
    	/**
         * Listener for dynamic button exhibition  
         */
    	tableFiscalDocument.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
    	    if (tableFiscalDocument.getSelectionModel().getSelectedItems().size()==0) {
    	        btnAdd.setDisable(false);
    	        btnEdit.setDisable(true);
    	        btnDelete.setDisable(true);
    	    }else if (tableFiscalDocument.getSelectionModel().getSelectedItems().size()==1){
    	        btnAdd.setDisable(false);
    	        btnEdit.setDisable(false);
    	        btnDelete.setDisable(false);
    	    }else if (tableFiscalDocument.getSelectionModel().getSelectedItems().size()>1){
    	        btnAdd.setDisable(false);
    	        btnEdit.setDisable(true);
    	        btnDelete.setDisable(false);
    	    }
    	});
    }
    
    
    /**
     * Populate combos on initialization   
     */
    private void populateCombos(){
    	logger.info("Calling populateCombos");
    	
    	cmbSituation.getItems().addAll(SituationTypeEnum.values());
    	cmbTypeDocument.getItems().addAll(IssueTypeEnum.values());
    }
    
    
    /**
     * Implements a tooltip alert on required fields  
     */
    private void setTooltips(){
    	logger.info("Calling setRequiredFields");
    	
    	/**
         * Tooltip subtitle buttons  
         */
    	Tooltip tAdd = new Tooltip("Criar Nova NF-e");
    	Tooltip tEdit = new Tooltip("Editar NF-e");
    	Tooltip tDelete = new Tooltip("Excluir NF-e");
    	hackTooltipStartTiming(tAdd);
    	hackTooltipStartTiming(tEdit);
    	hackTooltipStartTiming(tDelete);
    	btnAdd.setTooltip(tAdd);
    	btnEdit.setTooltip(tEdit);
    	btnDelete.setTooltip(tDelete);
    }
    
    
    /**
     * Define Field's Mask  
     */
    private void setMaskField(){
    	logger.info("Calling setFieldMask");
    	
    	MaskFieldUtil.cnpjField(txtCnpj);
    	MaskFieldUtil.maxTextField(txtCompanyName, 60);
    	MaskFieldUtil.maxTextField(txtNumber, 9);
    	MaskFieldUtil.numericField(txtNumber);
    	MaskFieldUtil.maxTextField(txtSerie, 3);
    	MaskFieldUtil.numericField(txtSerie);
    }
    
    
    /**
     * Mapping columns table with issuer object
     */
    private void setModelColumn(){
    	logger.info("Calling setModelColumn");
    	
    	tableFiscalDocument.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	screenHelper.setModelColumn(colSerie, "serie", true);
    	screenHelper.setModelColumn(colNumber, "number", true);
    	screenHelper.setModelColumn(colIssueDate, "dtIssueFormatted", true);
    	screenHelper.setModelColumn(colCnpj, "cnpjFormatted", true);
    	screenHelper.setModelColumn(colUF, "ufDest", true);
        screenHelper.setModelColumn(colType, "issueType", true);
        screenHelper.setModelColumn(colSituation, "situation", true);
    }
    
    
    /**
     * Clear Form
     */
    private void clear(){
    	logger.info("Calling clear");
    	
//    	issuerUpdate = null;
    	
    	dtInitialIssue.getEditor().clear();
    	dtFinalIssue.getEditor().clear();
    	txtCnpj.clear();
    	txtCompanyName.clear();
    	txtNumber.clear();
    	txtSerie.clear();
    	cmbSituation.getSelectionModel().clearSelection();
    	cmbTypeDocument.getSelectionModel().clearSelection();
    }
    
    
    /**
     * Refresh Data Table
     */
    private void refreshDataTable(){
    	logger.info("Calling refreshDataTable");
    	
		populateSearch();
    	
        if(listFiscalDocumentObservable == null){
            listFiscalDocumentObservable =  FXCollections.observableList(getFiscalDocumentService().findBySearchDocument(searchDocumentModel));
        }else {
            listFiscalDocumentObservable.clear();
            listFiscalDocumentObservable.addAll(FXCollections.observableList(getFiscalDocumentService().findBySearchDocument(searchDocumentModel)));
        }
        tableFiscalDocument.setItems(listFiscalDocumentObservable);
    }
    
    
    /**
     * Load data stream bar
     */
    private void loadComponents(){
    	logger.info("Calling selectWithService");
    	
        Service<Integer> service = new Service<Integer>() {
            @Override
            protected Task<Integer> createTask() {
                refreshDataTable();
                return new Task<Integer>() {           
                    @Override
                    protected Integer call() throws Exception {
                        Integer max = getIssuerService().findAllIssuers().size();
                        if (max > 35) {
                            max = 30;
                        }
                        updateProgress(0, max);
                        for (int k = 0; k < max; k++) {
                            Thread.sleep(40);
                            updateProgress(k+1, max);
                        }
                        return max;
                    }
                };
            }
        };
        service.start();
        bar.progressProperty().bind(service.progressProperty());
        service.setOnRunning((WorkerStateEvent event) -> {
            imgLoad.setVisible(true);
        });
        
        service.setOnSucceeded((WorkerStateEvent event) -> {
            imgLoad.setVisible(false);
            bar.setVisible(false);
            
            transitionController.executeTransition(null, TransitionType.FADE_IN_RIGHT, null, lblSearchFiscalDocument);
            transitionController.executeTransition(null, TransitionType.FADE_IN_UP, null, paneMain);
        });
    }
}
