package br.com.cd.control;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Controller;

import br.com.cd.config.AppConfig;
import br.com.cd.config.MailConfig;
import br.com.cd.service.FiscalDocumentService;
import br.com.cd.service.IssuerService;
import br.com.cd.service.StateService;
import br.com.cd.service.UserService;

/**
 * (FXML) Service Injection Controller
 *
 * @author lmpinheiro
 */
@Controller
public class InjectionController {
	
	private static final Logger logger = Logger.getLogger(InjectionController.class);
	private static AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
	
	public static AbstractApplicationContext getContext() {
		return context;
	}

	
	/***************************************************
    *                 Service Injection                *
    ****************************************************/
	
	/* *************************
     *      User Service       *
    **************************/
	public static UserService getUserService(){
		logger.info("Injecting User Service");
		
    	return (UserService) context.getBean("userService");
    }
	
	/* *************************
     *      Mail Service       *
    **************************/
	public static MailConfig getMailService(){
		logger.info("Injecting Mail Service");
		
    	return (MailConfig) context.getBean("mailService");
    }
	
	/* *************************
     *      State Service      *
    **************************/
	public static StateService getStateService(){
		logger.info("Injecting State Service");
		
    	return (StateService) context.getBean("stateService");
    }
	
	/* *************************
     *      Issuer Service      *
    **************************/
	public static IssuerService getIssuerService(){
		logger.info("Injecting Issuer Service");
		
    	return (IssuerService) context.getBean("issuerService");
    }
	
	/* *************************
     * Fiscal Document Service *
    **************************/
	public static FiscalDocumentService getFiscalDocumentService(){
		logger.info("Injecting Fiscal Document Service");
		
    	return (FiscalDocumentService) context.getBean("fiscalDocumentService");
    }
	
}
