package br.com.cd.control;

import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.hibernate.internal.util.StringHelper;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.model.UserAuthorityModel;
import br.com.cd.model.UserModel;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.SecurityUtil;

/**
 * FXML Controller for register a new user
 *
 * @author lmpinheiro
 */
@Controller
public class RegisterController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(RegisterController.class);
	
	private Stage stage;
	
	@Autowired
    ScreenConfig screenHelper;
	
	@FXML
    private Text lblTitle;
	@FXML
    private Text lblForm;
	@FXML
    private TextField txtName;
	@FXML
    private TextField txtSurname;
	@FXML
    private TextField txtEmail;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private TextField txtQuestion;
    @FXML
    private TextField txtAnswer;
    @FXML
    private Text lblName;
    @FXML
    private Text lblSurname;
    @FXML
    private Text lblEmail;
    @FXML
    private Text lblPassword;
    @FXML
    private Text lblQuestion;
    @FXML
    private Text lblAnswer;
    @FXML
    private Button btnRegister;
    @FXML
    private Button btnCancel;
    @FXML
    private Text lblCopyright;
    @FXML 
    private Label lblClose; 
    @FXML 
    private Label lblHelp; 
    
    
    /**
     * Initializes transaction effects.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing RegisterController");
    	
        Platform.runLater(() -> {
        	new FadeInLeftTransition(lblTitle).play();
        	new FadeInLeftTransition(lblForm).play();
        	new FadeInLeftTransition(lblName).play();
        	new FadeInLeftTransition(lblSurname).play();
        	new FadeInLeftTransition(lblEmail).play();
        	new FadeInLeftTransition(lblPassword).play();
        	new FadeInLeftTransition(lblQuestion).play();
        	new FadeInLeftTransition(lblAnswer).play();
            
            new FadeInLeftTransition(txtName).play();
            new FadeInLeftTransition(txtSurname).play();
            new FadeInLeftTransition(txtEmail).play();
            new FadeInLeftTransition(txtPassword).play();
            new FadeInLeftTransition(txtQuestion).play();
            new FadeInLeftTransition(txtAnswer).play();
            new FadeInRightTransition(btnRegister).play();
            new FadeInRightTransition(btnCancel).play();
            new FadeInRightTransition(lblHelp).play();
            
            lblClose.setOnMouseClicked((MouseEvent event) -> {
                Platform.exit();
                System.exit(0);
            });
            
            Tooltip t = new Tooltip(MessageConstant.PASSWORD_PATTERN);
            hackTooltipStartTiming(t);
            txtPassword.setTooltip(t);
            lblHelp.setTooltip(t);
        });
    }    

    /**
     * Cancel operation and return to Login Screen
     */
    @FXML
    private void actCancel(ActionEvent event) {
    	logger.info("Calling Login Screen - Cancel");
    	
    	screenHelper.newStage(stage, lblClose, "login.fxml", "Contribuinte Digital - Login", true, StageStyle.TRANSPARENT, null, false, this, null, true);
    }
    
    
    /**
     * Add a new user
     */
    @FXML
    private void actRegister(ActionEvent event) throws IOException {
    	logger.info("Calling Register ACtion");
    	
    	error.clear();
    	
    	if(StringHelper.isEmpty(txtName.getText())){
    		addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Nome"));
    	}
		if(StringHelper.isEmpty(txtSurname.getText())){
			addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Sobrenome"));
    	}
		if(StringHelper.isEmpty(txtEmail.getText())){
			addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "E-mail"));
		}else if(!DataValidateUtil.isValidEmail(txtEmail.getText())){
			addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "E-mail"));
		}
		if(StringHelper.isEmpty(txtPassword.getText())){
			addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Senha"));
		}else if(!DataValidateUtil.isValidPassword(txtPassword.getText())){
			addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Senha"));
		}
		if(StringHelper.isEmpty(txtQuestion.getText())){
			addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Pergunta Secreta"));
    	}
		if(StringHelper.isEmpty(txtAnswer.getText())){
			addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Resposta Secreta"));
    	}
					
		if(hasError()){
			screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
		}else{
			UserModel user = new UserModel();
			user.setName(txtName.getText());
	        user.setSurname(txtSurname.getText());
	        user.setDtRegister(new LocalDate());
	        user.setPass(SecurityUtil.generateHash(txtPassword.getText()));
	        user.setEmail(txtEmail.getText());
	        user.setQuestion(txtQuestion.getText());
	        user.setAnswer(txtAnswer.getText());
	        user.setIsEnable(1);
	        
	        UserAuthorityModel authority = new UserAuthorityModel();
	        authority.setAuthority("ROLE_USER");
	        authority.setUserModel(user);
	        
	        user.getUserAuthorityModel().add(authority);
	        
	        try{
	        	getUserService().saveUser(user);
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_INSERTED, "Usuário"));
	        	screenHelper.dialogDefault("Cadastro de Usuário", message, DialogType.INFORMATION);
	        	screenHelper.newStage(stage, lblClose, "login.fxml", "Contribuinte Digital - Login", true, StageStyle.TRANSPARENT, null, false, this, null, true);
	        }catch(Exception e){
	        	addError(e.getMessage());
	        	screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
	        }
		}
    }
}