package br.com.cd.control;

import java.net.URL;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.hibernate.annotations.common.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.animations.FadeInTransition;
import br.com.cd.animations.FadeOutLeftTransition;
import br.com.cd.animations.FadeOutTransition;
import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.control.TransitionController.TransitionType;
import br.com.cd.enumeration.FiscalDocumentEnum.ProcessOriginEnum;
import br.com.cd.model.IssuerModel;
import br.com.cd.nfe.TNFe.InfNFe.Cobr.Dup;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefECF;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNF;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNFP;
import br.com.cd.nfe.TNFe.InfNFe.InfAdic.ObsCont;
import br.com.cd.nfe.TNFe.InfNFe.InfAdic.ObsFisco;
import br.com.cd.nfe.TNFe.InfNFe.InfAdic.ProcRef;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.FormatterTextUtil;
import br.com.cd.util.MaskFieldUtil;
import br.com.cd.util.SecurityUtil;

/**
 * FXML Controller class for Menu
 *
 * @author lmpinheiro
 */
@Controller
public class ReferencedProcessController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(ReferencedProcessController.class);
	
	Stage stage;
	boolean exists = false;
	boolean editAction = false;
    
    @Autowired
    ScreenConfig screenHelper;
    
    @Autowired
    FiscalDocumentController fiscalDocumentController;
    
    @Autowired
    TransitionController transitionController;
    
    @FXML
    private AnchorPane pnlMain;
	@FXML
    private Button btnClose;
	@FXML
    private Label lblTitle;
	
	@FXML
    private ComboBox<ProcessOriginEnum> cmbProcessOrigin;
	@FXML
    private TextField txtProcessId;
	
	@FXML
    private Button btnAdd;
	@FXML
    private Button btnUpdate;
	@FXML
    private Button btnCancel;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing MenuController");
    	
    	Platform.runLater(() -> {
            stage = (Stage) btnClose.getScene().getWindow();
            
            setMaskField();
            installListeners();
            populateCombos();
            if(getData()!=null){
            	editAction = true;
            	fillForm((ProcRef)getData());
            	btnAdd.setVisible(false);
            	btnUpdate.setVisible(true);
            }
            
            transitionController.executeTransition(null, TransitionType.BOUNCE_IN, null, pnlMain);
        });
    }    
   

    /**
     * Add Object on table
     */
    @FXML
    private void actSave(ActionEvent event) {
    	logger.info("Calling actAdd");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	ProcRef referencedProcess = populateObject(null);
        	
        	exists = false;
        	fiscalDocumentController.listProcessesReferenced.forEach(item -> {
        		if(referencedProcess!=null && item!=null && 
        				!StringHelper.isEmpty(referencedProcess.getIndProc()) && referencedProcess.getIndProc().equals(item.getIndProc()) &&
        				!StringHelper.isEmpty(referencedProcess.getNProc()) && referencedProcess.getNProc().equals(item.getNProc())){
        			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Processo Referenciado"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
	        	fiscalDocumentController.listProcessesReferenced.add(referencedProcess);
	        	fiscalDocumentController.tableProcessesReferenced.setItems(fiscalDocumentController.listProcessesReferenced);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_INSERTED, "Processo Referenciado"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    
    
    /**
     * Update object on table
     */
    @FXML
    private void actUpdate(ActionEvent event) {
    	logger.info("Calling actUpdate");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	ProcRef referencedProcessOld = (ProcRef)getData();
        	ObservableList<ProcRef> listReferencedProcess = FXCollections.observableArrayList(fiscalDocumentController.listProcessesReferenced);
        	listReferencedProcess.remove(referencedProcessOld);
        	
        	ProcRef referencedProcessCurrent = populateObject(null);
        	
        	exists = false;
        	listReferencedProcess.forEach(item -> {
        		if(referencedProcessCurrent!=null && item!=null && 
        				!StringHelper.isEmpty(referencedProcessCurrent.getIndProc()) && referencedProcessCurrent.getIndProc().equals(item.getIndProc()) &&
        				!StringHelper.isEmpty(referencedProcessCurrent.getNProc()) && referencedProcessCurrent.getNProc().equals(item.getNProc())){
        			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Processo Referenciado"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
        		
        		ProcRef referencedProcess = populateObject((ProcRef)getData());
        		
        		fiscalDocumentController.listProcessesReferenced.set(fiscalDocumentController.tableProcessesReferenced.getSelectionModel().getSelectedIndex(), referencedProcess);
	        	fiscalDocumentController.tableProcessesReferenced.setItems(fiscalDocumentController.listProcessesReferenced);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_UPDATED, "Processo Referenciado"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    

    /**
     * Close Pane
     */
    @FXML
    private void actClose(ActionEvent event) {
    	logger.info("Calling actClose");
    	
    	message.clear();
    	
    	addMessage(MessageConstant.EXIT_CONFIRMATION);
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
        	stage.close();
        }
    }


    /**
     * Listeners Implementation
     */
    private void installListeners(){
    	txtProcessId.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtProcessId, 1, 60, false));
    }
    
    
    /**
     * Populate object form  
     */
    private ProcRef populateObject(ProcRef referencedProcess){
    	logger.info("Calling populateObject");
    	
    	if(referencedProcess==null)referencedProcess = new ProcRef();
    	
    	referencedProcess.setNProc(txtProcessId.getText());
    	referencedProcess.setIndProc(cmbProcessOrigin.getSelectionModel().getSelectedItem().toString());
    	
    	return referencedProcess;
    }
    
    
    /**
     * Fill Form  
     */
    private void fillForm(ProcRef referencedProcess){
    	logger.info("Calling fillForm");
    	
    	txtProcessId.setText(referencedProcess.getNProc());
    	cmbProcessOrigin.getSelectionModel().select(ProcessOriginEnum.getEnumByDescription(referencedProcess.getIndProc()));
    }
    
    /**
     * Populate combos on initialization   
     */
    private void populateCombos(){
    	logger.info("Calling populateCombos");
    	
    	cmbProcessOrigin.getItems().addAll(ProcessOriginEnum.values());
    }
    
    
    /**
     * Define Field's Mask  
     */
    private void setMaskField(){
    	logger.info("Calling setFieldMask");
    	
    	MaskFieldUtil.maxTextField(txtProcessId, 60);
    }
    
    /**
     * Fields Validation 
     */
    private void validateFields(){
    	logger.info("Calling validateFields");
    	
    	error.clear();
    	
    	
		/**
         * Validate if is a required field 
         */
    	if (cmbProcessOrigin.getSelectionModel().getSelectedIndex()<0) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Origem do Processo"));
        }
    	if (txtProcessId.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Identificador do Processo"));
        }
		
		
		/**
         * Validate if is a valid content 
         */
		if (cmbProcessOrigin.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Origem do Processo"));
        }
		if (txtProcessId.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Identificador do Processo"));
        }
    }
}
