package br.com.cd.control;


import java.net.URL;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.hibernate.internal.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.model.UserModel;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.SecurityUtil;

/**
 * FXML Controller for Login screen
 *
 * @author lmpinheiro
 */
@Controller
public class LoginController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(LoginController.class);
	
	private Stage stage;
	
	@Autowired
    ScreenConfig screenHelper;
	
	@FXML
    private TextField txtUsername;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private Text lblWelcome;
    @FXML
    private Text lblUserLogin;
    @FXML
    private Text lblUsername;
    @FXML
    private Text lblPassword;
    @FXML
    private Button btnLogin;
    @FXML
    private Button btnRegister;
    @FXML
    private Text lblCopyright;
    @FXML 
    private Label lblClose; 
    @FXML 
    private Label lblLostPass; 
    
    /**
     * Initializes transaction effects.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing LoginController");
    	
        Platform.runLater(() -> {
        	
            new FadeInRightTransition(lblUserLogin).play();
            new FadeInLeftTransition(lblWelcome).play();
            new FadeInLeftTransition(lblPassword).play();
            new FadeInLeftTransition(lblUsername).play();
            new FadeInLeftTransition(txtUsername).play();
            new FadeInLeftTransition(txtPassword).play();
            new FadeInRightTransition(btnLogin).play();
            new FadeInRightTransition(btnRegister).play();
            new FadeInRightTransition(lblLostPass).play();
            lblClose.setOnMouseClicked((MouseEvent event) -> {
                Platform.exit();
                System.exit(0);
            });
        });
    }    

    
    /**
     * User Login
     */
    @FXML
    private void actLogin(ActionEvent event) {
    	logger.info("Calling Login Action");
    	
    	error.clear();
    	
    	if(StringHelper.isEmpty(txtUsername.getText())){
			addError(MessageFormat.format(MessageConstant.REQUIRED_FIELD, "E-mail"));
		}else if(!DataValidateUtil.isValidEmail(txtUsername.getText())){
			addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "E-mail"));
		}
    	if(StringHelper.isEmpty(txtPassword.getText())){
    		addError(MessageFormat.format(MessageConstant.REQUIRED_FIELD, "Senha"));
    	}
    	
    	if(hasError()){
			screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
		}else{
	    	UserModel user = getUserService().findByEmail(txtUsername.getText());
	    	if(user!=null){
	    		if(SecurityUtil.isPasswordValid(txtPassword.getText(), user.getPass())){
			    	SecurityUtil.userAuthenticate(user);
			    	screenHelper.newStage(stage, lblClose, "form-menu.fxml", "Test App", true, StageStyle.TRANSPARENT, null, false, this, null, true);
	    		}else{
	    			addError(MessageConstant.USER_DO_NOT_NATCH);
	    		}
	    	}else{
	    		addError(MessageConstant.USER_DO_NOT_NATCH);
	    	}
	    	if(hasError()){
				screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
			}
		}
    }

    
    /**
     * User Registration
     */
    @FXML
    private void actRegister(ActionEvent event) {
    	logger.info("Calling Register Screen");
    	
        screenHelper.newStage(stage, lblClose, "register.fxml", "Contribuinte Digital - Registre-se", true, StageStyle.TRANSPARENT, null, false, this, null, true);
    }
    
    /**
     * Execute Login when the Enter key is pressed
     */
    @FXML
    private void onEnterPassField(ActionEvent event) {
    	logger.info("Calling onEnterPassField");
    	
    	actLogin(event);
    }
    
    
    /**
     * Password Recover
     */
    @FXML
    private void actLostPassword(MouseEvent event) {
    	logger.info("Calling LostPassword Screen");
    	
        screenHelper.newStage(stage, lblClose, "recover-password.fxml", "Contribuinte Digital - Recuperar Senha", true, StageStyle.TRANSPARENT, null, false, this, null, true);
    }
    
}
