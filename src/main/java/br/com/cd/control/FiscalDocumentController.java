package br.com.cd.control;

import java.net.URL;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.text.Normalizer.Form;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;

import org.apache.log4j.Logger;
import org.hibernate.annotations.common.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.control.TransitionController.TransitionType;
import br.com.cd.enumeration.FiscalDocumentEnum.AttendanceTypeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.CfopEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.DocumentTypeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.IssueTypeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.PaymentMethodsEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.PurposeIssueEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.ShippingModeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.SpecialTaxationEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.TargetOperationEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.TaxpayerTypeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.VehicleTypeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.VersionEnum;
import br.com.cd.enumeration.IssuerEnum.RegimeTributarioEnum;
import br.com.cd.model.CountyModel;
import br.com.cd.model.FiscalDocumentModel;
import br.com.cd.model.IssuerModel;
import br.com.cd.nfe.TNFe;
import br.com.cd.nfe.TNFe.InfNFe;
import br.com.cd.nfe.TNFe.InfNFe.AutXML;
import br.com.cd.nfe.TNFe.InfNFe.Cana.Deduc;
import br.com.cd.nfe.TNFe.InfNFe.Cana.ForDia;
import br.com.cd.nfe.TNFe.InfNFe.Cobr.Dup;
import br.com.cd.nfe.TNFe.InfNFe.Det.Prod;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefECF;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNFP;
import br.com.cd.nfe.TNFe.InfNFe.InfAdic.ObsCont;
import br.com.cd.nfe.TNFe.InfNFe.InfAdic.ObsFisco;
import br.com.cd.nfe.TNFe.InfNFe.InfAdic.ProcRef;
import br.com.cd.nfe.TNFe.InfNFe.Transp.Vol;
import br.com.cd.nfe.TVeiculo;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.FormatterTextUtil;
import br.com.cd.util.MaskFieldUtil;

/**
 * FXML Controller for Issuer'd Management
 *
 * @author lmpinheiro
 */
@Controller
public class FiscalDocumentController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(FiscalDocumentController.class);
    
	IssuerModel issuerUpdate;
	Stage stage;
	
	@Autowired
    private ScreenConfig screenHelper;
	
	@Autowired
    private TransitionController transitionController;
	
	@Autowired
	MenuController menuController;
	
	/**
	 * Header
	 */
	@FXML
    private Label lblFiscalDocument;
	@FXML
    private Pane pnlFiscalDocumentInformation;
	@FXML
    private Text lblAccessKeyHeader;
	@FXML
    private Text lblSerieHeader;
	@FXML
    private Text lblNumberHeader;
	@FXML
    private Text lblSituationHeader;
	@FXML
    private ImageView imgLoad;
	@FXML
    private ProgressBar bar;
	
	/**
	 * Menu
	 */
	@FXML
    private Button btnSave;
	@FXML
    private Button btnDelete;
	@FXML
    private Button btnCancel;
	
	
	/**
	 * Tab Pane
	 */
	@FXML
    private TabPane tabPaneMain;
	@FXML
    private Tab tabFiscalDocument;
	@FXML
    private Tab tabIssuer;
	@FXML
    private Tab tabReceiver;
	@FXML
    private Tab tabProducts;
	@FXML
    private Tab tabTotals;
	@FXML
    private Tab tabTransport;
	@FXML
    private Tab tabBilling;
	@FXML
    private Tab tabAditionalInformation;
	@FXML
    private Tab tabExport;
	@FXML
    private Tab tabPurchases;
	@FXML
    private Tab tabHarvest;
	@FXML
    private Tab tabDownloadAuthorization;
	
	
	/**
	 * NF-e Tab
	 */
	@FXML
    private Accordion accordionFiscalDocument;
		@FXML
	    private TitledPane tpFiscalDocumet1;
		@FXML
	    private TextField txtModel;
		@FXML
	    private TextField txtSerie;
		@FXML
	    private TextField txtNumber;
		@FXML
	    private TextField txtNumericCode;
		@FXML
	    private TextField txtDigitKey;
		@FXML
	    private TextField dtIssue;
		@FXML
	    private TextField dtInputOutput;
		@FXML
	    private ComboBox<DocumentTypeEnum> cmbDocumentType;
		@FXML
	    private ComboBox<PaymentMethodsEnum> cmbPaymentMethods;
		@FXML
	    private ComboBox<IssueTypeEnum> cmbIssueType;
		@FXML
	    private ComboBox<PurposeIssueEnum> cmbPurposeIssue;
		@FXML
	    private ComboBox<AttendanceTypeEnum> cmbAttendanceType;
		@FXML
	    private ComboBox<TargetOperationEnum> cmbTargetOperation;
		@FXML
	    private TextField txtOperationOrigins;
		@FXML
	    private ToggleGroup tgFinalConsumer;
		@FXML
	    private RadioButton rdFinalConsumerYes;
		@FXML
	    private RadioButton rdFinalConsumerNo;
		@FXML
	    private ComboBox<String> cmbUfFiscalDocument;
	    @FXML
	    private ComboBox<CountyModel> cmbCountyFiscalDocument;		
	@FXML
    private TitledPane tpFiscalDocumet2;
		public ObservableList<NFref> listFdRefereced;
		@FXML
	    public TableView<NFref> tableFdRefereced;
		@FXML
		private Button btnAddFdRefereced;
		@FXML
		private Button btnEditFdRefereced;
		@FXML
		private Button btnDeleteFdRefereced;
		@FXML
	    private TableColumn<NFref, String> colTypeFdRefereced;
	    @FXML
	    private TableColumn <NFref, String>colAccessKeyFdRefereced;
	    @FXML
	    private TableColumn<NFref, String> colUfFdRefereced;
	    @FXML
	    private TableColumn<NFref, String> colCnpjFdRefereced;
	    @FXML
	    private TableColumn<NFref, String> colModelFdRefereced;
	    @FXML
	    private TableColumn<NFref, String> colSerieFdRefereced;
	    @FXML
	    private TableColumn<NFref, String> colNumberFdRefereced;
	@FXML
    private TitledPane tpFiscalDocumet3;
		public ObservableList<RefNFP> listFdProducerRefereced;
		@FXML
		private Button btnAddFdProducerRefereced;
		@FXML
		private Button btnEditFdProducerRefereced;
		@FXML
		private Button btnDeleteFdProducerRefereced;
		@FXML
		public TableView<RefNFP> tableFdProducerRefereced;
		@FXML
	    private TableColumn<RefNFP, String> colUfFdProducerRefereced;
		@FXML
	    private TableColumn<RefNFP, String> colCnpjFdProducerRefereced;
		@FXML
	    private TableColumn<RefNFP, String> colIeFdProducerRefereced;
	    @FXML
	    private TableColumn<RefNFP, String> colModelFdProducerRefereced;
	    @FXML
	    private TableColumn<RefNFP, String> colNumberFdProducerRefereced;
	    @FXML
	    private TableColumn<RefNFP, String> colSerieFdProducerRefereced;
	@FXML
    private TitledPane tpFiscalDocumet4;
		public ObservableList<RefECF> listFdFiscalReceipt;
		@FXML
		private Button btnAddFdFiscalReceipt;
		@FXML
		private Button btnEditFdFiscalReceipt;
		@FXML
		private Button btnDeleteFdFiscalReceipt;
		@FXML
		public TableView<RefECF> tableFdFiscalReceipt;
		@FXML
	    private TableColumn<RefECF, String> colEcfFdFiscalReceipt;
		@FXML
	    private TableColumn<RefECF, String> colCooFdFiscalReceipt;
		@FXML
	    private TableColumn<RefECF, String> colModelFdFiscalReceipt;
	
	
	/**
	 * Issuer Tab
	 */
	@FXML
    private TextField txtCnpjIssuer;
    @FXML
    private TextField txtIeIssuer;
    @FXML
    private TextField txtCompanyNameIssuer;
    @FXML
    private TextField txtTradeNameIssuer;
    @FXML
    private TextField txtImIssuer;
    @FXML
    private TextField txtCnaeIssuer;
    @FXML
    private TextField txtIeStIssuer;
    @FXML
    private ComboBox<RegimeTributarioEnum> cmbRegimeTribIssuer;
    @FXML
    private ComboBox<String> cmbUFIssuer;
    @FXML
    private ComboBox<CountyModel> cmbCountyIssuer;
    @FXML
    private TextField txtZipCodeIssuer;
    @FXML
    private TextField txtStreetIssuer;
    @FXML
    private TextField txtNumberIssuer;
    @FXML
    private TextField txtComplementIssuer;
    @FXML
    private TextField txtDistrictIssuer;
    @FXML
    private TextField txtEmailIssuer;
    @FXML
    private TextField txtPhoneNumberIssuer;	
		
	    
    /**
	 * Receiver Tab
	 */
    private TitledPane tpReceiver1;
	    @FXML
	    private TextField txtCnpjReceiver;
	    @FXML
	    private ComboBox<TaxpayerTypeEnum> cmbTaxpayerIssuer;
	    @FXML
	    private TextField txtIeReceiver;
	    @FXML
	    private TextField txtSuframaReceiver;
	    @FXML
	    private TextField txtCompanyNameReceiver;
	    @FXML
	    private TextField txtImReceiver;
	    @FXML
	    private ComboBox<String> cmbUFReceiver;
	    @FXML
	    private ComboBox<CountyModel> cmbCountyReceiver;
	    @FXML
	    private TextField txtZipCodeReceiver;
	    @FXML
	    private TextField txtPhoneNumberReceiver;	
	    @FXML
	    private TextField txtStreetReceiver;
	    @FXML
	    private TextField txtNumberReceiver;
	    @FXML
	    private TextField txtComplementReceiver;
	    @FXML
	    private TextField txtDistrictReceiver;
	    @FXML
	    private TextField txtEmailReceiver;
    private TitledPane tpReceiver2;
	    @FXML
	    private ToggleGroup tgPickupLocation;
		@FXML
	    private RadioButton rdPickupLocationYes;
		@FXML
	    private RadioButton rdPickupLocationNo;
		@FXML
	    private ComboBox<String> cmbUFPickupLocation;
	    @FXML
	    private ComboBox<CountyModel> cmbCountyPickupLocation;
	    @FXML
	    private TextField txtZipCodePickupLocation;
	    @FXML
	    private TextField txtPhoneNumberPickupLocation;	
	    @FXML
	    private TextField txtStreetPickupLocation;
	    @FXML
	    private TextField txtNumberPickupLocation;
	    @FXML
	    private TextField txtComplementPickupLocation;
	    @FXML
	    private TextField txtDistrictPickupLocation;
	    @FXML
	    private TextField txtCNPJPickupLocation;
    private TitledPane tpReceiver3;   
	    @FXML
	    private ToggleGroup tgDeliveryLocation;
		@FXML
	    private RadioButton rdDeliveryLocationYes;
		@FXML
	    private RadioButton rdDeliveryLocationNo;
		@FXML
	    private ComboBox<String> cmbUFDeliveryLocation;
	    @FXML
	    private ComboBox<CountyModel> cmbCountyDeliveryLocation;
	    @FXML
	    private TextField txtZipCodeDeliveryLocation;
	    @FXML
	    private TextField txtPhoneNumberDeliveryLocation;	
	    @FXML
	    private TextField txtStreetDeliveryLocation;
	    @FXML
	    private TextField txtNumberDeliveryLocation;
	    @FXML
	    private TextField txtComplementDeliveryLocation;
	    @FXML
	    private TextField txtDistrictDeliveryLocation;
	    @FXML
	    private TextField txtCNPJDeliveryLocation;
	    
	    
	    /**
		 * Products and Services Tab
		 */
	    private TitledPane tpProducts;  
		    private ObservableList<Prod> listProduct;
			@FXML
			private Button btnAddProduct;
			@FXML
			private Button btnEditProduct;
			@FXML
			private Button btnDeleteProduct;
			@FXML
		    private TableView<Prod> tableProduct;
			@FXML
		    private TableColumn<Prod, String> colItemProduct;
			@FXML
		    private TableColumn<Prod, String> colCodeProduct;
			@FXML
		    private TableColumn<Prod, String> colDescriptionProduct;
			@FXML
		    private TableColumn<Prod, String> colNcmProduct;
			@FXML
		    private TableColumn<Prod, String> colCfopProduct;
			@FXML
		    private TableColumn<Prod, String> colUnitProduct;
			@FXML
		    private TableColumn<Prod, String> colAmountProduct;
			@FXML
		    private TableColumn<Prod, String> colUnitValueProduct;
			@FXML
		    private TableColumn<Prod, String> colTotalValueProduct;
			@FXML
		    private TableColumn<Prod, String> colBcIcmsProduct;
			@FXML
		    private TableColumn<Prod, String> colIcmsValueProduct;
			@FXML
		    private TableColumn<Prod, String> colIcmsAliqProduct;
			@FXML
		    private TableColumn<Prod, String> colIpiValueProduct;
			@FXML
		    private TableColumn<Prod, String> colIpiAliqProduct;
			
	    
	/**
	 * Totals Tab
	 */
	private TitledPane tpTotals1; 
		@FXML
	    private TextField txtCalcBasisIcmsTotals;
		@FXML
	    private TextField txtIcmsTotals;
		@FXML
	    private TextField txtCalcBasisIcmsStTotals;
		@FXML
	    private TextField txtIcmsStTotals;
		@FXML
	    private TextField txtProductTotals;
		@FXML
	    private TextField txtFreightTotals;
		@FXML
	    private TextField txtInsuranceTotals;
		@FXML
	    private TextField txtDiscountTotals;
		@FXML
	    private TextField txtIiTotals;
		@FXML
	    private TextField txtIpiTotals;
		@FXML
	    private TextField txtPisTotals;
		@FXML
	    private TextField txtCofinsTotals;
		@FXML
	    private TextField txtOtherExpensesTotals;
		@FXML
	    private TextField txtIcmsFreeTotals;
		@FXML
	    private TextField txtDocumentTotals;
	private TitledPane tpTotals2; 
		@FXML
	    private TextField txtCalcBasisIssTotals;
		@FXML
	    private TextField txtIssTotals;
		@FXML
	    private TextField txtPisOnServiceTotals;
		@FXML
	    private TextField txtCofinsOnServiceTotals;
		@FXML
	    private TextField txtNonTaxTotals;
		@FXML
	    private TextField txtDeductionsTotals;
		@FXML
	    private TextField txtConditionDiscountTotals;
		@FXML
	    private TextField txtUnconditionDiscountTotals;
		@FXML
	    private TextField txtIssWithholdingTotals;
		@FXML
	    private TextField txtOtherWithholdingTotals;
		@FXML
		private ComboBox<SpecialTaxationEnum> cmbSpecialTaxationTotals;
		@FXML
	    private TextField dtServiceTotals;
	private TitledPane tpTotals3;  
		@FXML
	    private TextField txtPisWithholdingTotals;
		@FXML
	    private TextField txtCofinsWithholdingTotals;
		@FXML
	    private TextField txtCsllWithholdingTotals;
		@FXML
	    private TextField txtCalcBasisSocialSecurityTotals;
		@FXML
	    private TextField txtSocialSecurityWithholdingTotals;
		@FXML
	    private TextField txtCalcBasisIrrfTotals;
		@FXML
	    private TextField txtIrrfWithholdingTotals;
		
		
	/**
	 * Transport Tab
	 */
	@FXML
    private ComboBox<ShippingModeEnum> cmbShippingModeTransport;
	@FXML
	private TitledPane tpTransport1;
		@FXML
		private ToggleGroup tgDocumentTypeTransport;
		@FXML
		private RadioButton rdDocumentTypeCpf;
		@FXML
		private RadioButton rdDocumentTypeCnpj;
		@FXML
		private TextField txtCnpjNumberTransport;
		@FXML
		private TextField txtCpfNumberTransport;
		@FXML
		private TextField txtIeTransport;
		@FXML
		private TextField txtCompanyNameTransport;
		@FXML
		private ComboBox<String> cmbUfTransport;
		@FXML
		private ComboBox<CountyModel> cmbCountyTransport;
		@FXML
		private TextField txtStreetTransport;
	@FXML
    private TitledPane tpTransport2;
		@FXML
	    private TextField txtCalcBasisIcmsTransport;
		@FXML
	    private TextField txtAliqTransport;
		@FXML
	    private TextField txtServiceValueTransport;
		@FXML
	    private ComboBox<CfopEnum> cmbCfopTransport;
		@FXML
	    private ComboBox<String> cmbUfIcmsTransport;
	    @FXML
	    private ComboBox<CountyModel> cmbCountyIcmsTransport;
	    @FXML
	    private TextField txtIcmsWithholdingTransport;
	@FXML
	private TitledPane tpTransport3;
		@FXML
	    private ComboBox<VehicleTypeEnum> cmbTypeVehicleTransport;
		@FXML
	    private TabPane tabPaneTransport;
		@FXML
	    private Tab tabCarTransport1;
			@FXML
			private ComboBox<String> cmbUfCarTransport;
			@FXML
		    private TextField txtLicensePlateTransport;
			@FXML
		    private TextField txtRntcCarTransport;
		@FXML
	    private Tab tabCarTransport2;
		 	public ObservableList<TVeiculo> listTrailer;
			@FXML
			private Button btnAddTrailer;
			@FXML
			private Button btnEditTrailer;
			@FXML
			private Button btnDeleteTrailer;
			@FXML
		    public TableView<TVeiculo> tableTrailer;
			@FXML
		    private TableColumn<TVeiculo, String> colItemTrailer;
			@FXML
		    private TableColumn<TVeiculo, String> colLicensePlateTrailer;
			@FXML
		    private TableColumn<TVeiculo, String> colUfTrailer;
			@FXML
		    private TableColumn<TVeiculo, String> colRntcTrailer;
		@FXML
	    private Tab tabCarTransport3;
			@FXML
		    private TextField txtFerryIdentificationTransport;
		@FXML
	    private Tab tabCarTransport4;
			@FXML
		    private TextField txtWagonIdentificationTransport;
	@FXML	
	private TitledPane tpTransport4;
		public ObservableList<Vol> listVolume;
		@FXML
		private Button btnAddVolume;
		@FXML
		private Button btnEditVolume;
		@FXML
		private Button btnDeleteVolume;
		@FXML
	    public TableView<Vol> tableVolume;
		@FXML
	    private TableColumn<Vol, String> colAmountVolume;
		@FXML
	    private TableColumn<Vol, String> colSpecieVolume;
		@FXML
	    private TableColumn<Vol, String> colBrandVolume;
		@FXML
	    private TableColumn<Vol, String> colNumberVolume;
		@FXML
	    private TableColumn<Vol, String> colNetWeightVolume;
		@FXML
	    private TableColumn<Vol, String> colGrossWeightVolume;
		@FXML
	    private TableColumn<Vol, String> colSealAmount;
	
	
	/**
	 * Collection Tab
	 */	
	@FXML	
	private TitledPane tpCollection1;
		@FXML
	    private TextField txtInvoiceNumberCollection;
		@FXML
	    private TextField txtOriginalValueCollection;
		@FXML
	    private TextField txtDiscountValueCollection;
		@FXML
	    private TextField txtNetValueCollection;
	@FXML	
	private TitledPane tpCollection2;
		public ObservableList<Dup> listDuplicates;
		@FXML
		private Button btnAddDuplicates;
		@FXML
		private Button btnEditDuplicates;
		@FXML
		private Button btnDeleteDuplicates;
		@FXML
		public TableView<Dup> tableDuplicates;
		@FXML
	    private TableColumn<Dup, String> colNumberDuplicates;
		@FXML
	    private TableColumn<Dup, String> colDueDateDuplicates;
		@FXML
	    private TableColumn<Dup, String> colValueDuplicates;
	
	
	/**
	 * Additional Information Tab
	 */	
	@FXML	
	private TitledPane tpAdditionalInformation1;
		@FXML
	    private TextArea txtFiscoAdditionalInformation;
		@FXML
	    private TextArea txtTaxpayerAdditionalInformation;
	@FXML	
	private TitledPane tpAdditionalInformation2;
		public ObservableList<ObsCont> listTaxpayerComment;
		@FXML
		private Button btnAddTaxpayerComment;
		@FXML
		private Button btnEditTaxpayerComment;
		@FXML
		private Button btnDeleteTaxpayerComment;
		@FXML
		public TableView<ObsCont> tableTaxpayerComment;
		@FXML
	    private TableColumn<ObsCont, String> colNameTaxpayerComment;
		@FXML
	    private TableColumn<ObsCont, String> colTaxpayerComment;
	@FXML	
	private TitledPane tpAdditionalInformation3;
		public ObservableList<ObsFisco> listTaxAuthorities;
		@FXML
		private Button btnAddTaxAuthorities;
		@FXML
		private Button btnEditTaxAuthorities;
		@FXML
		private Button btnDeleteTaxAuthorities;
		@FXML
		public TableView<ObsFisco> tableTaxAuthorities;
		@FXML
	    private TableColumn<ObsFisco, String> colNameTaxAuthorities;
		@FXML
	    private TableColumn<ObsFisco, String> colTaxAuthoritiesComment;
	@FXML	
	private TitledPane tpAdditionalInformation4;
		public ObservableList<ProcRef> listProcessesReferenced;
		@FXML
		private Button btnAddProcessesReferenced;
		@FXML
		private Button btnEditProcessesReferenced;
		@FXML
		private Button btnDeleteProcessesReferenced;
		@FXML
		public TableView<ProcRef> tableProcessesReferenced;
		@FXML
	    private TableColumn<ProcRef, String> colIdentifierProcessesReferenced;
		@FXML
	    private TableColumn<ProcRef, String> colOriginProcessesReferenced;
		
		
	/**
	 * Export Tab
	 */
	@FXML
	private ComboBox<String> cmbUfExport;	
	@FXML
    private TextField txtBoarding;
	@FXML
    private TextField txtDispatch;	
	
	
	/**
	 * Purchases Tab
	 */
	@FXML
    private TextField txtPublicPurchase;	
	@FXML
    private TextField txtRequestInfPurchase;
	@FXML
    private TextField txtContractInfPurchase;	
	
	
	/**
	 * Cane Tab
	 */	
	@FXML	
	private TitledPane tpCane1;
		@FXML
	    private TextField txtHarvestIdentificationCane;
		@FXML
	    private TextField txtReferenceDateCane;
	@FXML	
	private TitledPane tpCane2;
		public ObservableList<ForDia> listDailySupplyCane;
		@FXML
		private Button btnAddDailySupplyCane;
		@FXML
		private Button btnEditDailySupplyCane;
		@FXML
		private Button btnDeleteDailySupplyCane;
		@FXML
		public TableView<ForDia> tableDailySupplyCane;
		@FXML
	    private TableColumn<ForDia, String> colDayDailySupplyCane;
		@FXML
	    private TableColumn<ForDia, String> colNetWeightDailySupplyCane;
	@FXML	
	private TitledPane tpCane3;
		public ObservableList<Deduc> listDeductionCane;
		@FXML
		private Button btnAddDeductionCane;
		@FXML
		private Button btnEditDeductionCane;
		@FXML
		private Button btnDeleteDeductionCane;
		@FXML
		public TableView<Deduc> tableDeductionCane;
		@FXML
	    private TableColumn<Deduc, String> colValueDeductionCane;
		@FXML
	    private TableColumn<Deduc, String> colDescriptionDeductionCane;
	@FXML	
	private TitledPane tpCane4;
		@FXML
	    private TextField txtMonthTotalSupplies;	
		@FXML
	    private TextField txtPreviousTotalSupplies;
		@FXML
	    private TextField txtGeralTotalSupplies;
		@FXML
	    private TextField txtValueSupplies;	
		@FXML
	    private TextField txtValueTotalDeductionSupplies;
		@FXML
	    private TextField txtNetValueSupplies;
			
	/**
	 * Download Authorization Tab
	 */	
	public ObservableList<AutXML> listDownloadAuthorization;
	@FXML
	private Button btnAddDownloadAuthorization;
	@FXML
	private Button btnEditDownloadAuthorization;
	@FXML
	private Button btnDeleteDownloadAuthorization;
	@FXML
	public TableView<AutXML> tableDownloadAuthorization;
	@FXML
    private TableColumn<AutXML, String> colDocumentDownloadAuthorization;
		
		
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Platform.runLater(() -> {
        	logger.info("Initializing IssuerController");
        	
            buttonsDisable();
            setModelColumn();
            setTooltips();
            setMaskField();
            populateCombos();
            installObservableList();
            installListeners();
            loadComponents();
        });
    } 
    
    /**
     * Add Fiscal Document Referred
     */
    @FXML
    private void actAddFdRefereced(ActionEvent event) {
    	logger.info("Calling actAddFdRefereced");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "referred-document.fxml", "Documentos Referenciados", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    /**
     * Edit Fiscal Document Referred
     */
    @FXML
    private void actEditFdRefereced(ActionEvent event) {
    	logger.info("Calling actEditFdRefereced");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "referred-document.fxml", "Documentos Referenciados", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableFdRefereced.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Fiscal Document Referred
     */
    @FXML
    private void actDeleteFdRefereced(ActionEvent event) {
    	logger.info("Calling actDeleteFdRefereced");
    	
    	message.clear();
    	
    	List<NFref> nfrefSelected = tableFdRefereced.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, nfrefSelected.size()+" Documento(s) Referenciado(s)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listFdRefereced.removeAll(nfrefSelected);
        }
    }
    
    
    /**
     * Add Producer Fiscal Document Referred
     */
    @FXML
    private void actAddProducerFdRefereced(ActionEvent event) {
    	logger.info("Calling actAddProducerFdRefereced");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "producer-referred-document.fxml", "Documentos Referenciados do Produtor", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    /**
     * Edit Producer Fiscal Document Referred
     */
    @FXML
    private void actEditProducerFdRefereced(ActionEvent event) {
    	logger.info("Calling actEditProducerFdRefereced");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "producer-referred-document.fxml", "Documentos Referenciados do Produtor", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableFdProducerRefereced.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Producer Fiscal Document Referred
     */
    @FXML
    private void actDeleteProducerFdRefereced(ActionEvent event) {
    	logger.info("Calling actDeleteProducerFdRefereced");
    	
    	message.clear();
    	
    	List<RefNFP> refNFPSelected = tableFdProducerRefereced.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, refNFPSelected.size()+" Documento(s) Referenciado(s) do Produtor"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listFdProducerRefereced.removeAll(refNFPSelected);
        }
    }
    
    
    /**
     * Add Fiscal Receipt Referred
     */
    @FXML
    private void actAddFiscalReceipt(ActionEvent event) {
    	logger.info("Calling actAddFiscalReceipt");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "fiscal-receipt-referred.fxml", "Cupom Fiscal", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    
    /**
     * Edit Fiscal Receipt Referred
     */
    @FXML
    private void actEditFiscalReceipt(ActionEvent event) {
    	logger.info("Calling actEditFiscalReceipt");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "fiscal-receipt-referred.fxml", "Cupom Fiscal", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableFdFiscalReceipt.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Fiscal Receipt Referred
     */
    @FXML
    private void actDeleteFiscalReceipt(ActionEvent event) {
    	logger.info("Calling actDeleteFiscalReceip");
    	
    	message.clear();
    	
    	List<RefECF> refEcfSelected = tableFdFiscalReceipt.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, refEcfSelected.size()+" Cupom(ns) Fiscal(is)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listFdFiscalReceipt.removeAll(refEcfSelected);
        }
    }
    
    /**
     * Add Product
     */
    @FXML
    private void actAddProduct(ActionEvent event) {
    	logger.info("Calling actAddProduct");
    	
    	message.clear();
    	
//    	screenHelper.newStage(stage, lblFiscalDocument, "fiscal-receipt-referred.fxml", "Cupom Fiscal", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    
    /**
     * Edit Product
     */
    @FXML
    private void actEditProduct(ActionEvent event) {
    	logger.info("Calling actEditProduct");
    	
    	message.clear();
    	
//    	screenHelper.newStage(stage, lblFiscalDocument, "fiscal-receipt-referred.fxml", "Cupom Fiscal", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableFdFiscalReceipt.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Product
     */
    @FXML
    private void actDeleteProduct(ActionEvent event) {
    	logger.info("Calling actDeleteProduct");
    	
    	message.clear();
    	
//    	List<RefECF> refEcfSelected = tableFdFiscalReceipt.getSelectionModel().getSelectedItems();
//    	
//    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, refEcfSelected.size()+" Cupom(ns) Fiscal(is)"));
//        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
//    		listFdFiscalReceipt.removeAll(refEcfSelected);
//        }
    }
    
    
    /**
     * Add Trailer
     */
    @FXML
    private void actAddTrailer(ActionEvent event) {
    	logger.info("Calling actAddTrailer");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "trailer-transport.fxml", "Reboque", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    
    /**
     * Edit Trailer
     */
    @FXML
    private void actEditTrailer(ActionEvent event) {
    	logger.info("Calling actEditTrailer");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "trailer-transport.fxml", "Reboque", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableTrailer.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Trailer
     */
    @FXML
    private void actDeleteTrailer(ActionEvent event) {
    	logger.info("Calling actDeleteTrailer");
    	
    	message.clear();
    	
    	List<TVeiculo> trailerSelected = tableTrailer.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, trailerSelected.size()+" reboque(s)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listTrailer.removeAll(trailerSelected);
        }
    }
    
    /**
     * Add Volume
     */
    @FXML
    private void actAddVolume(ActionEvent event) {
    	logger.info("Calling actAddVolume");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "volume-transport.fxml", "Volume", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    
    /**
     * Edit Volume
     */
    @FXML
    private void actEditVolume(ActionEvent event) {
    	logger.info("Calling actEditVolume");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "volume-transport.fxml", "Volume", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableVolume.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Volume
     */
    @FXML
    private void actDeleteVolume(ActionEvent event) {
    	logger.info("Calling actDeleteVolume");
    	
    	message.clear();
    	
    	List<Vol> volumeSelected = tableVolume.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, volumeSelected.size()+" volume(s)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listVolume.removeAll(volumeSelected);
        }
    }
    
    
    /**
     * Add Duplicate
     */
    @FXML
    private void actAddDuplicate(ActionEvent event) {
    	logger.info("Calling actAddDuplicate");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "duplicate-invoice.fxml", "Duplicata", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    
    /**
     * Edit Duplicate
     */
    @FXML
    private void actEditDuplicate(ActionEvent event) {
    	logger.info("Calling actEditDuplicate");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "duplicate-invoice.fxml", "Duplicata", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableDuplicates.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Duplicate
     */
    @FXML
    private void actDeleteDuplicate(ActionEvent event) {
    	logger.info("Calling actDeleteDuplicate");
    	
    	message.clear();
    	
    	List<Dup> duplicateSelected = tableDuplicates.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, duplicateSelected.size()+" duplicata(s)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listDuplicates.removeAll(duplicateSelected);
        }
    }
    
    
    /**
     * Add Taxpayer Comment
     */
    @FXML
    private void actAddTaxpayerComment(ActionEvent event) {
    	logger.info("Calling actAddTaxpayerComment");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "taxpayer-comment.fxml", "Observação do Contribuinte", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    
    /**
     * Edit Taxpayer Comment
     */
    @FXML
    private void actEditTaxpayerComment(ActionEvent event) {
    	logger.info("Calling actEditTaxpayerComment");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "taxpayer-comment.fxml", "Observação do Contribuinte", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableTaxpayerComment.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Taxpayer Comment
     */
    @FXML
    private void actDeleteTaxpayerComment(ActionEvent event) {
    	logger.info("Calling actDeleteTaxpayerComment");
    	
    	message.clear();
    	
    	List<ObsCont> taxpayerCommentSelected = tableTaxpayerComment.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, taxpayerCommentSelected.size()+" Observação(ões) do(s) Contribuinte(s)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listTaxpayerComment.removeAll(taxpayerCommentSelected);
        }
    }
    
    
    /**
     * Add Tax Authority Comment
     */
    @FXML
    private void actAddTaxAuthorityComment(ActionEvent event) {
    	logger.info("Calling actAddTaxAuthorityComment");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "tax-authority-comment.fxml", "Observação do Fisco", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    
    /**
     * Edit Tax Authority Comment
     */
    @FXML
    private void actEditTaxAuthorityComment(ActionEvent event) {
    	logger.info("Calling actEditTaxAuthorityComment");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "tax-authority-comment.fxml", "Observação do Fisco", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableTaxAuthorities.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Tax Authority Comment
     */
    @FXML
    private void actDeleteTaxAuthorityComment(ActionEvent event) {
    	logger.info("Calling actDeleteTaxAuthorityComment");
    	
    	message.clear();
    	
    	List<ObsFisco> taxAuthorityCommentSelected = tableTaxAuthorities.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, taxAuthorityCommentSelected.size()+" Observação(ões) do(s) Fisco(s)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listTaxAuthorities.removeAll(taxAuthorityCommentSelected);
        }
    }
    
    /**
     * Add Referenced Process
     */
    @FXML
    private void actAddReferencedProcess(ActionEvent event) {
    	logger.info("Calling actAddReferencedProcess");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "referenced-process.fxml", "Processo Referenciado", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    
    /**
     * Edit Referenced Process
     */
    @FXML
    private void actEditReferencedProcess(ActionEvent event) {
    	logger.info("Calling actEditReferencedProcess");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "referenced-process.fxml", "Processo Referenciado", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableProcessesReferenced.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Referenced Process
     */
    @FXML
    private void actDeleteReferencedProcess(ActionEvent event) {
    	logger.info("Calling actDeleteReferencedProcess");
    	
    	message.clear();
    	
    	List<ProcRef> referencedProcessSelected = tableProcessesReferenced.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, referencedProcessSelected.size()+" Processo(s) Referenciado(s)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listProcessesReferenced.removeAll(referencedProcessSelected);
        }
    }
    
    /**
     * Add Daily Supply
     */
    @FXML
    private void actAddDailySuply(ActionEvent event) {
    	logger.info("Calling actAddDailySuply");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "daily-supply-cane.fxml", "Fornecimento Diário", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    /**
     * Edit Daily Supply
     */
    @FXML
    private void actEditDailySuply(ActionEvent event) {
    	logger.info("Calling actEditDailySuply");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "daily-supply-cane.fxml", "Fornecimento Diário", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableDailySupplyCane.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Daily Supply
     */
    @FXML
    private void actDeleteDailySuply(ActionEvent event) {
    	logger.info("Calling actDeleteDailySuply");
    	
    	message.clear();
    	
    	List<ForDia> dailySupplySelected = tableDailySupplyCane.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, dailySupplySelected.size()+" Fornecimento(s) Diário(s)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listDailySupplyCane.removeAll(dailySupplySelected);
        }
    }
    
    /**
     * Deduction Table
     */
    @FXML
    private void actAddDeduction(ActionEvent event) {
    	logger.info("Calling actAddDeduction");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "deduction-cane.fxml", "Dedução", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    /**
     * Deduction Process
     */
    @FXML
    private void actEditDeduction(ActionEvent event) {
    	logger.info("Calling actEditDeduction");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "deduction-cane.fxml", "Dedução", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableDeductionCane.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Deduction Process
     */
    @FXML
    private void actDeleteDeduction(ActionEvent event) {
    	logger.info("Calling actDeleteDeduction");
    	
    	message.clear();
    	
    	List<Deduc> deductionsSelected = tableDeductionCane.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, deductionsSelected.size()+" Dedução(ões)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listDeductionCane.removeAll(deductionsSelected);
        }
    }
    
    /**
     * Download Authorization Table
     */
    @FXML
    private void actAddDownloadAuthorization(ActionEvent event) {
    	logger.info("Calling actAddDownloadAuthorization");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "download-authorization.fxml", "Autorização de Download", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    /**
     * Download Authorization Table
     */
    @FXML
    private void actEditDownloadAuthorization(ActionEvent event) {
    	logger.info("Calling actEditDownloadAuthorization");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblFiscalDocument, "download-authorization.fxml", "Autorização de Download", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableDownloadAuthorization.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Download Authorization Table
     */
    @FXML
    private void actDeleteDownloadAuthorization(ActionEvent event) {
    	logger.info("Calling actDeleteDownloadAuthorization");
    	
    	message.clear();
    	
    	List<AutXML> downloadAuthorizationSelected = tableDownloadAuthorization.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, downloadAuthorizationSelected.size()+" Autorização(ões) de Download"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listDownloadAuthorization.removeAll(downloadAuthorizationSelected);
        }
    }
    
    /**
     * Action to Issuer's Login 
     */
    @FXML
    private void actEnter(ActionEvent e){
    	logger.info("Calling actEnter");
//    	getIssuerService().setIssuerSession(tableIssuer.getSelectionModel().getSelectedItem());
//    	menuController.refreshIssuerLogged(tableIssuer.getSelectionModel().getSelectedItem());
    	menuController.updateRightFrame("search-document.fxml");
    }
    
    
    /**
     * Action to show the Issuer's Form
     */
    @FXML
    private void actAdd(ActionEvent e){
    	logger.info("Calling actAdd");

    	Platform.runLater(() -> {
            clear();
//            btnSave.setVisible(true);
//            btnSave.setDefaultButton(true);
//            btnUpdate.setVisible(false);
//            btnUpdate.setDefaultButton(false);
//            transitionController.executeTransition(TransitionType.FADE_OUT_UP,TransitionType.FADE_IN_UP, paneTable, paneIssuer);
        });
    }
    
    
    /**
     * Action to show the Issuer's Form to edit 
     */
    @FXML
    private void actEdit(ActionEvent e){
    	logger.info("Calling actEdit");
    	
    	clear();
    	
//    	issuerUpdate = tableIssuer.getSelectionModel().getSelectedItem();
//    	
//        txtCnpj.setText(issuerUpdate.getCnpj());
//        txtIe.setText(issuerUpdate.getIe());
//        txtCompanyName.setText(issuerUpdate.getCompanyName());
//        txtTradeName.setText(issuerUpdate.getTradeName());
//        txtIm.setText(issuerUpdate.getIm());
//        txtCnae.setText(issuerUpdate.getCnae());
//        txtIeSt.setText(issuerUpdate.getIeSt());
//        cmbRegimeTrib.getSelectionModel().select(issuerUpdate.getRegimeTrib());
//        cmbUF.getSelectionModel().select(issuerUpdate.getUf());
//        txtZipCode.setText(issuerUpdate.getZipCode());
//        txtStreet.setText(issuerUpdate.getStreet());
//        txtNumber.setText(issuerUpdate.getNumber());
//        txtComplement.setText(issuerUpdate.getComplement());
//        txtDistrict.setText(issuerUpdate.getDistrict());
//        txtEmail.setText(issuerUpdate.getEmail());
//        txtPhoneNumber.setText(issuerUpdate.getPhone());
//        if(issuerUpdate.getLogo()!=null)
//        	imgLogo.setImage(ImageUtil.getImageFromBytes(issuerUpdate.getLogo()));
//        
//        populateCountyCombo(issuerUpdate.getUf());
//        cmbCounty.getSelectionModel().select(issuerUpdate.getCountyModel());;
//        
//        btnSave.setVisible(false);
//        btnSave.setDefaultButton(false);
//        btnUpdate.setVisible(true);
//        btnUpdate.setDefaultButton(true);
//        
//        transitionController.executeTransition(TransitionType.FADE_OUT_UP, TransitionType.FADE_IN_UP, paneTable, paneIssuer);
    }
    
    
    /**
     * Delete an Issuer
     */
    @FXML
    private void actDelete(ActionEvent e){
    	logger.info("Calling actDelete");
    	
    	message.clear();
    	error.clear();
    	
//    	List<IssuerModel> issuersSelected = tableIssuer.getSelectionModel().getSelectedItems();
//    	
//    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, issuersSelected.size()+" Emitente(s)"));
//        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
//        	try{
//        		getIssuerService().deleteIssuers(issuersSelected);
//	        	message.clear();
//	        	addMessage(MessageFormat.format(MessageConstant.RECORD_DELETED, issuersSelected.size()+" Emitente(s)"));
//	        	screenHelper.dialogDefault("Excusï¿½o de Emitente", message, DialogType.INFORMATION);
//	        	// Remove Issuer Logged
//        		IssuerModel issuerSession = getIssuerService().getIssuerSession();
//        		if(issuerSession!=null){
//        			issuersSelected.forEach(issuerSelected -> {
//        				if(issuerSelected.getId()==issuerSession.getId()){
//        					menuController.removeIssuerLogged();
//        				}
//        			});
//        		}
//	        	refreshDataTable();
//        	}catch(Exception ex){
//        		logger.error(ex);
//        		addError(ex.getMessage());
//        		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
//        	}
//        }
    }
    
    
    /**
     * Update an Issuer
     */
    @FXML
    private void actUpdate(ActionEvent e){
    	logger.info("Calling actUpdate");
    	
//    	error.clear();
//    	message.clear();
//    	
//    	validateFields();
//    	
//    	if(hasError()){
//    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
//        }else{
//        	
//        	issuerUpdate = populateIssuer(issuerUpdate);
//            try {
//				getIssuerService().updateIssuer(issuerUpdate);
//				
//				message.clear();
//	        	addMessage(MessageFormat.format(MessageConstant.RECORD_UPDATED, "Emitente"));
//	        	screenHelper.dialogDefault("Yahooo...", message, DialogType.INFORMATION);
//	        	refreshDataTable();
//	        	
//	        	// Update Issuer Logged
//        		IssuerModel issuerSession = getIssuerService().getIssuerSession();
//	        	if(issuerSession!=null){
//    				if(issuerUpdate.getId()==issuerSession.getId()){
//    					MenuController menuController = (MenuController)getData();
//    					menuController.refreshIssuerLogged(issuerUpdate);;
//    				}
//        		}
//	        	
//	        	transitionController.executeTransition(TransitionType.FADE_OUT_UP, TransitionType.FADE_IN_UP, paneIssuer, paneTable);
//	        	clear();
//				
//			} catch (Exception e1) {
//				logger.error(e1);
//        		addError(e1.getMessage());
//        		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
//			}
//        }
    }
    
    
    /**
     * Save the Issuer on DB
     */
    @FXML
    private void actSave(ActionEvent event) {
    	logger.info("Calling actSave");
    	
    	error.clear();
    	message.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
            IssuerModel issuer = populateFiscalDocument(null);
            
            try {
				getIssuerService().saveIssuer(issuer);
				
				message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_INSERTED, "Emitente"));
	        	screenHelper.dialogDefault("Yahooo...", message, DialogType.INFORMATION);
	        	refreshDataTable();
//	        	transitionController.executeTransition(TransitionType.FADE_OUT_UP, TransitionType.FADE_IN_UP, paneIssuer, paneTable);
	        	clear();
				
			} catch (Exception e) {
				logger.error(e);
        		addError(e.getMessage());
        		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
			}
        }
    }

    
    /**
     * Populate Issuer Object 
     */
    private IssuerModel populateFiscalDocument(FiscalDocumentModel fiscalDocument){
    	logger.info("Calling populateFiscalDocument");
    	
    	if(fiscalDocument==null)
    		fiscalDocument = new FiscalDocumentModel();
    	
    	TNFe nfe = new TNFe();
    	InfNFe infNFe = new InfNFe();
    	
    	infNFe.setVersao(VersionEnum.SCHEMA_NFE.getDescription());
    	infNFe.setId("");//TODO 
    	
//        fiscalDocument.setCnpj(null);
//        fiscalDocument.setIe(null);
//        fiscalDocument.setCompanyName(null);
//        fiscalDocument.setTradeName(null);
//        fiscalDocument.setIm(null);
//        fiscalDocument.setCnae(null);
//        fiscalDocument.setIeSt(null);
//        fiscalDocument.setRegimeTrib(null);
//        fiscalDocument.setUf(null);
//        fiscalDocument.setZipCode(null));
//        fiscalDocument.setStreet(null);
//        fiscalDocument.setNumber(null);
//        fiscalDocument.setComplement(null);
//        fiscalDocument.setDistrict(null);
//        fiscalDocument.setEmail(null);
//        fiscalDocument.setPhone(null);
//        
//        		
//        CountyModel countyModel = cmbCounty.getValue();
//        fiscalDocument.setCountyModel(countyModel);
//        
//        UserModel userModel = SecurityUtil.getUserCurrent();
//        userModel.getIssuerModel().add(fiscalDocument);
//        
//        fiscalDocument.setUserModel(userModel);
//        
//        return fiscalDocument;
    	return null;
    }
    
    
    /**
     * Fields Validation 
     */
    private void validateFields(){
    	logger.info("Calling validateFields");
    	
    	error.clear();
    	
    	/**
         * Validate if is a required field 
         */
//    	if (txtCnpj.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "CNPJ"));
//        }
//    	if (txtIe.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Inscriï¿½ï¿½o Estadual"));
//        }
//    	if (txtCompanyName.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Razï¿½o Social"));
//        }
//    	if (txtIm.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Inscriï¿½ï¿½o Municipal"));
//        }
//    	if (cmbRegimeTrib.getSelectionModel().getSelectedIndex()<0) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Regime Tributï¿½rio (Subst. Trib.)"));
//        }
//    	if (cmbUF.getSelectionModel().getSelectedIndex()<0) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "UF"));
//        }else if(txtIe.getStyle().contains("green")){
//        	try {
//				DataValidateUtil.isValidaIe(txtIe.getText(), cmbUF.getSelectionModel().getSelectedItem());
//			} catch (Exception e) {
//				addError(e.getMessage());
//			}
//        }
//    	if (cmbCounty.getSelectionModel().getSelectedIndex()<0) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Municï¿½pio"));
//        }
//    	if (txtZipCode.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "CEP"));
//        }
//    	if (txtStreet.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Logradouro"));
//        }
//    	if (txtNumber.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Nï¿½mero"));
//        }
//    	if (txtDistrict.getText().isEmpty()) {
//        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Bairro"));
//        }
//    	
//    	
//    	/**
//         * Validate if is a valid content 
//         */
//    	if (txtCnpj.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "CNPJ"));
//        }
//    	if (txtIe.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Inscriï¿½ï¿½o Estadual"));
//        }
//    	if (txtIeSt.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Inscriï¿½ï¿½o Estadual (Subst. Trib.)"));
//        }
//    	if (txtCompanyName.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Razï¿½o Social"));
//        }
//    	if (txtTradeName.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Nome Fantasia"));
//        }
//    	if (txtIm.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Inscriï¿½ï¿½o Municipal"));
//        }
//    	if (txtCnae.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "CNAE"));
//        }
//    	if (txtZipCode.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "CEP"));
//        }
//    	if (txtStreet.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Logradouro"));
//        }
//    	if (txtNumber.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Nï¿½mero"));
//        }
//    	if (txtComplement.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Complemento"));
//        }
//    	if (txtDistrict.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Bairro"));
//        }
//    	if (txtPhoneNumber.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Telefone"));
//        }
//    	if (txtEmail.getStyle().contains("red")) {
//        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "E-mail"));
//        }
    }
    
    
    /**
     * Initialize Table Screen with buttons disabled 
     */
    private void buttonsDisable(){
    	logger.info("Calling buttonsDisable");
    	
//    	btnSave.setVisible(true);
//        btnUpdate.setVisible(false);
//    	btnEnter.setDisable(true);
//    	btnAdd.setDisable(false);
//    	btnEdit.setDisable(true);
//    	btnDelete.setDisable(true);
    }
    
    
    /**
     * Install ObservableList on tables    
     */
    private void installObservableList(){
    	logger.info("Calling installObservableList");
    	
    	listFdRefereced = FXCollections.observableArrayList();
    	listFdProducerRefereced = FXCollections.observableArrayList();
    	listFdFiscalReceipt = FXCollections.observableArrayList();
    	listProduct = FXCollections.observableArrayList();
    	listTrailer = FXCollections.observableArrayList();
    	listVolume = FXCollections.observableArrayList();
    	listDuplicates = FXCollections.observableArrayList();
    	listTaxpayerComment = FXCollections.observableArrayList();
    	listTaxAuthorities = FXCollections.observableArrayList();
    	listProcessesReferenced = FXCollections.observableArrayList();
    	listDailySupplyCane = FXCollections.observableArrayList();
    	listDeductionCane = FXCollections.observableArrayList();
    	listDownloadAuthorization = FXCollections.observableArrayList();
    }
    
    
    /**
     * Populate combos on initialization   
     */
    private void populateCombos(){
    	logger.info("Calling populateCombos");
    	
    	//cmbCounty starts disabled and will be activated when cmbUF is selected
    	cmbCountyFiscalDocument.setDisable(true);
    	cmbCountyIssuer.setDisable(true);
    	cmbCountyReceiver.setDisable(true);
    	cmbCountyPickupLocation.setDisable(true);
    	cmbCountyDeliveryLocation.setDisable(true);
    	cmbCountyTransport.setDisable(true);
    	cmbCountyIcmsTransport.setDisable(true);
    	
    	cmbDocumentType.getItems().addAll(DocumentTypeEnum.values());
	    cmbPaymentMethods.getItems().addAll(PaymentMethodsEnum.values());
	    cmbIssueType.getItems().addAll(IssueTypeEnum.values());
	    cmbPurposeIssue.getItems().addAll(PurposeIssueEnum.values());
	    cmbAttendanceType.getItems().addAll(AttendanceTypeEnum.values());
	    cmbTargetOperation.getItems().addAll(TargetOperationEnum.values());
	    cmbUfFiscalDocument.getItems().addAll(getStateService().findUfs());
	    
	    cmbUFIssuer.getItems().addAll(getStateService().findUfs());
	    cmbRegimeTribIssuer.getItems().addAll(RegimeTributarioEnum.values());
	    
	    cmbTaxpayerIssuer.getItems().addAll(TaxpayerTypeEnum.values());
	    cmbUFReceiver.getItems().addAll(getStateService().findUfs());
	    cmbUFPickupLocation.getItems().addAll(getStateService().findUfs());
	    cmbUFDeliveryLocation.getItems().addAll(getStateService().findUfs());
	    
	    cmbSpecialTaxationTotals.getItems().addAll(SpecialTaxationEnum.values());
	    
	    cmbShippingModeTransport.getItems().addAll(ShippingModeEnum.values());
	    cmbUfTransport.getItems().addAll(getStateService().findUfs());
	    cmbCfopTransport.getItems().addAll(CfopEnum.values());
	    cmbUfIcmsTransport.getItems().addAll(getStateService().findUfs());
	    cmbUfCarTransport.getItems().addAll(getStateService().findUfs());
	    
	    cmbTypeVehicleTransport.getItems().addAll(VehicleTypeEnum.values());
	    
	    cmbUfExport.getItems().addAll(getStateService().findUfs());
    }
    
    
    /**
     * Populate cmbCounty after cmbUF is selected  
     */
    private void populateCountyCombo(ComboBox cmbCounty, String uf){
    	logger.info("Calling populateCountyCombo");
    	
    	cmbCounty.getItems().clear();
    	cmbCounty.setDisable(false);
    	cmbCounty.getItems().addAll(getStateService().findByUf(uf));
    }
    
    
    /**
     * Implements a tooltip alert on required fields  
     */
    private void setTooltips(){
    	logger.info("Calling setRequiredFields");
    	
    	/**
         * Tooltip subtitle buttons  
         */
//    	Tooltip tEnter = new Tooltip("Abrir Emitente");
//    	Tooltip tAdd = new Tooltip("Cadastrar Emitente");
//    	Tooltip tEdit = new Tooltip("Editar Emitente");
//    	Tooltip tDelete = new Tooltip("Excluir Emitente(s)");
//    	hackTooltipStartTiming(tEnter);
//    	hackTooltipStartTiming(tAdd);
//    	hackTooltipStartTiming(tEdit);
//    	hackTooltipStartTiming(tDelete);
//    	btnEnter.setTooltip(tEnter);
//    	btnAdd.setTooltip(tAdd);
//    	btnEdit.setTooltip(tEdit);
//    	btnDelete.setTooltip(tDelete);
    	
    	/**
         * Tooltip alert on required fields  
         */
    	Tooltip t = new Tooltip(MessageConstant.REQUIRED_FIELD);
    	hackTooltipStartTiming(t);
    	
//    	lblCnpj.setTooltip(t);
//    	lblCompanyName.setTooltip(t);
//    	lblIm.setTooltip(t);
//    	lblIe.setTooltip(t);
//    	lblUf.setTooltip(t);
//    	lblCounty.setTooltip(t);
//    	lblRegimeTrib.setTooltip(t);
//    	lblZipCode.setTooltip(t);
//    	lblStreet.setTooltip(t);
//    	lblNumber.setTooltip(t);
//    	lblDistrict.setTooltip(t);
    }
    
    
    /**
     * Listeners Implementation
     */
    private void installListeners(){
    	logger.info("Calling installListeners");
    	
    	/**
	     * NF-e Tab  
	     */
		/*Listeners for dynamic field validation*/
		txtSerie.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtSerie, DataValidateUtil.SERIE_PATTERN, false));
		txtNumber.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtNumber, DataValidateUtil.NUMBER_PATTERN, false));
		txtNumericCode.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNumericCode, 8, 8, false));
		dtIssue.setOnKeyReleased(e -> DataValidateUtil.isValidDateHour(dtIssue));
		dtInputOutput.setOnKeyReleased(e -> DataValidateUtil.isValidDateHour(dtInputOutput));
		txtOperationOrigins.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtOperationOrigins, 1, 60, false));
		
		/* Listener for dynamic comboBox between cmbUF and cmbCounty */
		cmbUfFiscalDocument.valueProperty().addListener((observableValue, oldValue, newValue) -> {
		      if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
		    	  populateCountyCombo(cmbCountyFiscalDocument, newValue);
		    	  
		      }
		});
		tableFdRefereced.setRowFactory( tv -> {
		    TableRow<NFref> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblFiscalDocument, "referred-document.fxml", "Documentos Referenciados", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		tableFdProducerRefereced.setRowFactory( tv -> {
		    TableRow<RefNFP> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblFiscalDocument, "producer-referred-document.fxml", "Documentos Referenciados do Produtor", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		tableFdFiscalReceipt.setRowFactory( tv -> {
		    TableRow<RefECF> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblFiscalDocument, "fiscal-receipt-referred.fxml", "Cupom Fiscal", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableTrailer.setRowFactory( tv -> {
		    TableRow<TVeiculo> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblFiscalDocument, "trailer-transport.fxml", "Reboque", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableVolume.setRowFactory( tv -> {
		    TableRow<Vol> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblFiscalDocument, "volume-transport.fxml", "Volume", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableDuplicates.setRowFactory( tv -> {
		    TableRow<Dup> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblFiscalDocument, "duplicate-invoice.fxml", "Duplicata", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableTaxpayerComment.setRowFactory( tv -> {
		    TableRow<ObsCont> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblFiscalDocument, "taxpayer-comment.fxml", "Observação do Contribuinte", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableProcessesReferenced.setRowFactory( tv -> {
		    TableRow<ProcRef> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblFiscalDocument, "referenced-process.fxml", "Processo Referenciado", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableDailySupplyCane.setRowFactory( tv -> {
		    TableRow<ForDia> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblFiscalDocument, "daily-supply-cane.fxml", "Fornecimento Diário", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableDeductionCane.setRowFactory( tv -> {
		    TableRow<Deduc> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblFiscalDocument, "deduction-cane.fxml", "Dedução", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		tableDownloadAuthorization.setRowFactory( tv -> {
		    TableRow<AutXML> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblFiscalDocument, "download-authorization.fxml", "Autorização de Download", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
		
		/* Listener for dynamic button exhibition*/
		tableFdRefereced.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableFdRefereced.getSelectionModel().getSelectedItems().size()==0) {
		        btnAddFdRefereced.setDisable(false);
		        btnEditFdRefereced.setDisable(true);
		        btnDeleteFdRefereced.setDisable(true);
		    }else if (tableFdRefereced.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddFdRefereced.setDisable(false);
		    	btnEditFdRefereced.setDisable(false);
		    	btnDeleteFdRefereced.setDisable(false);
		    }else if (tableFdRefereced.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddFdRefereced.setDisable(false);
		    	btnEditFdRefereced.setDisable(true);
		    	btnDeleteFdRefereced.setDisable(false);
		    }
		});
		tableFdProducerRefereced.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableFdProducerRefereced.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddFdProducerRefereced.setDisable(false);
		    	btnEditFdProducerRefereced.setDisable(true);
		    	btnDeleteFdProducerRefereced.setDisable(true);
		    }else if (tableFdProducerRefereced.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddFdProducerRefereced.setDisable(false);
		    	btnEditFdProducerRefereced.setDisable(false);
		    	btnDeleteFdProducerRefereced.setDisable(false);
		    }else if (tableFdProducerRefereced.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddFdProducerRefereced.setDisable(false);
		    	btnEditFdProducerRefereced.setDisable(true);
		    	btnDeleteFdProducerRefereced.setDisable(false);
		    }
		});
		tableFdFiscalReceipt.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableFdFiscalReceipt.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddFdFiscalReceipt.setDisable(false);
		    	btnEditFdFiscalReceipt.setDisable(true);
		    	btnDeleteFdFiscalReceipt.setDisable(true);
		    }else if (tableFdFiscalReceipt.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddFdFiscalReceipt.setDisable(false);
		    	btnEditFdFiscalReceipt.setDisable(false);
		    	btnDeleteFdFiscalReceipt.setDisable(false);
		    }else if (tableFdFiscalReceipt.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddFdFiscalReceipt.setDisable(false);
		    	btnEditFdFiscalReceipt.setDisable(true);
		    	btnDeleteFdFiscalReceipt.setDisable(false);
		    }
		});
		tableProduct.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableProduct.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddProduct.setDisable(false);
		    	btnEditProduct.setDisable(true);
		    	btnDeleteProduct.setDisable(true);
		    }else if (tableProduct.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddProduct.setDisable(false);
		    	btnEditProduct.setDisable(false);
		    	btnDeleteProduct.setDisable(false);
		    }else if (tableProduct.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddProduct.setDisable(false);
		    	btnEditProduct.setDisable(true);
		    	btnDeleteProduct.setDisable(false);
		    }
		});
    	
		/**
	     * Issuer Tab  
	     */
		txtCnpjIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidCnpj(txtCnpjIssuer, true));
    	txtIeIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtIeIssuer, DataValidateUtil.IE_PATTERN, false));
    	txtCompanyNameIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCompanyNameIssuer, 2, 60, false));
    	txtTradeNameIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtTradeNameIssuer, 1, 60, false));
    	txtImIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtImIssuer, 1, 15, false));
    	txtCnaeIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtCnaeIssuer, DataValidateUtil.CNAE_PATTERN, false));
    	txtIeStIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtIeStIssuer, DataValidateUtil.IEST_PATTERN, false));
    	txtZipCodeIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtZipCodeIssuer, DataValidateUtil.ZIPCODE_PATTERN, true));
    	txtStreetIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtStreetIssuer, 2, 60, false));
    	txtNumberIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNumberIssuer, 1, 60, false));
    	txtComplementIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtComplementIssuer, 1, 60, false));
    	txtDistrictIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDistrictIssuer, 2, 60, false));
    	txtEmailIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtEmailIssuer, DataValidateUtil.EMAIL_PATTERN, false));
    	txtPhoneNumberIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtPhoneNumberIssuer, DataValidateUtil.PHONE_PATTERN, true));
    	/* Listener for dynamic comboBox between cmbUF and cmbCounty */
		cmbUFIssuer.valueProperty().addListener((observableValue, oldValue, newValue) -> {
		      if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
		    	  populateCountyCombo(cmbCountyIssuer, newValue);
		    	  
		      }
		});
		
		/**
	     * Receiver Tab  
	     */
		txtCnpjReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidCnpj(txtCnpjReceiver, true));
		txtIeReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtIeReceiver, DataValidateUtil.IE_PATTERN, false));
		txtSuframaReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtSuframaReceiver, DataValidateUtil.SUFRAMA_PATTERN, false));
		txtCompanyNameReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCompanyNameReceiver, 2, 60, false));
    	txtTradeNameIssuer.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtTradeNameIssuer, 1, 60, false));
    	txtImReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtImReceiver, 1, 15, false));
    	txtZipCodeReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtZipCodeReceiver, DataValidateUtil.ZIPCODE_PATTERN, true));
    	txtPhoneNumberReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtPhoneNumberReceiver, DataValidateUtil.PHONE_PATTERN, true));
    	txtStreetReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtStreetReceiver, 2, 60, false));
    	txtNumberReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNumberReceiver, 1, 60, false));
    	txtComplementReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtComplementReceiver, 1, 60, false));
    	txtDistrictReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDistrictReceiver, 2, 60, false));
    	txtEmailReceiver.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtEmailReceiver, DataValidateUtil.EMAIL_PATTERN, false));
	    /* Listener for dynamic comboBox between cmbUF and cmbCounty */
		cmbUFReceiver.valueProperty().addListener((observableValue, oldValue, newValue) -> {
		      if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
		    	  populateCountyCombo(cmbCountyReceiver, newValue);
		    	  
		      }
		});
		
		txtZipCodePickupLocation.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtZipCodePickupLocation, DataValidateUtil.ZIPCODE_PATTERN, true));
		txtPhoneNumberPickupLocation.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtPhoneNumberPickupLocation, DataValidateUtil.PHONE_PATTERN, true));
		txtStreetPickupLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtStreetPickupLocation, 2, 60, false));
		txtNumberPickupLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNumberPickupLocation, 1, 60, false));
		txtComplementPickupLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtComplementPickupLocation, 1, 60, false));
		txtDistrictPickupLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDistrictPickupLocation, 2, 60, false));
		txtCNPJPickupLocation.setOnKeyReleased(e -> DataValidateUtil.isValidCnpj(txtCNPJPickupLocation, true));
    	/* Listener for dynamic comboBox between cmbUF and cmbCounty */
    	cmbUFPickupLocation.valueProperty().addListener((observableValue, oldValue, newValue) -> {
		      if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
		    	  populateCountyCombo(cmbCountyPickupLocation, newValue);
		    	  
		      }
		});
    	
    	txtZipCodeDeliveryLocation.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtZipCodeDeliveryLocation, DataValidateUtil.ZIPCODE_PATTERN, true));
    	txtPhoneNumberDeliveryLocation.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtPhoneNumberDeliveryLocation, DataValidateUtil.PHONE_PATTERN, true));
    	txtStreetDeliveryLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtStreetDeliveryLocation, 2, 60, false));
    	txtNumberDeliveryLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNumberDeliveryLocation, 1, 60, false));
    	txtComplementDeliveryLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtComplementDeliveryLocation, 1, 60, false));
    	txtDistrictDeliveryLocation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDistrictDeliveryLocation, 2, 60, false));
    	txtCNPJDeliveryLocation.setOnKeyReleased(e -> DataValidateUtil.isValidCnpj(txtCNPJDeliveryLocation, true));
    	/* Listener for dynamic comboBox between cmbUF and cmbCounty */
    	cmbUFDeliveryLocation.valueProperty().addListener((observableValue, oldValue, newValue) -> {
		      if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
		    	  populateCountyCombo(cmbCountyDeliveryLocation, newValue);
		    	  
		      }
		});
    	
    	
    	/**
	     * Totals Tab  
	     */
    	txtCalcBasisIcmsTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCalcBasisIcmsTotals, 1, 20, false));
    	txtIcmsTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIcmsTotals, 1, 20, false));
    	txtCalcBasisIcmsStTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCalcBasisIcmsStTotals, 1, 20, false));
    	txtIcmsStTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIcmsStTotals, 1, 20, false));
    	txtProductTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtProductTotals, 1, 20, false));
    	txtFreightTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtFreightTotals, 1, 20, false));
    	txtInsuranceTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtInsuranceTotals, 1, 20, false));
    	txtDiscountTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDiscountTotals, 1, 20, false));
    	txtIiTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIiTotals, 1, 20, false));
    	txtIpiTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIpiTotals, 1, 20, false));
    	txtPisTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtPisTotals, 1, 20, false));
    	txtCofinsTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCofinsTotals, 1, 20, false));
    	txtOtherExpensesTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtOtherExpensesTotals, 1, 20, false));
    	txtIcmsFreeTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIcmsFreeTotals, 1, 20, false));
    	txtDocumentTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDocumentTotals, 1, 20, false));
    	
    	txtCalcBasisIssTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCalcBasisIssTotals, 1, 20, false));
    	txtIssTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIssTotals, 1, 20, false));
    	txtPisOnServiceTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtPisOnServiceTotals, 1, 20, false));
    	txtCofinsOnServiceTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCofinsOnServiceTotals, 1, 20, false));
    	txtNonTaxTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNonTaxTotals, 1, 20, false));
    	txtDeductionsTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDeductionsTotals, 1, 20, false));
    	txtConditionDiscountTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtConditionDiscountTotals, 1, 20, false));
    	txtUnconditionDiscountTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtUnconditionDiscountTotals, 1, 20, false));
    	txtIssWithholdingTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIssWithholdingTotals, 1, 20, false));
    	txtOtherWithholdingTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtOtherWithholdingTotals, 1, 20, false));
    	dtServiceTotals.setOnKeyReleased(e -> DataValidateUtil.isValidDate(dtServiceTotals));
    	
    	txtPisWithholdingTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtPisWithholdingTotals, 1, 20, false));
    	txtCofinsWithholdingTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCofinsWithholdingTotals, 1, 20, false));
    	txtCsllWithholdingTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCsllWithholdingTotals, 1, 20, false));
    	txtCalcBasisSocialSecurityTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCalcBasisSocialSecurityTotals, 1, 20, false));
    	txtSocialSecurityWithholdingTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtSocialSecurityWithholdingTotals, 1, 20, false));
    	txtCalcBasisIrrfTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCalcBasisIrrfTotals, 1, 20, false));
    	txtIrrfWithholdingTotals.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIrrfWithholdingTotals, 1, 20, false));
    	
    	/**
	     * Transport Tab  
	     */
    	txtCpfNumberTransport.setOnKeyReleased(e -> DataValidateUtil.isValidCpf(txtCpfNumberTransport, true));
    	txtCnpjNumberTransport.setOnKeyReleased(e -> DataValidateUtil.isValidCnpj(txtCnpjNumberTransport, true));
    	txtIeTransport.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtIeTransport, DataValidateUtil.IE_PATTERN, false));
    	txtCompanyNameTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCompanyNameTransport, 2, 60, false));
    	txtStreetTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtStreetTransport, 2, 60, false));
    	cmbUfTransport.valueProperty().addListener((observableValue, oldValue, newValue) -> {
		      if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
		    	  populateCountyCombo(cmbCountyTransport, newValue);
		    	  
		      }
		});
    	rdDocumentTypeCpf.selectedProperty().addListener((observableValue,  wasPreviouslySelected, isNowSelected) ->{
    	        if (isNowSelected) { 
    	        	transitionController.executeTransition(TransitionType.FADE_OUT_DOWN, TransitionType.FADE_IN_DOWN, txtCnpjNumberTransport, txtCpfNumberTransport);
    	        	txtCpfNumberTransport.clear();
    	        	txtCpfNumberTransport.setStyle("-fx-text-fill:black");
    	        }else{
    	        	transitionController.executeTransition(TransitionType.FADE_OUT_DOWN, TransitionType.FADE_IN_DOWN, txtCpfNumberTransport, txtCnpjNumberTransport);
    	        	txtCnpjNumberTransport.clear();
    	        	txtCnpjNumberTransport.setStyle("-fx-text-fill:black");
    	        }
    	});
    	
    	txtCalcBasisIcmsTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCalcBasisIcmsTransport, 1, 20, false));
    	txtServiceValueTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtServiceValueTransport, 1, 20, false));
    	txtAliqTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtAliqTransport, 1, 10, false));
    	txtIcmsWithholdingTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIcmsWithholdingTransport, 1, 20, false));
    	cmbUfIcmsTransport.valueProperty().addListener((observableValue, oldValue, newValue) -> {
		      if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
		    	  populateCountyCombo(cmbCountyIcmsTransport, newValue);
		    	  
		      }
		});
    	
    	txtLicensePlateTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtLicensePlateTransport, 1, 7, true));
    	txtRntcCarTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtRntcCarTransport, 1, 20, false));
    	
    	tableTrailer.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableTrailer.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddTrailer.setDisable(false);
		    	btnEditTrailer.setDisable(true);
		    	btnDeleteTrailer.setDisable(true);
		    }else if (tableTrailer.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddTrailer.setDisable(false);
		    	btnEditTrailer.setDisable(false);
		    	btnDeleteTrailer.setDisable(false);
		    }else if (tableTrailer.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddTrailer.setDisable(false);
		    	btnEditTrailer.setDisable(true);
		    	btnDeleteTrailer.setDisable(false);
		    }
		});
    	txtFerryIdentificationTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtFerryIdentificationTransport, 1, 20, false));
    	txtWagonIdentificationTransport.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtWagonIdentificationTransport, 1, 20, false));
    	
    	tableVolume.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableVolume.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddVolume.setDisable(false);
		    	btnEditVolume.setDisable(true);
		    	btnDeleteVolume.setDisable(true);
		    }else if (tableVolume.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddVolume.setDisable(false);
		    	btnEditVolume.setDisable(false);
		    	btnDeleteVolume.setDisable(false);
		    }else if (tableVolume.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddVolume.setDisable(false);
		    	btnEditVolume.setDisable(true);
		    	btnDeleteVolume.setDisable(false);
		    }
		});
    	
    	tableTrailer.setRowFactory( tv -> {
		    TableRow<TVeiculo> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblFiscalDocument, "trailer-transport.fxml", "Reboque", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});
    	
    	
    	/**
	     * Invoice Tab  
	     */
	    txtInvoiceNumberCollection.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtInvoiceNumberCollection, 1, 60, false));
	    txtOriginalValueCollection.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtOriginalValueCollection, 1, 20, false));
	    txtDiscountValueCollection.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDiscountValueCollection, 1, 20, false));
	    txtNetValueCollection.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNetValueCollection, 1, 20, false));
	   
	    tableDuplicates.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableDuplicates.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddDuplicates.setDisable(false);
		    	btnEditDuplicates.setDisable(true);
		    	btnDeleteDuplicates.setDisable(true);
		    }else if (tableDuplicates.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddDuplicates.setDisable(false);
		    	btnEditDuplicates.setDisable(false);
		    	btnDeleteDuplicates.setDisable(false);
		    }else if (tableDuplicates.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddDuplicates.setDisable(false);
		    	btnEditDuplicates.setDisable(true);
		    	btnDeleteDuplicates.setDisable(false);
		    }
		});
	    
	    
	    /**
	     * Additional Information Tab  
	     */
	    txtFiscoAdditionalInformation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextArea(txtFiscoAdditionalInformation, 1, 2000, false));
	    txtTaxpayerAdditionalInformation.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextArea(txtTaxpayerAdditionalInformation, 1, 5000, false));
	    
	    tableTaxpayerComment.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableTaxpayerComment.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddTaxpayerComment.setDisable(false);
		    	btnEditTaxpayerComment.setDisable(true);
		    	btnDeleteDuplicates.setDisable(true);
		    }else if (tableTaxpayerComment.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddTaxpayerComment.setDisable(false);
		    	btnEditTaxpayerComment.setDisable(false);
		    	btnDeleteTaxpayerComment.setDisable(false);
		    }else if (tableTaxpayerComment.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddTaxpayerComment.setDisable(false);
		    	btnEditTaxpayerComment.setDisable(true);
		    	btnDeleteTaxpayerComment.setDisable(false);
		    }
		});
	    
	    tableTaxAuthorities.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableTaxAuthorities.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddTaxAuthorities.setDisable(false);
		    	btnEditTaxAuthorities.setDisable(true);
		    	btnDeleteTaxAuthorities.setDisable(true);
		    }else if (tableTaxAuthorities.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddTaxAuthorities.setDisable(false);
		    	btnEditTaxAuthorities.setDisable(false);
		    	btnDeleteTaxAuthorities.setDisable(false);
		    }else if (tableTaxAuthorities.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddTaxAuthorities.setDisable(false);
		    	btnEditTaxAuthorities.setDisable(true);
		    	btnDeleteTaxAuthorities.setDisable(false);
		    }
		});
	    
	    tableProcessesReferenced.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableProcessesReferenced.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddProcessesReferenced.setDisable(false);
		    	btnEditProcessesReferenced.setDisable(true);
		    	btnDeleteProcessesReferenced.setDisable(true);
		    }else if (tableProcessesReferenced.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddProcessesReferenced.setDisable(false);
		    	btnEditProcessesReferenced.setDisable(false);
		    	btnDeleteProcessesReferenced.setDisable(false);
		    }else if (tableProcessesReferenced.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddProcessesReferenced.setDisable(false);
		    	btnEditProcessesReferenced.setDisable(true);
		    	btnDeleteProcessesReferenced.setDisable(false);
		    }
		});
	    
	    
	    /**
	     * Export Tab  
	     */
	    txtBoarding.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtBoarding, 1, 60, false));
	    txtDispatch.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDispatch, 1, 60, false));	
	    
	    
	    /**
	     * Purchase Tab  
	     */
	    txtPublicPurchase.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtPublicPurchase, 1, 22, false));	
	    txtRequestInfPurchase.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtRequestInfPurchase, 1, 60, false));
	    txtContractInfPurchase.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtContractInfPurchase, 1, 60, false));
	    
	    /**
		 * Cane Tab
		 */	
	    
	    txtHarvestIdentificationCane.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtHarvestIdentificationCane, DataValidateUtil.CANE_REF_PATTERN, false));
	    txtReferenceDateCane.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtReferenceDateCane, DataValidateUtil.CANE_PATTERN, false));
	    
	    tableDailySupplyCane.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableDailySupplyCane.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddDailySupplyCane.setDisable(false);
		    	btnEditDailySupplyCane.setDisable(true);
		    	btnDeleteDailySupplyCane.setDisable(true);
		    }else if (tableDailySupplyCane.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddDailySupplyCane.setDisable(false);
		    	btnEditDailySupplyCane.setDisable(false);
		    	btnDeleteDailySupplyCane.setDisable(false);
		    }else if (tableDailySupplyCane.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddDailySupplyCane.setDisable(false);
		    	btnEditDailySupplyCane.setDisable(true);
		    	btnDeleteDailySupplyCane.setDisable(false);
		    }
		});
	    
	    tableDeductionCane.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableDeductionCane.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddDeductionCane.setDisable(false);
		    	btnEditDeductionCane.setDisable(true);
		    	btnDeleteDeductionCane.setDisable(true);
		    }else if (tableDeductionCane.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddDeductionCane.setDisable(false);
		    	btnEditDeductionCane.setDisable(false);
		    	btnDeleteDeductionCane.setDisable(false);
		    }else if (tableDeductionCane.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddDeductionCane.setDisable(false);
		    	btnEditDeductionCane.setDisable(true);
		    	btnDeleteDeductionCane.setDisable(false);
		    }
		});
	    
	    txtMonthTotalSupplies.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtMonthTotalSupplies, 1, 26, false));
	    txtPreviousTotalSupplies.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtPreviousTotalSupplies, 1, 26, false));
	    txtGeralTotalSupplies.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtGeralTotalSupplies, 1, 26, false));
	    txtValueSupplies.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtValueSupplies, 1, 22, false));
	    txtValueTotalDeductionSupplies.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtValueTotalDeductionSupplies, 1, 22, false));
	    txtNetValueSupplies.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNetValueSupplies, 1, 22, false));
	    
	    
	    /**
		 * Download Autorization Tab
		 */
	    tableDownloadAuthorization.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableDownloadAuthorization.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddDownloadAuthorization.setDisable(false);
		    	btnEditDownloadAuthorization.setDisable(true);
		    	btnDeleteDownloadAuthorization.setDisable(true);
		    }else if (tableDownloadAuthorization.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddDownloadAuthorization.setDisable(false);
		    	btnEditDownloadAuthorization.setDisable(false);
		    	btnDeleteDownloadAuthorization.setDisable(false);
		    }else if (tableDownloadAuthorization.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddDownloadAuthorization.setDisable(false);
		    	btnEditDownloadAuthorization.setDisable(true);
		    	btnDeleteDownloadAuthorization.setDisable(false);
		    }
		});
	    
    }
    
    
    /**
     * Define Field's Mask  
     */
    private void setMaskField(){
    	logger.info("Calling setFieldMask");
    	
    	/**
	     * NF-e Tab  
	     */
    	MaskFieldUtil.numericField(txtSerie);
    	MaskFieldUtil.maxTextField(txtSerie,3);
    	MaskFieldUtil.numericField(txtNumber);
    	MaskFieldUtil.maxTextField(txtNumber,9);
    	MaskFieldUtil.numericField(txtNumericCode);
    	MaskFieldUtil.maxTextField(txtNumericCode,8);
    	MaskFieldUtil.dateHourField(dtIssue);
    	MaskFieldUtil.dateHourField(dtInputOutput);
    	MaskFieldUtil.maxTextField(txtOperationOrigins,60);
    	
    	/**
	     * Issuer Tab  
	     */
    	MaskFieldUtil.cnpjField(txtCnpjIssuer);
    	MaskFieldUtil.maxTextField(txtCompanyNameIssuer, 60);
    	MaskFieldUtil.maxTextField(txtTradeNameIssuer, 60);
    	MaskFieldUtil.maxTextField(txtImIssuer, 15);
    	MaskFieldUtil.numericField(txtImIssuer);
    	MaskFieldUtil.maxTextField(txtCnaeIssuer, 7);
    	MaskFieldUtil.numericField(txtCnaeIssuer);
    	MaskFieldUtil.maxTextField(txtIeStIssuer, 14);
    	MaskFieldUtil.numericField(txtIeStIssuer);
    	MaskFieldUtil.maxTextField(txtIeIssuer, 14);
    	MaskFieldUtil.numericField(txtIeIssuer);
    	MaskFieldUtil.zipCodeField(txtZipCodeIssuer);
    	MaskFieldUtil.maxTextField(txtStreetIssuer, 60);
    	MaskFieldUtil.maxTextField(txtNumberIssuer, 60);
    	MaskFieldUtil.maxTextField(txtComplementIssuer, 60);
    	MaskFieldUtil.maxTextField(txtDistrictIssuer, 60);
    	MaskFieldUtil.maxTextField(txtEmailIssuer, 256);
    	MaskFieldUtil.phoneField(txtPhoneNumberIssuer);
    	
    	/**
	     * Receiver Tab  
	     */
    	MaskFieldUtil.cnpjField(txtCnpjReceiver);
    	MaskFieldUtil.maxTextField(txtIeReceiver, 14);
    	MaskFieldUtil.numericField(txtIeReceiver);
    	MaskFieldUtil.maxTextField(txtSuframaReceiver, 9);
    	MaskFieldUtil.numericField(txtSuframaReceiver);
    	MaskFieldUtil.maxTextField(txtCompanyNameReceiver,60);
    	MaskFieldUtil.maxTextField(txtImReceiver,15);
    	MaskFieldUtil.numericField(txtImReceiver);
    	MaskFieldUtil.zipCodeField(txtZipCodeReceiver);
    	MaskFieldUtil.phoneField(txtPhoneNumberReceiver);	
    	MaskFieldUtil.maxTextField(txtStreetReceiver,60);
    	MaskFieldUtil.maxTextField(txtNumberReceiver,60);
    	MaskFieldUtil.maxTextField(txtComplementReceiver,60);
    	MaskFieldUtil.maxTextField(txtDistrictReceiver,60);
    	MaskFieldUtil.maxTextField(txtEmailReceiver, 256);
    	//Tab 2
	    MaskFieldUtil.zipCodeField(txtZipCodePickupLocation);
    	MaskFieldUtil.phoneField(txtPhoneNumberPickupLocation);	
    	MaskFieldUtil.maxTextField(txtStreetPickupLocation,60);
    	MaskFieldUtil.maxTextField(txtNumberPickupLocation,60);
    	MaskFieldUtil.maxTextField(txtComplementPickupLocation,60);
    	MaskFieldUtil.maxTextField(txtDistrictPickupLocation,60);
    	MaskFieldUtil.cnpjField(txtCNPJPickupLocation);
    	//Tab 3
    	MaskFieldUtil.zipCodeField(txtZipCodeDeliveryLocation);
    	MaskFieldUtil.phoneField(txtPhoneNumberDeliveryLocation);	
    	MaskFieldUtil.maxTextField(txtStreetDeliveryLocation,60);
    	MaskFieldUtil.maxTextField(txtNumberDeliveryLocation,60);
    	MaskFieldUtil.maxTextField(txtComplementDeliveryLocation,60);
    	MaskFieldUtil.maxTextField(txtDistrictDeliveryLocation,60);
    	MaskFieldUtil.cnpjField(txtCNPJDeliveryLocation);
    	
    	/**
	     * Totals Tab  
	     */
    	MaskFieldUtil.monetaryField(13, 2, txtCalcBasisIcmsTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIcmsTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtCalcBasisIcmsStTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIcmsStTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtProductTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtFreightTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtInsuranceTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtDiscountTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIiTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIpiTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtPisTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtCofinsTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtOtherExpensesTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIcmsFreeTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtDocumentTotals);
    	
    	MaskFieldUtil.monetaryField(13, 2, txtCalcBasisIssTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIssTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtPisOnServiceTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtCofinsOnServiceTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtNonTaxTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtDeductionsTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtConditionDiscountTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtUnconditionDiscountTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIssWithholdingTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtOtherWithholdingTotals);
    	MaskFieldUtil.dateField(dtServiceTotals);
    	
    	MaskFieldUtil.monetaryField(13, 2, txtPisWithholdingTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtCofinsWithholdingTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtCsllWithholdingTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtCalcBasisSocialSecurityTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtSocialSecurityWithholdingTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtCalcBasisIrrfTotals);
    	MaskFieldUtil.monetaryField(13, 2, txtIrrfWithholdingTotals);
    	
    	/**
	     * Transport Tab  
	     */
    	MaskFieldUtil.cpfField(txtCpfNumberTransport);
    	MaskFieldUtil.cnpjField(txtCnpjNumberTransport);
    	MaskFieldUtil.numericField(txtIeTransport);
    	MaskFieldUtil.maxTextField(txtIeTransport, 14);
    	MaskFieldUtil.maxTextField(txtCompanyNameTransport,60);
    	MaskFieldUtil.maxTextField(txtStreetTransport,60);
    	
    	MaskFieldUtil.monetaryField(13, 2, txtCalcBasisIcmsTransport);
    	MaskFieldUtil.monetaryField(13, 2, txtServiceValueTransport);
    	MaskFieldUtil.monetaryField(3, 4, txtAliqTransport);
    	MaskFieldUtil.monetaryField(13, 2, txtIcmsWithholdingTransport);
    	
    	MaskFieldUtil.licensePlateField(txtLicensePlateTransport);
    	MaskFieldUtil.maxTextField(txtRntcCarTransport, 20);
    	
    	MaskFieldUtil.maxTextField(txtFerryIdentificationTransport, 20);
    	MaskFieldUtil.maxTextField(txtWagonIdentificationTransport, 20);
    	
    	/**
	     * Invoice Tab  
	     */
    	MaskFieldUtil.maxTextField(txtInvoiceNumberCollection, 60);  
    	MaskFieldUtil.monetaryField(13, 2, txtOriginalValueCollection);    
    	MaskFieldUtil.monetaryField(13, 2, txtDiscountValueCollection);    
    	MaskFieldUtil.monetaryField(13, 2, txtNetValueCollection);    
    	
    	/**
	     * Additional Information Tab  
	     */
    	MaskFieldUtil.maxTextArea(txtFiscoAdditionalInformation, 2000);  
    	MaskFieldUtil.maxTextArea(txtTaxpayerAdditionalInformation, 5000);  
    	
    	/**
	     * Export Tab  
	     */
    	MaskFieldUtil.maxTextField(txtBoarding, 60);
    	MaskFieldUtil.maxTextField(txtDispatch, 60);
    	
    	/**
	     * Purchase Tab  
	     */
    	MaskFieldUtil.maxTextField(txtPublicPurchase, 22);	
    	MaskFieldUtil.maxTextField(txtRequestInfPurchase, 60);
    	MaskFieldUtil.maxTextField(txtContractInfPurchase, 60);
    	
    	/**
		 * Cane Tab
		 */	
    	MaskFieldUtil.numericField(txtHarvestIdentificationCane);
    	MaskFieldUtil.dateYearField(txtHarvestIdentificationCane);
    	MaskFieldUtil.numericField(txtReferenceDateCane);
    	MaskFieldUtil.dateMonthYearField(txtReferenceDateCane);
    	
    	MaskFieldUtil.monetaryField(11, 10, txtMonthTotalSupplies);
		MaskFieldUtil.monetaryField(11, 10, txtPreviousTotalSupplies);
		MaskFieldUtil.monetaryField(11, 10, txtGeralTotalSupplies);
		MaskFieldUtil.monetaryField(13, 2, txtValueSupplies);
		MaskFieldUtil.monetaryField(13, 2, txtValueTotalDeductionSupplies);
		MaskFieldUtil.monetaryField(13, 2, txtNetValueSupplies);
    }
    
    
    /**
     * Mapping columns table with issuer object
     */
    private void setModelColumn(){
    	logger.info("Calling setModelColumn");
    	
    	/**
		 * Referred Document Table
		 */	
    	tableFdRefereced.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colTypeFdRefereced.setCellValueFactory(nFref -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(nFref.getValue().getRefCTe()))
		    	  property.setValue("CT-e");
		      else if(!StringHelper.isEmpty(nFref.getValue().getRefNFe()))
		    	  property.setValue("NF-e");
		      else
		    	  property.setValue("NF");
		      return property;
    	});
    	colAccessKeyFdRefereced.setCellValueFactory(nFref -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(nFref.getValue().getRefCTe()))
		    	  property.setValue(nFref.getValue().getRefCTe());
		      else if(!StringHelper.isEmpty(nFref.getValue().getRefNFe()))
		    	  property.setValue(nFref.getValue().getRefNFe());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colUfFdRefereced.setCellValueFactory(nFref -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(nFref.getValue().getRefNF()!=null)
		    	  property.setValue(nFref.getValue().getRefNF().getCUF());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colCnpjFdRefereced.setCellValueFactory(nFref -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(nFref.getValue().getRefNF()!=null)
		    	  property.setValue(FormatterTextUtil.cnpjField(nFref.getValue().getRefNF().getCNPJ()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colModelFdRefereced.setCellValueFactory(nFref -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(nFref.getValue().getRefNF()!=null)
		    	  property.setValue(nFref.getValue().getRefNF().getMod());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colSerieFdRefereced.setCellValueFactory(nFref -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(nFref.getValue().getRefNF()!=null)
		    	  property.setValue(nFref.getValue().getRefNF().getSerie());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colNumberFdRefereced.setCellValueFactory(nFref -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(nFref.getValue().getRefNF()!=null)
		    	  property.setValue(nFref.getValue().getRefNF().getNNF());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	
    	/**
		 * ProducerReferred Document Table
		 */	
    	tableFdProducerRefereced.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colUfFdProducerRefereced.setCellValueFactory(refNfp -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refNfp.getValue().getCUF()))
		    	  property.setValue(refNfp.getValue().getCUF());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colCnpjFdProducerRefereced.setCellValueFactory(refNfp -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refNfp.getValue().getCNPJ()))
		    	  property.setValue(FormatterTextUtil.cnpjField(refNfp.getValue().getCNPJ()));
		      else if(!StringHelper.isEmpty(refNfp.getValue().getCPF()))
		    	  property.setValue(FormatterTextUtil.cpfField(refNfp.getValue().getCPF()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colModelFdProducerRefereced.setCellValueFactory(refNfp -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refNfp.getValue().getMod()))
		    	  property.setValue(refNfp.getValue().getMod());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colSerieFdProducerRefereced.setCellValueFactory(refNfp -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refNfp.getValue().getSerie()))
		    	  property.setValue(refNfp.getValue().getSerie());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colNumberFdProducerRefereced.setCellValueFactory(refNfp -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refNfp.getValue().getNNF()))
		    	  property.setValue(refNfp.getValue().getNNF());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colIeFdProducerRefereced.setCellValueFactory(refNfp -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refNfp.getValue().getIE()))
		    	  property.setValue(refNfp.getValue().getIE());
		      else
		    	  property.setValue("ISENTO");
		      return property;
    	});
    	
    	
    	/**
		 * Fiscal Receipt Table
		 */	
    	tableFdFiscalReceipt.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colModelFdFiscalReceipt.setCellValueFactory(refEcf -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refEcf.getValue().getMod()))
		    	  property.setValue(refEcf.getValue().getMod());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colCooFdFiscalReceipt.setCellValueFactory(refEcf -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refEcf.getValue().getNCOO()))
		    	  property.setValue(refEcf.getValue().getNCOO());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colEcfFdFiscalReceipt.setCellValueFactory(refEcf -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(refEcf.getValue().getNECF()))
		    	  property.setValue(refEcf.getValue().getNECF());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	
    	/**
		 * Trailer Table
		 */	
    	tableTrailer.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colLicensePlateTrailer.setCellValueFactory(trailer -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(trailer.getValue().getPlaca()))
		    	  property.setValue(FormatterTextUtil.licensePlateField(trailer.getValue().getPlaca()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colUfTrailer.setCellValueFactory(trailer -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(trailer.getValue().getUF()!=null)
		    	  property.setValue(trailer.getValue().getUF().toString());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colRntcTrailer.setCellValueFactory(trailer -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(trailer.getValue().getRNTC()))
		    	  property.setValue(trailer.getValue().getRNTC());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	
    	/**
		 * Trailer Table
		 */	
    	tableVolume.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colAmountVolume.setCellValueFactory(volume -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(volume.getValue().getQVol()))
		    	  property.setValue(volume.getValue().getQVol());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colNetWeightVolume.setCellValueFactory(volume -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(volume.getValue().getPesoL()))
		    	  property.setValue(FormatterTextUtil.monetaryField(12, 3, volume.getValue().getPesoL().toString()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colGrossWeightVolume.setCellValueFactory(volume -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(volume.getValue().getPesoB()))
		    	  property.setValue(FormatterTextUtil.monetaryField(12, 3, volume.getValue().getPesoB().toString()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	colNumberVolume.setCellValueFactory(volume -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(volume.getValue().getNVol()))
		    	  property.setValue(volume.getValue().getNVol());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colBrandVolume.setCellValueFactory(volume -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(volume.getValue().getMarca()))
		    	  property.setValue(volume.getValue().getMarca());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colSpecieVolume.setCellValueFactory(volume -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(volume.getValue().getEsp()))
		    	  property.setValue(volume.getValue().getEsp());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colSealAmount.setCellValueFactory(volume -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(volume.getValue().getLacres()!=null)
		    	  property.setValue(Integer.toString(volume.getValue().getLacres().size()));
		      else
		    	  property.setValue("0");
		      return property;
    	});
    	
    	
    	/**
		 * Duplicate Table
		 */	
    	tableDuplicates.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colDueDateDuplicates.setCellValueFactory(duplicate -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(duplicate.getValue().getDVenc()))
		    	  property.setValue(duplicate.getValue().getDVenc());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colNumberDuplicates.setCellValueFactory(duplicate -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(duplicate.getValue().getNDup()))
		    	  property.setValue(duplicate.getValue().getNDup().toString());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colValueDuplicates.setCellValueFactory(duplicate -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(duplicate.getValue().getVDup()))
		    	  property.setValue(FormatterTextUtil.monetaryField(12, 3, duplicate.getValue().getVDup()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	
    	/**
		 * Taxpayer Comment Table
		 */	
    	tableTaxpayerComment.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colNameTaxpayerComment.setCellValueFactory(taxpayerComment -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(taxpayerComment.getValue().getXCampo()))
		    	  property.setValue(taxpayerComment.getValue().getXCampo());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colTaxpayerComment.setCellValueFactory(taxpayerComment -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(taxpayerComment.getValue().getXTexto()))
		    	  property.setValue(taxpayerComment.getValue().getXTexto().toString());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	
    	/**
		 * Tax Authority Comment Table
		 */	
    	tableTaxAuthorities.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colNameTaxAuthorities.setCellValueFactory(taxAuthorityComment -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(taxAuthorityComment.getValue().getXCampo()))
		    	  property.setValue(taxAuthorityComment.getValue().getXCampo());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colTaxAuthoritiesComment.setCellValueFactory(taxAuthorityComment -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(taxAuthorityComment.getValue().getXTexto()))
		    	  property.setValue(taxAuthorityComment.getValue().getXTexto().toString());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	
    	/**
		 * Process Referenced Table
		 */	
    	tableProcessesReferenced.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colIdentifierProcessesReferenced.setCellValueFactory(taxAuthorityComment -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(taxAuthorityComment.getValue().getNProc()))
		    	  property.setValue(taxAuthorityComment.getValue().getNProc());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colOriginProcessesReferenced.setCellValueFactory(taxAuthorityComment -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(taxAuthorityComment.getValue().getIndProc()))
		    	  property.setValue(taxAuthorityComment.getValue().getIndProc().toString());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	/**
		 * Daily Supply Table
		 */	
    	tableDailySupplyCane.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colDayDailySupplyCane.setCellValueFactory(dailySupply -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(dailySupply.getValue().getDia()))
		    	  property.setValue(dailySupply.getValue().getDia());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colNetWeightDailySupplyCane.setCellValueFactory(dailySupply -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(dailySupply.getValue().getQtde()))
		    	  property.setValue(FormatterTextUtil.monetaryField(11, 10, dailySupply.getValue().getQtde()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	/**
		 * Deduction Table
		 */	
    	tableDeductionCane.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colDescriptionDeductionCane.setCellValueFactory(deduction -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(deduction.getValue().getXDed()))
		    	  property.setValue(deduction.getValue().getXDed());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	colValueDeductionCane.setCellValueFactory(deduction -> {
		      SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(deduction.getValue().getVDed()))
		    	  property.setValue(FormatterTextUtil.monetaryField(12, 3, deduction.getValue().getVDed()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    	
    	/**
		 * Download Authorization Table
		 */	
    	tableDownloadAuthorization.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colDocumentDownloadAuthorization.setCellValueFactory(downloadAuthorization -> {
    		 SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(downloadAuthorization.getValue().getCNPJ()))
		    	  property.setValue(FormatterTextUtil.cnpjField(downloadAuthorization.getValue().getCNPJ()));
		      else if(!StringHelper.isEmpty(downloadAuthorization.getValue().getCPF()))
		    	  property.setValue(FormatterTextUtil.cpfField(downloadAuthorization.getValue().getCPF()));
		      else
		    	  property.setValue("-");
		      return property;
    	});
    }
    
    
    /**
     * Clear Form
     */
    private void clear(){
    	logger.info("Calling clear");
    	
    	issuerUpdate = null;
    	
//    	txtCnpj.clear();
//    	txtIe.clear();
//    	txtCompanyName.clear();
//    	txtTradeName.clear();
//    	txtIm.clear();
//    	txtCnae.clear();
//    	txtIeSt.clear();
//    	cmbRegimeTrib.getSelectionModel().clearSelection();
//    	cmbUF.getSelectionModel().clearSelection();
//    	cmbCounty.getSelectionModel().clearSelection();
//    	txtZipCode.clear();
//    	txtStreet.clear();
//    	txtNumber.clear();
//    	txtComplement.clear();
//    	txtDistrict.clear();
//    	txtEmail.clear();
//    	txtPhoneNumber.clear();
    }
    
    
    /**
     * Refresh Data Table
     */
    private void refreshDataTable(){
    	logger.info("Calling refreshDataTable");
    	
//        if(listIssuerObservable == null){
//            listIssuerObservable =  FXCollections.observableList(getIssuerService().findAllIssuers());
//        }else {
//            listIssuerObservable.clear();
//            listIssuerObservable.addAll(FXCollections.observableList(getIssuerService().findAllIssuers()));
//        }
//        tableIssuer.setItems(listIssuerObservable);
    }
    
    
    /**
     * Load data stream bar
     */
    private void loadComponents(){
    	logger.info("Calling selectWithService");
    	
        Service<Integer> service = new Service<Integer>() {
            @Override
            protected Task<Integer> createTask() {
                refreshDataTable();
                return new Task<Integer>() {           
                    @Override
                    protected Integer call() throws Exception {
                        Integer max = getIssuerService().findAllIssuers().size();
                        if (max > 35) {
                            max = 30;
                        }
                        updateProgress(0, max);
                        for (int k = 0; k < max; k++) {
                            Thread.sleep(40);
                            updateProgress(k+1, max);
                        }
                        return max;
                    }
                };
            }
        };
        service.start();
        bar.progressProperty().bind(service.progressProperty());
        service.setOnRunning((WorkerStateEvent event) -> {
            imgLoad.setVisible(true);
        });
        
        service.setOnSucceeded((WorkerStateEvent event) -> {
            imgLoad.setVisible(false);
            bar.setVisible(false);
//            transitionController.executeTransition(null, TransitionType.FADE_IN_UP, null, paneTable);
//            transitionController.executeTransition(null, TransitionType.FADE_IN_RIGHT, null, lblIssuer);
        });
    }
}
