package br.com.cd.control;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInTransition;
import br.com.cd.animations.FadeOutLeftTransition;
import br.com.cd.animations.FadeOutTransition;
import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.model.IssuerModel;
import br.com.cd.util.FormatterTextUtil;
import br.com.cd.util.SecurityUtil;

/**
 * FXML Controller class for Menu
 *
 * @author lmpinheiro
 */
@Controller
public class MenuController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(MenuController.class);
	
	Stage stage;
    Rectangle2D rectangle2D;
    Double width,heigth;
    
    @Autowired
    ScreenConfig screenHelper;
    
	@FXML
    private Button btnClose;
    @FXML
    private Button btnMaximize;
    @FXML
    private Button btnMinimize;
    @FXML
    private Button btnResize;
    @FXML
    private Button btnFullScreen;
    @FXML
    private Label lblTitle;
    @FXML
    private Text lblIssuerCompany;
    @FXML
    private Text lblIssuerCnpj;
    @FXML
    private ImageView imgBig;
    @FXML
    private ImageView imgSmall;
    @FXML
    private ListView<String> listMenu;
    @FXML
    private ListView<String> listChild;
    @FXML
    private ListView<String> listFiscalDocument;
    @FXML
    private ListView<String> listEntries;
    @FXML
    private ListView<String> listProfile;
    @FXML
    private Accordion acdnMenu;
    @FXML
    private AnchorPane paneData;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private Button btnLogout;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing MenuController");
        rectangle2D = Screen.getPrimary().getVisualBounds(); 
        width = 0.1;
        heigth = 0.1;

        buildMenu();
        installListeners();
        
        Platform.runLater(() -> {
            stage = (Stage) btnMaximize.getScene().getWindow();
            stage.setMaximized(true);
            stage.setHeight(rectangle2D.getHeight());
            
            btnMaximize.getStyleClass().add("decoration-button-restore");
            btnResize.setVisible(false);
            listMenu.getSelectionModel().select(0);
            
            screenHelper.loadAnchorPane(paneData, "issuer.fxml", null);
            listMenu.requestFocus();
        });
    }    
   
    
    /**
     * Maximize Function
     */
    @FXML
    private void actMaximize(MouseEvent event) {
    	logger.info("Calling actMaximize");
    	
        stage = (Stage) btnMaximize.getScene().getWindow();
        if (stage.isMaximized()) {
            if (width == rectangle2D.getWidth() && heigth == rectangle2D.getHeight()) {
                stage.setMaximized(false);
                stage.setWidth(800);
                stage.setHeight(600);
                stage.centerOnScreen();
                btnMaximize.getStyleClass().remove("decoration-button-restore");
//                resize.setVisible(true);
                btnResize.setVisible(false);
            }else{
                stage.setMaximized(false);
                btnMaximize.getStyleClass().remove("decoration-button-restore");
//                resize.setVisible(true);
                btnResize.setVisible(false);
            }
            
        }else{
            stage.setMaximized(true);
            stage.setHeight(rectangle2D.getHeight());
            btnMaximize.getStyleClass().add("decoration-button-restore");
            btnResize.setVisible(false);
        }
    }

    /**
     * Minimize Function
     */
    @FXML
    private void actMinimize(ActionEvent event) {
    	logger.info("Calling actMinimize");
    	
        stage = (Stage) btnMinimize.getScene().getWindow();
        if (stage.isMaximized()) {
            width = rectangle2D.getWidth();
            heigth = rectangle2D.getHeight();
            stage.setMaximized(false);
            stage.setHeight(heigth);
            stage.setWidth(width);
            stage.centerOnScreen();
            Platform.runLater(() -> {
                stage.setIconified(true);
            });
        }else{
            stage.setIconified(true);
        }        
    }

    /**
     * Resize Function
     * 
     * Deprecated due bugs
     */
    @FXML
    @Deprecated
    private void actResize(ActionEvent event) {
    	logger.info("Calling actResize");
    }

    /**
     * Full Screen Function
     */
    @FXML
    private void actFullScreen(ActionEvent event) {
    	logger.info("Calling actFullScreen");
    	
        stage = (Stage) btnFullScreen.getScene().getWindow();
        if (stage.isFullScreen()) {
            stage.setFullScreen(false);
        }else{
            stage.setFullScreen(true);
        }
    }

    /**
     * Close Function
     */
    @FXML
    private void actClose(ActionEvent event) {
    	logger.info("Calling actClose");
    	
    	message.clear();
    	
    	addMessage(MessageConstant.USER_EXIT);
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
        	Platform.exit();
            System.exit(0);
        }
    }

    /**
     * Logout Function
     */
    @FXML
    private void actLogout(ActionEvent event) {
    	logger.info("Calling actLogout");
    	
    	message.clear();
    	
        addMessage(MessageConstant.USER_LOGOUT);
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
        	SecurityUtil.userLogout();
        	screenHelper.newStage(stage, lblTitle, "login.fxml", "Contribuinte Digital - Login", true, StageStyle.TRANSPARENT, null, false, this, null, true);
        }
    }
    
    /**
     * Action Send for List Menu
     */
    @FXML
    private void actClickIssuerMenu(MouseEvent event) {
    	logger.info("Calling actClickListMenu");
    	
    	selectMenuItem(MenuItemEnum.ENTRIES_ISSUER);
    }
    
    
    /**
     * Build Menu on Initialize
     */
    private void buildMenu() {
    	logger.info("Calling buildMenu");
    	
    	listMenu.getItems().addAll("Emitente");
    	
        listFiscalDocument.getItems().addAll(
        		MenuItemEnum.FISCAL_DOCUMENT_NEW.description, 
        		MenuItemEnum.FISCAL_DOCUMENT_SEARCH.description,
        		MenuItemEnum.FISCAL_DOCUMENT_IMPORT.description,
        		MenuItemEnum.FISCAL_DOCUMENT_UNUSED.description);
        
        listEntries.getItems().addAll(
        		MenuItemEnum.ENTRIES_ISSUER.description,
        		MenuItemEnum.ENTRIES_CLIENTS.description,
        		MenuItemEnum.ENTRIES_PRODUCTS.description,
        		MenuItemEnum.ENTRIES_TRACKER.description
        		);
        
        listProfile.getItems().addAll(
        		MenuItemEnum.INFORMATION_EDIT_USER.description,
        		MenuItemEnum.INFORMATION_EDIT_PREFERENCES.description
        		);
    }
    
    
    /**
     * Refresh Issuer Logged
     */
    public void refreshIssuerLogged(IssuerModel issuer) {
    	logger.info("Calling refreshIssuerLogged");
    	
    	listChild.getItems().clear();
    	Platform.runLater(() -> {
    		new FadeInLeftTransition(listChild).play();
    		new FadeInLeftTransition(listMenu).play();
            new FadeInLeftTransition(acdnMenu).play();
            new FadeOutTransition(imgBig).play();
            new FadeInTransition(imgSmall).play();
        });
    	getIssuerService().setIssuerSession(issuer);
    	listChild.getItems().addAll(issuer.getCompanyName());
    	listChild.getItems().addAll(FormatterTextUtil.cnpjField(issuer.getCnpj()));
    	
    }
    
    /**
     * Select menu item
     */
    public void selectMenuItem(MenuItemEnum menuItem) {
    	logger.info("Calling selectMenuItem");
    	
    	if(menuItem!=null)
	    	switch(menuItem){
		    	case FISCAL_DOCUMENT_NEW:{
		    		screenHelper.loadAnchorPane(paneData, "fiscal-document.fxml", null);
		    	}break;
		    	case FISCAL_DOCUMENT_SEARCH:{
		    		screenHelper.loadAnchorPane(paneData, "search-document.fxml", null);
		    	}break;
		    	case FISCAL_DOCUMENT_IMPORT:{
		
		    	}break;
		    	case FISCAL_DOCUMENT_UNUSED:{
		
		    	}break;
		    	case ENTRIES_ISSUER:{
		    		screenHelper.loadAnchorPane(paneData, "issuer.fxml", null);
		    	}break;
		    	case ENTRIES_CLIENTS:{
		
		    	}break;
		    	case ENTRIES_PRODUCTS:{
		
		    	}break;
		    	case ENTRIES_TRACKER:{
		
		    	}break;
		    	case INFORMATION_EDIT_USER:{
		
		    	}break;
		    	case INFORMATION_EDIT_PREFERENCES:{
		
		    	}break;
	    	}
    }
    
    
    /**
     * Remove Issuer Logged
     */
    public void removeIssuerLogged() {
    	logger.info("Calling removeIssuerLogged");
    	
    	Platform.runLater(() -> {
    		new FadeOutLeftTransition(listMenu).play();
            new FadeOutLeftTransition(listChild).play();
            new FadeOutLeftTransition(acdnMenu).play();
            new FadeOutTransition(imgSmall).play();
            new FadeInTransition(imgBig).play();
        });
    	listChild.getItems().clear();
    }
    
    
    /**
     * Update right frame
     */
    public void updateRightFrame(String pane) {
    	logger.info("Calling updateRightFrame");
    	
    	screenHelper.loadAnchorPane(paneData, pane, null);
    }
    
    
    /**
     * Listeners Implementation
     */
    private void installListeners(){
    	listFiscalDocument.setOnMouseClicked(e -> {
    		selectMenuItem(MenuItemEnum.getEnumByDescription(listFiscalDocument.getSelectionModel().getSelectedItem()));
    		listEntries.getSelectionModel().clearSelection();
    		listProfile.getSelectionModel().clearSelection();
        });
    	
    	listEntries.setOnMouseClicked(e -> {
    		selectMenuItem(MenuItemEnum.getEnumByDescription(listEntries.getSelectionModel().getSelectedItem()));
    		listFiscalDocument.getSelectionModel().clearSelection();
    		listProfile.getSelectionModel().clearSelection();
        });
    	
    	listProfile.setOnMouseClicked(e -> {
    		selectMenuItem(MenuItemEnum.getEnumByDescription(listProfile.getSelectionModel().getSelectedItem()));
    		listFiscalDocument.getSelectionModel().clearSelection();
    		listEntries.getSelectionModel().clearSelection();
        });
    }
    
    public static enum MenuItemEnum { 
    	
		FISCAL_DOCUMENT_NEW(0, "● Nova NF-e"), 
		FISCAL_DOCUMENT_SEARCH(1, "● Pesquisar"),
		FISCAL_DOCUMENT_IMPORT(2, "● Importar XML"),
		FISCAL_DOCUMENT_UNUSED(3, "● Inutilizar Faixa"),
		
		ENTRIES_ISSUER(4, "● Emitentes"),
		ENTRIES_CLIENTS(5, "● Clientes"),
		ENTRIES_PRODUCTS(6, "● Produtos"),
		ENTRIES_TRACKER(7, "● Transportadoras"),
		
		INFORMATION_EDIT_USER(8, "● Editar Dados"),
		INFORMATION_EDIT_PREFERENCES(9, "● Editar Preferências");
		
		private int item;
	    private String description;
	    
	    
	    MenuItemEnum (int item, String description) {
	        this.item = item;
	        this.description = description;
	    }
	    
	    public int getItem() {
	        return item;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
	    public static MenuItemEnum getEnumByCode(int item) {
			for(MenuItemEnum e:values()){
				if(e.getItem()==item)
					return e;
			}
			return null;
		}
	    
	    public static MenuItemEnum getEnumByDescription(String item) {
			for(MenuItemEnum e:values()){
				if(e.getDescription().equals(item))
					return e;
			}
			return null;
		}
	}
}
