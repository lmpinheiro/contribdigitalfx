package br.com.cd.control;

import java.net.URL;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.hibernate.annotations.common.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.animations.FadeInTransition;
import br.com.cd.animations.FadeOutLeftTransition;
import br.com.cd.animations.FadeOutTransition;
import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.control.TransitionController.TransitionType;
import br.com.cd.model.IssuerModel;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNF;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNFP;
import br.com.cd.nfe.TNFe.InfNFe.Transp.Vol;
import br.com.cd.nfe.TNFe.InfNFe.Transp.Vol.Lacres;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.FormatterTextUtil;
import br.com.cd.util.MaskFieldUtil;
import br.com.cd.util.SecurityUtil;

/**
 * FXML Controller class for Menu
 *
 * @author lmpinheiro
 */
@Controller
public class VolumeTransportController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(VolumeTransportController.class);
	
	Stage stage;
	boolean exists = false;
	boolean editAction = false;
    
    @Autowired
    ScreenConfig screenHelper;
    
    @Autowired
    FiscalDocumentController fiscalDocumentController;
    
    @Autowired
    TransitionController transitionController;
    
    @FXML
    private AnchorPane pnlMain;
	@FXML
    private Button btnClose;
	@FXML
    private Label lblTitle;
	
	@FXML
    private TextField txtAmount;
	@FXML
    private TextField txtNetWeight;
	@FXML
    private TextField txtGrossWeight;
	@FXML
    private TextField txtSpecie;
	@FXML
    private TextField txtBrand;
	@FXML
    private TextField txtNumber;
	
	public ObservableList<Lacres> listSeal;
	@FXML
	private Button btnAddSeal;
	@FXML
	private Button btnEditSeal;
	@FXML
	private Button btnDeleteSeal;
	@FXML
	public TableView<Lacres> tableSeal;
	@FXML
    private TableColumn<Lacres, String> colSealNumber;
	
	@FXML
    private Button btnSave;
	@FXML
    private Button btnUpdate;
	@FXML
    private Button btnCancel;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing MenuController");

    	Platform.runLater(() -> {
            stage = (Stage) btnClose.getScene().getWindow();
            
            setMaskField();
            installListeners();
            installObservableList();
            setModelColumn();
            if(getData()!=null){
            	editAction = true;
            	fillForm((Vol)getData());
            	btnSave.setVisible(false);
            	btnUpdate.setVisible(true);
            }
            
            transitionController.executeTransition(null, TransitionType.BOUNCE_IN, null, pnlMain);
        });
    }    
   

    /**
     * Add Object on table
     */
    @FXML
    private void actSave(ActionEvent event) {
    	logger.info("Calling actAdd");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	Vol volume = populateObject(null);
        	
        	exists = false;
        	
        	for(Vol item : fiscalDocumentController.listVolume){
        		if(item.getEsp().equals(volume.getEsp()) &&
    				item.getMarca().equals(volume.getMarca()) &&
    				item.getNVol().equals(volume.getNVol()) &&
    				item.getPesoB().equals(volume.getPesoB()) &&
    				item.getPesoL().equals(volume.getPesoL()) &&
    				item.getQVol().equals(volume.getQVol())){
        			
        			boolean isEquals = true;
        			if(volume.getLacres().size()==item.getLacres().size()){
	        			for(Lacres seal :volume.getLacres()){
	        				boolean isEqualsItem = false;
	        				for(Lacres sealItemTable: item.getLacres()){
	        					if(sealItemTable.getNLacre().equals(seal.getNLacre())){
	        						isEqualsItem=true;
	        					}
	        				}
	        				if(!isEqualsItem){
	        					isEquals = false;
	        					break;
	        				}
	        			}
        			}else{
        				isEquals = false;
        			}
        			exists = isEquals;
        			if(exists){
        				break;
        			}
        		}
        	};
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Volume"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
	        	fiscalDocumentController.listVolume.add(volume);
	        	fiscalDocumentController.tableVolume.setItems(fiscalDocumentController.listVolume);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_INSERTED, "Volume"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    
    
    /**
     * Update object on table
     */
    @FXML
    private void actUpdate(ActionEvent event) {
    	logger.info("Calling actUpdate");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	Vol volOld = (Vol)getData();
        	ObservableList<Vol> listVolume = FXCollections.observableArrayList(fiscalDocumentController.listVolume);
        	listVolume.remove(volOld);
        	
        	Vol volumeCurrent = populateObject(null);
        	
        	exists = false;
        	for(Vol item : listVolume){
        		if(item.getEsp().equals(volumeCurrent.getEsp()) &&
    				item.getMarca().equals(volumeCurrent.getMarca()) &&
    				item.getNVol().equals(volumeCurrent.getNVol()) &&
    				item.getPesoB().equals(volumeCurrent.getPesoB()) &&
    				item.getPesoL().equals(volumeCurrent.getPesoL()) &&
    				item.getQVol().equals(volumeCurrent.getQVol())){
        			
        			boolean isEquals = true;
        			if(volumeCurrent.getLacres().size()==item.getLacres().size()){
	        			for(Lacres seal :volumeCurrent.getLacres()){
	        				boolean isEqualsItem = false;
	        				for(Lacres sealItemTable: item.getLacres()){
	        					if(sealItemTable.getNLacre().equals(seal.getNLacre())){
	        						isEqualsItem=true;
	        					}
	        				}
	        				if(!isEqualsItem){
	        					isEquals = false;
	        					break;
	        				}
	        			}
        			}else{
        				isEquals = false;
        			}
        			exists = isEquals;
        			if(exists){
        				break;
        			}
        		}
        	};
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Volume"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
        		
        		Vol volume = populateObject((Vol)getData());
        		
        		fiscalDocumentController.listVolume.set(fiscalDocumentController.tableVolume.getSelectionModel().getSelectedIndex(), volume);
	        	fiscalDocumentController.tableVolume.setItems(fiscalDocumentController.listVolume);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_UPDATED, "Volume"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    

    /**
     * Close Pane
     */
    @FXML
    private void actClose(ActionEvent event) {
    	logger.info("Calling actClose");
    	
    	message.clear();
    	
    	addMessage(MessageConstant.EXIT_CONFIRMATION);
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
        	stage.close();
        }
    }
    
    /**
     * Add Seal
     */
    @FXML
    private void actAddSeal(ActionEvent event) {
    	logger.info("Calling actAddSeal");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblTitle, "seal-transport.fxml", "Lacres", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, null, false);
    }
    
    
    /**
     * Edit Seal
     */
    @FXML
    private void actEditSeal(ActionEvent event) {
    	logger.info("Calling actEditSeal");
    	
    	message.clear();
    	
    	screenHelper.newStage(stage, lblTitle, "seal-transport.fxml", "Lacres", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, tableSeal.getSelectionModel().getSelectedItem(), false);
    }
    
    /**
     * Delete Seal
     */
    @FXML
    private void actDeleteSeal(ActionEvent event) {
    	logger.info("actDeleteSeal");
    	
    	message.clear();
    	
    	List<Lacres> sealSelected = tableSeal.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, sealSelected.size()+" lacre(s)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
    		listSeal.removeAll(sealSelected);
        }
    }


    /**
     * Listeners Implementation
     */
    private void installListeners(){
    	
    	txtAmount.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtAmount, 1, 15, false));
    	txtNetWeight.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNetWeight, 1, 20, false));
    	txtGrossWeight.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtGrossWeight, 1, 20, false));
    	txtSpecie.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtSpecie, 1, 60, false));
    	txtBrand.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtBrand, 1, 60, false));
    	txtNumber.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNumber, 1, 60, false));
    	
    	tableSeal.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
		    if (tableSeal.getSelectionModel().getSelectedItems().size()==0) {
		    	btnAddSeal.setDisable(false);
		    	btnEditSeal.setDisable(true);
		    	btnDeleteSeal.setDisable(true);
		    }else if (tableSeal.getSelectionModel().getSelectedItems().size()==1){
		    	btnAddSeal.setDisable(false);
		    	btnEditSeal.setDisable(false);
		    	btnDeleteSeal.setDisable(false);
		    }else if (tableSeal.getSelectionModel().getSelectedItems().size()>1){
		    	btnAddSeal.setDisable(false);
		    	btnEditSeal.setDisable(true);
		    	btnDeleteSeal.setDisable(false);
		    }
		});
    	
    	tableSeal.setRowFactory( tv -> {
		    TableRow<Lacres> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
		        	screenHelper.newStage(stage, lblTitle, "seal-transport.fxml", "Volume", true, StageStyle.TRANSPARENT, Modality.APPLICATION_MODAL, false, this, row.getItem(), false);
		        }
		    });
		    return row ;
		});

    	
    }
    
    /**
     * Install ObservableList on tables    
     */
    private void installObservableList(){
    	logger.info("Calling installObservableList");
    	
    	listSeal = FXCollections.observableArrayList();
    }
    
    /**
     * Populate object form  
     */
    private Vol populateObject(Vol volume){
    	logger.info("Calling populateObject");
    	
    	if(volume==null)volume = new Vol();
    	
    	volume.setQVol(txtAmount.getText());
    	volume.setPesoB(MaskFieldUtil.removeMonetaryMask(txtGrossWeight.getText()));
    	volume.setPesoL(MaskFieldUtil.removeMonetaryMask(txtNetWeight.getText()));
    	volume.setEsp(txtSpecie.getText());
    	volume.setMarca(txtBrand.getText());
    	volume.setNVol(txtNumber.getText());
    	
    	if(listSeal.size()>0){
    		volume.getLacres().clear();
    		volume.getLacres().addAll(listSeal);
    	}
    	
    	return volume;
    }
    
    
    /**
     * Fill Form  
     */
    private void fillForm(Vol volume){
    	logger.info("Calling fillForm");
    	
    	txtAmount.setText(volume.getQVol());
    	txtGrossWeight.setText(volume.getPesoB());
    	txtNetWeight.setText(volume.getPesoL());
    	txtSpecie.setText(volume.getEsp());
    	txtBrand.setText(volume.getMarca());
    	txtNumber.setText(volume.getNVol());
    	
    	if(volume.getLacres().size()>0){
    		listSeal.clear();
    		listSeal.setAll(volume.getLacres());
    		tableSeal.setItems(listSeal);
    	}
    }
    
    
    /**
     * Define Field's Mask  
     */
    private void setMaskField(){
    	logger.info("Calling setFieldMask");
    	
    	MaskFieldUtil.numericField(txtAmount);
    	MaskFieldUtil.maxTextField(txtAmount, 15);
    	MaskFieldUtil.maxTextField(txtBrand, 60);
    	MaskFieldUtil.maxTextField(txtNumber, 60);
    	MaskFieldUtil.maxTextField(txtSpecie, 60);
    	MaskFieldUtil.monetaryField(12, 3, txtNetWeight);
    	MaskFieldUtil.monetaryField(12, 3, txtGrossWeight);
    }
    
    /**
     * Mapping columns table with issuer object
     */
    private void setModelColumn(){
    	logger.info("Calling setModelColumn");
    	
    	/**
		 * Referred Document Table
		 */	
    	tableSeal.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	colSealNumber.setCellValueFactory(nFref -> {
    		  SimpleStringProperty property = new SimpleStringProperty();
		      if(!StringHelper.isEmpty(nFref.getValue().getNLacre()))
		    	  property.setValue(nFref.getValue().getNLacre());
		      else
		    	  property.setValue("-");
		      return property;
    	});
    }
    
    /**
     * Fields Validation 
     */
    private void validateFields(){
    	logger.info("Calling validateFields");
    	
    	error.clear();
    	
		/**
         * Validate if is a valid content 
         */
		if (txtAmount.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Quantidade"));
        }
		if (txtNetWeight.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Peso Liquido (Kg)"));
        }
		if (txtGrossWeight.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Peso Bruto (Kg)"));
        }
		if (txtSpecie.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Espécie"));
        }
		if (txtBrand.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Marca"));
        }
		if (txtNumber.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Numeração"));
        }
    	
    }
}
