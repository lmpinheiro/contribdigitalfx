package br.com.cd.control;

import java.net.URL;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.hibernate.annotations.common.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.animations.FadeInTransition;
import br.com.cd.animations.FadeOutLeftTransition;
import br.com.cd.animations.FadeOutTransition;
import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.control.TransitionController.TransitionType;
import br.com.cd.model.IssuerModel;
import br.com.cd.nfe.TNFe.InfNFe.Cana.ForDia;
import br.com.cd.nfe.TNFe.InfNFe.Cobr.Dup;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefECF;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNF;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNFP;
import br.com.cd.nfe.TNFe.InfNFe.InfAdic.ObsCont;
import br.com.cd.nfe.TNFe.InfNFe.InfAdic.ObsFisco;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.FormatterTextUtil;
import br.com.cd.util.MaskFieldUtil;
import br.com.cd.util.SecurityUtil;

/**
 * FXML Controller class for Menu
 *
 * @author lmpinheiro
 */
@Controller
public class DailySupplyCaneController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(DailySupplyCaneController.class);
	
	Stage stage;
	boolean exists = false;
	boolean editAction = false;
    
    @Autowired
    ScreenConfig screenHelper;
    
    @Autowired
    FiscalDocumentController fiscalDocumentController;
    
    @Autowired
    TransitionController transitionController;
    
    @FXML
    private AnchorPane pnlMain;
	@FXML
    private Button btnClose;
	@FXML
    private Label lblTitle;
	
	@FXML
    private TextField txtDay;
	@FXML
    private TextField txtAmount;
	
	@FXML
    private Button btnAdd;
	@FXML
    private Button btnUpdate;
	@FXML
    private Button btnCancel;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing MenuController");
    	
    	Platform.runLater(() -> {
            stage = (Stage) btnClose.getScene().getWindow();
            
            setMaskField();
            installListeners();
            if(getData()!=null){
            	editAction = true;
            	fillForm((ForDia)getData());
            	btnAdd.setVisible(false);
            	btnUpdate.setVisible(true);
            }
            
            transitionController.executeTransition(null, TransitionType.BOUNCE_IN, null, pnlMain);
        });
    }    
   

    /**
     * Add Object on table
     */
    @FXML
    private void actSave(ActionEvent event) {
    	logger.info("Calling actAdd");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	ForDia dailySupplyComment = populateObject(null);
        	
        	exists = false;
        	fiscalDocumentController.listDailySupplyCane.forEach(item -> {
        		if(dailySupplyComment!=null && item!=null && 
        				!StringHelper.isEmpty(dailySupplyComment.getDia()) && dailySupplyComment.getDia().equals(item.getDia()) &&
        				!StringHelper.isEmpty(dailySupplyComment.getQtde()) && dailySupplyComment.getQtde().equals(item.getQtde())){
        			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Fornecimento Diário"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
	        	fiscalDocumentController.listDailySupplyCane.add(dailySupplyComment);
	        	fiscalDocumentController.tableDailySupplyCane.setItems(fiscalDocumentController.listDailySupplyCane);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_INSERTED, "Fornecimento Diário"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    
    
    /**
     * Update object on table
     */
    @FXML
    private void actUpdate(ActionEvent event) {
    	logger.info("Calling actUpdate");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	ForDia dailySupplyOld = (ForDia)getData();
        	ObservableList<ForDia> listDailySupply = FXCollections.observableArrayList(fiscalDocumentController.listDailySupplyCane);
        	listDailySupply.remove(dailySupplyOld);
        	
        	ForDia dailySupplyCurrent = populateObject(null);
        	
        	exists = false;
        	listDailySupply.forEach(item -> {
        		if(dailySupplyCurrent!=null && item!=null && 
        				!StringHelper.isEmpty(dailySupplyCurrent.getDia()) && dailySupplyCurrent.getDia().equals(item.getDia()) &&
        				!StringHelper.isEmpty(dailySupplyCurrent.getQtde()) && dailySupplyCurrent.getQtde().equals(item.getQtde())){
        			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Fornecimento Diário"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
        		
        		ForDia dailySupply = populateObject((ForDia)getData());
        		
        		fiscalDocumentController.listDailySupplyCane.set(fiscalDocumentController.tableDailySupplyCane.getSelectionModel().getSelectedIndex(), dailySupply);
	        	fiscalDocumentController.tableDailySupplyCane.setItems(fiscalDocumentController.listDailySupplyCane);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_UPDATED, "Fornecimento Diário"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    

    /**
     * Close Pane
     */
    @FXML
    private void actClose(ActionEvent event) {
    	logger.info("Calling actClose");
    	
    	message.clear();
    	
    	addMessage(MessageConstant.EXIT_CONFIRMATION);
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
        	stage.close();
        }
    }


    /**
     * Listeners Implementation
     */
    private void installListeners(){
    	txtDay.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDay, 1, 2, false));
    	txtAmount.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtAmount, 1, 25, false));
    }
    
    
    /**
     * Populate object form  
     */
    private ForDia populateObject(ForDia dailySupply){
    	logger.info("Calling populateObject");
    	
    	if(dailySupply==null)dailySupply = new ForDia();
    	
    	dailySupply.setDia(MaskFieldUtil.removeMonetaryMask(txtDay.getText()));
    	dailySupply.setQtde(txtAmount.getText());
    	
    	return dailySupply;
    }
    
    
    /**
     * Fill Form  
     */
    private void fillForm(ForDia dailySupply){
    	logger.info("Calling fillForm");
    	
    	txtAmount.setText(dailySupply.getQtde());
		txtDay.setText(dailySupply.getDia());
    }
    
    
    /**
     * Define Field's Mask  
     */
    private void setMaskField(){
    	logger.info("Calling setFieldMask");
    	
    	MaskFieldUtil.numericField(txtDay);
    	MaskFieldUtil.maxTextField(txtDay, 2);
    	MaskFieldUtil.monetaryField(11, 10, txtAmount);
    }
    
    /**
     * Fields Validation 
     */
    private void validateFields(){
    	logger.info("Calling validateFields");
    	
    	error.clear();
    	
    	
		/**
         * Validate if is a required field 
         */
    	if (txtDay.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Dia"));
        }
    	if (txtAmount.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Quantidade (Kg)"));
        }
		
		
		/**
         * Validate if is a valid content 
         */
		if (txtDay.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Dia"));
        }
		if (txtAmount.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Quantidade (Kg)"));
        }
    }
}
