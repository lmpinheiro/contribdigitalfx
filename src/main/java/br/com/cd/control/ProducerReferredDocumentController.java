package br.com.cd.control;

import java.net.URL;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.hibernate.annotations.common.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.animations.FadeInTransition;
import br.com.cd.animations.FadeOutLeftTransition;
import br.com.cd.animations.FadeOutTransition;
import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.control.TransitionController.TransitionType;
import br.com.cd.model.IssuerModel;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNF;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNFP;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.FormatterTextUtil;
import br.com.cd.util.MaskFieldUtil;
import br.com.cd.util.SecurityUtil;

/**
 * FXML Controller class for Menu
 *
 * @author lmpinheiro
 */
@Controller
public class ProducerReferredDocumentController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(ProducerReferredDocumentController.class);
	
	Stage stage;
	boolean exists = false;
	boolean editAction = false;
    
    @Autowired
    ScreenConfig screenHelper;
    
    @Autowired
    FiscalDocumentController fiscalDocumentController;
    
    @Autowired
    TransitionController transitionController;
    
    @FXML
    private AnchorPane pnlMain;
	@FXML
    private Button btnClose;
	@FXML
    private Label lblTitle;
	@FXML
    private ToggleGroup referredDocument;
	@FXML
    private RadioButton rdDocumentTypeCpf;
	@FXML
    private RadioButton rdDocumentTypeCnpj;
	@FXML
    private TextField txtCpfNumber;
	@FXML
    private TextField txtCnpjNumber;
	
	@FXML
    private ComboBox<String> cmbModel;
	@FXML
    private TextField txtSerie;
	@FXML
    private TextField txtNumber;
	@FXML
    private ComboBox<String> cmbUf;
	@FXML
    private TextField txtIssueDate;
	@FXML
    private TextField txtIe;
	@FXML
    private CheckBox chkFree;
	
	@FXML
    private Button btnAdd;
	@FXML
    private Button btnUpdate;
	@FXML
    private Button btnCancel;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing MenuController");

    	Platform.runLater(() -> {
            stage = (Stage) btnClose.getScene().getWindow();
            
            setMaskField();
            populateCombos();
            installListeners();
            if(getData()!=null){
            	editAction = true;
            	fillForm((RefNFP)getData());
            	btnAdd.setVisible(false);
            	btnUpdate.setVisible(true);
            }
            
            transitionController.executeTransition(null, TransitionType.BOUNCE_IN, null, pnlMain);
        });
    }    
   

    /**
     * Add Object on table
     */
    @FXML
    private void actSave(ActionEvent event) {
    	logger.info("Calling actAdd");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	RefNFP refNFP = populateObject(null);
        	
        	exists = false;
        	fiscalDocumentController.listFdProducerRefereced.forEach(item -> {
        		if(refNFP!=null && item!=null && (
    				(!StringHelper.isEmpty(refNFP.getCNPJ()) && refNFP.getCNPJ().equals(item.getCNPJ())) ||
					(!StringHelper.isEmpty(refNFP.getCPF()) && refNFP.getCPF().equals(item.getCPF()))) &&
    				!StringHelper.isEmpty(refNFP.getAAMM()) && refNFP.getAAMM().equals(item.getAAMM()) &&
    				!StringHelper.isEmpty(refNFP.getCNPJ()) && refNFP.getCNPJ().equals(item.getCNPJ()) &&
    				!StringHelper.isEmpty(refNFP.getCUF()) && refNFP.getCUF().equals(item.getCUF()) &&
    				!StringHelper.isEmpty(refNFP.getMod()) && refNFP.getMod().equals(item.getMod()) &&
    				!StringHelper.isEmpty(refNFP.getNNF()) && refNFP.getNNF().equals(item.getNNF()) &&
    				!StringHelper.isEmpty(refNFP.getSerie()) && refNFP.getSerie().equals(item.getSerie())){
        			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Documento"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
	        	fiscalDocumentController.listFdProducerRefereced.add(refNFP);
	        	fiscalDocumentController.tableFdProducerRefereced.setItems(fiscalDocumentController.listFdProducerRefereced);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_INSERTED, "Documento"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    
    
    /**
     * Update object on table
     */
    @FXML
    private void actUpdate(ActionEvent event) {
    	logger.info("Calling actUpdate");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	RefNFP refNFPOld = (RefNFP)getData();
        	ObservableList<RefNFP> listFdProducerRefereced = FXCollections.observableArrayList(fiscalDocumentController.listFdProducerRefereced);
        	listFdProducerRefereced.remove(refNFPOld);
        	
        	RefNFP refNFPCurrent = populateObject(null);
        	
        	exists = false;
        	listFdProducerRefereced.forEach(item -> {
        		if(refNFPCurrent!=null && item!=null && (
    				(!StringHelper.isEmpty(refNFPCurrent.getCNPJ()) && refNFPCurrent.getCNPJ().equals(item.getCNPJ())) ||
					(!StringHelper.isEmpty(refNFPCurrent.getCPF()) && refNFPCurrent.getCPF().equals(item.getCPF()))) &&
    				!StringHelper.isEmpty(refNFPCurrent.getAAMM()) && refNFPCurrent.getAAMM().equals(item.getAAMM()) &&
    				!StringHelper.isEmpty(refNFPCurrent.getCNPJ()) && refNFPCurrent.getCNPJ().equals(item.getCNPJ()) &&
    				!StringHelper.isEmpty(refNFPCurrent.getCUF()) && refNFPCurrent.getCUF().equals(item.getCUF()) &&
    				!StringHelper.isEmpty(refNFPCurrent.getMod()) && refNFPCurrent.getMod().equals(item.getMod()) &&
    				!StringHelper.isEmpty(refNFPCurrent.getNNF()) && refNFPCurrent.getNNF().equals(item.getNNF()) &&
    				!StringHelper.isEmpty(refNFPCurrent.getSerie()) && refNFPCurrent.getSerie().equals(item.getSerie())){
        			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Documento"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
        		
        		RefNFP refNFP = populateObject((RefNFP)getData());
        		
        		fiscalDocumentController.listFdProducerRefereced.set(fiscalDocumentController.tableFdProducerRefereced.getSelectionModel().getSelectedIndex(), refNFP);
	        	fiscalDocumentController.tableFdProducerRefereced.setItems(fiscalDocumentController.listFdProducerRefereced);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_UPDATED, "Documento"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    

    /**
     * Close Pane
     */
    @FXML
    private void actClose(ActionEvent event) {
    	logger.info("Calling actClose");
    	
    	message.clear();
    	
    	addMessage(MessageConstant.EXIT_CONFIRMATION);
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
        	stage.close();
        }
    }


    /**
     * Listeners Implementation
     */
    private void installListeners(){
    	rdDocumentTypeCnpj.selectedProperty().addListener((observableValue,  wasPreviouslySelected, isNowSelected) ->{
	        if (isNowSelected) { 
	        	transitionController.executeTransition(TransitionType.FADE_OUT_DOWN, TransitionType.FADE_IN_DOWN, txtCpfNumber, txtCnpjNumber);
	        	txtCnpjNumber.clear();
	        	txtCnpjNumber.setStyle("-fx-text-fill:black");
	        }else{
	        	transitionController.executeTransition(TransitionType.FADE_OUT_DOWN, TransitionType.FADE_IN_DOWN, txtCnpjNumber, txtCpfNumber);
	        	txtCpfNumber.clear();
	        	txtCpfNumber.setStyle("-fx-text-fill:black");
	        }
    	});
    	
    	chkFree.selectedProperty().addListener((observableValue,  wasPreviouslySelected, isNowSelected) ->{
	        if (isNowSelected) { 
	        	txtIe.setDisable(true);
	        	txtIe.setText("");
	        }else{
	        	txtIe.setDisable(false);
	        }
    	});
    	
    	txtSerie.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtSerie, 1, 3, false));
    	txtNumber.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNumber, 1, 9, false));
    	txtIssueDate.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIssueDate, 1, 7, false));
    	txtCnpjNumber.setOnKeyReleased(e -> DataValidateUtil.isValidCnpj(txtCnpjNumber, true));
    	txtCpfNumber.setOnKeyReleased(e -> DataValidateUtil.isValidCpf(txtCpfNumber, true));
    	txtIe.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtIe, DataValidateUtil.IE_PATTERN, false));
    }
    
    
    /**
     * Populate combos on initialization   
     */
    private void populateCombos(){
    	logger.info("Calling populateCombos");
    	
    	cmbUf.getItems().addAll(getStateService().findUfs());
    	
    	cmbModel.getItems().addAll("01","04");
    }
    
    
    /**
     * Populate object form  
     */
    private RefNFP populateObject(RefNFP refNFP){
    	logger.info("Calling populateObject");
    	
    	if(refNFP==null)refNFP = new RefNFP();
    	
    	if(rdDocumentTypeCpf.isSelected()){
    		refNFP.setCPF(MaskFieldUtil.removeMaskNumeral(txtCpfNumber.getText()));
    		refNFP.setCNPJ(null);
    	}else if(rdDocumentTypeCnpj.isSelected()){
    		refNFP.setCNPJ(MaskFieldUtil.removeMaskNumeral(txtCnpjNumber.getText()));
    		refNFP.setCPF(null);
    	}
		
    	refNFP.setAAMM(MaskFieldUtil.removeMaskNumeral(txtIssueDate.getText()));
    	refNFP.setCUF(cmbUf.getSelectionModel().getSelectedItem());
    	refNFP.setMod(cmbModel.getSelectionModel().getSelectedItem());
    	refNFP.setNNF(txtNumber.getText());
    	refNFP.setSerie(txtSerie.getText());
    	if(!chkFree.isSelected()){ 
    		refNFP.setIE(txtIe.getText());
    	}else{
    		refNFP.setIE("ISENTO");
    	}
    	
    	return refNFP;
    }
    
    
    /**
     * Fill Form  
     */
    private void fillForm(RefNFP refNFP){
    	logger.info("Calling fillForm");
    	
    	if(refNFP.getCNPJ()!=null){
    		rdDocumentTypeCnpj.setSelected(true);
    		txtCnpjNumber.setText(refNFP.getCNPJ());
    	}else if(refNFP.getCPF()!=null){
    		rdDocumentTypeCpf.setSelected(true);
    		txtCpfNumber.setText(refNFP.getCPF());
    	}
    	if(refNFP.getIE()!=null  && refNFP.getIE().equals("ISENTO")){
    		txtIe.setDisable(true);
    		chkFree.setSelected(true);
    	}
    	
		txtIssueDate.setText(refNFP.getAAMM());
		cmbUf.getSelectionModel().select(refNFP.getCUF());
		cmbModel.getSelectionModel().select(refNFP.getMod());
		txtNumber.setText(refNFP.getNNF());
		txtSerie.setText(refNFP.getSerie());
    }
    
    
    /**
     * Define Field's Mask  
     */
    private void setMaskField(){
    	logger.info("Calling setFieldMask");
    	
    	MaskFieldUtil.cnpjField(txtCnpjNumber);
    	MaskFieldUtil.cpfField(txtCpfNumber);
    	MaskFieldUtil.numericField(txtSerie);
    	MaskFieldUtil.maxTextField(txtSerie, 3);
    	MaskFieldUtil.numericField(txtNumber);
    	MaskFieldUtil.maxTextField(txtNumber, 9);
    	MaskFieldUtil.numericField(txtIssueDate);
    	MaskFieldUtil.dateMonthYearField(txtIssueDate);
    	MaskFieldUtil.maxTextField(txtIe, 14);
    	MaskFieldUtil.numericField(txtIe);
    }
    
    /**
     * Fields Validation 
     */
    private void validateFields(){
    	logger.info("Calling validateFields");
    	
    	error.clear();
    	
    	if(rdDocumentTypeCpf.isSelected()){
    		/**
             * Validate if is a required field 
             */
    		if (txtCpfNumber.getText().isEmpty()) {
            	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "CPF"));
            }
    		
    		/**
             * Validate if is a valid content 
             */
        	if (txtCpfNumber.getStyle().contains("red")) {
            	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "CPF"));
            }
    	}
    	
    	if(rdDocumentTypeCnpj.isSelected()){
    		/**
             * Validate if is a required field 
             */
    		if (rdDocumentTypeCnpj.getText().isEmpty()) {
            	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "CNPJ"));
            }
    		
    		/**
             * Validate if is a valid content 
             */
        	if (rdDocumentTypeCnpj.getStyle().contains("red")) {
            	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "CNPJ"));
            }
    	}
    	
		/**
         * Validate if is a required field 
         */
		if (cmbModel.getSelectionModel().getSelectedIndex()<0) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Modelo"));
        }
		if (txtSerie.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Série"));
        }
		if (txtNumber.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Número"));
        }
		if (txtIe.getText().isEmpty() && !chkFree.isSelected()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Inscrição Estadual"));
        }
		if (txtIssueDate.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Data de Emissão"));
        }
		if (cmbUf.getSelectionModel().getSelectedIndex()<0) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "UF"));
        }else if(txtIe.getStyle().contains("green")){
        	try {
				DataValidateUtil.isValidIe(txtIe.getText(), cmbUf.getSelectionModel().getSelectedItem());
			} catch (Exception e) {
				addError(e.getMessage());
			}
        }
		
		/**
         * Validate if is a valid content 
         */
		if (cmbModel.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Modelo"));
        }
		if (txtSerie.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Série"));
        }
		if (txtNumber.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Número"));
        }
		if (txtIssueDate.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Data de Emissão"));
        }
		if (cmbUf.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "UF"));
        }
		if (txtIe.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Inscrição Estadual"));
        }
    	
    }
}
