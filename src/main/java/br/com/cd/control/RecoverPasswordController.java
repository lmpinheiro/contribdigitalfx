package br.com.cd.control;

import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.hibernate.internal.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.model.UserModel;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.MailSenderUtil;

/**
 * FXML Controller for Recover Password and Secret Answer
 *
 * @author lmpinheiro
 */
@Controller
public class RecoverPasswordController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(RecoverPasswordController.class);
	
	private Stage stage;
    private UserModel user;
    
    @Autowired
    ScreenConfig screenHelper;
	
	@FXML
    private Text lblWelcome;
    @FXML
    private Text lblRecoverTitle;
    @FXML
    private Text lblEmail;
    @FXML
    private Text lblSecretAnswer;
    @FXML
    private Text lblSecretQuestion;
    @FXML
    private Text lblQuestion;
    @FXML
    private TextField txtEmail;
    @FXML
    private PasswordField txtSecretAnswer;
    @FXML
    private Button btnFindEmail;
    @FXML
    private Button btnSecretAnswerValidate;
    @FXML
    private Button btnCancel;
    @FXML
    private Text lblCopyright;
    @FXML 
    private Label lblClose; 
    @FXML 
    private Label lblLostSecretAnswer; 
    

    /**
     * Initializes transaction effects.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing RecoverPasswordController");
    	
        Platform.runLater(() -> {
            new FadeInLeftTransition(lblWelcome).play();
            new FadeInRightTransition(lblRecoverTitle).play();
            new FadeInLeftTransition(lblEmail).play();
            new FadeInLeftTransition(lblSecretQuestion).play();
            new FadeInLeftTransition(lblSecretAnswer).play();
            new FadeInRightTransition(lblQuestion).play();
            new FadeInRightTransition(txtEmail).play();
            new FadeInRightTransition(txtSecretAnswer).play();
            new FadeInRightTransition(btnFindEmail).play();
            new FadeInRightTransition(btnSecretAnswerValidate).play();
            new FadeInRightTransition(btnCancel).play();
            new FadeInRightTransition(lblLostSecretAnswer).play();
            lblClose.setOnMouseClicked((MouseEvent event) -> {
                Platform.exit();
                System.exit(0);
            });
        });
    }    
    
    
    /**
     * Cancel operation and return to Login Screen
     */
    @FXML
    private void actCancel(ActionEvent event) {
    	logger.info("Calling Login Screen - Cancel");
    	
        screenHelper.newStage(stage, lblClose, "login.fxml", "Contribuinte Digital - Login", true, StageStyle.TRANSPARENT, null, false, this, null, true);
    }
    
    
    /**
     * Find the registration by e-mail and return the secret question for be answered
     */
    @FXML
    private void actFindEmail(ActionEvent event) {
    	logger.info("Calling FindEmail Action");
    	
    	error.clear();
    	
    	if(StringHelper.isEmpty(txtEmail.getText())){
    		addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "E-mail"));
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
    	}else if(!DataValidateUtil.isValidEmail(txtEmail.getText())){
			addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "E-mail"));
			screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
		}else{
			user = getUserService().findByEmail(txtEmail.getText());
			if(user==null){
				addError(MessageFormat.format(MessageConstant.DOESNT_EXIST, "E-mail", txtEmail.getText()));
				screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
			}else{
				lblEmail.setVisible(false);
				txtEmail.setVisible(false);
				btnFindEmail.setVisible(false);
				initialize(null, null);
				lblSecretQuestion.setVisible(true);
				lblQuestion.setVisible(true);
				lblSecretAnswer.setVisible(true);
				txtSecretAnswer.setVisible(true);
				lblQuestion.setText(user.getQuestion());
				btnSecretAnswerValidate.setVisible(true);
				lblLostSecretAnswer.setVisible(true);
			}
		}
    }
    
        
    /**
     * If lost secret answer, sends a e-mail with user's security information to change the password
     */
    @FXML
    private void actLostSecretAnswer(MouseEvent event) {
    	logger.info("Calling LostSecretAnswer Action - Mail Sender");
    	
		message.clear();
		error.clear();
    	try {
			MailSenderUtil.getInstance().sendMailPasswordRecover(user);
			addMessage(MessageFormat.format(MessageConstant.SENDING_CONFIRMATION, user.getEmail()));
			screenHelper.dialogDefault("Foi...", message, DialogType.INFORMATION);
			screenHelper.newStage(stage, lblClose, "login.fxml", "Contribuinte Digital - Login", true, StageStyle.TRANSPARENT, null, false, this, null, true);
		} catch (IOException e) {
			addError(MessageConstant.ERROR_SENDING);
			screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
		}
    }
        
    
    /**
     * Secret answer validate
     */
    @FXML
    private void actSecretAnswerValidate(ActionEvent event) {
    	logger.info("Calling SecretAnswerValidate Action");
    	
    	error.clear();
    	
    	if(StringHelper.isEmpty(txtSecretAnswer.getText())){
    		addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Resposta Secreta"));
    	}
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
		}else if(!user.getAnswer().toLowerCase().equals(txtSecretAnswer.getText().toLowerCase())){
    		addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Resposta Secreta"));
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
    	}else{
    		screenHelper.newStage(stage, lblClose, "change-password.fxml", "Contribuinte Digital - Alteração de Senha", true, StageStyle.TRANSPARENT, null, false, this, user, true);
    	}
    }
}
