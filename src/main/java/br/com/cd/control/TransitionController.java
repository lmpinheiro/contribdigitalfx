package br.com.cd.control;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.scene.Node;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.BounceInDownTransition;
import br.com.cd.animations.BounceInLeftTransition;
import br.com.cd.animations.BounceInRightTransition;
import br.com.cd.animations.BounceInTransition;
import br.com.cd.animations.BounceInUpTransition;
import br.com.cd.animations.BounceOutDownTransition;
import br.com.cd.animations.BounceOutLeftTransition;
import br.com.cd.animations.BounceOutRightTransition;
import br.com.cd.animations.BounceOutTransition;
import br.com.cd.animations.BounceOutUpTransition;
import br.com.cd.animations.BounceTransition;
import br.com.cd.animations.FadeInDownBigTransition;
import br.com.cd.animations.FadeInDownTransition;
import br.com.cd.animations.FadeInLeftBigTransition;
import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInLeftTransition1;
import br.com.cd.animations.FadeInRightBigTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.animations.FadeInTransition;
import br.com.cd.animations.FadeInUpBigTransition;
import br.com.cd.animations.FadeInUpTransition;
import br.com.cd.animations.FadeOutDownBigTransition;
import br.com.cd.animations.FadeOutDownTransition;
import br.com.cd.animations.FadeOutLeftBigTransition;
import br.com.cd.animations.FadeOutLeftTransition;
import br.com.cd.animations.FadeOutRightBigTransition;
import br.com.cd.animations.FadeOutRightTransition;
import br.com.cd.animations.FadeOutTransition;
import br.com.cd.animations.FadeOutUpBigTransition;
import br.com.cd.animations.FadeOutUpTransition;
import br.com.cd.animations.FlashTransition;
import br.com.cd.animations.FlipInXTransition;
import br.com.cd.animations.FlipInYTransition;
import br.com.cd.animations.FlipOutXTransition;
import br.com.cd.animations.FlipOutYTransition;
import br.com.cd.animations.FlipTransition;
import br.com.cd.animations.HingeTransition;
import br.com.cd.animations.PulseTransition;
import br.com.cd.animations.RollInTransition;
import br.com.cd.animations.RollOutTransition;
import br.com.cd.animations.RotateInDownLeftTransition;
import br.com.cd.animations.RotateInDownRightTransition;
import br.com.cd.animations.RotateInTransition;
import br.com.cd.animations.RotateInUpLeftTransition;
import br.com.cd.animations.RotateInUpRightTransition;
import br.com.cd.animations.RotateOutDownLeftTransition;
import br.com.cd.animations.RotateOutDownRightTransition;
import br.com.cd.animations.RotateOutTransition;
import br.com.cd.animations.RotateOutUpLeftTransition;
import br.com.cd.animations.RotateOutUpRightTransition;
import br.com.cd.animations.ShakeTransition;
import br.com.cd.animations.SwingTransition;
import br.com.cd.animations.TadaTransition;
import br.com.cd.animations.WobbleTransition;

/**
 * (FXML) Transition Controller
 *
 * @author lmpinheiro
 */
@Controller
public class TransitionController {
	private static final Logger logger = Logger.getLogger(TransitionController.class);
	
	public static enum TransitionType {
		BOUNCE,
		BOUNCE_IN_DOWN,
		BOUNCE_IN_LEFT,
		BOUNCE_IN_RIGHT,
		BOUNCE_IN,
		BOUNCE_IN_UP,
		BOUNCE_OUT_DOWN,
		BOUNCE_OUT_LEFT,
		BOUNCE_OUT_RIGHT,
		BOUNCE_OUT,
		BOUNCE_OUT_UP,
		
		FADE_IN, 
		FADE_IN_DOWN,
		FADE_IN_DOWN_BIG, 
		FADE_IN_LEFT,
		FADE_IN_LEFT_1,
		FADE_IN_LEFT_BIG, 
		FADE_IN_RIGHT,
		FADE_IN_RIGHT_BIG,
		FADE_IN_UP, 
		FADE_IN_UP_BIG, 
		FADE_OUT, 
		FADE_OUT_DOWN,
		FADE_OUT_DOWN_BIG, 
		FADE_OUT_LEFT,
		FADE_OUT_LEFT_BIG, 
		FADE_OUT_RIGHT,
		FADE_OUT_RIGHT_BIG,
		FADE_OUT_UP, 
		FADE_OUT_UP_BIG, 
		
		FLASH,
		
		FLIP,
		FLIP_IN_X,
		FLIP_IN_Y,
		FLIP_OUT_X,
		FLIP_OUT_Y,
		
		HINGE,
		
		PULSE,
		
		ROLL_IN,
		ROLL_OUT,
		
		ROTATE_IN,
		ROTATE_IN_DOWN_LEFT,
		ROTATE_IN_DOWN_RIGHT,
		ROTATE_IN_UP_LEFT,
		ROTATE_IN_UP_RIGHT,
		ROTATE_OUT,
		ROTATE_OUT_DOWN_LEFT,
		ROTATE_OUT_DOWN_RIGHT,
		ROTATE_OUT_UP_LEFT,
		ROTATE_OUT_UP_RIGHT,
		
		SHAKE,
		
		SWING,
		
		TADA,
		
		WOBBLE		
	}
	
	
	public void executeTransition(TransitionType transitionTypeOut, TransitionType transitionTypeIn, Node oldPane, Node newPane){
		
		Service<Integer> service = new Service<Integer>() {
            @Override
            protected Task<Integer> createTask() {
                return new Task<Integer>() {           
                    @Override
                    protected Integer call() throws Exception {
                    	if(oldPane!=null){
                    		executeTransition(transitionTypeOut, oldPane);
                    	}
                    		return 1;
                    }
                };
            }
        };
        
        service.start();
        service.setOnSucceeded((WorkerStateEvent event) -> {
        	
        executeTransition(transitionTypeIn, newPane);
        newPane.toFront();
        });
		
	}
	
	private void executeTransition(TransitionType transitionType, Node pane){
		
		switch(transitionType){
    	
	    	/**
			 * BOUNCE
			 */
	    	case BOUNCE:{
	    		new BounceTransition(pane).play();
			}break;
	    	case BOUNCE_IN_DOWN:{
	    		new BounceInDownTransition(pane).play();
			}break;
	    	case BOUNCE_IN_LEFT:{
	    		new BounceInLeftTransition(pane).play();
			}break;
	    	case BOUNCE_IN_RIGHT:{
	    		new BounceInRightTransition(pane).play();
			}break;
	    	case BOUNCE_IN:{
	    		new BounceInTransition(pane).play();
			}break;
	    	case BOUNCE_IN_UP:{
	    		new BounceInUpTransition(pane).play();
			}break;
	    	case BOUNCE_OUT_DOWN:{
	    		new BounceOutDownTransition(pane).play();
			}break;
	    	case BOUNCE_OUT_LEFT:{
	    		new BounceOutLeftTransition(pane).play();
			}break;
	    	case BOUNCE_OUT_RIGHT:{
	    		new BounceOutRightTransition(pane).play();
			}break;
	    	case BOUNCE_OUT:{
	    		new BounceOutTransition(pane).play();
			}break;
	    	case BOUNCE_OUT_UP:{
	    		new BounceOutUpTransition(pane).play();
			}break;
			
			
			/**
			 * FADE
			 */
			case FADE_IN:{
				new FadeInTransition(pane).play();	
			}break;
			case FADE_IN_DOWN:{
				new FadeInDownTransition(pane).play();
			}break;
			case FADE_IN_DOWN_BIG:{
				new FadeInDownBigTransition(pane).play();
			}break;
			case FADE_IN_LEFT:{
				new FadeInLeftTransition(pane).play();
			}break;
			case FADE_IN_LEFT_1:{
				new FadeInLeftTransition1(pane).play();
			}break;
			case FADE_IN_LEFT_BIG:{
				new FadeInLeftBigTransition(pane).play();
			}break;
			case FADE_IN_RIGHT:{
				new FadeInRightTransition(pane).play();
			}break;
			case FADE_IN_RIGHT_BIG:{
				new FadeInRightBigTransition(pane).play();
			}break;
			case FADE_IN_UP:{
				new FadeInUpTransition(pane).play();
			}break;
			case FADE_IN_UP_BIG:{
				new FadeInUpBigTransition(pane).play();
			}break;
			case FADE_OUT:{
				new FadeOutTransition(pane).play();
			}break;
			case FADE_OUT_DOWN:{
				new FadeOutDownTransition(pane).play();
			}break;
			case FADE_OUT_DOWN_BIG:{
				new FadeOutDownBigTransition(pane).play();
			}break;
			case FADE_OUT_LEFT:{
				new FadeOutLeftTransition(pane).play();
			}break;
			case FADE_OUT_LEFT_BIG:{
				new FadeOutLeftBigTransition(pane).play();
			}break;
			case FADE_OUT_RIGHT:{
				new FadeOutRightTransition(pane).play();
			}break;
			case FADE_OUT_RIGHT_BIG:{
				new FadeOutRightBigTransition(pane).play();	
			}break;
			case FADE_OUT_UP:{
				new FadeOutUpTransition(pane).play();
			}break;
			case FADE_OUT_UP_BIG:{
				new FadeOutUpBigTransition(pane).play();
			}break;
			
			
			case FLASH:{
				new FlashTransition(pane).play();		
			}break;
			
			
			/**
			 * FLIP
			 */
			case FLIP:{
				new FlipTransition(pane).play();
			}break;
			
			case FLIP_IN_X:{
				new FlipInXTransition(pane).play();
			}break;
			case FLIP_IN_Y:{
				new FlipInYTransition(pane).play();
			}break;
			case FLIP_OUT_X:{
				new FlipOutXTransition(pane).play();
			}break;
			case FLIP_OUT_Y:{
				new FlipOutYTransition(pane).play();
			}break;
			
			
			case HINGE:{
				new HingeTransition(pane).play();
			}break;
			case PULSE:{
				new PulseTransition(pane).play();
			}break;
			
			
			/**
			 * ROLL
			 */
			case ROLL_IN:{
				new RollInTransition(pane).play();
			}break;
			case ROLL_OUT:{
				new RollOutTransition(pane).play();
			}break;
			
			
			/**
			 * ROTATE
			 */
			case ROTATE_IN:{
				new RotateInTransition(pane).play();
			}break;
			case ROTATE_IN_DOWN_LEFT:{
				new RotateInDownLeftTransition(pane).play();
			}break;
			case ROTATE_IN_DOWN_RIGHT:{
				new RotateInDownRightTransition(pane).play();
			}break;
			case ROTATE_IN_UP_LEFT:{
				new RotateInUpLeftTransition(pane).play();
			}break;
			case ROTATE_IN_UP_RIGHT:{
				new RotateInUpRightTransition(pane).play();
			}break;
			case ROTATE_OUT:{
				new RotateOutTransition(pane).play();
			}break;
			case ROTATE_OUT_DOWN_LEFT:{
				new RotateOutDownLeftTransition(pane).play();
			}break;
			case ROTATE_OUT_DOWN_RIGHT:{
				new RotateOutDownRightTransition(pane).play();
			}break;
			case ROTATE_OUT_UP_LEFT:{
				new RotateOutUpLeftTransition(pane).play();
			}break;
			case ROTATE_OUT_UP_RIGHT:{
				new RotateOutUpRightTransition(pane).play();
			}break;
			
			
			case SHAKE:{
				new ShakeTransition(pane).play();
			}break;
			case SWING:{
				new SwingTransition(pane).play();
			}break;
			case TADA:{
				new TadaTransition(pane).play();
			}break;
			case WOBBLE:{
				new WobbleTransition(pane).play();
			}break;
    	}
	}
}
