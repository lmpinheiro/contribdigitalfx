package br.com.cd.control;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.animations.FadeInTransition;
import br.com.cd.config.ScreenConfig;

/**
 * FXML Controller for Application Introduction
 *
 * @author lmpinheiro
 */
@Controller
public class AppIntroController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(AppIntroController.class);
	
	private Stage stage;
	
	@Autowired
    ScreenConfig screenHelper;
    
    @FXML
    private Text lblWelcome;
    @FXML
    private Text lblAppTitle;
    @FXML
    private VBox vboxBottom;
    @FXML
    private Label lblClose;
    @FXML
    private ImageView imgLoading;
    
    
    /**
     * Init method
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing AppIntroController");
    	
        longStart();
        lblClose.setOnMouseClicked(event -> {
            Platform.exit();
            System.exit(0);
        });
    }   
    
    
    /**
     * Play introduction pane
     */
    private void longStart() {
    	logger.info("Starting Introduction Task");
    	
        Service<ApplicationContext> service = new Service<ApplicationContext>() {
            @Override
            protected Task<ApplicationContext> createTask() {
                return new Task<ApplicationContext>() {           
                    @Override
                    protected ApplicationContext call() throws Exception {
                    	AbstractApplicationContext appContex = getContext();
                        int max = appContex.getBeanDefinitionCount();
                        updateProgress(0, max);
                        for (int k = 0; k < max; k++) {
                            Thread.sleep(50);
                            updateProgress(k+1, max);
                        }
                        return appContex;
                    }
                };
            }
        };
        
        service.start();
        
        /**
         * Initializes transaction effects
         */
        service.setOnRunning((WorkerStateEvent event) -> {
            new FadeInLeftTransition(lblWelcome).play();
            new FadeInRightTransition(lblAppTitle).play();
            new FadeInTransition(vboxBottom).play();
        });
        
        /**
         * Show Login Screen
         */
        service.setOnSucceeded((WorkerStateEvent event) -> {
        	logger.info("Calling Login Screen");
            screenHelper.newStage(stage, lblClose, "login.fxml", "Contruinte Digital - Login", true, StageStyle.TRANSPARENT, null, false, this, null, true);
        });
    } 
}
