package br.com.cd.control;

import java.net.URISyntaxException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.control.TransitionController.TransitionType;
import br.com.cd.enumeration.IssuerEnum.RegimeTributarioEnum;
import br.com.cd.model.CountyModel;
import br.com.cd.model.IssuerModel;
import br.com.cd.model.UserModel;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.ImageUtil;
import br.com.cd.util.MaskFieldUtil;
import br.com.cd.util.SecurityUtil;

/**
 * FXML Controller for Issuer'd Management
 *
 * @author lmpinheiro
 */
@Controller
public class IssuerController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(IssuerController.class);
    
	IssuerModel issuerUpdate;
	private ObservableList<IssuerModel> listIssuerObservable;
	
	@Autowired
    private ScreenConfig screenHelper;
	
	@Autowired
    private TransitionController transitionController;
	
	@Autowired
	MenuController menuController;
	
	/**
	 * Header
	 */
	@FXML
    private Text lblIssuer;
	@FXML
    private Button btnNew;
	@FXML
    private ImageView imgLoad;
	
	/**
	 * Table Pane
	 */
	@FXML
    private AnchorPane paneTable;
	@FXML
    private Button btnEnter;
	@FXML
    private Button btnAdd;
	@FXML
    private Button btnEdit;
	@FXML
    private Button btnDelete;
	@FXML
    private TableView<IssuerModel> tableIssuer;
	@FXML
    private TableColumn<IssuerModel, String> colCompanyName;
	@FXML
    private TableColumn<IssuerModel, String> colTradeName;
    @FXML
    private TableColumn <IssuerModel, String>colCnpj;
    @FXML
    private TableColumn<IssuerModel, String> colIe;
    @FXML
    private TableColumn<IssuerModel, String> colIm;
    @FXML
    private TableColumn<IssuerModel, String> colUf;
    @FXML
    private TableColumn<IssuerModel, String> colRegimeTrib;
    @FXML
    private TableColumn<IssuerModel, String> colPhoneNumber;
    @FXML
    private TableColumn<IssuerModel, String> colEmail;
    
    /**
	 * Issuer Pane
	 */
    @FXML
    private AnchorPane paneIssuer;
    @FXML
    private ImageView imgLogo;
    @FXML
    private Label lblCnpj;
    @FXML
    private TextField txtCnpj;
    @FXML
    private Label lblIe;
    @FXML
    private TextField txtIe;
    @FXML
    private Label lblCompanyName;
    @FXML
    private TextField txtCompanyName;
    @FXML
    private Label lblTradeName;
    @FXML
    private TextField txtTradeName;
    @FXML
    private Label lblIm;
    @FXML
    private TextField txtIm;
    @FXML
    private Label lblCnae;
    @FXML
    private TextField txtCnae;
    @FXML
    private Label lblIeSt;
    @FXML
    private TextField txtIeSt;
    @FXML
    private Label lblRegimeTrib;
    @FXML
    private ComboBox<RegimeTributarioEnum> cmbRegimeTrib;
    @FXML
    private Label lblUf;
    @FXML
    private ComboBox<String> cmbUF;
    @FXML
    private Label lblCounty;
    @FXML
    private ComboBox<CountyModel> cmbCounty;
    @FXML
    private Label lblZipCode;
    @FXML
    private TextField txtZipCode;
    @FXML
    private Label lblStreet;
    @FXML
    private TextField txtStreet;
    @FXML
    private Label lblNumber;
    @FXML
    private TextField txtNumber;
    @FXML
    private Label lblComplement;
    @FXML
    private TextField txtComplement;
    @FXML
    private Label lblDistrict;
    @FXML
    private TextField txtDistrict;
    @FXML
    private Label lblEmail;
    @FXML
    private TextField txtEmail;
    @FXML
    private Label lblPhoneNumber;
    @FXML
    private TextField txtPhoneNumber;
    @FXML
    private ProgressBar bar;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnUpdate;
    @FXML
    private Button btnBack;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Platform.runLater(() -> {
        	logger.info("Initializing IssuerController");
        	
        	//Initialize Issuer's observable List
            listIssuerObservable = FXCollections.observableArrayList();
            
            buttonsDisable();
            setModelColumn();
            setTooltips();
            setMaskField();
            populateCombos();
            installListeners();
            
            loadComponents();
        });
    } 
    
    
    /**
     * Action to Issuer's Login 
     */
    @FXML
    private void actEnter(ActionEvent e){
    	logger.info("Calling actEnter");
    	getIssuerService().setIssuerSession(tableIssuer.getSelectionModel().getSelectedItem());
    	menuController.refreshIssuerLogged(tableIssuer.getSelectionModel().getSelectedItem());
    	menuController.updateRightFrame("search-document.fxml");
    }
    
    
    /**
     * Action to show the Issuer's Form
     */
    @FXML
    private void actAdd(ActionEvent e){
    	logger.info("Calling actAdd");
    	
    	Platform.runLater(() -> {
            clear();
            btnSave.setVisible(true);
            btnSave.setDefaultButton(true);
            btnUpdate.setVisible(false);
            btnUpdate.setDefaultButton(false);
            transitionController.executeTransition(TransitionType.FADE_OUT_UP,TransitionType.FADE_IN_UP, paneTable, paneIssuer);
        });
    }
    
    
    /**
     * Action to show the Issuer's Form to edit 
     */
    @FXML
    private void actEdit(ActionEvent e){
    	logger.info("Calling actEdit");
    	
    	clear();
    	
    	issuerUpdate = tableIssuer.getSelectionModel().getSelectedItem();
    	
        txtCnpj.setText(issuerUpdate.getCnpj());
        txtIe.setText(issuerUpdate.getIe());
        txtCompanyName.setText(issuerUpdate.getCompanyName());
        txtTradeName.setText(issuerUpdate.getTradeName());
        txtIm.setText(issuerUpdate.getIm());
        txtCnae.setText(issuerUpdate.getCnae());
        txtIeSt.setText(issuerUpdate.getIeSt());
        cmbRegimeTrib.getSelectionModel().select(issuerUpdate.getRegimeTrib());
        cmbUF.getSelectionModel().select(issuerUpdate.getUf());
        txtZipCode.setText(issuerUpdate.getZipCode());
        txtStreet.setText(issuerUpdate.getStreet());
        txtNumber.setText(issuerUpdate.getNumber());
        txtComplement.setText(issuerUpdate.getComplement());
        txtDistrict.setText(issuerUpdate.getDistrict());
        txtEmail.setText(issuerUpdate.getEmail());
        txtPhoneNumber.setText(issuerUpdate.getPhone());
        if(issuerUpdate.getLogo()!=null)
        	imgLogo.setImage(ImageUtil.getImageFromBytes(issuerUpdate.getLogo()));
        
        populateCountyCombo(issuerUpdate.getUf());
        cmbCounty.getSelectionModel().select(issuerUpdate.getCountyModel());;
        
        btnSave.setVisible(false);
        btnSave.setDefaultButton(false);
        btnUpdate.setVisible(true);
        btnUpdate.setDefaultButton(true);
        
        transitionController.executeTransition(TransitionType.FADE_OUT_UP, TransitionType.FADE_IN_UP, paneTable, paneIssuer);
    }
    
    
    /**
     * Delete an Issuer
     */
    @FXML
    private void actDelete(ActionEvent e){
    	logger.info("Calling actDelete");
    	
    	message.clear();
    	error.clear();
    	
    	List<IssuerModel> issuersSelected = tableIssuer.getSelectionModel().getSelectedItems();
    	
    	addMessage(MessageFormat.format(MessageConstant.DELETE_CONFIRMATION, issuersSelected.size()+" Emitente(s)"));
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
        	try{
        		getIssuerService().deleteIssuers(issuersSelected);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_DELETED, issuersSelected.size()+" Emitente(s)"));
	        	screenHelper.dialogDefault("Excusão de Emitente", message, DialogType.INFORMATION);
	        	// Remove Issuer Logged
        		IssuerModel issuerSession = getIssuerService().getIssuerSession();
        		if(issuerSession!=null){
        			issuersSelected.forEach(issuerSelected -> {
        				if(issuerSelected.getId()==issuerSession.getId()){
        					menuController.removeIssuerLogged();
        				}
        			});
        		}
	        	refreshDataTable();
        	}catch(Exception ex){
        		logger.error(ex);
        		addError(ex.getMessage());
        		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        	}
        }
    }
    
    
    /**
     * Update an Issuer
     */
    @FXML
    private void actUpdate(ActionEvent e){
    	logger.info("Calling actUpdate");
    	
    	error.clear();
    	message.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	issuerUpdate = populateIssuer(issuerUpdate);
            try {
				getIssuerService().updateIssuer(issuerUpdate);
				
				message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_UPDATED, "Emitente"));
	        	screenHelper.dialogDefault("Yahooo...", message, DialogType.INFORMATION);
	        	refreshDataTable();
	        	
	        	// Update Issuer Logged
        		IssuerModel issuerSession = getIssuerService().getIssuerSession();
	        	if(issuerSession!=null){
    				if(issuerUpdate.getId()==issuerSession.getId()){
    					menuController.refreshIssuerLogged(issuerUpdate);;
    				}
        		}
	        	
	        	transitionController.executeTransition(TransitionType.FADE_OUT_UP, TransitionType.FADE_IN_UP, paneIssuer, paneTable);
	        	clear();
				
			} catch (Exception e1) {
				logger.error(e1);
        		addError(e1.getMessage());
        		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
			}
        }
    }
    
    
    /**
     * Choose a Logo
     */
    @FXML
    private void actImageChooser(ActionEvent event) {
    	logger.info("Calling actImageChooser");
    	
    	Image img = ImageUtil.chooseImgFile(paneTable.getScene().getWindow(), screenHelper);
		imgLogo.setImage(img);
    }
    
    
    /**
     * Save the Issuer on DB
     */
    @FXML
    private void actSave(ActionEvent event) {
    	logger.info("Calling actSave");
    	
    	error.clear();
    	message.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
            IssuerModel issuer = populateIssuer(null);
            
            try {
				getIssuerService().saveIssuer(issuer);
				
				message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_INSERTED, "Emitente"));
	        	screenHelper.dialogDefault("Yahooo...", message, DialogType.INFORMATION);
	        	refreshDataTable();
	        	transitionController.executeTransition(TransitionType.FADE_OUT_UP, TransitionType.FADE_IN_UP, paneIssuer, paneTable);
	        	clear();
				
			} catch (Exception e) {
				logger.error(e);
        		addError(e.getMessage());
        		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
			}
        }
    }

    
    /**
     * Exit to Issuer Form
     */
    @FXML
    private void actBack(ActionEvent event) {
    	logger.info("Calling actBack");
    	
    	Platform.runLater(() -> {
    		message.clear();
    		addMessage(MessageConstant.EXIT_CONFIRMATION);
    		if(screenHelper.dialogDefault("Atenção!!", message, DialogType.QUESTION)){
    			transitionController.executeTransition(TransitionType.FADE_OUT_UP, TransitionType.FADE_IN_UP, paneIssuer, paneTable);
    			clear();
    		}
    	});
    }
    
    
    /**
     * Populate Issuer Object 
     */
    private IssuerModel populateIssuer(IssuerModel issuer){
    	logger.info("Calling populateIssuer");
    	
    	if(issuer==null)
    		issuer = new IssuerModel();
    	
        issuer.setCnpj(MaskFieldUtil.removeMaskNumeral(txtCnpj.getText()));
        issuer.setIe(txtIe.getText());
        issuer.setCompanyName(txtCompanyName.getText());
        issuer.setTradeName(txtTradeName.getText());
        issuer.setIm(txtIm.getText());
        issuer.setCnae(txtCnae.getText());
        issuer.setIeSt(txtIeSt.getText());
        issuer.setRegimeTrib(cmbRegimeTrib.getValue());
        issuer.setUf(cmbUF.getValue().toString());
        issuer.setZipCode(MaskFieldUtil.removeMaskNumeral(txtZipCode.getText()));
        issuer.setStreet(txtStreet.getText());
        issuer.setNumber(txtNumber.getText());
        issuer.setComplement(txtComplement.getText());
        issuer.setDistrict(txtDistrict.getText());
        issuer.setEmail(txtEmail.getText());
        issuer.setPhone(MaskFieldUtil.removeMaskNumeral(txtPhoneNumber.getText()));
        
        if(imgLogo.getImage().impl_getUrl() == null || imgLogo.getImage().impl_getUrl().indexOf("logo-default.png")<=0)
        	issuer.setLogo(ImageUtil.getBytesFromImage(imgLogo.getImage()));
        		
        CountyModel countyModel = cmbCounty.getValue();
        issuer.setCountyModel(countyModel);
        
        UserModel userModel = SecurityUtil.getUserCurrent();
        userModel.getIssuerModel().add(issuer);
        
        issuer.setUserModel(userModel);
        
        return issuer;
    }
    
    
    /**
     * Fields Validation 
     */
    private void validateFields(){
    	logger.info("Calling validateFields");
    	
    	error.clear();
    	
    	/**
         * Validate if is a required field 
         */
    	if (txtCnpj.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "CNPJ"));
        }
    	if (txtIe.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Inscrição Estadual"));
        }
    	if (txtCompanyName.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Razão Social"));
        }
    	if (txtIm.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Inscrição Municipal"));
        }
    	if (cmbRegimeTrib.getSelectionModel().getSelectedIndex()<0) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Regime Tributário (Subst. Trib.)"));
        }
    	if (cmbUF.getSelectionModel().getSelectedIndex()<0) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "UF"));
        }else if(txtIe.getStyle().contains("green")){
        	try {
				DataValidateUtil.isValidIe(txtIe.getText(), cmbUF.getSelectionModel().getSelectedItem());
			} catch (Exception e) {
				addError(e.getMessage());
			}
        }
    	if (cmbCounty.getSelectionModel().getSelectedIndex()<0) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Município"));
        }
    	if (txtZipCode.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "CEP"));
        }
    	if (txtStreet.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Logradouro"));
        }
    	if (txtNumber.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Número"));
        }
    	if (txtDistrict.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Bairro"));
        }
    	
    	
    	/**
         * Validate if is a valid content 
         */
    	if (txtCnpj.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "CNPJ"));
        }
    	if (txtIe.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Inscrição Estadual"));
        }
    	if (txtIeSt.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Inscrição Estadual (Subst. Trib.)"));
        }
    	if (txtCompanyName.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Razão Social"));
        }
    	if (txtTradeName.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Nome Fantasia"));
        }
    	if (txtIm.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Inscrição Municipal"));
        }
    	if (txtCnae.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "CNAE"));
        }
    	if (txtZipCode.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "CEP"));
        }
    	if (txtStreet.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Logradouro"));
        }
    	if (txtNumber.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Número"));
        }
    	if (txtComplement.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Complemento"));
        }
    	if (txtDistrict.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Bairro"));
        }
    	if (txtPhoneNumber.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Telefone"));
        }
    	if (txtEmail.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "E-mail"));
        }
    }
    
    
    /**
     * Initialize Table Screen with buttons disabled 
     */
    private void buttonsDisable(){
    	logger.info("Calling buttonsDisable");
    	
    	btnSave.setVisible(true);
        btnUpdate.setVisible(false);
    	btnEnter.setDisable(true);
    	btnAdd.setDisable(false);
    	btnEdit.setDisable(true);
    	btnDelete.setDisable(true);
    }
    
    
    /**
     * Listeners Implementation
     */
    private void installListeners(){
    	logger.info("Calling installListeners");
    	
    	/**
         * Listeners for dynamic field validation  
         */
    	txtCnpj.setOnKeyReleased(e -> DataValidateUtil.isValidCnpj(txtCnpj, true));
    	txtIe.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtIe, DataValidateUtil.IE_PATTERN, false));
    	txtCompanyName.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCompanyName, 2, 60, false));
    	txtTradeName.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtTradeName, 1, 60, false));
    	txtIm.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtIm, 1, 15, false));
    	txtCnae.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtCnae, DataValidateUtil.CNAE_PATTERN, false));
    	txtIeSt.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtIeSt, DataValidateUtil.IEST_PATTERN, false));
    	txtZipCode.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtZipCode, DataValidateUtil.ZIPCODE_PATTERN, true));
    	txtStreet.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtStreet, 2, 60, false));
    	txtNumber.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtNumber, 1, 60, false));
    	txtComplement.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtComplement, 1, 60, false));
    	txtDistrict.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtDistrict, 2, 60, false));
    	txtEmail.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtEmail, DataValidateUtil.EMAIL_PATTERN, false));
    	txtPhoneNumber.setOnKeyReleased(e -> DataValidateUtil.isValidFormat(txtPhoneNumber, DataValidateUtil.PHONE_PATTERN, true));
    	
    	
    	/**
         * Listener for dynamic comboBox between cmbUF and cmbCounty  
         */
    	cmbUF.valueProperty().addListener((observableValue, oldValue, newValue) -> {
              if(newValue!=null && !newValue.isEmpty() && !newValue.equals(oldValue)){
            	  populateCountyCombo(newValue);
            	  cmbCounty.setDisable(false);
              }
        });
    	
    	
    	/**
         * Listener for dynamic button exhibition  
         */
    	tableIssuer.getSelectionModel().selectedItemProperty().addListener((observableValue, oldSelection, newSelection) -> {
    	    if (tableIssuer.getSelectionModel().getSelectedItems().size()==0) {
    	        btnEnter.setDisable(true);
    	        btnAdd.setDisable(false);
    	        btnEdit.setDisable(true);
    	        btnDelete.setDisable(true);
    	    }else if (tableIssuer.getSelectionModel().getSelectedItems().size()==1){
    	    	btnEnter.setDisable(false);
    	        btnAdd.setDisable(false);
    	        btnEdit.setDisable(false);
    	        btnDelete.setDisable(false);
    	    }else if (tableIssuer.getSelectionModel().getSelectedItems().size()>1){
    	    	btnEnter.setDisable(true);
    	        btnAdd.setDisable(false);
    	        btnEdit.setDisable(true);
    	        btnDelete.setDisable(false);
    	    }
    	});
    }
    
    
    /**
     * Populate combos on initialization   
     */
    private void populateCombos(){
    	logger.info("Calling populateCombos");
    	
    	//cmbCounty starts disabled and will be activated when cmbUF is selected
    	cmbCounty.setDisable(true);
    	
    	cmbRegimeTrib.getItems().addAll(RegimeTributarioEnum.values());
    	cmbUF.getItems().addAll(getStateService().findUfs());
    }
    
    
    /**
     * Populate cmbCounty after cmbUF is selected  
     */
    private void populateCountyCombo(String uf){
    	logger.info("Calling populateCountyCombo");
    	
    	cmbCounty.getItems().clear();
    	cmbCounty.getItems().addAll(getStateService().findByUf(uf));
    }
    
    
    /**
     * Implements a tooltip alert on required fields  
     */
    private void setTooltips(){
    	logger.info("Calling setRequiredFields");
    	
    	/**
         * Tooltip subtitle buttons  
         */
    	Tooltip tEnter = new Tooltip("Abrir Emitente");
    	Tooltip tAdd = new Tooltip("Cadastrar Emitente");
    	Tooltip tEdit = new Tooltip("Editar Emitente");
    	Tooltip tDelete = new Tooltip("Excluir Emitente(s)");
    	hackTooltipStartTiming(tEnter);
    	hackTooltipStartTiming(tAdd);
    	hackTooltipStartTiming(tEdit);
    	hackTooltipStartTiming(tDelete);
    	btnEnter.setTooltip(tEnter);
    	btnAdd.setTooltip(tAdd);
    	btnEdit.setTooltip(tEdit);
    	btnDelete.setTooltip(tDelete);
    	
    	/**
         * Tooltip alert on required fields  
         */
    	Tooltip t = new Tooltip(MessageConstant.REQUIRED_FIELD);
    	hackTooltipStartTiming(t);
    	
    	lblCnpj.setTooltip(t);
    	lblCompanyName.setTooltip(t);
    	lblIm.setTooltip(t);
    	lblIe.setTooltip(t);
    	lblUf.setTooltip(t);
    	lblCounty.setTooltip(t);
    	lblRegimeTrib.setTooltip(t);
    	lblZipCode.setTooltip(t);
    	lblStreet.setTooltip(t);
    	lblNumber.setTooltip(t);
    	lblDistrict.setTooltip(t);
    }
    
    
    /**
     * Define Field's Mask  
     */
    private void setMaskField(){
    	logger.info("Calling setFieldMask");
    	
    	MaskFieldUtil.cnpjField(txtCnpj);
    	MaskFieldUtil.maxTextField(txtCompanyName, 60);
    	MaskFieldUtil.maxTextField(txtTradeName, 60);
    	MaskFieldUtil.maxTextField(txtIm, 15);
    	MaskFieldUtil.numericField(txtIm);
    	MaskFieldUtil.maxTextField(txtCnae, 7);
    	MaskFieldUtil.numericField(txtCnae);
    	MaskFieldUtil.maxTextField(txtIeSt, 14);
    	MaskFieldUtil.numericField(txtIeSt);
    	MaskFieldUtil.maxTextField(txtIe, 14);
    	MaskFieldUtil.numericField(txtIe);
    	MaskFieldUtil.zipCodeField(txtZipCode);
    	MaskFieldUtil.maxTextField(txtStreet, 60);
    	MaskFieldUtil.maxTextField(txtNumber, 60);
    	MaskFieldUtil.maxTextField(txtComplement, 60);
    	MaskFieldUtil.maxTextField(txtDistrict, 60);
    	MaskFieldUtil.maxTextField(txtEmail, 256);
    	MaskFieldUtil.phoneField(txtPhoneNumber);
    }
    
    
    /**
     * Mapping columns table with issuer object
     */
    private void setModelColumn(){
    	logger.info("Calling setModelColumn");
    	
    	tableIssuer.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    	screenHelper.setModelColumn(colCompanyName, "companyName", true);
    	screenHelper.setModelColumn(colTradeName, "tradeName", false);
    	screenHelper.setModelColumn(colCnpj, "cnpjFormatted", true);
    	screenHelper.setModelColumn(colIe, "ie", true);
    	screenHelper.setModelColumn(colIm, "im", false);
        screenHelper.setModelColumn(colUf, "uf", true);
        screenHelper.setModelColumn(colRegimeTrib, "regimeTrib", true);
        screenHelper.setModelColumn(colPhoneNumber, "phoneFormatted", true);
        screenHelper.setModelColumn(colEmail, "email", false);
    }
    
    
    /**
     * Clear Form
     */
    private void clear(){
    	logger.info("Calling clear");
    	
    	try {
			imgLogo.setImage(new Image(ImageUtil.class.getResource("/br/com/cd/view/img/logo-default.png").toURI().toString(), 140, 140, false, false));
		} catch (URISyntaxException e) {
			logger.error(e);
		}
    	
    	issuerUpdate = null;
    	
    	txtCnpj.clear();
    	txtIe.clear();
    	txtCompanyName.clear();
    	txtTradeName.clear();
    	txtIm.clear();
    	txtCnae.clear();
    	txtIeSt.clear();
    	cmbRegimeTrib.getSelectionModel().clearSelection();
    	cmbUF.getSelectionModel().clearSelection();
    	cmbCounty.getSelectionModel().clearSelection();
    	txtZipCode.clear();
    	txtStreet.clear();
    	txtNumber.clear();
    	txtComplement.clear();
    	txtDistrict.clear();
    	txtEmail.clear();
    	txtPhoneNumber.clear();
    }
    
    
    /**
     * Refresh Data Table
     */
    private void refreshDataTable(){
    	logger.info("Calling refreshDataTable");
    	
        if(listIssuerObservable == null){
            listIssuerObservable =  FXCollections.observableList(getIssuerService().findAllIssuers());
        }else {
            listIssuerObservable.clear();
            listIssuerObservable.addAll(FXCollections.observableList(getIssuerService().findAllIssuers()));
        }
        tableIssuer.setItems(listIssuerObservable);
    }
    
    
    /**
     * Load data stream bar
     */
    private void loadComponents(){
    	logger.info("Calling selectWithService");
    	
        Service<Integer> service = new Service<Integer>() {
            @Override
            protected Task<Integer> createTask() {
                refreshDataTable();
                return new Task<Integer>() {           
                    @Override
                    protected Integer call() throws Exception {
                        Integer max = getIssuerService().findAllIssuers().size();
                        if (max > 35) {
                            max = 30;
                        }
                        updateProgress(0, max);
                        for (int k = 0; k < max; k++) {
                            Thread.sleep(40);
                            updateProgress(k+1, max);
                        }
                        return max;
                    }
                };
            }
        };
        service.start();
        bar.progressProperty().bind(service.progressProperty());
        service.setOnRunning((WorkerStateEvent event) -> {
            imgLoad.setVisible(true);
        });
        
        service.setOnSucceeded((WorkerStateEvent event) -> {
            imgLoad.setVisible(false);
            bar.setVisible(false);
            transitionController.executeTransition(null, TransitionType.FADE_IN_UP, null, paneTable);
            transitionController.executeTransition(null, TransitionType.FADE_IN_RIGHT, null, lblIssuer);
        });
    }
}
