package br.com.cd.control;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.cd.config.ScreenConfig;

/**
 * FXML Controller for Personalized Dialogs
 *
 * @author lmpinheiro
 */
@Controller
public class DialogController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(DialogController.class);
	
	@Autowired
    ScreenConfig screenHelper;
    
    @FXML
    private AnchorPane mainPane;
    @FXML
    private Label lblTitle;
    @FXML
    private Label lblContent;
    @FXML
    private Button btnOk;
    @FXML
    private Button btnYes;
    @FXML
    private Button btnNo;
    @FXML
    private Label lblClose;
    @FXML
    private ImageView imgLoading;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }   
        
    @FXML
    private void actOk(ActionEvent event) {
    	logger.info("Calling Dialog OK Action");
    	
    	stageParent.close();
    }
    
    @FXML
    private void actYes(ActionEvent event) {
    	logger.info("Calling Dialog YES Action");
    	
    	screenHelper.setDialogReturn(true);
    	stageParent.close();
    }
    
    @FXML
    private void actNo(ActionEvent event) {
    	logger.info("Calling Dialog NO Action");
    	
    	screenHelper.setDialogReturn(false);
    	stageParent.close();
    }
    
    /**
     * Set title from ScreenConfig
     */
    public void setTitle(String title){
    	lblTitle.setText(title);
    }
    
    /**
     * Set content from ScreenConfig
     */
    public void setContent(List<String> content){
    	String contentStr = "";

    	for(String msg : content){
    		contentStr += msg +"\n";
    		mainPane.setMinHeight(mainPane.getMinHeight()+22);
    	}
    	lblContent.setText(contentStr);
    }
    
    /**
     * Set ScreenController parent
     */
    public void setScrenConfig(ScreenConfig screenConfig){
    	this.screenHelper = screenConfig;
    }
}
