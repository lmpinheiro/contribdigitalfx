package br.com.cd.control;

import java.net.URL;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.hibernate.annotations.common.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.animations.FadeInTransition;
import br.com.cd.animations.FadeOutLeftTransition;
import br.com.cd.animations.FadeOutTransition;
import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.control.TransitionController.TransitionType;
import br.com.cd.model.IssuerModel;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefECF;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNF;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNFP;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.FormatterTextUtil;
import br.com.cd.util.MaskFieldUtil;
import br.com.cd.util.SecurityUtil;

/**
 * FXML Controller class for Menu
 *
 * @author lmpinheiro
 */
@Controller
public class FiscalReceiptReferredController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(FiscalReceiptReferredController.class);
	
	Stage stage;
	boolean exists = false;
	boolean editAction = false;
    
    @Autowired
    ScreenConfig screenHelper;
    
    @Autowired
    FiscalDocumentController fiscalDocumentController;
    
    @Autowired
    TransitionController transitionController;
    
    @FXML
    private AnchorPane pnlMain;
	@FXML
    private Button btnClose;
	@FXML
    private Label lblTitle;
	
	@FXML
    private ComboBox<String> cmbModel;
	@FXML
    private TextField txtEcf;
	@FXML
    private TextField txtCoo;
	
	@FXML
    private Button btnAdd;
	@FXML
    private Button btnUpdate;
	@FXML
    private Button btnCancel;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing MenuController");

    	Platform.runLater(() -> {
            stage = (Stage) btnClose.getScene().getWindow();
            
            setMaskField();
            populateCombos();
            installListeners();
            if(getData()!=null){
            	editAction = true;
            	fillForm((RefECF)getData());
            	btnAdd.setVisible(false);
            	btnUpdate.setVisible(true);
            }
            
            transitionController.executeTransition(null, TransitionType.BOUNCE_IN, null, pnlMain);
        });
    }    
   

    /**
     * Add Object on table
     */
    @FXML
    private void actSave(ActionEvent event) {
    	logger.info("Calling actAdd");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	RefECF refEcf = populateObject(null);
        	
        	exists = false;
        	fiscalDocumentController.listFdFiscalReceipt.forEach(item -> {
        		if(refEcf!=null && item!=null && 
    				!StringHelper.isEmpty(refEcf.getMod()) && refEcf.getMod().equals(item.getMod()) &&
    				!StringHelper.isEmpty(refEcf.getNCOO()) && refEcf.getNCOO().equals(item.getNCOO()) &&
    				!StringHelper.isEmpty(refEcf.getNECF()) && refEcf.getNECF().equals(item.getNECF())){
        			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Cupom Fiscal"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
	        	fiscalDocumentController.listFdFiscalReceipt.add(refEcf);
	        	fiscalDocumentController.tableFdFiscalReceipt.setItems(fiscalDocumentController.listFdFiscalReceipt);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_INSERTED, "Cupom Fiscal"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    
    
    /**
     * Update object on table
     */
    @FXML
    private void actUpdate(ActionEvent event) {
    	logger.info("Calling actUpdate");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	RefECF refEcfOld = (RefECF)getData();
        	ObservableList<RefECF> listFdProducerRefereced = FXCollections.observableArrayList(fiscalDocumentController.listFdFiscalReceipt);
        	listFdProducerRefereced.remove(refEcfOld);
        	
        	RefECF refEcfCurrent = populateObject(null);
        	
        	exists = false;
        	listFdProducerRefereced.forEach(item -> {
        		if(refEcfCurrent!=null && item!=null && 
        				!StringHelper.isEmpty(refEcfCurrent.getMod()) && refEcfCurrent.getMod().equals(item.getMod()) &&
        				!StringHelper.isEmpty(refEcfCurrent.getNCOO()) && refEcfCurrent.getNCOO().equals(item.getNCOO()) &&
        				!StringHelper.isEmpty(refEcfCurrent.getNECF()) && refEcfCurrent.getNECF().equals(item.getNECF())){
            			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Cupom Fiscal"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
        		
        		RefECF refEcf = populateObject((RefECF)getData());
        		
        		fiscalDocumentController.listFdFiscalReceipt.set(fiscalDocumentController.tableFdFiscalReceipt.getSelectionModel().getSelectedIndex(), refEcf);
	        	fiscalDocumentController.tableFdFiscalReceipt.setItems(fiscalDocumentController.listFdFiscalReceipt);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_UPDATED, "Cupom Fiscal"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    

    /**
     * Close Pane
     */
    @FXML
    private void actClose(ActionEvent event) {
    	logger.info("Calling actClose");
    	
    	message.clear();
    	
    	addMessage(MessageConstant.EXIT_CONFIRMATION);
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
        	stage.close();
        }
    }


    /**
     * Listeners Implementation
     */
    private void installListeners(){
    	txtEcf.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtEcf, 1, 3, false));
    	txtCoo.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtCoo, 1, 6, false));
    }
    
    
    /**
     * Populate combos on initialization   
     */
    private void populateCombos(){
    	logger.info("Calling populateCombos");
    	
    	cmbModel.getItems().addAll("2B","2C","2D");
    }
    
    
    /**
     * Populate object form  
     */
    private RefECF populateObject(RefECF refECF){
    	logger.info("Calling populateObject");
    	
    	if(refECF==null)refECF = new RefECF();
    	
    	refECF.setMod(cmbModel.getSelectionModel().getSelectedItem());
    	refECF.setNCOO(txtCoo.getText());
    	refECF.setNECF(txtEcf.getText());
    	
    	return refECF;
    }
    
    
    /**
     * Fill Form  
     */
    private void fillForm(RefECF refECF){
    	logger.info("Calling fillForm");
    	
    	
		cmbModel.getSelectionModel().select(refECF.getMod());
		txtCoo.setText(refECF.getNCOO());
		txtEcf.setText(refECF.getNECF());
    }
    
    
    /**
     * Define Field's Mask  
     */
    private void setMaskField(){
    	logger.info("Calling setFieldMask");
    	
    	MaskFieldUtil.numericField(txtCoo);
    	MaskFieldUtil.maxTextField(txtCoo, 6);
    	MaskFieldUtil.numericField(txtEcf);
    	MaskFieldUtil.maxTextField(txtEcf, 3);
    }
    
    /**
     * Fields Validation 
     */
    private void validateFields(){
    	logger.info("Calling validateFields");
    	
    	error.clear();
    	
    	
		/**
         * Validate if is a required field 
         */
		if (cmbModel.getSelectionModel().getSelectedIndex()<0) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Modelo"));
        }
		if (txtEcf.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Número ECF"));
        }
		if (txtCoo.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Número COO"));
        }
		
		
		/**
         * Validate if is a valid content 
         */
		if (cmbModel.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Modelo"));
        }
		if (txtEcf.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Número ECF"));
        }
		if (txtCoo.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Número COO"));
        }
    }
}
