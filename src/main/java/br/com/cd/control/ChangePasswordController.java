package br.com.cd.control;

import java.net.URL;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.hibernate.internal.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.model.UserModel;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.SecurityUtil;

/**
 * FXML Controller for change password screen
 *
 * @author lmpinheiro
 */

@Controller
public class ChangePasswordController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(ChangePasswordController.class);
	
	private Stage stage;
	private UserModel user;
	
	@Autowired
    ScreenConfig screenHelper;
	
	@FXML
    private Text lblWelcome;
    @FXML
    private Text lblChangePassword;
    @FXML
    private Text lblNewPassword;
    @FXML
    private Text lblNewPasswordAgain;
    @FXML
    private PasswordField txtNewPassword;
    @FXML
    private PasswordField txtNewPasswordAgain;
    @FXML
    private Button btnRedefine;
    @FXML
    private Button btnCancel;
    @FXML
    private Text lblCopyright;
    @FXML 
    private Label lblClose;
    @FXML 
    private Label lblHelp;
    @FXML 
    private Label lblHelpAgain;
    
    
    /**
     * Initializes transaction effects.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing ChangePasswordController");
    	
        Platform.runLater(() -> {
            new FadeInLeftTransition(lblWelcome).play();
            new FadeInLeftTransition(lblChangePassword).play();
            new FadeInLeftTransition(lblNewPassword).play();
            new FadeInLeftTransition(lblNewPasswordAgain).play();
            new FadeInRightTransition(txtNewPassword).play();
            new FadeInRightTransition(txtNewPasswordAgain).play();
            new FadeInRightTransition(lblHelp).play();
            new FadeInRightTransition(lblHelpAgain).play();
            new FadeInRightTransition(btnRedefine).play();
            new FadeInRightTransition(btnCancel).play();
            lblClose.setOnMouseClicked((MouseEvent event) -> {
                Platform.exit();
                System.exit(0);
            });
            
            Tooltip t = new Tooltip(MessageConstant.PASSWORD_PATTERN);
            hackTooltipStartTiming(t);
            
            txtNewPassword.setTooltip(t);
            txtNewPasswordAgain.setTooltip(t);
            lblHelp.setTooltip(t);
            lblHelpAgain.setTooltip(t);
        });
    }    
    
    
    /**
     * Cancel operation and return to Login Screen
     */
    @FXML
    private void actCancel(ActionEvent event) {
    	logger.info("Calling Cancel Action");
    	
        screenHelper.newStage(stage, lblClose, "login.fxml", "Contribuinte Digital - Login", true, StageStyle.TRANSPARENT, null, false, this, null, true);
    }

    
    /**
     * Set a new user's password 
     */ 
    @FXML		 
    private void actRedefine(ActionEvent event) {
    	logger.info("Calling Redefine Password Action");
    	
    	error.clear();
    	message.clear();
    	
    	if(StringHelper.isEmpty(txtNewPassword.getText())){
    		addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Nova Senha"));
    	}
    	if(StringHelper.isEmpty(txtNewPasswordAgain.getText())){
    		addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Repita a Senha"));
    	}
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
    	}else if(!txtNewPassword.getText().equals(txtNewPasswordAgain.getText())){
    		addError(MessageConstant.PASSWORD_DO_NOT_MATCH);
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
    	}else if(!DataValidateUtil.isValidPassword(txtNewPassword.getText())){
    		addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Senha"));
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
    	}else{
    		user = (UserModel)getData();
    		user.setPass(SecurityUtil.generateHash(txtNewPassword.getText()));
    		getUserService().updateUser(user);
    		addMessage(MessageFormat.format(MessageConstant.RECORD_UPDATED, "Senha"));
    		screenHelper.dialogDefault("Yahooo!", message, DialogType.INFORMATION);
    		screenHelper.newStage(stage, lblClose, "login.fxml", "Contribuinte Digital - Login", true, StageStyle.TRANSPARENT, null, false, this, null, true);
    	}
    }
    
}
