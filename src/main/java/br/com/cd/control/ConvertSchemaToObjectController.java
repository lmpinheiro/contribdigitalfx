package br.com.cd.control;


import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.config.ScreenConfig;
import br.com.cd.util.SchemaConverterUtil;

/**
 * FXML Controller for Convert Schema to Object
 *
 * @author lmpinheiro
 */
@Deprecated
@Controller
public class ConvertSchemaToObjectController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(ConvertSchemaToObjectController.class);
	
	@Autowired
    ScreenConfig screenHelper;
	
	@FXML
    private Text lblWelcome;
	@FXML
    private TextField txtJarName;
    @FXML
    private TextField txtSchemaDirectory;
    @FXML
    private Text lblJarName;
    @FXML
    private Text lblSchemaDirectory;
    @FXML
    private Button btnChooseDirectory;
    @FXML
    private Button btnGenerateObjects;
    @FXML
    private Text lblCopyright;
    @FXML 
    private Label lblClose; 
    
    
    /**
     * Initializes transaction effects.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing LoginController");
    	
        Platform.runLater(() -> {
            new FadeInLeftTransition(lblWelcome).play();
            new FadeInLeftTransition(lblJarName).play();
            new FadeInLeftTransition(lblSchemaDirectory).play();
            new FadeInLeftTransition(txtJarName).play();
            new FadeInLeftTransition(txtSchemaDirectory).play();
            new FadeInRightTransition(btnChooseDirectory).play();
            new FadeInRightTransition(btnGenerateObjects).play();
            lblClose.setOnMouseClicked((MouseEvent event) -> {
                Platform.exit();
                System.exit(0);
            });
        });
    }    

    
    /**
     * Create POJOs about the XML Schema
     */
    @FXML
    private void actGenerateObjects(ActionEvent event) {
    	logger.info("Calling actGenerateObjects");
    	
    	SchemaConverterUtil xmlBeans = new SchemaConverterUtil();
        xmlBeans.gerarJar(txtSchemaDirectory.getText(),txtJarName.getText());
    }

    
    /**
     * Choose XML Schema Repository
     */
    @FXML
    private void actChooseDirectory(ActionEvent event) {
    	logger.info("Calling actChooseDirectory");
    	
    	DirectoryChooser chooser = new DirectoryChooser();
    	chooser.setTitle("Escolha o repositório do Schema");
    	File selectedDirectory = chooser.showDialog(lblClose.getScene().getWindow());
    	if(selectedDirectory!=null)
    		txtSchemaDirectory.setText(selectedDirectory.getAbsolutePath());
    }
}
