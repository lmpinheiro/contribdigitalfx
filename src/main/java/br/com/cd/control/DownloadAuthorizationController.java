package br.com.cd.control;

import java.net.URL;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import org.apache.log4j.Logger;
import org.hibernate.annotations.common.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.control.TransitionController.TransitionType;
import br.com.cd.nfe.TNFe.InfNFe.AutXML;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.MaskFieldUtil;

/**
 * FXML Controller class for Menu
 *
 * @author lmpinheiro
 */
@Controller
public class DownloadAuthorizationController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(DownloadAuthorizationController.class);
	
	Stage stage;
	boolean exists = false;
	boolean editAction = false;
    
    @Autowired
    ScreenConfig screenHelper;
    
    @Autowired
    FiscalDocumentController fiscalDocumentController;
    
    @Autowired
    TransitionController transitionController;
    
    @FXML
    private AnchorPane pnlMain;
	@FXML
    private Button btnClose;
	@FXML
    private Label lblTitle;
	@FXML
    private RadioButton rdDocumentTypeCpf;
	@FXML
    private RadioButton rdDocumentTypeCnpj;
	@FXML
    private TextField txtCpfNumber;
	@FXML
    private TextField txtCnpjNumber;
	
	@FXML
    private Button btnAdd;
	@FXML
    private Button btnUpdate;
	@FXML
    private Button btnCancel;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing MenuController");
    	
    	Platform.runLater(() -> {
            stage = (Stage) btnClose.getScene().getWindow();
            
            setMaskField();
            installListeners();
            if(getData()!=null){
            	editAction = true;
            	fillForm((AutXML)getData());
            	btnAdd.setVisible(false);
            	btnUpdate.setVisible(true);
            }
            
            transitionController.executeTransition(null, TransitionType.BOUNCE_IN, null, pnlMain);
        });
    }    
   

    /**
     * Add Object on table
     */
    @FXML
    private void actSave(ActionEvent event) {
    	logger.info("Calling actAdd");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	AutXML downloadAuthorization = populateObject(null);
        	
        	exists = false;
        	fiscalDocumentController.listDownloadAuthorization.forEach(item -> {
        		if(downloadAuthorization!=null && item!=null && (
    				(!StringHelper.isEmpty(downloadAuthorization.getCNPJ()) && downloadAuthorization.getCNPJ().equals(item.getCNPJ())) ||
					(!StringHelper.isEmpty(downloadAuthorization.getCPF()) && downloadAuthorization.getCPF().equals(item.getCPF())))){
        			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Autorização de Download"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
	        	fiscalDocumentController.listDownloadAuthorization.add(downloadAuthorization);
	        	fiscalDocumentController.tableDownloadAuthorization.setItems(fiscalDocumentController.listDownloadAuthorization);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_INSERTED, "Autorização de Download"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    
    
    /**
     * Update object on table
     */
    @FXML
    private void actUpdate(ActionEvent event) {
    	logger.info("Calling actUpdate");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	AutXML downloadAuthorizationOld = (AutXML)getData();
        	ObservableList<AutXML> listDownloadAuthorization = FXCollections.observableArrayList(fiscalDocumentController.listDownloadAuthorization);
        	listDownloadAuthorization.remove(downloadAuthorizationOld);
        	
        	AutXML downloadAuthorizationCurrent = populateObject(null);
        	
        	exists = false;
        	listDownloadAuthorization.forEach(item -> {
        		if(downloadAuthorizationCurrent!=null && item!=null && (
        				(!StringHelper.isEmpty(downloadAuthorizationCurrent.getCNPJ()) && downloadAuthorizationCurrent.getCNPJ().equals(item.getCNPJ())) ||
        				(!StringHelper.isEmpty(downloadAuthorizationCurrent.getCPF()) && downloadAuthorizationCurrent.getCPF().equals(item.getCPF())))){
        			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Autorização de Download"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
        		
        		AutXML downloadAuthorization = populateObject((AutXML)getData());
        		
        		fiscalDocumentController.listDownloadAuthorization.set(fiscalDocumentController.tableDownloadAuthorization.getSelectionModel().getSelectedIndex(), downloadAuthorization);
	        	fiscalDocumentController.tableDownloadAuthorization.setItems(fiscalDocumentController.listDownloadAuthorization);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_UPDATED, "Autorização de Download"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    

    /**
     * Close Pane
     */
    @FXML
    private void actClose(ActionEvent event) {
    	logger.info("Calling actClose");
    	
    	message.clear();
    	
    	addMessage(MessageConstant.EXIT_CONFIRMATION);
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
        	stage.close();
        }
    }


    /**
     * Listeners Implementation
     */
    private void installListeners(){
    	rdDocumentTypeCnpj.selectedProperty().addListener((observableValue,  wasPreviouslySelected, isNowSelected) ->{
	        if (isNowSelected) { 
	        	transitionController.executeTransition(TransitionType.FADE_OUT_DOWN, TransitionType.FADE_IN_DOWN, txtCpfNumber, txtCnpjNumber);
	        	txtCnpjNumber.clear();
	        	txtCnpjNumber.setStyle("-fx-text-fill:black");
	        }else{
	        	transitionController.executeTransition(TransitionType.FADE_OUT_DOWN, TransitionType.FADE_IN_DOWN, txtCnpjNumber, txtCpfNumber);
	        	txtCpfNumber.clear();
	        	txtCpfNumber.setStyle("-fx-text-fill:black");
	        }
    	});
    	
    	txtCnpjNumber.setOnKeyReleased(e -> DataValidateUtil.isValidCnpj(txtCnpjNumber, true));
    	txtCpfNumber.setOnKeyReleased(e -> DataValidateUtil.isValidCpf(txtCpfNumber, true));
    }
    
    
    
    /**
     * Populate object form  
     */
    private AutXML populateObject(AutXML downloadAuthorization){
    	logger.info("Calling populateObject");
    	
    	if(downloadAuthorization==null)downloadAuthorization = new AutXML();
    	
    	if(rdDocumentTypeCpf.isSelected()){
    		downloadAuthorization.setCPF(MaskFieldUtil.removeMaskNumeral(txtCpfNumber.getText()));
    		downloadAuthorization.setCNPJ(null);
    	}else if(rdDocumentTypeCnpj.isSelected()){
    		downloadAuthorization.setCNPJ(MaskFieldUtil.removeMaskNumeral(txtCnpjNumber.getText()));
    		downloadAuthorization.setCPF(null);
    	}
    	
    	return downloadAuthorization;
    }
    
    
    /**
     * Fill Form  
     */
    private void fillForm(AutXML downloadAuthorization){
    	logger.info("Calling fillForm");
    	
    	if(downloadAuthorization.getCNPJ()!=null){
    		rdDocumentTypeCnpj.setSelected(true);
    		txtCnpjNumber.setText(downloadAuthorization.getCNPJ());
    	}else if(downloadAuthorization.getCPF()!=null){
    		rdDocumentTypeCpf.setSelected(true);
    		txtCpfNumber.setText(downloadAuthorization.getCPF());
    	}
    }
    
    
    /**
     * Define Field's Mask  
     */
    private void setMaskField(){
    	logger.info("Calling setFieldMask");
    	
    	MaskFieldUtil.cnpjField(txtCnpjNumber);
    	MaskFieldUtil.cpfField(txtCpfNumber);
    }
    
    /**
     * Fields Validation 
     */
    private void validateFields(){
    	logger.info("Calling validateFields");
    	
    	error.clear();
    	
    	if(rdDocumentTypeCpf.isSelected()){
    		/**
             * Validate if is a required field 
             */
    		if (txtCpfNumber.getText().isEmpty()) {
            	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "CPF"));
            }
    		
    		/**
             * Validate if is a valid content 
             */
        	if (txtCpfNumber.getStyle().contains("red")) {
            	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "CPF"));
            }
    	}
    	
    	if(rdDocumentTypeCnpj.isSelected()){
    		/**
             * Validate if is a required field 
             */
    		if (txtCnpjNumber.getText().isEmpty()) {
            	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "CNPJ"));
            }
    		
    		/**
             * Validate if is a valid content 
             */
        	if (txtCnpjNumber.getStyle().contains("red")) {
            	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "CNPJ"));
            }
    	}
		
    	
    }
}
