package br.com.cd.control;

import java.net.URL;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.hibernate.annotations.common.util.StringHelper;
import org.omg.CORBA.REBIND;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Controller;

import br.com.cd.animations.FadeInLeftTransition;
import br.com.cd.animations.FadeInRightTransition;
import br.com.cd.animations.FadeInTransition;
import br.com.cd.animations.FadeOutLeftTransition;
import br.com.cd.animations.FadeOutTransition;
import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;
import br.com.cd.control.TransitionController.TransitionType;
import br.com.cd.model.IssuerModel;
import br.com.cd.nfe.TNFe.InfNFe.Det.Prod.VeicProd;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefECF;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNF;
import br.com.cd.nfe.TNFe.InfNFe.Ide.NFref.RefNFP;
import br.com.cd.nfe.TNFe.InfNFe.Transp;
import br.com.cd.nfe.TUf;
import br.com.cd.nfe.TVeiculo;
import br.com.cd.util.DataValidateUtil;
import br.com.cd.util.FormatterTextUtil;
import br.com.cd.util.MaskFieldUtil;
import br.com.cd.util.SecurityUtil;

/**
 * FXML Controller class for Menu
 *
 * @author lmpinheiro
 */
@Controller
public class TrailerTransportController extends ScreenController implements Initializable {
	private static final Logger logger = Logger.getLogger(TrailerTransportController.class);
	
	Stage stage;
	boolean exists = false;
	boolean editAction = false;
    
    @Autowired
    ScreenConfig screenHelper;
    
    @Autowired
    FiscalDocumentController fiscalDocumentController;
    
    @Autowired
    TransitionController transitionController;
    
    @FXML
    private AnchorPane pnlMain;
	@FXML
    private Button btnClose;
	@FXML
    private Label lblTitle;
	
	@FXML
    private ComboBox<String> cmbUf;
	@FXML
    private TextField txtLicensePlate;
	@FXML
    private TextField txtRntc;
	
	@FXML
    private Button btnAdd;
	@FXML
    private Button btnUpdate;
	@FXML
    private Button btnCancel;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	logger.info("Initializing MenuController");

    	Platform.runLater(() -> {
            stage = (Stage) btnClose.getScene().getWindow();
            
            setMaskField();
            populateCombos();
            installListeners();
            if(getData()!=null){
            	editAction = true;
            	fillForm((TVeiculo)getData());
            	btnAdd.setVisible(false);
            	btnUpdate.setVisible(true);
            }
            
            transitionController.executeTransition(null, TransitionType.BOUNCE_IN, null, pnlMain);
        });
    }    
   

    /**
     * Add Object on table
     */
    @FXML
    private void actSave(ActionEvent event) {
    	logger.info("Calling actAdd");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	TVeiculo trailer = populateObject(null);
        	
        	exists = false;
        	fiscalDocumentController.listTrailer.forEach(item -> {
        		if(trailer!=null && item!=null && 
    				!StringHelper.isEmpty(trailer.getPlaca()) && trailer.getPlaca().equals(item.getPlaca()) &&
    				trailer.getUF()!=null && trailer.getUF().equals(item.getUF()) &&
    				trailer.getRNTC().equals(item.getRNTC())){
        			exists = true;
        		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Reboque"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
	        	fiscalDocumentController.listTrailer.add(trailer);
	        	fiscalDocumentController.tableTrailer.setItems(fiscalDocumentController.listTrailer);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_INSERTED, "Reboque"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    
    
    /**
     * Update object on table
     */
    @FXML
    private void actUpdate(ActionEvent event) {
    	logger.info("Calling actUpdate");
    	
    	error.clear();
    	
    	validateFields();
    	
    	if(hasError()){
    		screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
        }else{
        	
        	TVeiculo trailerOld = (TVeiculo)getData();
        	ObservableList<TVeiculo> listTrailer = FXCollections.observableArrayList(fiscalDocumentController.listTrailer);
        	listTrailer.remove(trailerOld);
        	
        	TVeiculo trailerCurrent = populateObject(null);
        	
        	exists = false;
        	listTrailer.forEach(item -> {
        		if(trailerCurrent!=null && item!=null && 
        				!StringHelper.isEmpty(trailerCurrent.getPlaca()) && trailerCurrent.getPlaca().equals(item.getPlaca()) &&
        				trailerCurrent.getUF()!=null && trailerCurrent.getUF().equals(item.getUF()) &&
        						trailerCurrent.getRNTC().equals(item.getRNTC())){
            			exists = true;
            		}
        	});
        	if(exists){
        		error.clear();
        		addError(MessageFormat.format(MessageConstant.ALREADY_INSERTED, "Reboque"));
        		screenHelper.dialogDefault("Opss!", error , DialogType.ERROR);
        	}else{
        		
        		TVeiculo trailer = populateObject((TVeiculo)getData());
        		
        		fiscalDocumentController.listTrailer.set(fiscalDocumentController.tableTrailer.getSelectionModel().getSelectedIndex(), trailer);
	        	fiscalDocumentController.tableTrailer.setItems(fiscalDocumentController.listTrailer);
	        	message.clear();
	        	addMessage(MessageFormat.format(MessageConstant.RECORD_UPDATED, "Reboque"));
	        	stage.close();
	        	screenHelper.dialogDefault("Obaa", message , DialogType.INFORMATION);
        	}
        }
    }
    

    /**
     * Close Pane
     */
    @FXML
    private void actClose(ActionEvent event) {
    	logger.info("Calling actClose");
    	
    	message.clear();
    	
    	addMessage(MessageConstant.EXIT_CONFIRMATION);
        if(screenHelper.dialogDefault("Tem certeza?", message, DialogType.QUESTION)){
        	stage.close();
        }
    }


    /**
     * Listeners Implementation
     */
    private void installListeners(){
    	txtLicensePlate.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtLicensePlate, 1, 7, true));
    	txtRntc.setOnKeyReleased(e -> DataValidateUtil.isValidSizeTextField(txtRntc, 1, 20, false));
    }
    
    
    /**
     * Populate combos on initialization   
     */
    private void populateCombos(){
    	logger.info("Calling populateCombos");
    	
    	cmbUf.getItems().addAll(getStateService().findUfs());
    }
    
    
    /**
     * Populate object form  
     */
    private TVeiculo populateObject(TVeiculo trailer){
    	logger.info("Calling populateObject");
    	
    	if(trailer==null)trailer = new TVeiculo();
    	
    	trailer.setUF(TUf.fromValue(cmbUf.getSelectionModel().getSelectedItem()));
    	trailer.setRNTC(txtRntc.getText());
    	trailer.setPlaca(MaskFieldUtil.removeMask(txtLicensePlate.getText()));
    	
    	return trailer;
    }
    
    
    /**
     * Fill Form  
     */
    private void fillForm(TVeiculo trailer){
    	logger.info("Calling fillForm");
    	
    	cmbUf.getSelectionModel().select(trailer.getUF().toString());
		txtRntc.setText(trailer.getRNTC());
		txtLicensePlate.setText(trailer.getPlaca());
    }
    
    
    /**
     * Define Field's Mask  
     */
    private void setMaskField(){
    	logger.info("Calling setFieldMask");
    	
    	MaskFieldUtil.licensePlateField(txtLicensePlate);
    	MaskFieldUtil.maxTextField(txtRntc, 20);
    }
    
    /**
     * Fields Validation 
     */
    private void validateFields(){
    	logger.info("Calling validateFields");
    	
    	error.clear();
    	
    	
		/**
         * Validate if is a required field 
         */
		if (cmbUf.getSelectionModel().getSelectedIndex()<0) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "UF"));
        }
		if (txtLicensePlate.getText().isEmpty()) {
        	addError(MessageFormat.format(MessageConstant.PARTICULAR_REQUIRED_FIELD, "Placa do Veículo"));
        }
		
		
		/**
         * Validate if is a valid content 
         */
		if (cmbUf.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "UF"));
        }
		if (txtLicensePlate.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "Placa do Veículo"));
        }
		if (txtRntc.getStyle().contains("red")) {
        	addError(MessageFormat.format(MessageConstant.INVALID_CONTENT, "RNTC"));
        }
    }
}
