package br.com.cd.enumeration;

public class FiscalDocumentEnum {
	
	public static enum AttendanceTypeEnum { 
		NOT_APPLICABLE(0, "Não se aplica"),
		FACE_OPERATION(1, "Operação Presencial"),
		INTERNET(2, "Operação via Internet"),
		TELEMARKETING(3, "Operação via Teleatendimento"),
		OTHER(9, "Operação Não Presencial - Outros");
		
		private int code;
		private String description;
	    
		AttendanceTypeEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static AttendanceTypeEnum getEnumByCode(int code) {
			for(AttendanceTypeEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	
	
	public static enum CfopEnum { 
		CFOP_1100(1100, "Compras para industrialização, comercialização ou prestação de serviços"),
		CFOP_1150(1150, "Transferências para industrialização, comercialização ou prestação de serviços"),
		CFOP_1200(1200, "Devoluções de vendas de produção própria, de terceiros ou anulações de valores"),
		CFOP_1250(1250, "Compras de energia elétrica"),
		CFOP_1300(1300, "Aquisições de serviços de comunicação"),
		CFOP_1350(1350, "Aquisições de serviços de transporte"),
		CFOP_1400(1400, "Entradas de mercadorias sujeitas ao regime de substituição tributária"),
		CFOP_1450(1450, "Sistemas de integração"),
		CFOP_1500(1500, "Entradas de mercadorias remetidas com fim específico de exportação e eventuais devoluções"),
		CFOP_1550(1550, "Operações com bens de ativo imobilizado e materiais para uso ou consumo"),
		CFOP_1600(1600, "Créditos e ressarcimentos de ICMS"),
		CFOP_1650(1650, "Entradas de combustíveis, derivados ou não de petróleo e lubrificantes"),
		CFOP_1900(1900, "Outras entradas de mercadorias ou aquisições de serviços"),
		CFOP_2000(2000, "Entradas ou aquisições de serviços de outros Estados"),
		CFOP_2100(2100, "Compras para industrialização, comercialização ou prestação de serviços"),
		CFOP_2150(2150, "Transferências para industrialização, comercialização ou prestação de serviços"),
		CFOP_2200(2200, "Devoluções de vendas de produção própria, de terceiros ou anulações de valores"),
		CFOP_2250(2250, "Compras de energia elétrica"),
		CFOP_2300(2300, "Aquisições de serviços de comunicação"),
		CFOP_2350(2350, "Aquisições de serviços de transporte"),
		CFOP_2400(2400, "Entradas de mercadorias sujeitas ao regime de substituição tributária"),
		CFOP_2500(2500, "Entradas de mercadorias remetidas com fim específico de exportação e eventuais devoluções"),
		CFOP_2550(2550, "Operações com bens de ativo imobilizado e materiais para uso ou consumo"),
		CFOP_2600(2600, "Créditos e ressarcimentos de ICMS"),
		CFOP_2650(2650, "Entradas de combustíveis, derivados ou não de petróleo e lubrificantes"),
		CFOP_2900(2900, "Outras entradas de mercadorias ou aquisições de serviços"),
		CFOP_3000(3000, "Entradas ou aquisições de serviços do Exterior"),
		CFOP_3100(3100, "Compras para industrialização, comercialização ou prestação de serviços"),
		CFOP_3200(3200, "Devoluções de vendas de produção própria, de terceiros ou anulações de valores"),
		CFOP_3250(3250, "Compras de energia elétrica"),
		CFOP_3300(3300, "Aquisições de serviços de comunicação"),
		CFOP_3350(3350, "Aquisições de serviços de transporte"),
		CFOP_3500(3500, "Entradas de mercadorias remetidas com fim específico de exportação e eventuais devoluções"),
		CFOP_3550(3550, "Operações com bens de ativo imobilizado e materiais para uso ou consumo"),
		CFOP_3650(3650, "Entradas de combustíveis, derivados ou não de petróleo e lubrificantes"),
		CFOP_3900(3900, "Outras entradas de mercadorias ou aquisições de serviços"),
		CFOP_5000(5000, "Saídas ou prestações de serviços para o Estado"),
		CFOP_5100(5100, "Vendas de produção própria ou de terceiros"),
		CFOP_5150(5150, "Transferências de produção própria ou de terceiros"),
		CFOP_5200(5200, "Devoluções de compras para industrialização, comercialização ou anulações de valores"),
		CFOP_5250(5250, "Vendas de energia elétrica"),
		CFOP_5300(5300, "Prestações de serviços de comunicação"),
		CFOP_5350(5350, "Prestações de serviços de transporte"),
		CFOP_5400(5400, "Saídas de mercadorias sujeitas ao regime de substituição tributária"),
		CFOP_5450(5450, "Sistemas de integração"),
		CFOP_5500(5500, "Remessas com fim específico de exportação e eventuais devoluções"),
		CFOP_5550(5550, "Operações com bens de ativo imobilizado e materiais para uso ou consumo"),
		CFOP_5600(5600, "Créditos e ressarcimentos de ICMS"),
		CFOP_5650(5650, "Saídas de combustíveis, derivados ou não de petróleo e lubrificantes"),
		CFOP_5900(5900, "Outras saídas de mercadorias ou prestações de serviços"),
		CFOP_6000(6000, "Saídas ou prestações de serviços para outros Estados"),
		CFOP_6100(6100, "Vendas de produção própria ou de terceiros"),
		CFOP_6150(6150, "Transferências de produção própria ou de terceiros"),
		CFOP_6200(6200, "Devoluções de compras para industrialização, comercialização ou anulações de valores"),
		CFOP_6250(6250, "Vendas de energia elétrica"),
		CFOP_6300(6300, "Prestações de serviços de comunicação"),
		CFOP_6350(6350, "Prestações de serviços de transporte"),
		CFOP_6400(6400, "Saídas de mercadorias sujeitas ao regime de substituição tributária"),
		CFOP_6500(6500, "Remessas com fim específico de exportação e eventuais devoluções"),
		CFOP_6550(6550, "Operações com bens de ativo imobilizado e materiais para uso ou consumo"),
		CFOP_6600(6600, "Créditos e ressarcimentos de ICMS"),
		CFOP_6650(6650, "Saídas de combustíveis, derivados ou não de petróleo e lubrificantes"),
		CFOP_6900(6900, "Outras saídas de mercadorias ou prestações de serviços"),
		CFOP_7000(7000, "Saídas ou prestações de serviços para o Exterior"),
		CFOP_7100(7100, "Vendas de produção própria ou de terceiros"),
		CFOP_7200(7200, "Devoluções de compras para industrialização, comercialização ou anulações de valores"),
		CFOP_7250(7250, "Vendas de energia elétrica"),
		CFOP_7300(7300, "Prestações de serviços de comunicação"),
		CFOP_7350(7350, "Prestações de serviços de transporte"),
		CFOP_7500(7500, "Exportação de mercadorias recebidas com fim específico de exportação"),
		CFOP_7550(7550, "Operações com bens de ativo imobilizado e materiais para uso ou consumo"),
		CFOP_7650(7650, "Saídas de combustíveis, derivados ou não de petróleo e lubrificantes"),
		CFOP_7900(7900, "Outras entradas de mercadorias ou aquisições de serviços");
		
		private int code;
		private String description;
	    
		CfopEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return Integer.toString(getCode());
	    }
	    
		public static CfopEnum getEnumByCode(int code) {
			for(CfopEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}

	
	public static enum DocumentTypeEnum { 
		INPUT(0, "Entrada"),
	    OUTPUT(1, "Saída");
		
		private int code;
		private String description;
	    
		DocumentTypeEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
	    
		public static DocumentTypeEnum getEnumByCode(int code) {
			for(DocumentTypeEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	
	
	public static enum IssueTypeEnum { 
		NORMAL(1, "Normal", "Normal"),
	    CONTINGENCY(2, "Contingência FS-IA", "Cont. FS-IA"),
	    CONTINGENCY_SCAN(3,"Contingência com SCAN", "Cont. SCAN"),
	    CONTINGENCY_EPEC(4,"Contingência via EPEC", "Cont. EPEC"),
	    CONTINGENCY_FS_DA(5,"Contingência FS-DA", "Cont. FS-DA"),
	    CONTINGENCY_SVC_AN(6,"Contingência SVC-AN", "Cont. SVC-AN"),
	    CONTINGENCY_SVC_RS(7,"Contingência SVC-RS", "Cont. SVC-RS");
		
		private int code;
		private String description;
		private String resume;
	    
	    IssueTypeEnum (int code, String description, String resume) {
	        this.code = code;
	        this.description = description;
	        this.resume = resume;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String getResume() {
	        return resume;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
	    
		public static IssueTypeEnum getEnumByCode(int code) {
			for(IssueTypeEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	
	
	public static enum PaymentMethodsEnum { 
		CASH_PAYMENT(0, "Pagamento à vista"),
		DEFERRED_PAYMENT(1, "Pagamento à prazo"),
		OTHER_PAYMENT(2, "Outros");
		
		private int code;
		private String description;
	    
		PaymentMethodsEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
	    
		public static PaymentMethodsEnum getEnumByCode(int code) {
			for(PaymentMethodsEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	
	public static enum ProcessOriginEnum { 
		SEFAZ(0, "SEFAZ"),
		FEDERA_COURT(1, "Justiça Federal"),
		STATE_COURT(2, "Justiça Estadual"),
		SECEX_RFB(3, "Secex/RFB"),
		OTHER(9, "Outros");
		
		private int code;
		private String description;
	    
		ProcessOriginEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static ProcessOriginEnum getEnumByCode(int code) {
			for(ProcessOriginEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
		
		public static ProcessOriginEnum getEnumByDescription(String description) {
			for(ProcessOriginEnum e:values()){
				if(e.getDescription().equals(description))
					return e;
			}
			return null;
		}
	}
	
	public static enum PurposeIssueEnum { 
		NORMAL(1, "NF-e Normal"),
		ADDITIONAL(2, "NF-e Complementar"),
		ADJUSTMENT(3, "NF-e de Ajuste"),
		DEVOLUTION(4, "Devolução de Mercadoria");
		
		private int code;
		private String description;
	    
		PurposeIssueEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static PurposeIssueEnum getEnumByCode(int code) {
			for(PurposeIssueEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	
	
	public static enum SituationTypeEnum { 
		SIGNED("Assinada"),
	    AUTHORIZED("Autorizada"),
	    CANCELED("Cancelada"),
	    SAVED("Em Digitação"),
	    WAITING_FOR_PROCESSING("Em Processamento na SEFAZ"),
	    REJECTED("Rejeitada");
		
		private String description;
	    
		SituationTypeEnum (String description) {
	        this.description = description;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	}
	
	public static enum ShippingModeEnum { 
		FOR_THE_ISSUER(0, "Por conta do emitente"),
		FOR_THE_RECEIVER(1, "Por conta do destinário"),
		FOR_THE_THIRD_PARTIES(2, "Por conta de terceiros"),
		NO_SHIPPING(3, "Sem frete");
		
		private int code;
		private String description;
	    
		ShippingModeEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static ShippingModeEnum getEnumByCode(int code) {
			for(ShippingModeEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	
	public static enum SpecialTaxationEnum { 
		MUNICIPAL_MICROENTERPRISE(0, "Microempresa Municipal"),
		ESTIMATE(1, "Estimativa"),
		PROFESSIONALS_SOCIETY(2, "Sociedade de Profissionais"),
		COOP(3, "Cooperativa"),
		SINGLE_MICROENTREPRENEURS (4, "Microempresário Individual (MEI)"),
		ME_EPP(5, "Microempresário (ME) e Empresa de Pequeno Porte (EPP)");
		
		private int code;
		private String description;
	    
		SpecialTaxationEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static SpecialTaxationEnum getEnumByCode(int code) {
			for(SpecialTaxationEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	
	
	public static enum TargetOperationEnum { 
		INTERNAL(1, "Operação Interna"),
		INTERSTATE(2, "Operação Interestadual"),
		EXTERNAL(3, "Operação Externa");
		
		private int code;
		private String description;
	    
		TargetOperationEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static TargetOperationEnum getEnumByCode(int code) {
			for(TargetOperationEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	
	
	public static enum TaxpayerTypeEnum { 
		ICMS(1, "Contribuinte ICMS"),
		FREE(2, "Contribuinte Isento"),
		NON_TAXPAYER(3, "Não Contribuínte");
		
		private int code;
		private String description;
	    
		TaxpayerTypeEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static TaxpayerTypeEnum getEnumByCode(int code) {
			for(TaxpayerTypeEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	public static enum VehicleTypeEnum { 
		CAR(1, "Contribuinte ICMS"),
		FERRY(2, "Contribuinte Isento"),
		WAGON(3, "Não Contribuinte");
		
		private int code;
		private String description;
	    
		VehicleTypeEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static VehicleTypeEnum getEnumByCode(int code) {
			for(VehicleTypeEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	
	public static enum VersionEnum { 
		SCHEMA_NFE("3.10");
		
		private String description;
	    
		VersionEnum (String description) {
	        this.description = description;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	}
}