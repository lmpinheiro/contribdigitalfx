package br.com.cd.enumeration;

public class ProductServiceEnum {

	public static enum RegimeTributarioEnum { 
		SIMPLES_NACIONAL(1, "Simples Nacional"), 
		ROLE_ADMIN(3, "Regime Normal");
		
		private int code;
	    private String description;
	    
		RegimeTributarioEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
	    
		public static RegimeTributarioEnum getEnumByCode(int code) {
			for(RegimeTributarioEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	
	
	public static enum SpecificProductEnum { 
		VEHICLE(1, "Veículo"), 
		MEDICINE(2, "Medicamento"), 
		ARMAMENT(3, "Armamento"), 
		FUEL(4, "Combustível"), 
		IMUNE_ROLE(5, "Papel Imune");
		
		private int code;
	    private String description;
	    
	    SpecificProductEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
	    
		public static SpecificProductEnum getEnumByCode(int code) {
			for(SpecificProductEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	
	public static enum SimpleNationalTaxSituationEnum { 
		TS101(101, "Tributada com permissão de crédito"),
		TS102(102, "Tributada sem permissão de crédito"),
		TS103(103, "Isenção do ICMS para faixa de receita bruta"),
		TS201(201, "Tributada com permissão de crédito e com cobrança do ICMS por ST"),
		TS202(202, "Tributada sem permissão de crédito e com cobrança do ICMS por ST"),
		TS203(203, "Isenção do ICMS para faixa de receita bruta e com cobrança do ICMS por ST"),
		TS300(300, "Imune"),
		TS400(400, "Não tributada"),
		TS500(500, "ICMS cobrado anteriormente por ST ou por antecipação"),
		TS900(900, "Outros");
		
		private int code;
	    private String description;
	    
	    SimpleNationalTaxSituationEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
	    
		public static SimpleNationalTaxSituationEnum getEnumByCode(int code) {
			for(SimpleNationalTaxSituationEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	
	public static enum NormalTaxationTaxSituationEnum { 
		 TS00("00", "00 - Tributada integralmente"),
		 TS10("10", "10 - Tributada com cobrança do ICMS por ST"),
		 TS10_Part("10_Part", "10 - Tributada com cobrança do ICMS por ST (com partilha do ICMS entre a UF de origem e a UF de destino ou a UF definida na legislação)"),
		 TS20("20", "20 - Com redução da base de cálculo"),
		 TS30("30", "30 - Isenta ou não tributada e com cobrança do ICMS por ST"),
		 TS40("40", "40 - Isenta"),
		 TS41("41", "41 - Não tributada"),
		 TS41_ST("41_ST", "41 - Não tributada (ICMSST devido para a UF de destino, nas operações interestaduais de produtos que tiveram retenção antecipada de ICMS por ST na UF do rementente)"),
		 TS50("50", "50 - Suspensão"),
		 TS51("51", "51 - Diferimento"),
		 TS60("60", "60 - Cobrado anteriormente por ST"),
		 TS70("70", "70 - Com redução da base de cálculo e cobrança do ICMS por ST"),
		 TS90_Part("90_Part", "90 - Outras (com partilha do ICMS entre a UF de origem e a UF de destino ou a UF definida na legislação)"),
		 TS90("90", "90 - Outras");
		
		private String code;
	    private String description;
	    
	    NormalTaxationTaxSituationEnum (String code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public String getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
	    
		public static NormalTaxationTaxSituationEnum getEnumByCode(String code) {
			for(NormalTaxationTaxSituationEnum e:values()){
				if(e.getCode().equals(code))
					return e;
			}
			return null;
		}
	}
	
public static enum OriginEnum { 
		
		NATIONAL(0, "Nacional, exceto as indicadas nos códigos 3,4,5 E 8"),
		FOREIGN_DIRECT_IMPORT(1, "Estrangeira - Importação Direta, exceto a indicada no código 6"),
		FOREIGN_ACQ_INT_MKT(2, "Estrangeira - Adquirida no Mercado Interno, exceto a indicada no código 7"),
		NATIONAL_SUP_40(3, "Nacional, mercadoria ou bem com conteúdo de Importação superior a 40% e inferior ou igual a 70%"),
		NATIONAL_BASIC(4, "Nacional, cuja produção tenha sido feita em conformidade com os processos produtivos básicos de que tratam as legislações citadas nos Ajustes"),
		NATIONAL_BELOW_40(5, "Nacional, mercadoria ou bem com conteúdo de Importação inferior ou igual a 40%"),
		FOREIGN_DIRECT_IMPORT_SIMILAR(6,"Estrangeira - Importação direta, sem similar nacional, constante em lista da CAMEX e gás natural"),
		FOREIGN_INT_MKT_NO_SIMILAR(7,"Estrangeira - Adquirida no mercado interno, sem similar nacional, constante em lista da CAMEX e gás natural"),
		NATIONAL_SUP_70(8, "Nacional, mercadoria ou bem com conteúdo de Importação superior a 70%");
		
		private int code;
	    private String description;
	    
	    OriginEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static OriginEnum getEnumByCode(int code) {
			for(OriginEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}

	public static enum CalculationBaseModeEnum { 
		
		MARGIN_VALUE(0, "Margem do Valor Agregado (%)"),
		TARIFF_VALUE(1, "Valor da Pauta"),
		PRICE_TABLED (2, "Preço Tabelado Máximo"),
	    OPERATION_VALUE(3, "Valor da Operação");
		
		private int code;
	    private String description;
	    
	    CalculationBaseModeEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static CalculationBaseModeEnum getEnumByCode(int code) {
			for(CalculationBaseModeEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}


public static enum CalculationBaseModeSTEnum { 
		
		PRICE_TABLED(0, "Preço Tabelado ou Máximo Sugerido"),
	    NEGATIVE_LIST(1, "Valor da Lista Negativa"),
	    POSITIVE_LIST(2, "Valor da Lista Positiva"),
	    NEUTRAL_LIST(3, "Valor da Lista Neutra"),
	    MARGIN_VALUE(4, "Margem do Valor Agregado (%)"),
	    TARIFF_VALUE(5, "Valor da Pauta");
		
		private int code;
	    private String description;
	    
	    CalculationBaseModeSTEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static CalculationBaseModeSTEnum getEnumByCode(int code) {
			for(CalculationBaseModeSTEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}

	public static enum DesonerationReasonEnum { 
		
		TAXI(1, "Táxi"),
		HANDICAPPED(2,"Deficiente Físico"),
		AGRICULTURAL_PRODUCER(3,"Produtor Agropecuário"),
	    RENTAL(4,"Frotista/Locadora"),
	    DIPLOMATIC_CONSULAR(5,"Diplomático/Consular"),
	    FREE_ZONE(6,"Utilit. e Motoc. da Amazônia Ocid. e Áreas de Livre Comércio"),
	    SUFRAMA(7,"SUFRAMA"),
	    PUBLIC_AGENCIES(8,"Venda a Órgãos Públicos"),    
	    OTHERS(9,"Outros"),
	    HANDICAPPED_DRIVER(10,"Deficiente Condutor"),
	    HANDICAPPED_NON_DRIVER(11,"Deficiente Não Condutor"),
	    AGRICULTURAL_DEVELOPMENT(12,"Órgão de fomento e desenvolvimento agropecuário"),
	    OLYMPICS_2016(16,"Olimpíadas Rio 2016");
		
		private int code;
	    private String description;
	    
	    DesonerationReasonEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static DesonerationReasonEnum getEnumByCode(int code) {
			for(DesonerationReasonEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	

	public static enum IssqnServicesEnum { 
		
		IS01_01("01.01","Análise e desenvolvimento de sistemas."),
		IS01_02("01.02","Programação."),
		IS01_03("01.03","Processamento de dados e congêneres."),
		IS01_04("01.04","Elaboração de programas de computadores, inclusive de jogos eletrônicos."),
		IS01_05("01.05","Licenciamento ou cessão de direito de uso de programas de computação."),
		IS01_06("01.06","Assessoria e consultoria em informática."),
		IS01_07("01.07","Suporte técnico em informática, inclusive instalação, configuração e manutenção de programas de computação e bancos de dados."),
		IS01_08("01.08","Planejamento, confecção, manutenção e atualização de páginas eletrônicas."),
		IS02_01("02.01","Serviços de pesquisas e desenvolvimento de qualquer natureza."),
		IS03_02("03.02","Cessão de direito de uso de marcas e de sinais de propaganda."),
		IS03_03("03.03","Exploração de salões de festas, centro de convenções, escritórios virtuais, stands, quaISas esportivas, estádios, ginásios, auditórios, casas de espetáculos, parques de diversões, canchas e congêneres, para realização de eventos ou negócios de qualquer natureza."),
		IS03_04("03.04","Locação, sublocação, arrendamento, direito de passagem ou permissão de uso, compartilhado ou não, de ferrovia, rodovia, postes, cabos, dutos e condutos de qualquer natureza."),
		IS03_05("03.05","Cessão de andaimes, palcos, coberturas e outras estruturas de uso temporário."),
		IS04_01("04.01","Medicina e biomedicina."),
		IS04_02("04.02","Análises clínicas, patologia, eletricidade médica, radioterapia, quimioterapia, ultra-sonografia, ressonância magnética, radiologia, tomografia e congêneres."),
		IS04_03("04.03","Hospitais, clínicas, laboratórios, sanatórios, manicômios, casas de saúde, prontos-socorros, ambulatórios e congêneres."),
		IS04_04("04.04","Instrumentação cirúrgica."),
		IS04_05("04.05","Acupuntura."),
		IS04_06("04.06","Enfermagem, inclusive serviços auxiliares."),
		IS04_07("04.07","Serviços farmacêuticos."),
		IS04_08("04.08","Terapia ocupacional, fisioterapia e fonoaudiologia."),
		IS04_09("04.09","Terapias de qualquer espécie destinadas ao tratamento físico, orgânico e mental."),
		IS04_10("04.10","Nutrição."),
		IS04_11("04.11","Obstetrícia."),
		IS04_12("04.12","Odontologia."),
		IS04_13("04.13","Ortóptica."),
		IS04_14("04.14","Próteses sob encomenda."),
		IS04_15("04.15","Psicanálise."),
		IS04_16("04.16","Psicologia."),
		IS04_17("04.17","Casas de repouso e de recuperação, creches, asilos e congêneres."),
		IS04_18("04.18","Inseminação artificial, fertilização in vitro e congêneres."),
		IS04_19("04.19","Bancos de sangue, leite, pele, olhos, óvulos, sêmen e congêneres."),
		IS04_20("04.20","Coleta de sangue, leite, tecidos, sêmen, órgãos e materiais biológicos de qualquer espécie."),
		IS04_21("04.21","Unidade de atendimento, assistência ou tratamento móvel e congêneres."),
		IS04_22("04.22","Planos de medicina de grupo ou individual e convênios para prestação de assistência médica, hospitalar, odontológica e congêneres."),
		IS04_23("04.23","Outros planos de saúde que se cumpram através de serviços de terceiros contratados, credenciados, cooperados ou apenas pagos pelo operador do plano mediante indicação do beneficiário."),
		IS05_01("05.01","Medicina veterinária e zootecnia."),
		IS05_02("05.02","Hospitais, clínicas, ambulatórios, prontos-socorros e congêneres, na área veterinária."),
		IS05_03("05.03","Laboratórios de análise na área veterinária."),
		IS05_04("05.04","Inseminação artificial, fertilização in vitro e congêneres."),
		IS05_05("05.05","Bancos de sangue e de órgãos e congêneres."),
		IS05_06("05.06","Coleta de sangue, leite, tecidos, sêmen, órgãos e materiais biológicos de qualquer espécie."),
		IS05_07("05.07","Unidade de atendimento, assistência ou tratamento móvel e congêneres."),
		IS05_08("05.08","Guarda, tratamento, amestramento, embelezamento, alojamento e congêneres."),
		IS05_09("05.09","Planos de atendimento e assistência médico-veterinária."),
		IS06_01("06.01","Barbearia, cabeleireiros, manicuros, pedicuros e congêneres."),
		IS06_02("06.02","Esteticistas, tratamento de pele, depilação e congêneres."),
		IS06_03("06.03","Banhos, duchas, sauna, massagens e congêneres."),
		IS06_04("06.04","Ginástica, dança, esportes, natação, artes marciais e demais atividades físicas."),
		IS06_05("06.05","Centros de emagrecimento, spa e congêneres."),
		IS07_01("07.01","Engenharia, agronomia, agrimensura, arquitetura, geologia, urbanismo, paisagismo e congêneres."),
		IS07_02("07.02","Execução, por administração, empreitada ou subempreitada, de obras de construção civil, hiISáulica ou elétrica e de outras obras semelhantes, inclusive sondagem, perfuração de poços, escavação, ISenagem e irrigação, terraplanagem, pavimentação, concretagem e a instalação e montagem de produtos, peças e equipamentos (exceto o fornecimento de mercadorias produzidas pelo prestador de serviços fora do local da prestação dos serviços, que fica sujeito ao ICMS)."),
		IS07_03("07.03","Elaboração de planos diretores, estudos de viabilidade, estudos organizacionais e outros, relacionados com obras e serviços de engenharia; elaboração de anteprojetos, projetos básicos e projetos executivos para trabalhos de engenharia."),
		IS07_04("07.04","Demolição."),
		IS07_05("07.05","Reparação, conservação e reforma de edifícios, estradas, pontes, portos e congêneres (exceto o fornecimento de mercadorias produzidas pelo prestador dos serviços, fora do local da prestação dos serviços, que fica sujeito ao ICMS)."),
		IS07_06("07.06","Colocação e instalação de tapetes, carpetes, assoalhos, cortinas, revestimentos de parede, viISos, divisórias, placas de gesso e congêneres, com material fornecido pelo tomador do serviço."),
		IS07_07("07.07","Recuperação, raspagem, polimento e lustração de pisos e congêneres."),
		IS07_08("07.08","Calafetação."),
		IS07_09("07.09","Varrição, coleta, remoção, incineração, tratamento, reciclagem, separação e destinação final de lixo, rejeitos e outros resíduos quaisquer."),
		IS07_10("07.10","Limpeza, manutenção e conservação de vias e logradouros públicos, imóveis, chaminés, piscinas, parques, jardins e congêneres."),
		IS07_11("07.11","Decoração e jardinagem, inclusive corte e poda de árvores."),
		IS07_12("07.12","Controle e tratamento de efluentes de qualquer natureza e de agentes físicos, químicos e biológicos."),
		IS07_13("07.13","Dedetização, desinfecção, desinsetização, imunização, higienização, desratização, pulverização e congêneres."),
		IS07_16("07.16","Florestamento, reflorestamento, semeadura, adubação e congêneres."),
		IS07_17("07.17","Escoramento, contenção de encostas e serviços congêneres."),
		IS07_18("07.18","Limpeza e ISagagem de rios, portos, canais, baías, lagos, lagoas, represas, açudes e congêneres."),
		IS07_19("07.19","Acompanhamento e fiscalização da execução de obras de engenharia, arquitetura e urbanismo."),
		IS07_20("07.20","Aerofotogrametria (inclusive interpretação), cartografia, mapeamento, levantamentos topográficos, batimétricos, geográficos, geodésicos, geológicos, geofísicos e congêneres."),
		IS07_21("07.21","Pesquisa, perfuração, cimentação, mergulho, perfilagem, concretação, testemunhagem, pescaria, estimulação e outros serviços relacionados com a exploração e explotação de petróleo, gás natural e de outros recursos minerais."),
		IS07_22("07.22","Nucleação e bombardeamento de nuvens e congêneres."),
		IS08_01("08.01","Ensino regular pré-escolar, fundamental, médio e superior."),
		IS08_02("08.02","Instrução, treinamento, orientação pedagógica e educacional, avaliação de conhecimentos de qualquer natureza."),
		IS09_01("09.01","Hospedagem de qualquer natureza em hotéis, apart-service condominiais, flat, apart-hotéis, hotéis residência, residence-service, suite service, hotelaria marítima, motéis, pensões e congêneres; ocupação por temporada com fornecimento de serviço (o valor da alimentação e gorjeta, quando incluído no preço da diária, fica sujeito ao Imposto Sobre Serviços)."),
		IS09_02("09.02","Agenciamento, organização, promoção, intermediação e execução de programas de turismo, passeios, viagens, excursões, hospedagens e congêneres."),
		IS09_03("09.03","Guias de turismo."),
		IS10_01("10.01","Agenciamento, corretagem ou intermediação de câmbio, de seguros, de cartões de crédito, de planos de saúde e de planos de previdência privada."),
		IS10_02("10.02","Agenciamento, corretagem ou intermediação de títulos em geral, valores mobiliários e contratos quaisquer."),
		IS10_03("10.03","Agenciamento, corretagem ou intermediação de direitos de propriedade industrial, artística ou literária."),
		IS10_04("10.04","Agenciamento, corretagem ou intermediação de contratos de arrendamento mercantil (leasing), de franquia (franchising) e de faturização (factoring)."),
		IS10_05("10.05","Agenciamento, corretagem ou intermediação de bens móveis ou imóveis, não abrangidos em outros itens ou subitens, inclusive aqueles realizados no âmbito de Bolsas de Mercadorias e Futuros, por quaisquer meios."),
		IS10_06("10.06","Agenciamento marítimo."),
		IS10_07("10.07","Agenciamento de notícias."),
		IS10_08("10.08","Agenciamento de publicidade e propaganda, inclusive o agenciamento de veiculação por quaisquer meios."),
		IS10_09("10.09","Representação de qualquer natureza, inclusive comercial."),
		IS10_10("10.10","Distribuição de bens de terceiros."),
		IS11_01("11.01","Guarda e estacionamento de veículos terrestres automotores, de aeronaves e de embarcações."),
		IS11_02("11.02","Vigilância, segurança ou monitoramento de bens e pessoas."),
		IS11_03("11.03","Escolta, inclusive de veículos e cargas."),
		IS11_04("11.04","Armazenamento, depósito, carga, descarga, arrumação e guarda de bens de qualquer espécie."),
		IS12_01("12.01","Espetáculos teatrais."),
		IS12_02("12.02","Exibições cinematográficas."),
		IS12_03("12.03","Espetáculos circenses."),
		IS12_04("12.04","Programas de auditório."),
		IS12_05("12.05","Parques de diversões, centros de lazer e congêneres."),
		IS12_06("12.06","Boates, taxi-dancing e congêneres."),
		IS12_07("12.07","Shows, ballet, danças, desfiles, bailes, óperas, concertos, recitais, festivais e congêneres."),
		IS12_08("12.08","Feiras, exposições, congressos e congêneres."),
		IS12_09("12.09","Bilhares, boliches e diversões eletrônicas ou não."),
		IS12_10("12.10","Corridas e competições de animais."),
		IS12_11("12.11","Competições esportivas ou de destreza física ou intelectual, com ou sem a participação do espectador."),
		IS12_12("12.12","Execução de música."),
		IS12_13("12.13","Produção, mediante ou sem encomenda prévia, de eventos, espetáculos, entrevistas, shows, ballet, danças, desfiles, bailes, teatros, óperas, concertos, recitais, festivais e congêneres."),
		IS12_14("12.14","Fornecimento de música para ambientes fechados ou não, mediante transmissão por qualquer processo."),
		IS12_15("12.15","Desfiles de blocos carnavalescos ou folclóricos, trios elétricos e congêneres."),
		IS12_16("12.16","Exibição de filmes, entrevistas, musicais, espetáculos, shows, concertos, desfiles, óperas, competições esportivas, de destreza intelectual ou congêneres."),
		IS12_17("12.17","Recreação e animação, inclusive em festas e eventos de qualquer natureza."),
		IS13_02("13.02","Fonografia ou gravação de sons, inclusive trucagem, dublagem, mixagem e congêneres."),
		IS13_03("13.03","Fotografia e cinematografia, inclusive revelação, ampliação, cópia, reprodução, trucagem e congêneres."),
		IS13_04("13.04","Reprografia, microfilmagem e digitalização."),
		IS13_05("13.05","Composição gráfica, fotocomposição, clicheria, zincografia, litografia, fotolitografia."),
		IS14_01("14.01","Lubrificação, limpeza, lustração, revisão, carga e recarga, conserto, restauração, blindagem, manutenção e conservação de máquinas, veículos, aparelhos, equipamentos, motores, elevadores ou de qualquer objeto (exceto peças e partes empregadas, que ficam sujeitas ao ICMS)."),
		IS14_02("14.02","Assistência técnica."),
		IS14_03("14.03","Recondicionamento de motores (exceto peças e partes empregadas, que ficam sujeitas ao ICMS)."),
		IS14_04("14.04","Recauchutagem ou regeneração de pneus."),
		IS14_05("14.05","Restauração, recondicionamento, acondicionamento, pintura, beneficiamento, lavagem, secagem, tingimento, galvanoplastia, anodização, corte, recorte, polimento, plastificação e congêneres, de objetos quaisquer."),
		IS14_06("14.06","Instalação e montagem de aparelhos, máquinas e equipamentos, inclusive montagem industrial, prestados ao usuário final, exclusivamente com material por ele fornecido."),
		IS14_07("14.07","Colocação de molduras e congêneres."),
		IS14_08("14.08","Encadernação, gravação e douração de livros, revistas e congêneres."),
		IS14_09("14.09","Alfaiataria e costura, quando o material for fornecido pelo usuário final, exceto aviamento."),
		IS14_10("14.10","Tinturaria e lavanderia."),
		IS14_11("14.11","Tapeçaria e reforma de estofamentos em geral."),
		IS14_12("14.12","Funilaria e lanternagem."),
		IS14_13("14.13","Carpintaria e serralheria."),
		IS15_01("15.01","Administração de fundos quaisquer, de consórcio, de cartão de crédito ou débito e congêneres, de carteira de clientes, de cheques pré-datados e congêneres."),
		IS15_02("15.02","Abertura de contas em geral, inclusive conta-corrente, conta de investimentos e aplicação e caderneta de poupança, no País e no exterior, bem como a manutenção das referidas contas ativas e inativas."),
		IS15_03("15.03","Locação e manutenção de cofres particulares, de terminais eletrônicos, de terminais de atendimento e de bens e equipamentos em geral."),
		IS15_04("15.04","Fornecimento ou emissão de atestados em geral, inclusive atestado de idoneidade, atestado de capacidade financeira e congêneres."),
		IS15_05("15.05","Cadastro, elaboração de ficha cadastral, renovação cadastral e congêneres, inclusão ou exclusão no Cadastro de Emitentes de Cheques sem Fundos  - CCF ou em quaisquer outros bancos cadastrais."),
		IS15_06("15.06","Emissão, reemissão e fornecimento de avisos, comprovantes e documentos em geral; abono de firmas; coleta e entrega de documentos, bens e valores; comunicação com outra agência ou com a administração central; licenciamento eletrônico de veículos; transferência de veículos; agenciamento fiduciário ou depositário; devolução de bens em custódia."),
		IS15_07("15.07","Acesso, movimentação, atendimento e consulta a contas em geral, por qualquer meio ou processo, inclusive por telefone, fac-símile, internet e telex, acesso a terminais de atendimento, inclusive vinte e quatro horas; acesso a outro banco e a rede compartilhada; fornecimento de saldo, extrato e demais informações relativas a contas em geral, por qualquer meio ou processo."),
		IS15_08("15.08","Emissão, reemissão, alteração, cessão, substituição, cancelamento e registro de contrato de crédito; estudo, análise e avaliação de operações de crédito; emissão, concessão, alteração ou contratação de aval, fiança, anuência e congêneres; serviços relativos a abertura de crédito, para quaisquer fins."),
		IS15_09("15.09","Arrendamento mercantil (leasing) de quaisquer bens, inclusive cessão de direitos e obrigações, substituição de garantia, alteração, cancelamento e registro de contrato, e demais serviços relacionados ao arrendamento mercantil (leasing)."),
		IS15_10("15.10","Serviços relacionados a cobranças, recebimentos ou pagamentos em geral, de títulos quaisquer, de contas ou carnês, de câmbio, de tributos e por conta de terceiros, inclusive os efetuados por meio eletrônico, automático ou por máquinas de atendimento; fornecimento de posição de cobrança, recebimento ou pagamento; emissão de carnês, fichas de compensação, impressos e documentos em geral."),
		IS15_11("15.11","Devolução de títulos, protesto de títulos, sustação de protesto, manutenção de títulos, reapresentação de títulos, e demais serviços a eles relacionados."),
		IS15_12("15.12","Custódia em geral, inclusive de títulos e valores mobiliários."),
		IS15_13("15.13","Serviços relacionados a operações de câmbio em geral, edição, alteração, prorrogação, cancelamento e baixa de contrato de câmbio; emissão de registro de exportação ou de crédito; cobrança ou depósito no exterior; emissão, fornecimento e cancelamento de cheques de viagem; fornecimento, transferência, cancelamento e demais serviços relativos a carta de crédito de importação, exportação e garantias recebidas; envio e recebimento de mensagens em geral relacionadas a operações de câmbio."),
		IS15_14("15.14","Fornecimento, emissão, reemissão, renovação e manutenção de cartão magnético, cartão de crédito, cartão de débito, cartão salário e congêneres."),
		IS15_15("15.15","Compensação de cheques e títulos quaisquer; serviços relacionados a depósito, inclusive depósito identificado, a saque de contas quaisquer, por qualquer meio ou processo, inclusive em terminais eletrônicos e de atendimento."),
		IS15_16("15.16","Emissão, reemissão, liquidação, alteração, cancelamento e baixa de ordens de pagamento, ordens de crédito e similares, por qualquer meio ou processo; serviços relacionados à transferência de valores, dados, fundos, pagamentos e similares, inclusive entre contas em geral."),
		IS15_17("15.17","Emissão, fornecimento, devolução, sustação, cancelamento e oposição de cheques quaisquer, avulso ou por talão."),
		IS15_18("15.18","Serviços relacionados a crédito imobiliário, avaliação e vistoria de imóvel ou obra, análise técnica e jurídica, emissão, reemissão, alteração, transferência e renegociação de contrato, emissão e reemissão do termo de quitação e demais serviços relacionados a crédito imobiliário."),
		IS16_01("16.01","Serviços de transporte de natureza municipal."),
		IS17_01("17.01","Assessoria ou consultoria de qualquer natureza, não contida em outros itens desta lista; análise, exame, pesquisa, coleta, compilação e fornecimento de dados e informações de qualquer natureza, inclusive cadastro e similares."),
		IS17_02("17.02","Datilografia, digitação, estenografia, expediente, secretaria em geral, resposta audível, redação, edição, interpretação, revisão, tradução, apoio e infra-estrutura administrativa e congêneres."),
		IS17_03("17.03","Planejamento, coordenação, programação ou organização técnica, financeira ou administrativa."),
		IS17_04("17.04","Recrutamento, agenciamento, seleção e colocação de mão-de-obra."),
		IS17_05("17.05","Fornecimento de mão-de-obra, mesmo em caráter temporário, inclusive de empregados ou trabalhadores, avulsos ou temporários, contratados pelo prestador de serviço."),
		IS17_06("17.06","Propaganda e publicidade, inclusive promoção de vendas, planejamento de campanhas ou sistemas de publicidade, elaboração de desenhos, textos e demais materiais publicitários."),
		IS17_08("17.08","Franquia (franchising)."),
		IS17_09("17.09","Perícias, laudos, exames técnicos e análises técnicas."),
		IS17_10("17.10","Planejamento, organização e administração de feiras, exposições, congressos e congêneres."),
		IS17_11("17.11","Organização de festas e recepções; bufê (exceto o fornecimento de alimentação e bebidas, que fica sujeito ao ICMS)."),
		IS17_12("17.12","Administração em geral, inclusive de bens e negócios de terceiros."),
		IS17_13("17.13","Leilão e congêneres."),
		IS17_14("17.14","Advocacia."),
		IS17_15("17.15","Arbitragem de qualquer espécie, inclusive jurídica."),
		IS17_16("17.16","Auditoria."),
		IS17_17("17.17","Análise de Organização e Métodos."),
		IS17_18("17.18","Atuária e cálculos técnicos de qualquer natureza."),
		IS17_19("17.19","Contabilidade, inclusive serviços técnicos e auxiliares."),
		IS17_20("17.20","Consultoria e assessoria econômica ou financeira."),
		IS17_21("17.21","Estatística."),
		IS17_22("17.22","Cobrança em geral."),
		IS17_23("17.23","Assessoria, análise, avaliação, atendimento, consulta, cadastro, seleção, gerenciamento de informações, administração de contas a receber ou a pagar e em geral, relacionados a operações de faturização (factoring)."),
		IS17_24("17.24","Apresentação de palestras, conferências, seminários e congêneres."),
		IS18_01("18.01","Serviços de regulação de sinistros vinculados a contratos de seguros; inspeção e avaliação de riscos para cobertura de contratos de seguros; prevenção e gerência de riscos seguráveis e congêneres."),
		IS19_01("19.01","Serviços de distribuição e venda de bilhetes e demais produtos de loteria, bingos, cartões, pules ou cupons de apostas, sorteios, prêmios, inclusive os decorrentes de títulos de capitalização e congêneres."),
		IS20_01("20.01","Serviços portuários, ferroportuários, utilização de porto, movimentação de passageiros, reboque de embarcações, rebocador escoteiro, atracação, desatracação, serviços de praticagem, capatazia, armazenagem de qualquer natureza, serviços acessórios, movimentação de mercadorias, serviços de apoio marítimo, de movimentação ao largo, serviços de armadores, estiva, conferência, logística e congêneres."),
		IS20_02("20.02","Serviços aeroportuários, utilização de aeroporto, movimentação de passageiros, armazenagem de qualquer natureza, capatazia, movimentação de aeronaves, serviços de apoio aeroportuários, serviços acessórios, movimentação de mercadorias, logística e congêneres."),
		IS20_03("20.03","Serviços de terminais rodoviários, ferroviários, metroviários, movimentação de passageiros, mercadorias, inclusive     suas operações, logística e congêneres."),
		IS21_01("21.01","Serviços de registros públicos, cartorários e notariais."),
		IS22_01("22.01","Serviços de exploração de rodovia mediante cobrança de preço ou pedágio dos usuários, envolvendo execução de serviços de conservação, manutenção, melhoramentos para adequação de capacidade e segurança de trânsito, operação, monitoração, assistência aos usuários e outros serviços definidos em contratos, atos de concessão ou de permissão ou em      normas oficiais."),
		IS23_01("23.01","Serviços de programação e comunicação visual, desenho industrial e congêneres."),
		IS24_01("24.01","Serviços de chaveiros, confecção de carimbos, placas, sinalização visual, banners, adesivos e congêneres."),
		IS25_01("25.01","Funerais, inclusive fornecimento de caixão, urna ou esquifes; aluguel de capela; transporte do corpo cadavérico; fornecimento de flores, coroas e outros paramentos; desembaraço de certidão de óbito; fornecimento de véu, essa e outros adornos; embalsamento, embelezamento, conservação ou restauração de cadáveres."),
		IS25_02("25.02","Cremação de corpos e partes de corpos cadavéricos."),
		IS25_03("25.03","Planos ou convênio funerários."),
		IS25_04("25.04","Manutenção e conservação de jazigos e cemitérios."),
		IS26_01("26.01","Serviços de coleta, remessa ou entrega de correspondências, documentos, objetos, bens ou valores, inclusive pelos correios e suas agências franqueadas; courrier e congêneres."),
		IS27_01("27.01","Serviços de assistência social."),
		IS28_01("28.01","Serviços de avaliação de bens e serviços de qualquer natureza."),
		IS29_01("29.01","Serviços de biblioteconomia."),
		IS30_01("30.01","Serviços de biologia, biotecnologia e química."),
		IS31_01("31.01","Serviços técnicos em edificações, eletrônica, eletrotécnica, mecânica, telecomunicações e congêneres."),
		IS32_01("32.01","Serviços de desenhos técnicos."),
		IS33_01("33.01","Serviços de desembaraço aduaneiro, comissários, despachantes e congêneres."),
		IS34_01("34.01","Serviços de investigações particulares, detetives e congêneres."),
		IS35_01("35.01","Serviços de reportagem, assessoria de imprensa, jornalismo e relações públicas."),
		IS36_01("36.01","Serviços de meteorologia."),
		IS37_01("37.01","Serviços de artistas, atletas, modelos e manequins."),
		IS38_01("38.01","Serviços de museologia."),
		IS39_01("39.01","Serviços de ourivesaria e lapidação (quando o material for fornecido pelo tomador do serviço)."),
		IS40_01("40.01","Obras de arte sob encomenda.");
		
		
		private String code;
	    private String description;
	    
	    IssqnServicesEnum (String code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public String getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
	    
		public static IssqnServicesEnum getEnumByCode(String code) {
			for(IssqnServicesEnum e:values()){
				if(e.getCode().equals(code))
					return e;
			}
			return null;
		}
	}
	
	
	public static enum IssMandatoryEnum { 
		
		REQUIRED(1, "Exígivel"),
		IRRELEVANT(2, "Não incidência"),
		EXEMPTION(3, "Isenção"),
		EXPORT(4, "Exportação"),
		REQUIRED_SUSP_DJ(6, "Exigibilidade suspensa por decisão judicial"),
		REQUIRED_SUSP_PA(7, "Exigibilidade suspensa por processo administrativo"),
		IMMUNITY(8, "Imunidade");
		
		private int code;
	    private String description;
	    
	    IssMandatoryEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static IssMandatoryEnum getEnumByCode(int code) {
			for(IssMandatoryEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}

	
public static enum IpiEnum { 
		
		IPI_00("00", "IPI 00 - Entrada com recuperação de crédito"),
		IPI_01("01", "IPI 01 - Entrada tributada com alíquota zero"),
		IPI_02("02", "IPI 02 - Entrada isenta"),
		IPI_03("03", "IPI 03 - Entrada não-tributada"),
		IPI_04("04", "IPI 04 - Entrada imune"),
		IPI_05("05", "IPI 05 - Entrada com suspensão"),
		IPI_49("49", "IPI 49 - Outras entradas"),
		IPI_50("50", "IPI 50 - Saída tributada"),
		IPI_51("51", "IPI 51 - Saída tributada com alíquota zero"),
		IPI_52("52", "IPI 52  - Saída isenta"),
		IPI_53("53", "IPI 53 - Saída não-tributada"),
		IPI_54("54", "IPI 54 - Saída imune"),
		IPI_55("55", "IPI 55 - Saída com suspensão"),
		IPI_99("99", "IPI 99 - Outras saídas");
			
		private String code;
	    private String description;
	    
	    IpiEnum (String code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public String getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static IpiEnum getEnumByCode(String code) {
			for(IpiEnum e:values()){
				if(e.getCode().equals(code))
					return e;
			}
			return null;
		}
	}


	public static enum PisEnum { 
	
		PIS_01("01", "PIS 01 - Operação Tributável - Base de Cálculo = Valor da Operação Alíquota Normal (Cumulativo/Não Cumulativo)"),
	    PIS_02("02", "PIS 02 - Operação Tributável - Base de Cálculo = Valor da Operação (Alíquota Diferenciada)"),
	    PIS_03("03", "PIS 03 - Operação Tributável - Base de Cálculo = Quantidade Vendida x Alíquota por Unidade de Produto"),
	    PIS_04("04", "PIS 04 - Operação Tributável - Tributação Monofásica - (Alíquota Zero)"),
	    PIS_05("05", "PIS 05 - Operação Tributável (ST)"),
	    PIS_06("06", "PIS 06 - Operação Tributável - Alíquota Zero"),
	    PIS_07("07", "PIS 07 - Operação Isenta da Contribuição"),
	    PIS_08("08", "PIS 08 - Operação sem Incidência da Contribuição"),
	    PIS_09("09", "PIS 09 - Operação com Suspensão da Contribuição"),
	    PIS_49("49", "PIS 49 - Outras Operações de Saída"),
	    PIS_50("50", "PIS 50 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno"),
	    PIS_51("51", "PIS 51 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno"),
	    PIS_52("52", "PIS 52 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita de Exportação"),
	    PIS_53("53", "PIS 53 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno"),
	    PIS_54("54", "PIS 54 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas no Mercado Interno e de Exportação"),
	    PIS_55("55", "PIS 55 - Operação com Direito a Crédito - Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação"),
	    PIS_56("56", "PIS 56 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação"),
	    PIS_60("60", "PIS 60 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Tributada no Mercado Interno"),
	    PIS_61("61", "PIS 61 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Não-Tributada no Mercado Interno"),
	    PIS_62("62", "PIS 62 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita de Exportação"),
	    PIS_63("63", "PIS 63 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno"),
	    PIS_64("64", "PIS 64 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas no Mercado Interno e de Exportação"),
	    PIS_65("65", "PIS 65 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação"),
	    PIS_66("66", "PIS 66 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação"),
	    PIS_67("67", "PIS 67 - Crédito Presumido - Outras Operações"),
	    PIS_70("70", "PIS 70 - Operação de Aquisição sem Direito a Crédito"),
	    PIS_71("71", "PIS 71 - Operação de Aquisição com Isenção"),
	    PIS_72("72", "PIS 72 - Operação de Aquisição com Suspensão"),
	    PIS_73("73", "PIS 73 - Operação de Aquisição a Alíquota Zero"),
	    PIS_74("74", "PIS 74 - Operação de Aquisição sem Incidência da Contribuição"),
	    PIS_75("75", "PIS 75 - Operação de Aquisição por Substituição Tributária"),
	    PIS_98("98", "PIS 98 - Outras Operações de Entrada"),
	    PIS_99("99", "PIS 99 - Outras Operações");
			
		private String code;
	    private String description;
	    
	    PisEnum (String code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public String getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static PisEnum getEnumByCode(String code) {
			for(PisEnum e:values()){
				if(e.getCode().equals(code))
					return e;
			}
			return null;
		}
	}
	
	public static enum CofinsEnum { 
		
		COFINS_01("01", "COFINS 01 - Operação Tributável - Base de Cálculo = Valor da Operação Alíquota Normal (Cumulativo/Não Cumulativo)"),
	    COFINS_02("02", "COFINS 02 - Operação Tributável - Base de Cálculo = Valor da Operação (Alíquota Diferenciada)"),
	    COFINS_03("03", "COFINS 03 - Operação Tributável - Base de Cálculo = Quantidade Vendida x Alíquota por Unidade de Produto"),
	    COFINS_04("04", "COFINS 04 - Operação Tributável - Tributação Monofásica - (Alíquota Zero)"),
	    COFINS_05("05", "COFINS 05 - Operação Tributável (ST)"),
	    COFINS_06("06", "COFINS 06 - Operação Tributável - Alíquota Zero"),
	    COFINS_07("07", "COFINS 07 - Operação Isenta da Contribuição"),
	    COFINS_08("08", "COFINS 08 - Operação sem Incidência da Contribuição"),
	    COFINS_09("09", "COFINS 09 - Operação com Suspensão da Contribuição"),
	    COFINS_49("49", "COFINS 49 - Outras Operações de Saída"),
	    COFINS_50("50", "COFINS 50 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno"),
	    COFINS_51("51", "COFINS 51 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno"),
	    COFINS_52("52", "COFINS 52 - Operação com Direito a Crédito - Vinculada Exclusivamente a Receita de Exportação"),
	    COFINS_53("53", "COFINS 53 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno"),
	    COFINS_54("54", "COFINS 54 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas no Mercado Interno e de Exportação"),
	    COFINS_55("55", "COFINS 55 - Operação com Direito a Crédito - Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação"),
	    COFINS_56("56", "COFINS 56 - Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação"),
	    COFINS_60("60", "COFINS 60 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Tributada no Mercado Interno"),
	    COFINS_61("61", "COFINS 61 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Não-Tributada no Mercado Interno"),
	    COFINS_62("62", "COFINS 62 - Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita de Exportação"),
	    COFINS_63("63", "COFINS 63 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno"),
	    COFINS_64("64", "COFINS 64 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas no Mercado Interno e de Exportação"),
	    COFINS_65("65", "COFINS 65 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação"),
	    COFINS_66("66", "COFINS 66 - Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação"),
	    COFINS_67("67", "COFINS 67 - Crédito Presumido - Outras Operações"),
	    COFINS_70("70", "COFINS 70 - Operação de Aquisição sem Direito a Crédito"),
	    COFINS_71("71", "COFINS 71 - Operação de Aquisição com Isenção"),
	    COFINS_72("72", "COFINS 72 - Operação de Aquisição com Suspensão"),
	    COFINS_73("73", "COFINS 73 - Operação de Aquisição a Alíquota Zero"),
	    COFINS_74("74", "COFINS 74 - Operação de Aquisição sem Incidência da Contribuição"),
	    COFINS_75("75", "COFINS 75 - Operação de Aquisição por Substituição Tributária"),
	    COFINS_98("98", "COFINS 98 - Outras Operações de Entrada"),
	    COFINS_99("99", "COFINS 99 - Outras Operações");
			
		private String code;
	    private String description;
	    
	    CofinsEnum (String code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public String getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static CofinsEnum getEnumByCode(String code) {
			for(CofinsEnum e:values()){
				if(e.getCode().equals(code))
					return e;
			}
			return null;
		}
	}
	
	public static enum VehicleOperationTypeEnum { 
		
		SELLING_DEALERSHIP(1, "Venda Concessionária"),
		DIRECT_BILLING(2, "Faturamento Direto"),
		DIRECT_SALES(3, "Venda Direta"),
		OTHER(0, "Outros");
			
		private int code;
	    private String description;
	    
	    VehicleOperationTypeEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static VehicleOperationTypeEnum getEnumByCode(int code) {
			for(VehicleOperationTypeEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	
	public static enum VehicleConditionEnum { 
		
		FINISHED(1, "Acabado"),
		UNFINISHED(2, "Inacabado"),
		ALMOST_FINISHED(3, "Semi-acabado");
			
		private int code;
	    private String description;
	    
	    VehicleConditionEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static VehicleConditionEnum getEnumByCode(int code) {
			for(VehicleConditionEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
	
	
	public static enum VehicleRestrictionEnum { 
		
		NOT_EXIST(0, "Não há"),
		FIDUCIARY_ALIENATION(1, "Alienação Fiduciária"),
		LEASING(2, "Arrendamento Mercantil"),
		DOMAIN_RESERVATION(3, "Reserva de Domínio"),
		PLEDGE(4, "Penhor de Veículos"),
		OTHER(9, "outras");
			
		private int code;
	    private String description;
	    
	    VehicleRestrictionEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
		public static VehicleRestrictionEnum getEnumByCode(int code) {
			for(VehicleRestrictionEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
}