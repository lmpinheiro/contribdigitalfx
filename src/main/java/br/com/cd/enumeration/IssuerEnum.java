package br.com.cd.enumeration;

public class IssuerEnum {

	public static enum RegimeTributarioEnum { 
		SIMPLES_NACIONAL(1, "Simples Nacional"), 
		SIMPLES_NACIONAL_ESRB(2, "Simples Nacional - Excesso Sublimite de Receita Bruta"), 
		ROLE_ADMIN(3, "Regime Normal");
		
		private int code;
	    private String description;
	    
		RegimeTributarioEnum (int code, String description) {
	        this.code = code;
	        this.description = description;
	    }
	    
	    public int getCode() {
	        return code;
	    }
	    
	    public String getDescription() {
	        return description;
	    }
	    
	    public String toString() {
	        return getDescription();
	    }
	    
	    
		public static RegimeTributarioEnum getEnumByCode(int code) {
			for(RegimeTributarioEnum e:values()){
				if(e.getCode()==code)
					return e;
			}
			return null;
		}
	}
}