package br.com.cd.constant;

public class MessageConstant {
	
	
	/**
	 * GENERIC MESSAGES
	 */
	public final static String ALREADY_INSERTED = "● {0} já cadastrado";
	public final static String DOESNT_EXIST = "● {0} {1} não encontrado";
	public final static String INVALID_CONTENT = "● Campo \"{0}\" com valor inválido";
	public final static String RECORD_INSERTED = "● {0} inserido com sucesso!";
	public final static String RECORD_DELETED = "● {0} excluído com sucesso!";
	public final static String RECORD_UPDATED = "● {0} alterado com sucesso!";
	public final static String PARTICULAR_REQUIRED_FIELD = "● Campo \"{0}\" de preenchimento obrigatório";
	public final static String REQUIRED_FIELD = "● Campo de preenchimento obrigatório";
	public final static String DELETE_CONFIRMATION = "● Tem certeza que deseja excluir {0}?";
	public final static String EXIT_CONFIRMATION = "Tem certeza que deseja sair dessa tela? \nAs alterações digitadas serão perdidas!";
	
	
	/**
	 * USER REGISTRATION
	 */
	public final static String PASSWORD_DO_NOT_MATCH ="● As senhas não correspondem";
	public final static String PASSWORD_PATTERN ="A sua senha deve conter:\n "
			+ "● De 6 a 20 caracteres \n"
			+ "● Ao menos uma letra minúscula\n"
			+ "● Ao menos uma letra maiúscula\n"
			+ "● Ao menos um dos caracteres especiais [@#$%]";
	
	
	/**
	 * SENDING EMAIL
	 */
	public final static String ERROR_SENDING="● Não foi possível enviar o e-mail com suas informações de segurança";
	public final static String SENDING_CONFIRMATION="● As informações de segurança foram enviadas para o e-mail {0}";
	
	
	/**
	 * LOGIN
	 */
	public final static String USER_DO_NOT_NATCH= "● Usuário e/ou senha inválido(s)!";
	public final static String USER_LOGOUT= "● Tem certeza que deseja finalizar a sessão do seu usuário?";
	public final static String USER_EXIT= "● Tem certeza que deseja sair do aplicativo?";
	
	
	/**
	 * FILE CHOOSER
	 */
	public final static String FILE_VERY_LARGE = "● Arquivo maior que 512 kb";
	
	
	/**
	 * IE
	 */
	public final static String IE_INVALID_SIZE = "● IE com quantidade de dígitos inválida.";
	public final static String IE_TESTER_DIGIT_INVALID = "● IE com dígito verificador inválido.";
	public final static String IE_INVALID = "● Inscrição estadual inválida.";
	
	
	/**
	 * KEY ACCESS
	 */
	public final static String KEY_ACCESS_INVALID_SIZE = "● Chave de Acesso deve possuir 44 dígitos.";
	public final static String KEY_ACCESS_INVALID_INFORMATION = "● Campo\"{0}\" da Chave de Acesso com valor inválido.";
	
}
