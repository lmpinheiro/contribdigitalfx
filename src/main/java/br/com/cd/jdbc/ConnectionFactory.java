package br.com.cd.jdbc;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

/**
 * Data base connection test
 *
 * @author lmpinheiro
 */
public class ConnectionFactory {
	

	
	private static String host = "localhost";
	private static String port = "3306";
	private static String database = "contdigital";
	private static String user = "root";
	private static String password = "admin";
	
	/**
	 * Main class for connection test
	 * 
	 * @param argv
	 */
	public static void main(String[] argv) {
		 
		System.out.println("-------- MySQL JDBC Connection Testing ------------");
	 
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your MySQL JDBC Driver?");
			e.printStackTrace();
			return;
		}
	 
		System.out.println("MySQL JDBC Driver Registered!");
		Connection connection = null;
	 
		try {
			connection = (Connection) DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+database, user, password);
	 
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
	 
		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}
	}
	
	/**
	 * Get connection
	 * 
	 * @return Connection
	 */
	public Connection getConnection() {
	     try {
	         return (Connection)DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+database, user, password);
	     } catch (SQLException e) {
	         throw new RuntimeException(e);
	     }
	 }
}
