package br.com.cd.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * User Authority Entity Mapping
 * 
 * @author lmpinheiro
 *
 */
@Entity
@Table(name="tb_user_authority")
public class UserAuthorityModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_AUTHORITY", nullable = false)
	private int id;
	
	@JoinColumn(name = "ID_USER", referencedColumnName = "ID_USER")
    @ManyToOne
    private UserModel userModel;

	@Column(name = "X_AUTHORITY", nullable = false)
	private String authority;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public UserModel getUserModel() {
		return userModel;
	}

	public void setUserModel(UserModel userModel) {
		this.userModel = userModel;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	@Override
	public String toString() {
		return "UserAuthorityModel [id=" + id + ", userModel=" + userModel
				+ ", authority=" + authority + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authority == null) ? 0 : authority.hashCode());
		result = prime * result + id;
		result = prime * result
				+ ((userModel == null) ? 0 : userModel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserAuthorityModel other = (UserAuthorityModel) obj;
		if (authority == null) {
			if (other.authority != null)
				return false;
		} else if (!authority.equals(other.authority))
			return false;
		if (id != other.id)
			return false;
		if (userModel == null) {
			if (other.userModel != null)
				return false;
		} else if (!userModel.equals(other.userModel))
			return false;
		return true;
	}
	
}
