package br.com.cd.model;


import org.joda.time.LocalDateTime;

import br.com.cd.enumeration.FiscalDocumentEnum.IssueTypeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.SituationTypeEnum;

/**
 * Seach Fiscal Document Object
 * 
 * @author lmpinheiro
 *
 */
public class SearchDocumentModel {
	
	private LocalDateTime dtInitialIssue;
	private LocalDateTime dtFinalIssue;
	private String cnpj;
	private String companyName;
	private String number;
	private String serie;
	private SituationTypeEnum situation;
	private IssueTypeEnum issue;
	
	
	public LocalDateTime getDtInitialIssue() {
		return dtInitialIssue;
	}
	public void setDtInitialIssue(LocalDateTime dtInitialIssue) {
		this.dtInitialIssue = dtInitialIssue;
	}
	public LocalDateTime getDtFinalIssue() {
		return dtFinalIssue;
	}
	public void setDtFinalIssue(LocalDateTime dtFinalIssue) {
		this.dtFinalIssue = dtFinalIssue;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public SituationTypeEnum getSituation() {
		return situation;
	}
	public void setSituation(SituationTypeEnum situation) {
		this.situation = situation;
	}
	public IssueTypeEnum getIssue() {
		return issue;
	}
	public void setIssue(IssueTypeEnum issue) {
		this.issue = issue;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result
				+ ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result
				+ ((dtFinalIssue == null) ? 0 : dtFinalIssue.hashCode());
		result = prime * result
				+ ((dtInitialIssue == null) ? 0 : dtInitialIssue.hashCode());
		result = prime * result + ((issue == null) ? 0 : issue.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((serie == null) ? 0 : serie.hashCode());
		result = prime * result
				+ ((situation == null) ? 0 : situation.hashCode());
		return result;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchDocumentModel other = (SearchDocumentModel) obj;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (companyName == null) {
			if (other.companyName != null)
				return false;
		} else if (!companyName.equals(other.companyName))
			return false;
		if (dtFinalIssue == null) {
			if (other.dtFinalIssue != null)
				return false;
		} else if (!dtFinalIssue.equals(other.dtFinalIssue))
			return false;
		if (dtInitialIssue == null) {
			if (other.dtInitialIssue != null)
				return false;
		} else if (!dtInitialIssue.equals(other.dtInitialIssue))
			return false;
		if (issue != other.issue)
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (serie == null) {
			if (other.serie != null)
				return false;
		} else if (!serie.equals(other.serie))
			return false;
		if (situation != other.situation)
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "SearchFiscalDocumentModel [dtInitialIssue=" + dtInitialIssue
				+ ", dtFinalIssue=" + dtFinalIssue + ", cnpj=" + cnpj
				+ ", companyName=" + companyName + ", number=" + number
				+ ", serie=" + serie + ", situation=" + situation + ", issue="
				+ issue + "]";
	}
}
