package br.com.cd.model;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import br.com.cd.enumeration.FiscalDocumentEnum.IssueTypeEnum;
import br.com.cd.enumeration.FiscalDocumentEnum.SituationTypeEnum;
import br.com.cd.util.FormatterTextUtil;

/**
 * Fiscal Document Entity Mapping
 * 
 * @author lmpinheiro
 *
 */
@Entity
@Table(name="tb_fiscal_document")
public class FiscalDocumentModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_FISCAL_DOCUMENT", nullable = false)
	private int id;
	
	@OneToOne
    @JoinColumn(name = "ID_ISSUER")
    private IssuerModel issuerModel;
	
	@Column(name = "X_NUMBER", nullable = false)
	private String number;
	
	@Column(name = "X_SERIE", nullable = false)
	private String serie;
	
	@Column(name = "X_MODEL", nullable = false)
	private String model;
	
	@Column(name = "X_CNPJ", nullable = false)
	private String cnpj;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "X_SITUATION", nullable = false)
	private SituationTypeEnum situation;
	
	@Column(name = "X_MONTH", nullable = false)
	private String month;
	
	@Column(name = "X_YEAR", nullable = false)
	private String year;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "X_TYPE_ISSUE", nullable = false)
	private IssueTypeEnum issueType;
	
	@Column(name = "DT_ISSUE", nullable = false)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime dtIssue;
	
	@Column(name = "DT_AUTORIZATION", nullable = false)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime dtAutorization;
	
	@Column(name = "X_COD_KEY_ACCESS", nullable = false)
	private String codAccessKey;
	
	@Column(name = "X_DIGIT_KEY_ACCESS", nullable = false)
	private String digitAccessKey;
	
	@Column(name = "X_RECEIVER_DOCUMENT", nullable = false)
	private String receiverDocument;
	
	@Column(name = "SG_UF_DEST", nullable = false)
	private String ufDest;
	
	@Column(name = "X_NUMBER_RECEIPT", nullable = false)
	private String receiptNumber;
	
	@Column(name = "X_NR_PROTOCOL", nullable = false)
	private String protocolNumber;
	
	@Column(name = "DT_PROTOCOL", nullable = false)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime dtProcol;
	
	@Lob
    @Column(name = "BL_XML_DOC")
    private byte[] xmlDoc;
	
	@Lob
    @Column(name = "BL_PROTOCOL")
    private byte[] xmlProtocol;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public IssuerModel getIssuerModel() {
		return issuerModel;
	}

	public void setIssuerModel(IssuerModel issuerModel) {
		this.issuerModel = issuerModel;
	}

	public String getCnpj() {
		return cnpj;
	}
	
	public String getCnpjFormatted() {
		return FormatterTextUtil.cnpjField(cnpj);
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public SituationTypeEnum getSituation() {
		return situation;
	}

	public void setSituation(SituationTypeEnum situation) {
		this.situation = situation;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public IssueTypeEnum getIssueType() {
		return issueType;
	}

	public void setIssueType(IssueTypeEnum issueType) {
		this.issueType = issueType;
	}

	public LocalDateTime getDtIssue() {
		return dtIssue;
	}
	
	public String getDtIssueFormatted() {
		return dtIssue.toString("dd/MM/yyyy - HH:mm:ss");
	}

	public void setDtIssue(LocalDateTime dtIssue) {
		this.dtIssue = dtIssue;
	}

	public LocalDateTime getDtAutorization() {
		return dtAutorization;
	}

	public void setDtAutorization(LocalDateTime dtAutorization) {
		this.dtAutorization = dtAutorization;
	}

	public String getCodAccessKey() {
		return codAccessKey;
	}

	public void setCodAccessKey(String codAccessKey) {
		this.codAccessKey = codAccessKey;
	}

	public String getDigitAccessKey() {
		return digitAccessKey;
	}

	public void setDigitAccessKey(String digitAccessKey) {
		this.digitAccessKey = digitAccessKey;
	}

	public String getReceiverDocument() {
		return receiverDocument;
	}

	public void setReceiverDocument(String receiverDocument) {
		this.receiverDocument = receiverDocument;
	}

	public String getUfDest() {
		return ufDest;
	}

	public void setUfDest(String ufDest) {
		this.ufDest = ufDest;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getProtocolNumber() {
		return protocolNumber;
	}

	public void setProtocolNumber(String protocolNumber) {
		this.protocolNumber = protocolNumber;
	}

	public LocalDateTime getDtProcol() {
		return dtProcol;
	}

	public void setDtProcol(LocalDateTime dtProcol) {
		this.dtProcol = dtProcol;
	}

	public byte[] getXmlDoc() {
		return xmlDoc;
	}

	public void setXmlDoc(byte[] xmlDoc) {
		this.xmlDoc = xmlDoc;
	}

	public byte[] getXmlProtocol() {
		return xmlProtocol;
	}

	public void setXmlProtocol(byte[] xmlProtocol) {
		this.xmlProtocol = xmlProtocol;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result
				+ ((codAccessKey == null) ? 0 : codAccessKey.hashCode());
		result = prime * result
				+ ((digitAccessKey == null) ? 0 : digitAccessKey.hashCode());
		result = prime * result
				+ ((dtAutorization == null) ? 0 : dtAutorization.hashCode());
		result = prime * result + ((dtIssue == null) ? 0 : dtIssue.hashCode());
		result = prime * result
				+ ((dtProcol == null) ? 0 : dtProcol.hashCode());
		result = prime * result + id;
		result = prime * result
				+ ((issueType == null) ? 0 : issueType.hashCode());
		result = prime * result
				+ ((issuerModel == null) ? 0 : issuerModel.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result
				+ ((protocolNumber == null) ? 0 : protocolNumber.hashCode());
		result = prime * result
				+ ((receiptNumber == null) ? 0 : receiptNumber.hashCode());
		result = prime
				* result
				+ ((receiverDocument == null) ? 0 : receiverDocument.hashCode());
		result = prime * result + ((serie == null) ? 0 : serie.hashCode());
		result = prime * result
				+ ((situation == null) ? 0 : situation.hashCode());
		result = prime * result + ((ufDest == null) ? 0 : ufDest.hashCode());
		result = prime * result + Arrays.hashCode(xmlDoc);
		result = prime * result + Arrays.hashCode(xmlProtocol);
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FiscalDocumentModel other = (FiscalDocumentModel) obj;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (codAccessKey == null) {
			if (other.codAccessKey != null)
				return false;
		} else if (!codAccessKey.equals(other.codAccessKey))
			return false;
		if (digitAccessKey == null) {
			if (other.digitAccessKey != null)
				return false;
		} else if (!digitAccessKey.equals(other.digitAccessKey))
			return false;
		if (dtAutorization == null) {
			if (other.dtAutorization != null)
				return false;
		} else if (!dtAutorization.equals(other.dtAutorization))
			return false;
		if (dtIssue == null) {
			if (other.dtIssue != null)
				return false;
		} else if (!dtIssue.equals(other.dtIssue))
			return false;
		if (dtProcol == null) {
			if (other.dtProcol != null)
				return false;
		} else if (!dtProcol.equals(other.dtProcol))
			return false;
		if (id != other.id)
			return false;
		if (issueType != other.issueType)
			return false;
		if (issuerModel == null) {
			if (other.issuerModel != null)
				return false;
		} else if (!issuerModel.equals(other.issuerModel))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (protocolNumber == null) {
			if (other.protocolNumber != null)
				return false;
		} else if (!protocolNumber.equals(other.protocolNumber))
			return false;
		if (receiptNumber == null) {
			if (other.receiptNumber != null)
				return false;
		} else if (!receiptNumber.equals(other.receiptNumber))
			return false;
		if (receiverDocument == null) {
			if (other.receiverDocument != null)
				return false;
		} else if (!receiverDocument.equals(other.receiverDocument))
			return false;
		if (serie == null) {
			if (other.serie != null)
				return false;
		} else if (!serie.equals(other.serie))
			return false;
		if (situation != other.situation)
			return false;
		if (ufDest == null) {
			if (other.ufDest != null)
				return false;
		} else if (!ufDest.equals(other.ufDest))
			return false;
		if (!Arrays.equals(xmlDoc, other.xmlDoc))
			return false;
		if (!Arrays.equals(xmlProtocol, other.xmlProtocol))
			return false;
		if (year == null) {
			if (other.year != null)
				return false;
		} else if (!year.equals(other.year))
			return false;
		return true;
	}

	
	@Override
	public String toString() {
		return "FiscalDocumentModel [id=" + id + ", issuerModel=" + issuerModel
				+ ", number=" + number
				+ ", serie=" + serie + ", model=" + model + ", cnpj=" + cnpj
				+ ", situation=" + situation + ", month=" + month + ", year="
				+ year + ", issueType=" + issueType + ", dtIssue=" + dtIssue
				+ ", dtAutorization=" + dtAutorization + ", codAccessKey="
				+ codAccessKey + ", digitAccessKey=" + digitAccessKey
				+ ", receiverDocument=" + receiverDocument + ", ufDest="
				+ ufDest + ", receiptNumber=" + receiptNumber
				+ ", protocolNumber=" + protocolNumber + ", dtProcol="
				+ dtProcol + ", xmlDoc=" + Arrays.toString(xmlDoc)
				+ ", xmlProtocol=" + Arrays.toString(xmlProtocol) + "]";
	}
}
