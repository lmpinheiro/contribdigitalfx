package br.com.cd.model;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.cd.enumeration.IssuerEnum.RegimeTributarioEnum;
import br.com.cd.util.FormatterTextUtil;

/**
 * Issuer Entity Mapping
 * 
 * @author lmpinheiro
 *
 */
@Entity
@Table(name="tb_issuer")
public class IssuerModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ISSUER", nullable = false)
	private int id;
	
	@JoinColumn(name = "ID_USER", referencedColumnName = "ID_USER")
    @ManyToOne
    private UserModel userModel;
	
	@OneToOne
	@JoinColumn(name = "ID_COUNTY")
    private CountyModel countyModel;

	@Column(name = "X_CNPJ", nullable = false)
	private String cnpj;
	
	@Column(name = "X_COMPANY_NAME", nullable = false)
	private String companyName;
	
	@Column(name = "X_TRADE_NAME", nullable = false)
	private String tradeName;
	
	@Column(name = "X_STREET", nullable = false)
	private String street;
	
	@Column(name = "NR_NUMBER", nullable = false)
	private String number;
	
	@Column(name = "X_COMPLEMENT", nullable = false)
	private String complement;
	
	@Column(name = "X_DISTRICT", nullable = false)
	private String district;
	
	@Column(name = "SG_UF", nullable = false)
	private String uf;
	
	@Column(name = "X_ZIP_CODE", nullable = false)
	private String zipCode;
	
	@Column(name = "X_PHONE", nullable = false)
	private String phone;
	
	@Column(name = "X_IE", nullable = false)
	private String ie;
	
	@Column(name = "X_IE_ST", nullable = false)
	private String ieSt;
	
	@Column(name = "X_IM", nullable = false)
	private String im;
	
	@Column(name = "X_CNAE", nullable = false)
	private String cnae;
	
	@Lob
    @Column(name = "BL_LOGO")
    private byte[] logo;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ST_REGIME_TRIB", nullable = false)
	private RegimeTributarioEnum regimeTrib;
	
	@Column(name = "X_EMAIL", nullable = false)
	private String email;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public UserModel getUserModel() {
		return userModel;
	}

	public void setUserModel(UserModel userModel) {
		this.userModel = userModel;
	}

	public CountyModel getCountyModel() {
		return countyModel;
	}

	public void setCountyModel(CountyModel countyModel) {
		this.countyModel = countyModel;
	}

	public String getCnpj() {
		return cnpj;
	}
	
	public String getCnpjFormatted() {
		return FormatterTextUtil.cnpjField(cnpj);
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPhone() {
		return phone;
	}
	
	public String getPhoneFormatted() {
		return FormatterTextUtil.phoneField(phone);
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIe() {
		return ie;
	}

	public void setIe(String ie) {
		this.ie = ie;
	}

	public String getIeSt() {
		return ieSt;
	}

	public void setIeSt(String ieSt) {
		this.ieSt = ieSt;
	}

	public String getIm() {
		return im;
	}

	public void setIm(String im) {
		this.im = im;
	}

	public String getCnae() {
		return cnae;
	}

	public void setCnae(String cnae) {
		this.cnae = cnae;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public RegimeTributarioEnum getRegimeTrib() {
		return regimeTrib;
	}

	public void setRegimeTrib(RegimeTributarioEnum regimeTrib) {
		this.regimeTrib = regimeTrib;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cnae == null) ? 0 : cnae.hashCode());
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result
				+ ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result
				+ ((complement == null) ? 0 : complement.hashCode());
		result = prime * result
				+ ((district == null) ? 0 : district.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + id;
		result = prime * result + ((ie == null) ? 0 : ie.hashCode());
		result = prime * result + ((ieSt == null) ? 0 : ieSt.hashCode());
		result = prime * result + ((im == null) ? 0 : im.hashCode());
		result = prime * result + Arrays.hashCode(logo);
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result
				+ ((regimeTrib == null) ? 0 : regimeTrib.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		result = prime * result
				+ ((tradeName == null) ? 0 : tradeName.hashCode());
		result = prime * result + ((uf == null) ? 0 : uf.hashCode());
		result = prime * result
				+ ((userModel == null) ? 0 : userModel.hashCode());
		result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IssuerModel other = (IssuerModel) obj;
		if (cnae == null) {
			if (other.cnae != null)
				return false;
		} else if (!cnae.equals(other.cnae))
			return false;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (companyName == null) {
			if (other.companyName != null)
				return false;
		} else if (!companyName.equals(other.companyName))
			return false;
		if (complement == null) {
			if (other.complement != null)
				return false;
		} else if (!complement.equals(other.complement))
			return false;
		if (district == null) {
			if (other.district != null)
				return false;
		} else if (!district.equals(other.district))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id != other.id)
			return false;
		if (ie == null) {
			if (other.ie != null)
				return false;
		} else if (!ie.equals(other.ie))
			return false;
		if (ieSt == null) {
			if (other.ieSt != null)
				return false;
		} else if (!ieSt.equals(other.ieSt))
			return false;
		if (im == null) {
			if (other.im != null)
				return false;
		} else if (!im.equals(other.im))
			return false;
		if (!Arrays.equals(logo, other.logo))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (regimeTrib == null) {
			if (other.regimeTrib != null)
				return false;
		} else if (!regimeTrib.equals(other.regimeTrib))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		if (tradeName == null) {
			if (other.tradeName != null)
				return false;
		} else if (!tradeName.equals(other.tradeName))
			return false;
		if (uf == null) {
			if (other.uf != null)
				return false;
		} else if (!uf.equals(other.uf))
			return false;
		if (userModel == null) {
			if (other.userModel != null)
				return false;
		} else if (!userModel.equals(other.userModel))
			return false;
		if (zipCode == null) {
			if (other.zipCode != null)
				return false;
		} else if (!zipCode.equals(other.zipCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IssuerModel [id=" + id + ", userModel=" + userModel + ", cnpj="
				+ cnpj + ", companyName=" + companyName + ", tradeName="
				+ tradeName + ", street=" + street + ", number=" + number
				+ ", complement=" + complement + ", district=" + district
				+ ", uf=" + uf + ", zipCode=" + zipCode
				+ ", phone=" + phone + ", ie=" + ie + ", ieSt=" + ieSt
				+ ", im=" + im + ", cnae=" + cnae + ", logo="
				+ Arrays.toString(logo) + ", regimeTrib=" + regimeTrib
				+ ", email=" + email + "]";
	}
}
