package br.com.cd.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

/**
 * User Entity Mapping
 * 
 * @author lmpinheiro
 *
 */
@Entity
@Table(name="tb_user")
public class UserModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_USER", nullable = false)
	private int id;

	@Column(name = "X_NAME", nullable = false)
	private String name;
	
	@Column(name = "X_SURNAME", nullable = false)
	private String surname;
	
	@Column(name = "X_EMAIL", nullable = false)
	private String email;
	
	@Column(name = "X_PASS", nullable = false)
	private String pass;
	
	@Column(name = "X_QUESTION", nullable = false)
	private String question;
	
	@Column(name = "X_ANSWER", nullable = false)
	private String answer;
	
	@Column(name = "DT_REGISTER", nullable = false)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate dtRegister;
	
	@Column(name = "IS_ENABLE", nullable = false)
	private int isEnable;
	
	@OneToMany(mappedBy="userModel", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<UserAuthorityModel> userAuthorityModel = new ArrayList<UserAuthorityModel>();

	@OneToMany(mappedBy="userModel", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)  
	private List<IssuerModel> issuerModel = new ArrayList<IssuerModel>();
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public LocalDate getDtRegister() {
		return dtRegister;
	}

	public void setDtRegister(LocalDate dtRegister) {
		this.dtRegister = dtRegister;
	}

	public int getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(int isEnable) {
		this.isEnable = isEnable;
	}

	public List<UserAuthorityModel> getUserAuthorityModel() {
		return userAuthorityModel;
	}

	public void setUserAuthorityModel(List<UserAuthorityModel> userAuthorityModel) {
		this.userAuthorityModel = userAuthorityModel;
	}

	public List<IssuerModel> getIssuerModel() {
		return issuerModel;
	}

	public void setIssuerModel(List<IssuerModel> issuerModel) {
		this.issuerModel = issuerModel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((answer == null) ? 0 : answer.hashCode());
		result = prime * result
				+ ((dtRegister == null) ? 0 : dtRegister.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pass == null) ? 0 : pass.hashCode());
		result = prime * result
				+ ((question == null) ? 0 : question.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserModel other = (UserModel) obj;
		if (answer == null) {
			if (other.answer != null)
				return false;
		} else if (!answer.equals(other.answer))
			return false;
		if (dtRegister == null) {
			if (other.dtRegister != null)
				return false;
		} else if (!dtRegister.equals(other.dtRegister))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pass == null) {
			if (other.pass != null)
				return false;
		} else if (!pass.equals(other.pass))
			return false;
		if (question == null) {
			if (other.question != null)
				return false;
		} else if (!question.equals(other.question))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserModel [id=" + id + ", name=" + name + ", surname="
				+ surname + ", email=" + email + ", pass=" + pass
				+ ", question=" + question + ", answer=" + answer
				+ ", dtRegister=" + dtRegister + "]";
	}
}
