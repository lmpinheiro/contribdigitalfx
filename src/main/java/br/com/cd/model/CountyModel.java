package br.com.cd.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * County Entity Mapping
 * 
 * @author lmpinheiro
 *
 */
@Entity
@Table(name="tb_county")
public class CountyModel {
	
	@Id
	@Column(name = "ID_COUNTY", nullable = false)
	private int id;
	
	@Column(name = "UF", nullable = false)
	private String uf;
	
	@Column(name = "COD_UF", nullable = false)
	private String codUf;
	
	@Column(name = "UF_NAME", nullable = false)
	private String ufName;
	
	@Column(name = "COD_COUNTY", nullable = false)
	private String codCounty;
	
	@Column(name = "COUNTY_NAME", nullable = false)
	private String countyName;

	public int getId() {
		return id;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}
	
	public String getCodUf() {
		return codUf;
	}

	public void setCodUf(String codUf) {
		this.codUf = codUf;
	}

	public String getUfName() {
		return ufName;
	}

	public void setUfName(String ufName) {
		this.ufName = ufName;
	}

	public String getCodCounty() {
		return codCounty;
	}

	public void setCodCounty(String codCounty) {
		this.codCounty = codCounty;
	}

	public String getCountyName() {
		return countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

//	public IssuerModel getIssuerModel() {
//		return issuerModel;
//	}

//	public void setIssuerModel(IssuerModel issuerModel) {
//		this.issuerModel = issuerModel;
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codCounty == null) ? 0 : codCounty.hashCode());
		result = prime * result + ((codUf == null) ? 0 : codUf.hashCode());
		result = prime * result
				+ ((countyName == null) ? 0 : countyName.hashCode());
		result = prime * result + id;
		result = prime * result + ((uf == null) ? 0 : uf.hashCode());
		result = prime * result + ((ufName == null) ? 0 : ufName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CountyModel other = (CountyModel) obj;
		if (codCounty == null) {
			if (other.codCounty != null)
				return false;
		} else if (!codCounty.equals(other.codCounty))
			return false;
		if (codUf == null) {
			if (other.codUf != null)
				return false;
		} else if (!codUf.equals(other.codUf))
			return false;
		if (countyName == null) {
			if (other.countyName != null)
				return false;
		} else if (!countyName.equals(other.countyName))
			return false;
		if (id != other.id)
			return false;
		if (uf == null) {
			if (other.uf != null)
				return false;
		} else if (!uf.equals(other.uf))
			return false;
		if (ufName == null) {
			if (other.ufName != null)
				return false;
		} else if (!ufName.equals(other.ufName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return countyName;
	}
}
