package br.com.cd.dao;

import java.util.List;

import br.com.cd.model.UserModel;
 
/**
 * Interface for UserDao persistent class
 * 
 * @author lmpinheiro
 */
public interface UserDao {
 
    /**
     * Save user on DB
     * 
     * @param user
     */
    void saveUser(UserModel user);
     
    /**
     * Find all users on DB
     * 
     * @return List<UserModel>
     */
    List<UserModel> findAllUsers();
     
    /**
     * Delete a user by ID
     * 
     * @param id
     */
    void deleteUserById(String id);
     
    /**
     * Find a user by Email
     * 
     * @param email
     * @return UserModel
     */
    UserModel findByEmail(String email);
     
    /**
     * Update the user informations
     * 
     * @param user
     */
    void updateUser(UserModel user);
}