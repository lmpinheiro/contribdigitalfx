package br.com.cd.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.com.cd.model.UserModel;

/**
 * @author lmpinheiro
 *
 * Implementation for UserDao persistent class
 */
@Repository("userDao")
public class UserDaoImpl extends AbstractDao implements UserDao{
	private static final Logger logger = Logger.getLogger(UserDaoImpl.class);
	
	/**
     * Save user on DB
     * 
     * @param user
     */
	public void saveUser(UserModel user) {
		try{
			logger.info("Saving User: "+user.getEmail());
			persist(user);
		}catch(Exception e){
			logger.error(e);
		}
	}

	/**
     * Find all users on DB
     * 
     * @return List<UserModel>
     */
	@SuppressWarnings("unchecked")
	public List<UserModel> findAllUsers() {
		logger.info("Finding all users");
		
		Criteria criteria = getSession().createCriteria(UserModel.class);
		return (List<UserModel>) criteria.list();
	}

	 /**
	 * Delete a user by ID
	 * 
	 * @param id
	 */
	@Deprecated
	public void deleteUserById(String id) {
		logger.info("Deleting user id: "+id);
		
		Query query = getSession().createSQLQuery("delete from tb_user where id = :id");
		query.setString("id", id);
		query.executeUpdate();
	}

	/**
     * Find a user by Email
     * 
     * @param email
     * @return UserModel
     */
	public UserModel findByEmail(String email){
		logger.info("Finding user by email: "+email);
		
		Criteria criteria = getSession().createCriteria(UserModel.class);
		criteria.add(Restrictions.eq("email",email));
		return (UserModel) criteria.uniqueResult();
	}
	
	/**
     * Update the user informations
     * 
     * @param user
     */
	public void updateUser(UserModel user){
		logger.info("Updating user: "+user.getEmail());
		getSession().update(user);
	}
	
}
