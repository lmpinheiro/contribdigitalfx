package br.com.cd.dao;

import java.util.List;

import br.com.cd.model.CountyModel;

 
/**
 * Interface for CountyDao persistent class
 * 
 * @author lmpinheiro
 */
public interface CountyDao {
 
    /**
     * Save County on DB
     * 
     * @param county
     */
	@Deprecated
    void saveCounty(CountyModel county);
     
    /**
     * Find all County on DB
     * 
     * @return List<CountyModel>
     */
    List<CountyModel> findAllCountys();
     
    /**
     * Delete a County by ID
     * 
     * @param id
     */
    @Deprecated
    void deleteCountyById(String id);
     
    /**
     * Find a County by UF
     * 
     * @param uf
     * @return List<CountyModel>
     */
    List<CountyModel> findCountyByUf(String uf);
    
    /**
     * Find a County by Cod
     * 
     * @param cod
     * @return CountyModel
     */
    CountyModel findCountyByCod(String cod);
    
    /**
     * Find  UF's
     * 
     * @return List<CountyModel>
     */
    List<String> findUfs();
    
    /**
     * Update the County informations
     * 
     * @param county
     */
    @Deprecated
    void updateCounty(CountyModel county);
}