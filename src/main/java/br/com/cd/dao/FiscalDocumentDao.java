package br.com.cd.dao;

import java.util.List;

import br.com.cd.model.FiscalDocumentModel;
import br.com.cd.model.SearchDocumentModel;

 
/**
 * Interface for FiscalDocumentDao persistent class
 * 
 * @author lmpinheiro
 */
public interface FiscalDocumentDao {
 
    /**
     * Save Fiscal Document on DB
     * 
     * @param fiscalDocument
     */
    void saveFiscalDocument(FiscalDocumentModel fiscalDocument);
     
    /**
     * Find Fiscal Document by Issuer and User
     * 
     * @return List<FiscalDocumentModel>
     */
    List<FiscalDocumentModel> findByIssuerAndUser();
    
    /**
     * Find all Fiscal Document on DB
     * 
     * @return List<FiscalDocumentModel>
     */
    List<FiscalDocumentModel> findAllFiscalDocuments();
     
    /**
     * Delete a Fiscal Document by ID
     * 
     * @param id
     */
    void deleteFiscalDocumentById(int id);
     
    /**
     * Find a Fiscal Document by SearchDocument Object
     * 
     * @param searchDocumentModel
     * @return List<FiscalDocumentModel>
     */
    List<FiscalDocumentModel> findBySearchDocument(SearchDocumentModel searchDocumentModel);
     
    /**
     * Update the Fiscal Document informations
     * 
     * @param fiscalDocument
     */
    void updateFiscalDocument(FiscalDocumentModel fiscalDocument);
}