package br.com.cd.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.com.cd.model.CountyModel;

/**
 * @author lmpinheiro
 *
 * Implementation for CountyDao persistent class
 */
@Repository("countyDao")
public class CountyDaoImpl extends AbstractDao implements CountyDao{
	private static final Logger logger = Logger.getLogger(CountyDaoImpl.class);
	
	/**
     * Save county on DB
     * 
     * @param county
     */
	@Deprecated
	public void saveCounty(CountyModel county) {
		try{
			logger.info("Saving County: "+county.getId());
			persist(county);
		}catch(Exception e){
			logger.error(e);
		}
	}

	/**
     * Find all countys on DB
     * 
     * @return List<CountyModel>
     */
	@SuppressWarnings("unchecked")
	public List<CountyModel> findAllCountys() {
		logger.info("Finding all countys");
		
		Criteria criteria = getSession().createCriteria(CountyModel.class);
		return (List<CountyModel>) criteria.list();
	}

	 /**
	 * Delete a county by ID
	 * 
	 * @param id
	 */
	@Deprecated
	public void deleteCountyById(String id) {
		logger.info("Deleting county id: "+id);
		
		Query query = getSession().createSQLQuery("delete from tb_county where id = :id");
		query.setString("id", id);
		query.executeUpdate();
	}

	/**
     * Find a county by UF
     * 
     * @param uf
     * @return CountyModel
     */
	public List<CountyModel> findCountyByUf(String uf){
		logger.info("Finding county by UF: "+uf);
		
		Criteria criteria = getSession().createCriteria(CountyModel.class);
		criteria.add(Restrictions.eq("uf",uf));
		return (List<CountyModel>) criteria.list();
	}
	
	/**
     * Find a county by Code
     * 
     * @param cod
     * @return CountyModel
     */
	public CountyModel findCountyByCod(String cod){
		logger.info("Finding county by Code: "+cod);
		
		Criteria criteria = getSession().createCriteria(CountyModel.class);
		criteria.add(Restrictions.eq("id",cod));
		return  (CountyModel) criteria.uniqueResult();
	}
	
	/**
     * Find  UF's
     * 
     * @return List<CountyModel>
     */
	public List<String> findUfs(){
		logger.info("Finding UF's ");
//		TODO: Create a grouping query
		Criteria criteria = getSession().createCriteria(CountyModel.class);
		criteria.setProjection( Projections.projectionList()
//			    .add( Projections.property("codUf").as("codUf") )
			    .add( Projections.groupProperty("uf").as("uf") ));
		return (List<String>) criteria.list();
	}
	
	/**
     * Update the county informations
     * 
     * @param county
     */
	@Deprecated
	public void updateCounty(CountyModel county){
		logger.info("Updating county: "+county.getId());
		getSession().update(county);
	}
	
}
