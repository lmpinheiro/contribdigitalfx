package br.com.cd.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
 
/**
 * Generic DAO structure
 * 
 * @author lmpinheiro
 */
public abstract class AbstractDao {
 
    @Autowired
    private SessionFactory sessionFactory;
 
    /**
     * Get current session in context
     * 
     * @return Session
     */
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
 
    
    /**
     * Persist the entity object
     * 
     * @param entity
     */
    public void persist(Object entity) {
        getSession().save(entity);
    }
 
    
    /**
     * Delete the entity object that was referenced
     * 
     * @param entity
     */
    public void delete(Object entity) {
        getSession().delete(entity);
    }
}