package br.com.cd.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.com.cd.model.IssuerModel;
import br.com.cd.model.UserModel;

/**
 * @author lmpinheiro
 *
 * Implementation for IssuerDao persistent class
 */
@Repository("issuerDao")
public class IssuerDaoImpl extends AbstractDao implements IssuerDao{
	private static final Logger logger = Logger.getLogger(IssuerDaoImpl.class);
	
	/**
     * Save issuer on DB
     * 
     * @param issuer
     */
	public void saveIssuer(IssuerModel issuer) {
		try{
			logger.info("Saving Issuer: "+issuer.getCnpj());
			persist(issuer);
		}catch(Exception e){
			logger.error(e);
		}
	}

	/**
     * Find all issuers on DB
     * 
     * @return List<IssuerModel>
     */
	@SuppressWarnings("unchecked")
	public List<IssuerModel> findAllIssuers() {
		logger.info("Finding all issuers");
		
		Criteria criteria = getSession().createCriteria(IssuerModel.class);
		return (List<IssuerModel>) criteria.list();
	}

	 /**
	 * Delete a issuer by ID
	 * 
	 * @param id
	 */
	public void deleteIssuerById(int id) {
		logger.info("Deleting issuer id: "+id);
		
		Query query = getSession().createSQLQuery("delete from TB_ISSUER where ID_ISSUER = :id");
		query.setInteger("id", id);
		query.executeUpdate();
	}

	/**
     * Find a issuer by CNPJ
     * 
     * @param cnpj
     * @return IssuerModel
     */
	public IssuerModel findByCnpj(String cnpj){
		logger.info("Finding issuer by cnpj: "+cnpj);
		
		Criteria criteria = getSession().createCriteria(IssuerModel.class);
		criteria.add(Restrictions.eq("cnpj",cnpj));
		return (IssuerModel) criteria.uniqueResult();
	}
	
	/**
     * Find a issuer by CNPJ
     * 
     * @param cnpj
     * @return IssuerModel
     */
	public List<IssuerModel> findByUser(UserModel user){
		logger.info("Finding issuer by user: "+user.getEmail());
		
		Criteria criteria = getSession().createCriteria(IssuerModel.class);
		criteria.add(Restrictions.eq("userModel",user));
		return (List<IssuerModel>) criteria.list();
	}
	
	/**
     * Update the issuer informations
     * 
     * @param issuer
     */
	public void updateIssuer(IssuerModel issuer){
		logger.info("Updating issuer: "+issuer.getCnpj());
		getSession().update(issuer);
	}

}
