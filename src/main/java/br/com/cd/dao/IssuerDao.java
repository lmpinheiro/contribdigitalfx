package br.com.cd.dao;

import java.util.List;

import br.com.cd.model.IssuerModel;
import br.com.cd.model.UserModel;

 
/**
 * Interface for IssueDao persistent class
 * 
 * @author lmpinheiro
 */
public interface IssuerDao {
 
    /**
     * Save Issuer on DB
     * 
     * @param issuer
     */
    void saveIssuer(IssuerModel issuer);
     
    /**
     * Find all Issuer on DB
     * 
     * @return List<IssuerModel>
     */
    List<IssuerModel> findByUser(UserModel user);
    
    /**
     * Find Issuer By User
     * 
     * @return List<IssuerModel>
     */
    List<IssuerModel> findAllIssuers();
     
    /**
     * Delete a Issuer by ID
     * 
     * @param id
     */
    void deleteIssuerById(int id);
     
    /**
     * Find a Issuer by Email
     * 
     * @param cnpj
     * @return IssuerModel
     */
    IssuerModel findByCnpj(String cnpj);
     
    /**
     * Update the Issuer informations
     * 
     * @param issuer
     */
    void updateIssuer(IssuerModel issuer);
}