package br.com.cd.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.com.cd.control.InjectionController;
import br.com.cd.model.FiscalDocumentModel;
import br.com.cd.model.IssuerModel;
import br.com.cd.model.SearchDocumentModel;

/**
 * @author lmpinheiro
 *
 * Implementation for FiscalDocumentDao persistent class
 */
@Repository("fiscalDocumentDao")
public class FiscalDocumentDaoImpl extends AbstractDao implements FiscalDocumentDao{
	private static final Logger logger = Logger.getLogger(FiscalDocumentDaoImpl.class);
	
	/**
     * Save fiscal document on DB
     * 
     * @param fiscalDocument
     */
	public void saveFiscalDocument(FiscalDocumentModel fiscalDocument) {
		try{
			logger.info("Saving FiscalDocument: "+fiscalDocument.getCnpj());
			persist(fiscalDocument);
		}catch(Exception e){
			logger.error(e);
		}
	}

	/**
     * Find all fiscal documents on DB
     * 
     * @return List<FiscalDocumentModel>
     */
	@SuppressWarnings("unchecked")
	public List<FiscalDocumentModel> findAllFiscalDocuments() {
		logger.info("Finding all fiscal documents");
		
		Criteria criteria = getSession().createCriteria(FiscalDocumentModel.class);
		return (List<FiscalDocumentModel>) criteria.list();
	}

	 /**
	 * Delete a fiscal document by ID
	 * 
	 * @param id
	 */
	public void deleteFiscalDocumentById(int id) {
		logger.info("Deleting fiscal document by id: "+id);
		
		Query query = getSession().createSQLQuery("delete from TB_FISCAL_DOCUMENT where ID_FISCAL_DOCUMENT = :id");
		query.setInteger("id", id);
		query.executeUpdate();
	}

	/**
     * Find a fiscal document by SearchDocument Object
     * 
     * @param SearchDocumentModel
     * @return List<FiscalDocumentModel>
     */
	public List<FiscalDocumentModel> findBySearchDocument(SearchDocumentModel searchDocumentModel){
		logger.info("Finding fiscal document by searchDocumentModel");
		
		IssuerModel issuerLogged = InjectionController.getIssuerService().getIssuerSession();
		
		Criteria criteria = getSession().createCriteria(FiscalDocumentModel.class);
		Disjunction disjunction = Restrictions.disjunction();
		Conjunction conjunction = Restrictions.conjunction();
		conjunction.add(Restrictions.eq("issuerModel", issuerLogged));
		if(searchDocumentModel.getSituation()!=null)
			conjunction.add(Restrictions.eq("situation", searchDocumentModel.getSituation()));
		if(searchDocumentModel.getIssue()!=null)
			conjunction.add(Restrictions.eq("issueType", searchDocumentModel.getIssue()));
		if(!searchDocumentModel.getNumber().isEmpty())
			conjunction.add(Restrictions.ilike("number", searchDocumentModel.getNumber(), MatchMode.ANYWHERE));
		if(!searchDocumentModel.getSerie().isEmpty())
			conjunction.add(Restrictions.ilike("serie", searchDocumentModel.getSerie(), MatchMode.ANYWHERE));
		if(!searchDocumentModel.getCnpj().isEmpty())
			conjunction.add(Restrictions.ilike("cnpj", searchDocumentModel.getCnpj(), MatchMode.ANYWHERE));
		if(searchDocumentModel.getDtInitialIssue()!=null)
			conjunction.add(Restrictions.ge("dtIssue", searchDocumentModel.getDtInitialIssue()) );
		if(searchDocumentModel.getDtFinalIssue()!=null)
			conjunction.add(Restrictions.lt("dtIssue", searchDocumentModel.getDtFinalIssue()) );
		
		criteria.add(conjunction);
		return (List<FiscalDocumentModel>) criteria.list();
	}
	
	/**
     * Find a fiscal document by Issuer and User
     * 
     * @return List<FiscalDocumentModel>
     */
	public List<FiscalDocumentModel> findByIssuerAndUser(){
		logger.info("Finding fiscal document by user ");
		
		Criteria criteria = getSession().createCriteria(FiscalDocumentModel.class);
//		criteria.add(Restrictions.eq("userModel",user));
		return (List<FiscalDocumentModel>) criteria.list();
	}
	
	/**
     * Update the fiscal document informations
     * 
     * @param fiscalDocument
     */
	public void updateFiscalDocument(FiscalDocumentModel fiscalDocument){
//		logger.info("Updating fiscal document: "+fiscalDocument.getCnpj());
		getSession().update(fiscalDocument);
	}

}
