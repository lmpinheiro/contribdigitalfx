package br.com.cd.service;

import java.util.List;

import org.springframework.security.access.annotation.Secured;

import br.com.cd.model.CountyModel;
 
/**
 * State Service Interface
 * 
 * @author lmpinheiro
 *
 */
public interface StateService {
	
 
	public void saveCounty(CountyModel county) throws Exception;
 
	
	public List<CountyModel> findAllCountys();
	
 
    @Secured("ROLE_ADMIN")
    public void deleteCountyById(String id);
    
 
    public List<CountyModel> findByUf(String uf);
    
    
    public List<String> findUfs();
    
    
    public CountyModel findByCod(String cod);
    
    
    @Secured("ROLE_ADMIN")
    public void updateCounty(CountyModel user);
}