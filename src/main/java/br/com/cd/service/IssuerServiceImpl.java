package br.com.cd.service;

import java.text.MessageFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.cd.constant.MessageConstant;
import br.com.cd.dao.IssuerDao;
import br.com.cd.model.IssuerModel;
import br.com.cd.model.UserModel;
 
 
@Service("issuerService")
@Transactional
public class IssuerServiceImpl implements IssuerService{
	private static final Logger logger = Logger.getLogger(IssuerServiceImpl.class);
	
	private IssuerModel issuerSession;
 
    @Autowired
    private IssuerDao dao;
     
    public void saveIssuer(IssuerModel issuer) throws Exception {
    	logger.info("Calling service saveIssuer: "+issuer.getCnpj());
    	
    	if(findByCnpj(issuer.getCnpj())==null){
    		dao.saveIssuer(issuer);
    	}else{
    		throw new Exception(MessageFormat.format(MessageConstant.ALREADY_INSERTED,"Emitente"));
    	}
    }
 
    public List<IssuerModel> findAllIssuers() {
    	logger.info("Calling service findAllIssuers");
    	
        return dao.findAllIssuers();
    }
 
    public void deleteIssuerById(int id) {
    	logger.info("Calling service deleteIssuerById: "+ id);
    	
        dao.deleteIssuerById(id);
    }
    
    public void deleteIssuers(List<IssuerModel> issuerModels) {
    	logger.info("Calling service deleteIssuers: "+ issuerModels.size());
    	
    	issuerModels.forEach(issuer -> dao.deleteIssuerById(issuer.getId()));
    }
 
    public IssuerModel findByCnpj(String cnpj) {
    	logger.info("Calling service findByCnpj: "+ cnpj);
    	
        return dao.findByCnpj(cnpj);
    }
    
 
    public void updateIssuer(IssuerModel issuer){
    	logger.info("Calling service updateIssuer: "+ issuer.getCnpj());
    	
        dao.updateIssuer(issuer);
    }

	public List<IssuerModel> findByUser(UserModel user) {
		logger.info("Calling service findByUser: "+ user.getEmail());
		
		dao.findByUser(user);
		return null;
	}

	public IssuerModel getIssuerSession() {
		return issuerSession;
	}

	public  void setIssuerSession(IssuerModel issuerSession) {
		this.issuerSession = issuerSession;
	}
	
}