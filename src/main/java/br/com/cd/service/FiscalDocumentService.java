package br.com.cd.service;

import java.util.List;

import org.springframework.security.access.annotation.Secured;

import br.com.cd.model.FiscalDocumentModel;
import br.com.cd.model.SearchDocumentModel;
 
/**
 * Fiscal Document Service Interface
 * 
 * @author lmpinheiro
 *
 */
public interface FiscalDocumentService {
 
	
	public void saveFiscalDocument(FiscalDocumentModel fiscalDocument) throws Exception;
	
	
	public List<FiscalDocumentModel> findAllFiscalDocuments();
	
	
	@Secured("ROLE_ADMIN")
    public void deleteFiscalDocumentById(int id);
	
	
    public void deleteFiscalDocuments(List<FiscalDocumentModel> fiscalDocuments);
    
	
    public List<FiscalDocumentModel> findByIssuerAndUser();

    
    public List<FiscalDocumentModel> findBySearchDocument(SearchDocumentModel searchDocumentModel);
    
    
//    @Secured("ROLE_ADMIN")
    public void updateFiscalDocument(FiscalDocumentModel fiscalDocument);
    
}