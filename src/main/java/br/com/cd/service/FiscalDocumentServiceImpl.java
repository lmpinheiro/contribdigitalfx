package br.com.cd.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.cd.dao.FiscalDocumentDao;
import br.com.cd.model.FiscalDocumentModel;
import br.com.cd.model.SearchDocumentModel;
 
 
@Service("fiscalDocumentService")
@Transactional
public class FiscalDocumentServiceImpl implements FiscalDocumentService{
	private static final Logger logger = Logger.getLogger(FiscalDocumentServiceImpl.class);
	
    @Autowired
    private FiscalDocumentDao dao;
     
    public void saveFiscalDocument(FiscalDocumentModel fiscalDocument) throws Exception {
    	logger.info("Calling service saveFiscalDocument: "+fiscalDocument.getCnpj());
    	
//    	if(findByCnpj(fiscalDocument.getCnpj())==null){
    		dao.saveFiscalDocument(fiscalDocument);
//    	}else{
//    		throw new Exception(MessageFormat.format(MessageConstant.ALREADY_INSERTED,"Emitente"));
//    	}
    }
 
    public List<FiscalDocumentModel> findAllFiscalDocuments() {
    	logger.info("Calling service findAllFiscalDocuments");
    	
        return dao.findAllFiscalDocuments();
    }
 
    public void deleteFiscalDocumentById(int id) {
    	logger.info("Calling service deleteFiscalDocumentById: "+ id);
    	
        dao.deleteFiscalDocumentById(id);
    }
    
    public void deleteFiscalDocuments(List<FiscalDocumentModel> fiscalDocumentModels) {
    	logger.info("Calling service deleteFiscalDocuments: "+ fiscalDocumentModels.size());
    	
    	fiscalDocumentModels.forEach(fiscalDocument -> dao.deleteFiscalDocumentById(fiscalDocument.getId()));
    }
 
    public List<FiscalDocumentModel> findBySearchDocument(SearchDocumentModel searchDocumentModel){
    	logger.info("Calling service findBySearchDocument ");
    	
        return dao.findBySearchDocument(searchDocumentModel);
    }
 
    public void updateFiscalDocument(FiscalDocumentModel fiscalDocument){
    	logger.info("Calling service updateFiscalDocument: "+ fiscalDocument.getCnpj());
    	
        dao.updateFiscalDocument(fiscalDocument);
    }

	public  List<FiscalDocumentModel> findByIssuerAndUser() {
		logger.info("Calling service findByIssuerAndUser ");
		
		dao.findByIssuerAndUser();
		return null;
	}
	
}