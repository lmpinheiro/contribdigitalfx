package br.com.cd.service;

import java.text.MessageFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.cd.constant.MessageConstant;
import br.com.cd.dao.CountyDao;
import br.com.cd.model.CountyModel;
 
 
@Service("stateService")
@Transactional
public class StateServiceImpl implements StateService{
	private static final Logger logger = Logger.getLogger(StateServiceImpl.class);
 
    @Autowired
    private CountyDao dao;
     
    public void saveCounty(CountyModel county) throws Exception {
    	logger.info("Calling service saveCounty: "+county.getCodCounty());
    	
    	if(findByCod(county.getCodCounty())==null){
    		dao.saveCounty(county);
    	}else{
    		throw new Exception(MessageFormat.format(MessageConstant.ALREADY_INSERTED,"Município"));
    	}
    }
 
    public List<CountyModel> findAllCountys() {
    	logger.info("Calling service findAllCountys");
    	
        return dao.findAllCountys();
    }
 
    public void deleteCountyById(String id) {
    	logger.info("Calling service deleteCountyById: "+ id);
    	
        dao.deleteCountyById(id);
    }
 
    public List<CountyModel> findByUf(String uf) {
    	logger.info("Calling service findByUf: "+ uf);
    	
        return dao.findCountyByUf(uf);
    }
    
    public List<String> findUfs() {
    	logger.info("Calling service findUfs ");
    	
        return dao.findUfs();
    }
    
    public CountyModel findByCod(String cod) {
    	logger.info("Calling service findByCod: "+ cod);
    	
        return dao.findCountyByCod(cod);
    }
 
    public void updateCounty(CountyModel county){
    	logger.info("Calling service updateCounty: "+ county.getCodCounty());
    	
        dao.updateCounty(county);
    }
}