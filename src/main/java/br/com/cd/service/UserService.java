package br.com.cd.service;

import java.util.List;

import org.springframework.security.access.annotation.Secured;

import br.com.cd.model.UserModel;
 
/**
 * User Service Interface
 * 
 * @author lmpinheiro
 *
 */
public interface UserService {
	
 
	void saveUser(UserModel user) throws Exception;
	
 
	@Secured("ROLE_ADMIN")
    List<UserModel> findAllUsers();
	
 
    @Secured("ROLE_ADMIN")
    void deleteUserById(String id);
    
 
    UserModel findByEmail(String email);
    
 
    void updateUser(UserModel user);
}