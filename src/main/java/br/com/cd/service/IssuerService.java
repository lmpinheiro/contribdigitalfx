package br.com.cd.service;

import java.util.List;

import org.springframework.security.access.annotation.Secured;

import br.com.cd.model.IssuerModel;
import br.com.cd.model.UserModel;
 
/**
 * Issuer Service Interface
 * 
 * @author lmpinheiro
 *
 */
public interface IssuerService {
	
 
	public void saveIssuer(IssuerModel issuer) throws Exception;
 
	
	public List<IssuerModel> findAllIssuers();
	
 
	@Secured("ROLE_ADMIN")
    public void deleteIssuerById(int id);
	
	
    public void deleteIssuers(List<IssuerModel> issuers);
    
	
    public List<IssuerModel> findByUser(UserModel user);

    
    public IssuerModel findByCnpj(String cnpj);
    
    
//    @Secured("ROLE_ADMIN")
    public void updateIssuer(IssuerModel issuer);
    
    
    public IssuerModel getIssuerSession();
    
    
    public void setIssuerSession(IssuerModel issuer);
}