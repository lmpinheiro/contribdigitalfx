package br.com.cd.service;

import java.text.MessageFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.cd.constant.MessageConstant;
import br.com.cd.dao.UserDao;
import br.com.cd.model.UserModel;
 
 
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{
	private static final Logger logger = Logger.getLogger(UserServiceImpl.class);
 
    @Autowired
    private UserDao dao;
     
    public void saveUser(UserModel user) throws Exception {
    	logger.info("Calling service saveUser: "+user.getEmail());
    	
    	if(findByEmail(user.getEmail())==null){
    		dao.saveUser(user);
    	}else{
    		throw new Exception(MessageFormat.format(MessageConstant.ALREADY_INSERTED,"Usuário"));
    	}
    }
 
    public List<UserModel> findAllUsers() {
    	logger.info("Calling service findAllUsers");
    	
        return dao.findAllUsers();
    }
 
    public void deleteUserById(String id) {
    	logger.info("Calling service deleteUserById: "+ id);
    	
        dao.deleteUserById(id);
    }
 
    public UserModel findByEmail(String email) {
    	logger.info("Calling service findByEmail: "+ email);
    	
        return dao.findByEmail(email);
    }
 
    public void updateUser(UserModel user){
    	logger.info("Calling service updateUser: "+ user.getEmail());
    	
        dao.updateUser(user);
    }
}