package br.com.cd.util;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import br.com.cd.model.UserModel;

/**
 * Security Utilities
 * 
 * @author lmpinheiro
 */
public abstract class SecurityUtil {
	private static final Logger logger = Logger.getLogger(SecurityUtil.class);

	private static Object salt;
	
	
	/**
	 * User Levels Enun
	 *
	 */
	public static enum UserLevelRole { 
		ROLE_GUEST, ROLE_USER, ROLE_ADMIN;
	}

	/**
	 * Generate Hash Password
	 */
	public static String generateHash(String password) {
		logger.info("Calling generateHash");
		
		MessageDigestPasswordEncoder digestPasswordEncoder = getInstanceMessageDisterPassword();
		String encodePassword = digestPasswordEncoder.encodePassword(password, salt);
		return encodePassword;
	}

	/**
	 * Password Encoding type set
	 */
	private static MessageDigestPasswordEncoder getInstanceMessageDisterPassword() {
		logger.info("Calling getInstanceMessageDisterPassword");
		
		MessageDigestPasswordEncoder digestPasswordEncoder = new MessageDigestPasswordEncoder("MD5");
		return digestPasswordEncoder;
	}

	
	/**
	 * Validate if the password is compatible with hash password
	 */
	public static boolean isPasswordValid(String password, String hashPassword) {
		logger.info("Calling isPasswordValid");
		
		MessageDigestPasswordEncoder digestPasswordEncoder = getInstanceMessageDisterPassword();
		return digestPasswordEncoder.isPasswordValid(hashPassword, password,salt);
	}
	
	
	/**
	 * User Authenticate
	 */
	public static void userAuthenticate(UserModel user) {
		logger.info("Calling userAuthenticate");
		
		try {
    		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
    		user.getUserAuthorityModel().forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getAuthority())));
            Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, authorities);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (AuthenticationException e) {
            System.out.print(e);
        }
	}
	
	/**
	 * Get User Logged
	 */
	public static UserModel getUserCurrent() {
		return (UserModel)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	
	/**
	 * User Logougt
	 */
	public static void userLogout() {
		SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
	}
}