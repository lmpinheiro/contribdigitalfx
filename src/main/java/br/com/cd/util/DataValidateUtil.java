package br.com.cd.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import org.apache.log4j.Logger;

/**
 * Validate general data
 * 
 * @author lmpinheiro
 */
public class DataValidateUtil {
	private static final Logger logger = Logger.getLogger(DataValidateUtil.class);

	private static Matcher matcher;
	
	/**
	 * Calc Basis
	 */
	private static final int[] baseCNPJ = { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
	private static final int[] baseCPF = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

	
	/**
	 * Regex field validations
	 */
	public static final String CANE_PATTERN = "[0-2]{2}/[0-9]{4}";
	public static final String CANE_REF_PATTERN = "[0-9]{4}|[0-9]{4}/[0-9]{4}";
	public static final String CNAE_PATTERN = "[0-9]{7}";
	public static final String CNPJ_PATTERN = "[0-9]{14}";
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static final String IE_PATTERN = "[0-9]{2,14}|ISENTO";
	public static final String IEST_PATTERN = "[0-9]{2,14}|ISENTO";
	public static final String NUMBER_PATTERN = "[1-9]{1}[0-9]{0,8}";
	public static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
	public static final String PHONE_PATTERN = "[0-9]{6,14}";
	public static final String SERIE_PATTERN = "0|[1-9]{1}[0-9]{0,2}";
	public static final String SUFRAMA_PATTERN = "[0-9]{8,9}";
	public static final String ZIPCODE_PATTERN = "[0-9]{8}";
	
	
	
	/**
	 * Calculate digit for CPF and CNPJ field
	 */
	private static int calculateDigit(String str, int[] peso) {
		logger.info("Calling calculateDigit");
		
		int soma = 0;
		for (int indice = str.length() - 1, digito; indice >= 0; indice--) {
			digito = Integer.parseInt(str.substring(indice, indice + 1));
			soma += digito * peso[peso.length - str.length() + indice];
		}
		soma = 11 - soma % 11;
		return soma > 9 ? 0 : soma;
	}
	
	
	/**
	 * Validate CNPJ with regular expression
	 */
	public static void isValidCnpj(TextField textField, boolean removeMask) {
		logger.info("Calling isValidCNPJ");
		
		if(!textField.getText().isEmpty()){
			String cnpj = textField.getText();
			if(removeMask) cnpj = MaskFieldUtil.removeMaskNumeral(cnpj);
			if ((cnpj == null) || (cnpj.length() != 14)){
				textField.setStyle("-fx-text-fill:red");
			}else{
				Integer digito1 = calculateDigit(cnpj.substring(0, 12), baseCNPJ);
				Integer digito2 = calculateDigit(cnpj.substring(0, 12) + digito1,
						baseCNPJ);
				
				if(cnpj.equals(cnpj.substring(0, 12) + digito1.toString()
						+ digito2.toString())){
					textField.setStyle("-fx-text-fill:green");
				}else{
					textField.setStyle("-fx-text-fill:red");
				}
			}
		}else{
			textField.setStyle("-fx-text-fill:black");
		}
	}
	
	/**
	 * Validate CNPJ with regular expression
	 */
	public static boolean isValidCnpj(String cnpj, boolean removeMask) {
		logger.info("Calling isValidCNPJ");
		
		if(removeMask) cnpj = MaskFieldUtil.removeMaskNumeral(cnpj);
		Integer digito1 = calculateDigit(cnpj.substring(0, 12), baseCNPJ);
		Integer digito2 = calculateDigit(cnpj.substring(0, 12) + digito1,
				baseCNPJ);
		
		if(cnpj.equals(cnpj.substring(0, 12) + digito1.toString()
				+ digito2.toString())){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Validate Key Access 
	 */
	public static void isValidAccessKey(TextField textField, boolean removeMask) {
		logger.info("Calling isValidAccessKey");
		
		if(!textField.getText().isEmpty()){
			String accessKey = textField.getText();
			if ((accessKey == null) || (accessKey.length() != 44)){
				textField.setStyle("-fx-text-fill:red");
			}else{
				
				if(AccessKeyUtil.validateAccessKey(accessKey).size()==0){
					textField.setStyle("-fx-text-fill:green");
				}else{
					textField.setStyle("-fx-text-fill:red");
				}
			}
		}else{
			textField.setStyle("-fx-text-fill:black");
		}
	}
	
	
	/**
	 * Validate CNPJ and CNPJ with regular expression
	 */
	public static void isValidCnpjCpf(TextField textField, boolean removeMask) {
		logger.info("Calling isValidCnpjCpf");
		
		if(!textField.getText().isEmpty()){
			String cnpjCpf = textField.getText();
			if(removeMask) cnpjCpf = MaskFieldUtil.removeMaskNumeral(cnpjCpf);
			if ((cnpjCpf == null) || ((cnpjCpf.length() != 14) && (cnpjCpf.length() != 11))){
				textField.setStyle("-fx-text-fill:red");
			}else if (cnpjCpf.length() == 14){
				Integer digito1 = calculateDigit(cnpjCpf.substring(0, 12), baseCNPJ);
				Integer digito2 = calculateDigit(cnpjCpf.substring(0, 12) + digito1,
						baseCNPJ);
				
				if(cnpjCpf.equals(cnpjCpf.substring(0, 12) + digito1.toString()
						+ digito2.toString())){
					textField.setStyle("-fx-text-fill:green");
				}else{
					textField.setStyle("-fx-text-fill:red");
				}
			}else{
				Integer digito1 = calculateDigit(cnpjCpf.substring(0, 9), baseCPF);
				Integer digito2 = calculateDigit(cnpjCpf.substring(0, 9) + digito1, baseCPF);
				if(cnpjCpf.equals(cnpjCpf.substring(0, 9) + digito1.toString()
						+ digito2.toString())){
					textField.setStyle("-fx-text-fill:green");
				}else{
					textField.setStyle("-fx-text-fill:red");
				}
			}
		}else{
			textField.setStyle("-fx-text-fill:black");
		}
	}
	
	
	/**
	 * Validate CPF with regular expression
	 */
	public static void isValidCpf(TextField textField, boolean removeMask) {
		logger.info("Calling isValidCPF");
		
		if(!textField.getText().isEmpty()){
			String cpf = textField.getText();
			if(removeMask) cpf = MaskFieldUtil.removeMaskNumeral(cpf);
			if ((cpf == null) || (cpf.length() != 11)){
				textField.setStyle("-fx-text-fill:red");
			}else{
				Integer digito1 = calculateDigit(cpf.substring(0, 9), baseCPF);
				Integer digito2 = calculateDigit(cpf.substring(0, 9) + digito1, baseCPF);
				
				if(cpf.equals(cpf.substring(0, 9) + digito1.toString()
						+ digito2.toString())){
					textField.setStyle("-fx-text-fill:green");
				}else{
					textField.setStyle("-fx-text-fill:red");
				}
			}
		}else{
			textField.setStyle("-fx-text-fill:black");
		}
	}
	
	
	/**
	 * Validate Date
	 */
	public static void isValidDate(TextField textField) {
		logger.info("Calling isValidDate");
		
		if(!textField.getText().isEmpty()){
			String value = textField.getText();

			if(value.length() != 10){
				textField.setStyle("-fx-text-fill:red");
			}else{
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				try{
					LocalDate dateHour = LocalDate.parse(value, formatter);
					textField.setStyle("-fx-text-fill:green");
				}catch(Exception e){
					textField.setStyle("-fx-text-fill:red");
				}
			}
		}else{
			textField.setStyle("-fx-text-fill:black");
		}
	}
	
	
	/**
	 * Validate DateHour
	 */
	public static void isValidDateHour(TextField textField) {
		logger.info("Calling isValidDateHour");
		
		if(!textField.getText().isEmpty()){
			String value = textField.getText();

			if(value.length() != 19){
				textField.setStyle("-fx-text-fill:red");
			}else{
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
				try{
					LocalDateTime dateHour = LocalDateTime.parse(value, formatter);
					textField.setStyle("-fx-text-fill:green");
				}catch(Exception e){
					textField.setStyle("-fx-text-fill:red");
				}
				
			}
		}else{
			textField.setStyle("-fx-text-fill:black");
		}
	}
	
	
	/**
	 * Validate Email with regular expression
	 */
	public static boolean isValidEmail(final String email) {
		logger.info("Calling emailValidate: " + email);
		
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(email);
		return matcher.matches();
	}
	
	
	/**
	 * Validate textField with regex
	 */
	public static void isValidFormat(TextField textField, String regex, boolean removeMask) {
		logger.info("Calling isValidFormat");
		
		Pattern pattern = Pattern.compile(regex);
		if(!textField.getText().isEmpty()){
			String value = textField.getText();
			if(removeMask) value = MaskFieldUtil.removeMaskNumeral(value);
			if(!pattern.matcher(value).matches()){
				textField.setStyle("-fx-text-fill:red");
			}else{
				textField.setStyle("-fx-text-fill:green");
			}
		}else{
			textField.setStyle("-fx-text-fill:black");
		}
	}

	
	/**
	 * Validate IE field by UF
	 */
	public static void isValidIe(String ie, String uf) throws Exception {
		logger.info("Calling isValidaIe");
		
		IeValidateUtil.validate(ie, uf);
	}
	
	
	/**
	 * Validate Password with regular expression
	 */
	public static boolean isValidPassword(final String password) {
		logger.info("Calling passwordValidate");
		
		Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
		matcher = pattern.matcher(password);
		return matcher.matches();
	}

	
	
	/**
	 * Validate size on TextField
	 */
	public static void isValidSizeTextField(TextField textField, int min, int max, boolean removeMask) {
		logger.info("Calling isValidSizeTextField");
		
		if(!textField.getText().isEmpty()){
			String value = textField.getText();
			if(removeMask) value = MaskFieldUtil.removeMaskNumeral(value);
			if(value.length()>max || value.length() < min){
				textField.setStyle("-fx-text-fill:red");
			}else{
				textField.setStyle("-fx-text-fill:green");
			}
		}else{
			textField.setStyle("-fx-text-fill:black");
		}
	}
	
	
	/**
	 * Validate size on TextArea
	 */
	public static void isValidSizeTextArea(TextArea textArea, int min, int max, boolean removeMask) {
		logger.info("Calling isValidSizeTextArea");
		
		if(!textArea.getText().isEmpty()){
			String value = textArea.getText();
			if(removeMask) value = MaskFieldUtil.removeMaskNumeral(value);
			if(value.length()>max || value.length() < min){
				textArea.setStyle("-fx-text-fill:red");
			}else{
				textArea.setStyle("-fx-text-fill:green");
			}
		}else{
			textArea.setStyle("-fx-text-fill:black");
		}
	}
	
}
