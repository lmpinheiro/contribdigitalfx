package br.com.cd.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.constant.MessageConstant;

public class ImageUtil {
	private static final Logger logger = Logger.getLogger(ImageUtil.class);
	
	public static int MAX_IMAGE_SIZE=512000;
	
    public static Image chooseImgFile(Window window, ScreenConfig screenHelper)   {
    	
        FileChooser fileChooser = new FileChooser();
        List<String> error = new ArrayList<String>();
        // Define um filtro de extensão
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Imagem JPG, GIF ou PNG", "*.jpg", "*.gif","*.jpeg","*.png");
        fileChooser.getExtensionFilters().add(extFilter);
        Image image = null;

        // Mostra a janela de salvar arquivo
        File file = fileChooser.showOpenDialog(window);

	    if (file != null) {
	    	error.add(MessageConstant.FILE_VERY_LARGE);
	    	 if(file.length()>MAX_IMAGE_SIZE){
	    		 screenHelper.dialogDefault("Ops!", error, DialogType.ERROR);
	         }else{
	             image = new Image(file.toURI().toString(), 140, 140, false, false);
	        	 if(image!=null){
	        		 return image;
	        	 }
	            
	         }
	    }
	    if(image==null){
   		 	try {
				file = new File(ImageUtil.class.getResource("/br/com/cd/view/img/logo-default.png").toURI());
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
   		 	image = new Image(file.toURI().toString());
	    }
        return image;
    }
    
    
    public static Image getImageFromBytes(byte[] imgByte) {
    	try {
	        ByteArrayInputStream in = new ByteArrayInputStream(imgByte);
	        BufferedImage read;
			read = ImageIO.read(in);
			return SwingFXUtils.toFXImage(read, null);
        } catch (IOException e1) {
        	logger.error(e1);
		}
		return null;
    }
    
    public static byte[] getBytesFromImage(Image image) {
    	try {
            BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
            ByteArrayOutputStream s = new ByteArrayOutputStream();
			ImageIO.write(bImage, "png", s);
			return s.toByteArray();
        } catch (IOException e1) {
			logger.error(e1);
		}
		return null;
    }
}
