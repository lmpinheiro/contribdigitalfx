package br.com.cd.util;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.cd.config.ScreenConfig;
import br.com.cd.constant.MessageConstant;

/**
 * Key Access Utilities	
 * 
 * @author lmpinheiro
 *
 */
public abstract class AccessKeyUtil extends ScreenConfig{
	private static final Logger logger = Logger.getLogger(AccessKeyUtil.class);
	
	/**
	 * Format (START, END)
	 */
	public static enum AccessKeyFormatEnum { 
		
		UF(0, 2),
		YEAR(2, 4),
		MONTH(4, 6),
		CNPJ(6, 20),
		MODEL(20, 22),
		SERIE(22, 25),
		NUMBER(25, 34),
		ISSUE_TYPE(34, 35),
		CODE(35, 43),
		DIGIT(43, 44);
		
		private int start;
		private int end;
	    
		AccessKeyFormatEnum (int start, int end) {
	        this.start = start;
	        this.end = end;
	    }
		
	    public int getStart() {
	        return start;
	    }
	    
	    public int getEnd() {
	        return end;
	    }
	}
	
	/**
	 * Key Access Validate
	 */
	public static List<String> validateAccessKey(String accessKey) {
		List<String> errors = new ArrayList<String>();
		
		if (accessKey == null || accessKey.length() != 44) {
			errors.add(MessageConstant.KEY_ACCESS_INVALID_SIZE);
			return errors;
		}
		
		if(!digitValidate(accessKey)){
			errors.add(MessageFormat.format(MessageConstant.KEY_ACCESS_INVALID_INFORMATION, "Dígito Verificador"));
		}
		
		if(!ufValidate(accessKey.substring(AccessKeyFormatEnum.UF.getStart(), AccessKeyFormatEnum.UF.getEnd()))){
			errors.add(MessageFormat.format(MessageConstant.KEY_ACCESS_INVALID_INFORMATION, "UF"));
		}
		
		if(!yearValidate(accessKey.substring(AccessKeyFormatEnum.YEAR.getStart(), AccessKeyFormatEnum.YEAR.getEnd()))){
			errors.add(MessageFormat.format(MessageConstant.KEY_ACCESS_INVALID_INFORMATION, "Ano"));
		}
		
		if(!monthValidate(accessKey.substring(AccessKeyFormatEnum.MONTH.getStart(), AccessKeyFormatEnum.MONTH.getEnd()))){
			errors.add(MessageFormat.format(MessageConstant.KEY_ACCESS_INVALID_INFORMATION, "Mês"));
		}
		
		if(!cnpjValidate(accessKey.substring(AccessKeyFormatEnum.CNPJ.getStart(), AccessKeyFormatEnum.CNPJ.getEnd()))){
			errors.add(MessageFormat.format(MessageConstant.KEY_ACCESS_INVALID_INFORMATION, "CNPJ"));
		}
		
		if(!modelValidate(accessKey.substring(AccessKeyFormatEnum.MODEL.getStart(), AccessKeyFormatEnum.MODEL.getEnd()))){
			errors.add(MessageFormat.format(MessageConstant.KEY_ACCESS_INVALID_INFORMATION, "Modelo"));
		}
		
		return errors;
	}
	

	/**
	 * Digit Validate on Key Access
	 */
	public static boolean digitValidate(String accessKey) {
		if (accessKey == null || accessKey.length() != 44) {
			return false;
		}
		String digit = digitGenerate(accessKey);
		return digit.equals(accessKey.substring(43));
	}
	
	/**
	 * Generate digit for comparison
	 */
	public static String digitGenerate(String accessKey) {
		int firstDigit = 0;
		int calc = Integer.parseInt(accessKey.substring(0, 1)) * 4
		+ Integer.parseInt(accessKey.substring(1, 2)) * 3
		+ Integer.parseInt(accessKey.substring(2, 3)) * 2
		+ Integer.parseInt(accessKey.substring(3, 4)) * 9
		+ Integer.parseInt(accessKey.substring(4, 5)) * 8
		+ Integer.parseInt(accessKey.substring(5, 6)) * 7
		+ Integer.parseInt(accessKey.substring(6, 7)) * 6
		+ Integer.parseInt(accessKey.substring(7, 8)) * 5
		+ Integer.parseInt(accessKey.substring(8, 9)) * 4
		+ Integer.parseInt(accessKey.substring(9, 10)) * 3
		+ Integer.parseInt(accessKey.substring(10, 11)) * 2
		+ Integer.parseInt(accessKey.substring(11, 12)) * 9
		+ Integer.parseInt(accessKey.substring(12, 13)) * 8
		+ Integer.parseInt(accessKey.substring(13, 14)) * 7
		+ Integer.parseInt(accessKey.substring(14, 15)) * 6
		+ Integer.parseInt(accessKey.substring(15, 16)) * 5
		+ Integer.parseInt(accessKey.substring(16, 17)) * 4
		+ Integer.parseInt(accessKey.substring(17, 18)) * 3
		+ Integer.parseInt(accessKey.substring(18, 19)) * 2
		+ Integer.parseInt(accessKey.substring(19, 20)) * 9
		+ Integer.parseInt(accessKey.substring(20, 21)) * 8
		+ Integer.parseInt(accessKey.substring(21, 22)) * 7
		+ Integer.parseInt(accessKey.substring(22, 23)) * 6
		+ Integer.parseInt(accessKey.substring(23, 24)) * 5
		+ Integer.parseInt(accessKey.substring(24, 25)) * 4
		+ Integer.parseInt(accessKey.substring(25, 26)) * 3
		+ Integer.parseInt(accessKey.substring(26, 27)) * 2
		+ Integer.parseInt(accessKey.substring(27, 28)) * 9
		+ Integer.parseInt(accessKey.substring(28, 29)) * 8
		+ Integer.parseInt(accessKey.substring(29, 30)) * 7
		+ Integer.parseInt(accessKey.substring(30, 31)) * 6
		+ Integer.parseInt(accessKey.substring(31, 32)) * 5
		+ Integer.parseInt(accessKey.substring(32, 33)) * 4
		+ Integer.parseInt(accessKey.substring(33, 34)) * 3
		+ Integer.parseInt(accessKey.substring(34, 35)) * 2
		+ Integer.parseInt(accessKey.substring(35, 36)) * 9
		+ Integer.parseInt(accessKey.substring(36, 37)) * 8
		+ Integer.parseInt(accessKey.substring(37, 38)) * 7
		+ Integer.parseInt(accessKey.substring(38, 39)) * 6
		+ Integer.parseInt(accessKey.substring(39, 40)) * 5
		+ Integer.parseInt(accessKey.substring(40, 41)) * 4
		+ Integer.parseInt(accessKey.substring(41, 42)) * 3
		+ Integer.parseInt(accessKey.substring(42, 43)) * 2;
		if (calc < 11) {
			firstDigit = 11 - calc;
		} else {
			if ((calc % 11) <= 1) {
				firstDigit = 0;
			} else {
				firstDigit = 11 - (calc % 11);
			}
		}
		return String.valueOf(firstDigit);
	}
	
	
	/**
	 * UF Validate on Key Access
	 */
	public static boolean ufValidate(String uf) {
		int countUF = 0;
		String[] validUfs = {"11","12","13","14","15","16","17","21","22","23","24","25","26","27","28","29","31","32","33","35","41","42","43","50","51","52","53"};
		while(countUF < validUfs.length){
			if(validUfs[countUF].equals(uf)){
				return true;
			}
			countUF++;
		}
		return false;
	}
	
	
	/**
	 * Model Validate on Key Access
	 */
	public static boolean modelValidate(String model) {
		if(!"55".equals(model)){
			return false;
		}
		return true;
	}
	
	/**
	 * Year Validate on Key Access
	 */
	public static boolean yearValidate(String year) {
		if(Integer.parseInt(year) < 05 || Integer.parseInt(year)>(LocalDate.now().getYear())){
			return false;
		}
		return true;
	}
	
	
	/**
	 * Month Validate on Key Access
	 */
	public static boolean monthValidate(String month) {
		if(Integer.parseInt(month) == 0 || Integer.parseInt(month) > 12){
			return false;
		}
		return true;
	}
	
	
	/**
	 * CNPJ Validate on Key Access
	 */
	public static boolean cnpjValidate(String cnpj) {
		if(!DataValidateUtil.isValidCnpj(cnpj, true)){
			return false;
		}
		return true;
	}
}
