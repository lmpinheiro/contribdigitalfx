package br.com.cd.util;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;

import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import br.com.cd.control.InjectionController;
import br.com.cd.model.UserModel;

/**
 * Mail Sender Service
 * 
 * @author lmpinheiro
 */

public class MailSenderUtil {
	private static final Logger logger = Logger.getLogger(MailSenderUtil.class);
	
	private static MailSenderUtil mailSender;
	private final JavaMailSender javaMailSender;
	private static String to;
	private static String replyTo;
	private static String from;
	private static String subject;
	private static String content;

	/**
	 * Template Types:
	 *  - PASSWORD_RECOVER
	 *  - DANFE
	 *  - REPORT
	 */
	public static enum TemplateType {
		PASSWORD_RECOVER("password_recover.html"), 
		DANFE("password_recover.html"), 
		REPORT("password_recover.html");

		private String template;

		/** Creates a new instance of TemplateType */
		TemplateType(String template) {
			this.template = template;
		}
		public String getTemplate() {
			return template;
		}
		public String toString() {
			return template;
		}
	}

	
	/**
	 *  Constructor injecting Mail Service
	 */
	@Autowired
	public MailSenderUtil() {
		javaMailSender = InjectionController.getMailService().javaMailSender();
	}

	
	/**
	 *  Obtains Instance
	 */
	public static MailSenderUtil getInstance() {
		return mailSender = new MailSenderUtil();
	}

	
	/**
	 *  Define Default Email Template
	 */
	@Deprecated
	private void sendMailDefaut(String to, String replyTo, String from,
			String subject, String content) {
		logger.info("Calling sendMailDefaut");
		
		setTo(to);
		setReplyTo(replyTo);
		setFrom(from);
		setSubject(subject);
		setContent(content);
		send();
	}

	
	/**
	 *  Define Password Recover Email Template
	 */
	public void sendMailPasswordRecover(UserModel user) throws IOException {
		logger.info("Calling sendMailPasswordRecover: "+user.getEmail());
		
		setTo(user.getEmail());
		setReplyTo("luan.mackenzie@gmail.com");
		setFrom("luan.mackenzie@gmail.com");
		setSubject("Contribuinte Digital - Recuperação de Senha");

		String content = getTemplateHtml(TemplateType.PASSWORD_RECOVER);
		content = MessageFormat.format(content, user.getName(),
				user.getQuestion(), user.getAnswer());

		setContent(content);
		send();
	}

	
	/**
	 *  Send Email
	 */
	private void send() {
		logger.info("Sending a e-mail: "+from);
		
		Thread t = new Thread(() -> {
			try {
				MimeMessage message = javaMailSender.createMimeMessage();
				MimeMessageHelper mime = new MimeMessageHelper(message,true);

				message.setContent(content, "text/html");
				
				mime.setFrom(from);
				mime.setTo(to);
				mime.setReplyTo(replyTo);
				mime.setSubject(subject);

				javaMailSender.send(message);
			} catch (Exception e1) {
				logger.error("Error on Sending a e-mail: "+e1);
			}
		});
		t.start();
	}

	
	/**
	 *  Load Email Template
	 */
	private String getTemplateHtml(TemplateType templateType) throws IOException {
		logger.info("Getting a template: "+templateType);
		
		InputStream inputStream = this.getClass().getResourceAsStream(
				"/br/com/cd/view/email/" + templateType);
		StringBuilder builder = new StringBuilder();
		int c;
		while ((c = inputStream.read()) != -1) {
			builder.append((char) c);
		}
		return builder.toString();
	}


	/* *
	 * Setters and Getters
	 */
	public static String getTo() {
		return to;
	}

	public static void setTo(String to) {
		MailSenderUtil.to = to;
	}

	public static String getReplyTo() {
		return replyTo;
	}

	public static void setReplyTo(String replyTo) {
		MailSenderUtil.replyTo = replyTo;
	}

	public static String getFrom() {
		return from;
	}

	public static void setFrom(String from) {
		MailSenderUtil.from = from;
	}

	public static String getSubject() {
		return subject;
	}

	public static void setSubject(String subject) {
		MailSenderUtil.subject = subject;
	}

	public static String getContent() {
		return content;
	}

	public static void setContent(String content) {
		MailSenderUtil.content = content;
	}

}
