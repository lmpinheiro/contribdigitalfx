package br.com.cd.util;

import br.com.cd.constant.MessageConstant;

/**
 * Validate IE field by UF
 * 
 * @author lmpinheiro
 */
public class IeValidateUtil { 

	private static String removeMascara(String ie) {
		String strIE = "";
		for (int i = 0; i < ie.length(); i++) {
			if (Character.isDigit(ie.charAt(i))) {
				strIE += ie.charAt(i);
			}
		}
		return strIE;
	}

	/**
	 * Validate IE field by UF
	 */
	public static void validate(String ie, String uf)
			throws Exception {
		String strIE = removeMascara(ie);
		uf = uf.toUpperCase();
		if (uf.equals("AC")) {
			validateIEAcre(strIE);
		} else if (uf.equals("AL")) {
			validateIEAlagoas(strIE);
		} else if (uf.equals("AP")) {
			validateIEAmapa(strIE);
		} else if (uf.equals("AM")) {
			validateIEAmazonas(strIE);
		} else if (uf.equals("BA")) {
			validateIEBahia(strIE);
		} else if (uf.equals("CE")) {
			validateIECeara(strIE);
		} else if (uf.equals("ES")) {
			validateIEEspiritoSanto(strIE);
		} else if (uf.equals("GO")) {
			validateIEGoias(strIE);
		} else if (uf.equals("MA")) {
			validateIEMaranhao(strIE);
		} else if (uf.equals("MT")) {
			validateIEMatoGrosso(strIE);
		} else if (uf.equals("MS")) {
			validateIEMatoGrossoSul(strIE);
		} else if (uf.equals("MG")) {
			validateIEMinasGerais(strIE);
		} else if (uf.equals("PA")) {
			validateIEPara(strIE);
		} else if (uf.equals("PB")) {
			validateIEParaiba(strIE);
		} else if (uf.equals("PR")) {
			validateIEParana(strIE);
		} else if (uf.equals("PE")) {
			validateIEPernambuco(strIE);
		} else if (uf.equals("PI")) {
			validateIEPiaui(strIE);
		} else if (uf.equals("RJ")) {
			validateIERioJaneiro(strIE);
		} else if (uf.equals("RN")) {
			validateIERioGrandeNorte(strIE);
		} else if (uf.equals("RS")) {
			validateIERioGrandeSul(strIE);
		} else if (uf.equals("RO")) {
			validateIERondonia(strIE);
		} else if (uf.equals("RR")) {
			validateIERoraima(strIE);
		} else if (uf.equals("SC")) {
			validateIESantaCatarina(strIE);
		} else if (uf.equals("SP")) {
			if (ie.charAt(0) == 'P') {
				strIE = "P" + strIE;
			}
			validateIESaoPaulo(strIE);
		} else if (uf.equals("SE")) {
			validateIESergipe(strIE);
		} else if (uf.equals("TO")) {
			validateIETocantins(strIE);
		} else if (uf.equals("DF")) {
			validateIEDistritoFederal(strIE);
		} else {
			throw new Exception("Estado não encontrado : " + uf);
		}
	}

	/**
	 * validatete IE of Acre
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEAcre(String ie) throws Exception {
																	
		// validate size
		if (ie.length() != 13) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// validate two first digits - is equals 01
		for (int i = 0; i < 2; i++) {
			if (Integer.parseInt(String.valueOf(ie.charAt(i))) != i) {
				throw new Exception(MessageConstant.IE_INVALID);
			}
		}

		int soma = 0;
		int pesoInicial = 4;
		int pesoFinal = 9;
		int d1 = 0; // first the second tester digit
		int d2 = 0; // second the second tester digit

		// calculate the first digit
		for (int i = 0; i < ie.length() - 2; i++) {
			if (i < 3) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoInicial;
				pesoInicial--;
			} else {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoFinal;
				pesoFinal--;
			}
		}
		d1 = 11 - (soma % 11);
		if (d1 == 10 || d1 == 11) {
			d1 = 0;
		}

		// calculate the second digit
		soma = d1 * 2;
		pesoInicial = 5;
		pesoFinal = 9;
		for (int i = 0; i < ie.length() - 2; i++) {
			if (i < 4) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoInicial;
				pesoInicial--;
			} else {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoFinal;
				pesoFinal--;
			}
		}

		d2 = 11 - (soma % 11);
		if (d2 == 10 || d2 == 11) {
			d2 = 0;
		}

		// validate tester digit
		String dv = d1 + "" + d2;
		if (!dv.equals(ie.substring(ie.length() - 2, ie.length()))) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	} 
	
	
	/**
	 * validatete IE of Alagoas
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEAlagoas(String ie) throws Exception {
		// validate size
		if (ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// validate the two first digits - is equals 24
		if (!ie.substring(0, 2).equals("24")) {
			throw new Exception(MessageConstant.IE_INVALID);
		}

		// validate the third digit - is 0,3,5,7,8
		int[] digits = { 0, 3, 5, 7, 8 };
		boolean check = false;
		for (int i = 0; i < digits.length; i++) {
			if (Integer.parseInt(String.valueOf(ie.charAt(2))) == digits[i]) {
				check = true;
				break;
			}
		}
		if (!check) {
			throw new Exception(MessageConstant.IE_INVALID);
		}

		// calculate the tester digit
		int soma = 0;
		int peso = 9;
		int d = 0; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}
		d = ((soma * 10) % 11);
		if (d == 10) {
			d = 0;
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}
	

	/**
	 * validatete IE of Amapá
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEAmapa(String ie) throws Exception {
		//Size validation
		if (ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// check the two first digits - deve ser igual 03
		if (!ie.substring(0, 2).equals("03")) {
			throw new Exception(MessageConstant.IE_INVALID);
		}

		// calculate the tester digit
		int d1 = -1;
		int soma = -1;
		int peso = 9;

		// configure tester digit value
		long x = Long.parseLong(ie.substring(0, ie.length() - 1)); // x =
																	// inscrição
																	// estadual
																	// sem o
																	// dígito
																	// verificador
		if (x >= 3017001L && x <= 3019022L) {
			d1 = 1;
			soma = 9;
		} else if (x >= 3000001L && x <= 3017000L) {
			d1 = 0;
			soma = 5;
		} else if (x >= 3019023L) {
			d1 = 0;
			soma = 0;
		}

		for (int i = 0; i < ie.length() - 1; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}

		int d = 11 - ((soma % 11)); // 

		if (d == 10) {
			d = 0;
		} else if (d == 11) {
			d = d1;
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Amazonas
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEAmazonas(String ie) throws Exception {
		//Size validation
		if (ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		int soma = 0;
		int peso = 9;
		int d = -1; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}

		if (soma < 11) {
			d = 11 - soma;
		} else if ((soma % 11) <= 1) {
			d = 0;
		} else {
			d = 11 - (soma % 11);
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Bahia
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEBahia(String ie) throws Exception {
		//Size validation
		if (ie.length() != 8 && ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE + ie);
		}

		int modulo = 10;
		int firstDigit = Integer
				.parseInt(String.valueOf(ie.charAt(ie.length() == 8 ? 0 : 1)));
		if (firstDigit == 6 || firstDigit == 7 || firstDigit == 9)
			modulo = 11;

		// Calculate second digit
		int d2 = -1; // second tester digit
		int soma = 0;
		int peso = ie.length() == 8 ? 7 : 8;
		for (int i = 0; i < ie.length() - 2; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}

		int resto = soma % modulo;

		if (resto == 0 || (modulo == 11 && resto == 1)) {
			d2 = 0;
		} else {
			d2 = modulo - resto;
		}

		// Calculate first digit
		int d1 = -1; // first tester digit
		soma = d2 * 2;
		peso = ie.length() == 8 ? 8 : 9;
		for (int i = 0; i < ie.length() - 2; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}

		resto = soma % modulo;

		if (resto == 0 || (modulo == 11 && resto == 1)) {
			d1 = 0;
		} else {
			d1 = modulo - resto;
		}

		// validate tester digit
		String dv = d1 + "" + d2;
		if (!dv.equals(ie.substring(ie.length() - 2, ie.length()))) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID + ie);
		}
	}

	
	/**
	 * validatete IE of Ceará
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIECeara(String ie) throws Exception {
		//Size validation
		if (ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// Calculate tester digit
		int soma = 0;
		int peso = 9;
		int d = -1; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}

		d = 11 - (soma % 11);
		if (d == 10 || d == 11) {
			d = 0;
		}
		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Espírito Santo
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEEspiritoSanto(String ie) throws Exception {
		//Size validation
		if (ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// Calculate tester digit
		int soma = 0;
		int peso = 9;
		int d = -1; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}

		int resto = soma % 11;
		if (resto < 2) {
			d = 0;
		} else if (resto > 1) {
			d = 11 - resto;
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Goiás
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEGoias(String ie) throws Exception {
		//Size validation
		if (ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// validate the two first digit
		if (!"10".equals(ie.substring(0, 2))) {
			if (!"11".equals(ie.substring(0, 2))) {
				if (!"15".equals(ie.substring(0, 2))) {
					throw new Exception(MessageConstant.IE_INVALID);
				}
			}
		}
	
		if (ie.substring(0, ie.length() - 1).equals("11094402")) {
			if (!ie.substring(ie.length() - 1, ie.length()).equals("0")) {
				if (!ie.substring(ie.length() - 1, ie.length()).equals("1")) {
					throw new Exception(MessageConstant.IE_INVALID);
				}
			}
		} else {

			// Calculate tester digit
			int soma = 0;
			int peso = 9;
			int d = -1; // tester digit
			for (int i = 0; i < ie.length() - 1; i++) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
				peso--;
			}

			int resto = soma % 11;
			long faixaInicio = 10103105;
			long faixaFim = 10119997;
			long insc = Long.parseLong(ie.substring(0, ie.length() - 1));
			if (resto == 0) {
				d = 0;
			} else if (resto == 1) {
				if (insc >= faixaInicio && insc <= faixaFim) {
					d = 1;
				} else {
					d = 0;
				}
			} else if (resto != 0 && resto != 1) {
				d = 11 - resto;
			}

			// validate the second tester digit
			String dv = d + "";
			if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
				throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
			}
		}
	}

	
	/**
	 * validatete IE of Maranhão
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEMaranhao(String ie) throws Exception {
		//Size validation
		if (ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// validate the two first digits
		if (!ie.substring(0, 2).equals("12")) {
			throw new Exception(MessageConstant.IE_INVALID);
		}

		// Calculate o tester digit
		int soma = 0;
		int peso = 9;
		int d = -1; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}

		d = 11 - (soma % 11);
		if ((soma % 11) == 0 || (soma % 11) == 1) {
			d = 0;
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Mato Grosso
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEMatoGrosso(String ie) throws Exception {
		//Size validation
		if (ie.length() != 11) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// Calculate the tester digit
		int soma = 0;
		int pesoInicial = 3;
		int pesoFinal = 9;
		int d = -1;

		for (int i = 0; i < ie.length() - 1; i++) {
			if (i < 2) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoInicial;
				pesoInicial--;
			} else {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoFinal;
				pesoFinal--;
			}
		}

		d = 11 - (soma % 11);
		if ((soma % 11) == 0 || (soma % 11) == 1) {
			d = 0;
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Mato Grosso do Sul
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEMatoGrossoSul(String ie) throws Exception {
		//Size validation
		if (ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// validate the two first digits
		if (!ie.substring(0, 2).equals("28")) {
			throw new Exception(MessageConstant.IE_INVALID);
		}

		// Calculate the tester digit
		int soma = 0;
		int peso = 9;
		int d = -1; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}

		int resto = soma % 11;
		int result = 11 - resto;
		if (resto == 0) {
			d = 0;
		} else if (resto > 0) {
			if (result > 9) {
				d = 0;
			} else if (result < 10) {
				d = result;
			}
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Minas Gerais
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEMinasGerais(String ie) throws Exception {
		/*
		 * FORMATO GERAL: A1A2A3B1B2B3B4B5B6C1C2D1D2 Onde: A= Código do
		 * município B= Número da inscrição C= Número de ordem do
		 * estabelecimento D= dígitos de controle
		 */

		//Size validation
		if (ie.length() != 13) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// iguala a casas para o cálculo
		// em inserir o algarismo zero "0" imediatamente após o Número de Código
		// do município,
		// desprezando-se os dígitos de controle.
		String str = "";
		for (int i = 0; i < ie.length() - 2; i++) {
			if (Character.isDigit(ie.charAt(i))) {
				if (i == 3) {
					str += "0";
					str += ie.charAt(i);
				} else {
					str += ie.charAt(i);
				}
			}
		}

		// calculate the first tester digit
		int soma = 0;
		int pesoInicio = 1;
		int pesoFim = 2;
		int d1 = -1; // first tester digit
		for (int i = 0; i < str.length(); i++) {
			if (i % 2 == 0) {
				int x = Integer.parseInt(String.valueOf(str.charAt(i)))
						* pesoInicio;
				String strX = Integer.toString(x);
				for (int j = 0; j < strX.length(); j++) {
					soma += Integer.parseInt(String.valueOf(strX.charAt(j)));
				}
			} else {
				int y = Integer.parseInt(String.valueOf(str.charAt(i)))
						* pesoFim;
				String strY = Integer.toString(y);
				for (int j = 0; j < strY.length(); j++) {
					soma += Integer.parseInt(String.valueOf(strY.charAt(j)));
				}
			}
		}

		int dezenaExata = soma;
		while (dezenaExata % 10 != 0) {
			dezenaExata++;
		}
		d1 = dezenaExata - soma; // result - first tester digit

		// calculate the second tester digit
		soma = d1 * 2;
		pesoInicio = 3;
		pesoFim = 11;
		int d2 = -1;
		for (int i = 0; i < ie.length() - 2; i++) {
			if (i < 2) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoInicio;
				pesoInicio--;
			} else {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoFim;
				pesoFim--;
			}
		}

		d2 = 11 - (soma % 11); // result - second tester digit
		if ((soma % 11 == 0) || (soma % 11 == 1)) {
			d2 = 0;
		}

		// validate tester digit
		String dv = d1 + "" + d2;
		if (!dv.equals(ie.substring(ie.length() - 2, ie.length()))) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Pará
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEPara(String ie) throws Exception {
		//Size validation
		if (ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// validate the two first digits
		if (!ie.substring(0, 2).equals("15")) {
			throw new Exception(MessageConstant.IE_INVALID);
		}

		// Calculate the tester digit
		int soma = 0;
		int peso = 9;
		int d = -1; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}

		d = 11 - (soma % 11);
		if ((soma % 11) == 0 || (soma % 11) == 1) {
			d = 0;
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Paraíba
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEParaiba(String ie) throws Exception {
		//Size validation
		if (ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// Calculate the tester digit
		int soma = 0;
		int peso = 9;
		int d = -1; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}

		d = 11 - (soma % 11);
		if (d == 10 || d == 11) {
			d = 0;
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Paraná
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEParana(String ie) throws Exception {
		//Size validation
		if (ie.length() != 10) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// Calculate first digit
		int soma = 0;
		int pesoInicio = 3;
		int pesoFim = 7;
		int d1 = -1; // tester digit
		for (int i = 0; i < ie.length() - 2; i++) {
			if (i < 2) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoInicio;
				pesoInicio--;
			} else {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoFim;
				pesoFim--;
			}
		}

		d1 = 11 - (soma % 11);
		if ((soma % 11) == 0 || (soma % 11) == 1) {
			d1 = 0;
		}

		// Calculate second digit
		soma = d1 * 2;
		pesoInicio = 4;
		pesoFim = 7;
		int d2 = -1; // second digit
		for (int i = 0; i < ie.length() - 2; i++) {
			if (i < 3) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoInicio;
				pesoInicio--;
			} else {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoFim;
				pesoFim--;
			}
		}

		d2 = 11 - (soma % 11);
		if ((soma % 11) == 0 || (soma % 11) == 1) {
			d2 = 0;
		}

		// validate tester digit
		String dv = d1 + "" + d2;
		if (!dv.equals(ie.substring(ie.length() - 2, ie.length()))) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Pernambuco
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEPernambuco(String ie) throws Exception {
		//Size validation
		if (ie.length() != 14) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// Calculate tester digit
		int soma = 0;
		int pesoInicio = 5;
		int pesoFim = 9;
		int d = -1; // tester digit

		for (int i = 0; i < ie.length() - 1; i++) {
			if (i < 5) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoInicio;
				pesoInicio--;
			} else {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoFim;
				pesoFim--;
			}
		}

		d = 11 - (soma % 11);
		if (d > 9) {
			d -= 10;
		}

		System.out.println(soma);
		System.out.println(11 - (soma % 11));
		System.out.println(d);

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Piauí
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEPiaui(String ie) throws Exception {
		//Size validation
		if (ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		//Calculation Tester Digit
		int soma = 0;
		int peso = 9;
		int d = -1; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}

		d = 11 - (soma % 11);
		if (d == 11 || d == 10) {
			d = 0;
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Rio de Janeiro
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIERioJaneiro(String ie) throws Exception {
		//Size validation
		if (ie.length() != 10) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		//Calculation Tester Digit
		int soma = 0;
		int peso = 7;
		int d = -1; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			if (i == 0) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * 2;
			} else {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
				peso--;
			}
		}

		d = 11 - (soma % 11);
		if ((soma % 11) <= 1) {
			d = 0;
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Rio Grande do Norte
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIERioGrandeNorte(String ie) throws Exception {
		//Size validation
		if (ie.length() != 10 && ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// validate the two first digits
		if (!ie.substring(0, 2).equals("20")) {
			throw new Exception(MessageConstant.IE_INVALID);
		}

		// calculate 9 positions digit
		if (ie.length() == 9) {
			int soma = 0;
			int peso = 9;
			int d = -1; // tester digit
			for (int i = 0; i < ie.length() - 1; i++) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
				peso--;
			}

			d = ((soma * 10) % 11);
			if (d == 10) {
				d = 0;
			}

			// validate the second tester digit
			String dv = d + "";
			if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
				throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
			}
		} else {
			int soma = 0;
			int peso = 10;
			int d = -1; // tester digit
			for (int i = 0; i < ie.length() - 1; i++) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
				peso--;
			}
			d = ((soma * 10) % 11);
			if (d == 10) {
				d = 0;
			}

			// validate the second tester digit
			String dv = d + "";
			if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
				throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
			}
		}

	}

	
	/**
	 * validatete IE of Rio Grande do Sul
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIERioGrandeSul(String ie) throws Exception {
		//Size validation
		if (ie.length() != 10) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// Calculate tester digit
		int soma = Integer.parseInt(String.valueOf(ie.charAt(0))) * 2;
		int peso = 9;
		int d = -1; // tester digit
		for (int i = 1; i < ie.length() - 1; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}

		d = 11 - (soma % 11);
		if (d == 10 || d == 11) {
			d = 0;
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Rondônia
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIERondonia(String ie) throws Exception {
		//Size validation
		if (ie.length() != 14) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// Calculate tester digit
		int soma = 0;
		int pesoInicio = 6;
		int pesoFim = 9;
		int d = -1; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			if (i < 5) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoInicio;
				pesoInicio--;
			} else {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoFim;
				pesoFim--;
			}
		}

		d = 11 - (soma % 11);
		if (d == 11 || d == 10) {
			d -= 10;
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Roraíma
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIERoraima(String ie) throws Exception {
		//Size validation
		if (ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// validate the two first digits
		if (!ie.substring(0, 2).equals("24")) {
			throw new Exception(MessageConstant.IE_INVALID);
		}

		int soma = 0;
		int peso = 1;
		int d = -1; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso++;
		}

		d = soma % 9;

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Santa Catarina
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIESantaCatarina(String ie) throws Exception {
		//Size validation
		if (ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// Calculate tester digit
		int soma = 0;
		int peso = 9;
		int d = -1; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}

		d = 11 - (soma % 11);
		if ((soma % 11) == 0 || (soma % 11) == 1) {
			d = 0;
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of São Paulo
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIESaoPaulo(String ie) throws Exception {
		//Size validation
		if (ie.length() != 12 && ie.length() != 13) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		if (ie.length() == 12) {
			int soma = 0;
			int peso = 1;
			int d1 = -1; // first tester digit
			// calculate the first tester digit (nine position)
			for (int i = 0; i < ie.length() - 4; i++) {
				if (i == 1 || i == 7) {
					soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
							* ++peso;
					peso++;
				} else {
					soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
							* peso;
					peso++;
				}
			}

			d1 = soma % 11;
			String strD1 = Integer.toString(d1); // O dígito é igual ao
													// algarismo mais a direita
													// do resultado de (soma %
													// 11)
			d1 = Integer
					.parseInt(String.valueOf(strD1.charAt(strD1.length() - 1)));

			// Calculate second digit
			soma = 0;
			int pesoInicio = 3;
			int pesoFim = 10;
			int d2 = -1; // second tester digit
			for (int i = 0; i < ie.length() - 1; i++) {
				if (i < 2) {
					soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
							* pesoInicio;
					pesoInicio--;
				} else {
					soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
							* pesoFim;
					pesoFim--;
				}
			}

			d2 = soma % 11;
			String strD2 = Integer.toString(d2); 	// O dígito é igual ao
													// algarismo mais a direita
													// do resultado de (soma %
													// 11)
			d2 = Integer
					.parseInt(String.valueOf(strD2.charAt(strD2.length() - 1)));

			// validate the tester digits
			if (!ie.substring(8, 9).equals(d1 + "")) {
				throw new Exception(MessageConstant.IE_INVALID);
			}
			if (!ie.substring(11, 12).equals(d2 + "")) {
				throw new Exception(MessageConstant.IE_INVALID);
			}

		} else {
			// validate the first char
			if (ie.charAt(0) != 'P') {
				throw new Exception(MessageConstant.IE_INVALID);
			}

			String strIE = ie.substring(1, 10); // Obtem somente os dígitos
												// utilizados no Calculate
												// tester digit
			int soma = 0;
			int peso = 1;
			int d1 = -1; // first tester digit
			// calculate the first tester digit (nine position)
			for (int i = 0; i < strIE.length() - 1; i++) {
				if (i == 1 || i == 7) {
					soma += Integer.parseInt(String.valueOf(strIE.charAt(i)))
							* ++peso;
					peso++;
				} else {
					soma += Integer.parseInt(String.valueOf(strIE.charAt(i)))
							* peso;
					peso++;
				}
			}

			d1 = soma % 11;
			String strD1 = Integer.toString(d1); // O dígito é igual ao
													// algarismo mais a direita
													// do resultado de (soma %
													// 11)
			d1 = Integer
					.parseInt(String.valueOf(strD1.charAt(strD1.length() - 1)));

			// validate o tester digit
			if (!ie.substring(9, 10).equals(d1 + "")) {
				throw new Exception(MessageConstant.IE_INVALID);
			}
		}
	}

	
	/**
	 * validatete IE of Sergipe
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIESergipe(String ie) throws Exception {
		//Size validation
		if (ie.length() != 9) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// Calculate tester digit
		int soma = 0;
		int peso = 9;
		int d = -1; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
			peso--;
		}

		d = 11 - (soma % 11);
		if (d == 11 || d == 11 || d == 10) {
			d = 0;
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Tocantins
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIETocantins(String ie) throws Exception {
		//Size validation
		if (ie.length() != 9 && ie.length() != 11) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		} else if (ie.length() == 9) {
			ie = ie.substring(0, 2) + "02" + ie.substring(2);
		}

		int soma = 0;
		int peso = 9;
		int d = -1; // tester digit
		for (int i = 0; i < ie.length() - 1; i++) {
			if (i != 2 && i != 3) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i))) * peso;
				peso--;
			}
		}
		d = 11 - (soma % 11);
		if ((soma % 11) < 2) {
			d = 0;
		}

		// validate the second tester digit
		String dv = d + "";
		if (!ie.substring(ie.length() - 1, ie.length()).equals(dv)) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}

	
	/**
	 * validatete IE of Distrito Federal
	 * 
	 * @param ie
	 *            (IE)
	 * @throws Exception
	 */
	private static void validateIEDistritoFederal(String ie) throws Exception {
		//Size validation
		if (ie.length() != 13) {
			throw new Exception(MessageConstant.IE_INVALID_SIZE);
		}

		// calculate the first tester digit
		int soma = 0;
		int pesoInicio = 4;
		int pesoFim = 9;
		int d1 = -1; // first tester digit
		for (int i = 0; i < ie.length() - 2; i++) {
			if (i < 3) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoInicio;
				pesoInicio--;
			} else {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoFim;
				pesoFim--;
			}
		}

		d1 = 11 - (soma % 11);
		if (d1 == 11 || d1 == 10) {
			d1 = 0;
		}

		// calculate the second tester digit
		soma = d1 * 2;
		pesoInicio = 5;
		pesoFim = 9;
		int d2 = -1; // second tester digit
		for (int i = 0; i < ie.length() - 2; i++) {
			if (i < 4) {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoInicio;
				pesoInicio--;
			} else {
				soma += Integer.parseInt(String.valueOf(ie.charAt(i)))
						* pesoFim;
				pesoFim--;
			}
		}

		d2 = 11 - (soma % 11);
		if (d2 == 11 || d2 == 10) {
			d2 = 0;
		}

		// validate tester digit
		String dv = d1 + "" + d2;
		if (!dv.equals(ie.substring(ie.length() - 2, ie.length()))) {
			throw new Exception(MessageConstant.IE_TESTER_DIGIT_INVALID);
		}
	}
} 
