package br.com.cd.util;

import javafx.scene.control.TextField;

import org.apache.log4j.Logger;
import org.hibernate.annotations.common.util.StringHelper;

/**
 * Format Strings with Regex
 * 
 * @author lmpinheiro
 */
public abstract class FormatterTextUtil {
	private static final Logger logger = Logger.getLogger(FormatterTextUtil.class);

    /**
     * Format a String as CPF or CNPJ regex
     */
    public static String cpfCnpjField(String value) {
    	logger.info("Calling cpfCnpjField");
    	
        if (value.length() == 11) {
            value = value.replaceAll("[^0-9]", "");
            value = value.replaceFirst("([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})$", "$1.$2.$3-$4");
        }
        if (value.length() == 14) {
            value = value.replaceAll("[^0-9]", "");
            value = value.replaceFirst("([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})([0-9]{2})$", "$1.$2.$3/$4-$5");
        }
        return value;
    }

    /**
     * Format a String as CNPJ regex
     */
    public static String cnpjField(String value) {
    	logger.info("Calling cnpjField");
    	
        value = value.replaceAll("[^0-9]", "");
        value = value.replaceFirst("(\\d{2})(\\d)", "$1.$2");
        value = value.replaceFirst("(\\d{2})\\.(\\d{3})(\\d)", "$1.$2.$3");
        value = value.replaceFirst("\\.(\\d{3})(\\d)", ".$1/$2");
        value = value.replaceFirst("(\\d{4})(\\d)", "$1-$2");
        return value;
    }
    
    /**
     * Format a String as CPF regex
     */
    public static String cpfField(String value) {
    	logger.info("Calling cpfField");
    	
        value = value.replaceAll("[^0-9]", "");
        value = value.replaceFirst("(\\d{3})(\\d)", "$1.$2");
        value = value.replaceFirst("(\\d{3})\\.(\\d{3})(\\d)", "$1.$2.$3");
        value = value.replaceFirst("\\.(\\d{3})(\\d)", ".$1-$2");
        return value;
    }
    
    /**
     * License Plate Mask.
     */
    public static String licensePlateField(String value) {
    	logger.info("Calling licensePlateField");
    	
	    value = value.replaceFirst("([A-Z]{1,3})([0-9])", "$1-$2");
	    return value;
    }
    
    /**
     * Monetary Field.
     */
    public static String monetaryField(int intDigits, int decimalDigits, String value) {
    	logger.info("Calling monetaryField");
    	
                value = value.replaceAll("[^0-9]", "");
                value = value.replaceAll("([0-9]{1})([0-9]{"+(12+decimalDigits)+"})$", "$1.$2");
                value = value.replaceAll("([0-9]{1})([0-9]{"+(9+decimalDigits)+"})$", "$1.$2");
                value = value.replaceAll("([0-9]{1})([0-9]{"+(6+decimalDigits)+"})$", "$1.$2");
                value = value.replaceAll("([0-9]{1})([0-9]{"+(3+decimalDigits)+"})$", "$1.$2");
                value = value.replaceAll("([0-9]{1})([0-9]{"+decimalDigits+"})$", "$1,$2");
                
        return value;
    }
    
    /**
     * Format a String as ZipCode regex
     */
    public static String zipCodeField(String value) {
    	logger.info("Calling zipCodeField");
    	
        value = value.replaceAll("[^0-9]", "");
        value = value.replaceFirst("(\\d{5})(\\d)", "$1-$2");
        return value;
    }
    
    
    /**
     * Format a String as Phone regex
     */
    public static String phoneField(String value) {
    	logger.info("Calling phoneField");
    	
        value = value.replaceAll("[^0-9]", "");
        value = value.replaceFirst("(\\d{2})(\\d)", "($1) $2");
        if(value.length()>13)
        	value = value.replaceFirst("(\\d{5})(\\d)", "$1-$2");
        else
        	value = value.replaceFirst("(\\d{4})(\\d)", "$1-$2");
        
        return value;
    }
}
