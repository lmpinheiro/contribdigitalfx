package br.com.cd.util;

import java.io.File;
import java.security.Permission;

import org.apache.xmlbeans.impl.tool.SchemaCompiler;

import br.com.cd.config.ScreenConfig;
import br.com.cd.config.ScreenConfig.DialogType;
import br.com.cd.control.ScreenController;
import br.com.cd.model.IssuerModel;
import br.com.cd.nfe.TEnderEmi;
import br.com.cd.nfe.TNFe.InfNFe.Emit;
import br.com.cd.nfe.TUfEmi;

public class SchemaConverterUtil extends ScreenController {
	private final static String COD_BRAZIL = "1058";
	private final static String BRAZIL = "BRASIL";

	public static void main(String[] args) {
		if (args != null && args.length < 2) {
			System.out.println("Erro!!!");
		} else {
			SchemaConverterUtil xmlBeans = new SchemaConverterUtil();
			xmlBeans.gerarJar(args[0], args[1]);
		}
	}

	public void gerarJar(String caminhoSchemas, String nomeJar) {
		String[] parametros = { caminhoSchemas + File.separator,
				caminhoSchemas + File.separator + "config.xsdconfig", "-out",
				caminhoSchemas + File.separator + nomeJar };

		final SecurityManager securityManager = new SecurityManager() {
			public void checkPermission(Permission perm) {
				// allow anything.
			}

			public void checkPermission(Permission perm, Object context) {
				// allow anything.
			}

			public void checkExit(int status) {
				super.checkExit(status);
				throw new SecurityException("Exit " + status);
			}
		};

		try {
			System.setSecurityManager(securityManager);
			SchemaCompiler.main(parametros);
		} catch (SecurityException t) {
			if ("Exit 0".equals(t.getMessage())) {
				ScreenConfig screenHelper = new ScreenConfig();
				addMessage("JAR Gerado com Sucesso!!");
				screenHelper.dialogDefault("Uopa!!", message,
						DialogType.INFORMATION);
			}
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {
			System.setSecurityManager(null);
		}
	}

	public static Emit convertIssuer(IssuerModel issuer) {
		Emit emit = new Emit();
		emit.setCNAE(issuer.getCnae());
		emit.setCNPJ(issuer.getCnpj());
		emit.setCRT(Integer.toString(issuer.getRegimeTrib().getCode()));
		emit.setIE(issuer.getIe());
		emit.setIEST(issuer.getIeSt());
		emit.setIM(issuer.getIm());
		emit.setXFant(issuer.getTradeName());
		emit.setXNome(issuer.getCompanyName());

		TEnderEmi enderEmi = new TEnderEmi();
		enderEmi.setCEP(issuer.getZipCode());
		enderEmi.setCMun(Integer.toString(issuer.getCountyModel().getId()));
		enderEmi.setCPais(COD_BRAZIL);
		enderEmi.setFone(issuer.getPhone());
		enderEmi.setNro(issuer.getNumber());
		enderEmi.setUF(TUfEmi.fromValue(issuer.getCountyModel().getUf()));
		enderEmi.setXBairro(issuer.getDistrict());
		enderEmi.setXCpl(issuer.getComplement());
		enderEmi.setXLgr(issuer.getStreet());
		enderEmi.setXMun(issuer.getCountyModel().getCountyName());
		enderEmi.setXPais(BRAZIL);

		emit.setEnderEmit(enderEmi);
		return emit;

		// try {
		// Emit emit = SchemaConverterUtil.convertIssuer(issuer);
		//
		// SchemaFactory sf =
		// SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		// Schema schema = sf.newSchema(new
		// File(getClass().getResource("/br/com/cd/xsd/nfe/PL_008g/NFe_v3.10.xsd").toURI()));
		//
		// JAXBContext jc = JAXBContext.newInstance(TNFe.class);
		// Marshaller marshaller = jc.createMarshaller();
		//
		// marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		// marshaller.setSchema(schema);
		//
		// TNFe tnFe = new TNFe();
		// InfNFe infNFe = new InfNFe();
		// infNFe.setEmit(emit);
		// tnFe.setInfNFe(infNFe);
		//
		// JAXBElement<TNFe> jaxbElement = new ObjectFactory().createNFe(tnFe);
		// marshaller.marshal(jaxbElement, System.out);

		// Emit emit = SchemaConverterUtil.convertIssuer(issuer);
		//
		// SchemaFactory sf =
		// SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		// Schema schema = sf.newSchema(new
		// File(getClass().getResource("/br/com/cd/xsd/nfe/PL_008g/NFe_v3.10.xsd").toURI()));
		//
		// JAXBContext jc = JAXBContext.newInstance(Emit.class);
		// Marshaller marshaller = jc.createMarshaller();
		//
		// marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		// marshaller.setSchema(schema);
		// JAXBElement<Emit> jaxbElement = new
		// ObjectFactory().createTNFeInfNFeEmit();
		// marshaller.marshal(jaxbElement, System.out);
		//
		// jaxbElement
		//
		// System.out.print(emit);

		//
		// } catch (SAXException e1) {
		// logger.error(e1);
		// } catch (JAXBException e) {
		// logger.error(e);
		// // } catch (IOException e) {
		// // logger.error(e);
		// } catch (URISyntaxException e) {
		// logger.error(e);
		// }
	}

}
