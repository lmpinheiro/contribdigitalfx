package br.com.cd.util;

import javafx.application.Platform;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import org.apache.log4j.Logger;
import org.hibernate.annotations.common.util.StringHelper;

/**
 * Set Mask on text fields
 * 
 * @author lmpinheiro
 *
 */
public abstract class MaskFieldUtil {
	private static final Logger logger = Logger.getLogger(MaskFieldUtil.class);
	
	 /**
     * CPF Mask.
     */
    public static void cpfField(final TextField textField) {
    	logger.info("Calling cpfField");
    	
    	maxTextField(textField, 14);
    		    	
        textField.lengthProperty().addListener( (observableValue,  number,  number2) -> {
                String value = textField.getText();
                
                value = value.replaceAll("[^0-9]", "");
                value = value.replaceFirst("(\\d{3})(\\d)", "$1.$2");
                value = value.replaceFirst("(\\d{3})\\.(\\d{3})(\\d)", "$1.$2.$3");
                value = value.replaceFirst("\\.(\\d{3})(\\d)", ".$1-$2");
                textField.setText(value);
                positionCaret(textField);
        });
    }
	
	/**
     * CNPJ and CPF Mask.
     */
    public static void cpfCnpjField(final TextField textField, boolean isCpf) {
    	logger.info("Calling cpfCnpjField");

    	textField.lengthProperty().addListener( (observableValue,  number,  number2) -> {
    		if(isCpf){
	            String value = textField.getText();
	            
	            value = value.replaceAll("[^0-9]", "");
	            value = value.replaceFirst("(\\d{3})(\\d)", "$1.$2");
	            value = value.replaceFirst("(\\d{3})\\.(\\d{3})(\\d)", "$1.$2.$3");
	            value = value.replaceFirst("\\.(\\d{3})(\\d)", ".$1-$2");
	            textField.setText(value);
	            positionCaret(textField);
    		}else{
    			String value = textField.getText();
	                
            	value = value.replaceAll("[^0-9]", "");
                value = value.replaceFirst("(\\d{2})(\\d)", "$1.$2");
                value = value.replaceFirst("(\\d{2})\\.(\\d{3})(\\d)", "$1.$2.$3");
                value = value.replaceFirst("\\.(\\d{3})(\\d)", ".$1/$2");
                value = value.replaceFirst("(\\d{4})(\\d)", "$1-$2");
                textField.setText(value);
                positionCaret(textField);
    		}
    });
    }
    
    
    /**
     * CNPJ Field.
     */
    public static void cnpjField(final TextField textField) {
    	logger.info("Calling cnpjField");
    	
    	maxTextField(textField, 18);
    	
        textField.lengthProperty().addListener( (observableValue,  number,  number2) -> {
                String value = textField.getText();
                
                value = value.replaceAll("[^0-9]", "");
                value = value.replaceFirst("(\\d{2})(\\d)", "$1.$2");
                value = value.replaceFirst("(\\d{2})\\.(\\d{3})(\\d)", "$1.$2.$3");
                value = value.replaceFirst("\\.(\\d{3})(\\d)", ".$1/$2");
                value = value.replaceFirst("(\\d{4})(\\d)", "$1-$2");
                textField.setText(value);
                positionCaret(textField);
        });
    }
    
	
	/**
     * Set date mask (dd/MM/yyyy).
     */
    public static void dateField(final TextField textField) {
    	logger.info("Calling dateField");
    	
        maxTextField(textField, 10);
        textField.lengthProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.intValue() < 11) {
                    String value = textField.getText();
                    value = value.replaceAll("[^0-9]", "");
                    value = value.replaceFirst("(\\d{2})(\\d)", "$1/$2");
                    value = value.replaceFirst("(\\d{2})\\/(\\d{2})(\\d)", "$1/$2/$3");
                    textField.setText(value);
                    positionCaret(textField);
                }
        });
    }
    
    
    /**
     * Set DateHour Mask (dd/MM/yyyy hh:mm:ss).
     */
    public static void dateHourField(final TextField textField) {
    	logger.info("Calling dateHourField");
    	
        maxTextField(textField, 19);
        textField.lengthProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.intValue() < 20) {
                    String value = textField.getText();
                    value = value.replaceAll("[^0-9]", "");
                    value = value.replaceFirst("(\\d{2})(\\d)", "$1/$2");
                    value = value.replaceFirst("(\\d{2})\\/(\\d{2})(\\d)", "$1/$2/$3");
                    value = value.replaceFirst("(\\d{4})(\\d)", "$1 $2");
                    value = value.replaceFirst("\\ (\\d{2})(\\d)", " $1:$2");
                    value = value.replaceFirst("\\:(\\d{2})(\\d)", ":$1:$2");
                    textField.setText(value);
                    
                    positionCaret(textField);
                }
        });
    }
    
    
    /**
     * Set date mask (MM/yyyy).
     */
    public static void dateMonthYearField(final TextField textField) {
    	logger.info("Calling dateMonthYearField");
    	
        maxTextField(textField, 7);
        textField.lengthProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.intValue() < 8) {
                    String value = textField.getText();
                    value = value.replaceAll("[^0-9]", "");
                    value = value.replaceFirst("(\\d{2})(\\d)", "$1/$2");
                    value = value.replaceFirst("(\\d{2})\\/(\\d{4})(\\d)", "$1/$2/$3");
                    textField.setText(value);
                    positionCaret(textField);
                }
        });
    }
    
    /**
     * Set date mask (yyyy/yyyy).
     */
    public static void dateYearField(final TextField textField) {
    	logger.info("Calling dateYearField");
    	
        maxTextField(textField, 9);
        textField.lengthProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.intValue() < 10) {
                    String value = textField.getText();
                    value = value.replaceAll("[^0-9]", "");
                    value = value.replaceFirst("(\\d{4})(\\d)", "$1/$2");
                    value = value.replaceFirst("(\\d{4})\\/(\\d{4})(\\d)", "$1/$2/$3");
                    textField.setText(value);
                    positionCaret(textField);
                }
        });
    }
    
    /**
     * License Plate Mask.
     */
    public static void licensePlateField(final TextField textField) {
    	logger.info("Calling licensePlateField");
    	
    	maxTextField(textField, 8);
    		    	
        textField.lengthProperty().addListener( (observableValue,  number,  number2) -> {
                String value = textField.getText().toUpperCase();
                
                if(value.length()>3){
                	value = value.substring(0, 3) +value.substring(3, value.length()).replaceAll("\\D{1,4}$", "");
                }else{
                	value = value.replaceAll("[0-9]", "");
                }
                value = value.replaceFirst("([A-Z]{1,3})([0-9])", "$1-$2");
                textField.setText(value);
                positionCaret(textField);
        });
    }
    
    
    /**
     * Monetary Field.
     */
    public static void monetaryField(int intDigits, int decimalDigits, final TextField textField) {
    	logger.info("Calling monetaryField");
    	
        textField.lengthProperty().addListener((observable, oldValue, newValue) -> {
                String value = textField.getText();
                value = value.replaceAll("[^0-9]", "");
                value = value.replaceAll("([0-9]{1})([0-9]{"+(12+decimalDigits)+"})$", "$1.$2");
                value = value.replaceAll("([0-9]{1})([0-9]{"+(9+decimalDigits)+"})$", "$1.$2");
                value = value.replaceAll("([0-9]{1})([0-9]{"+(6+decimalDigits)+"})$", "$1.$2");
                value = value.replaceAll("([0-9]{1})([0-9]{"+(3+decimalDigits)+"})$", "$1.$2");
                value = value.replaceAll("([0-9]{1})([0-9]{"+decimalDigits+"})$", "$1,$2");
                
                textField.setText(value);
                positionCaret(textField);
                
                

                textField.textProperty().addListener((observable1, oldValue1, newValue1) -> {
                        if (removeMaskNumeral(newValue1).length() > intDigits+decimalDigits)
                            textField.setText(oldValue1);
                });
                
        });
        
        textField.focusedProperty().addListener((observableValue, aBoolean, fieldChange)-> {
                if (!fieldChange) {
                    final int length = textField.getText().length();
                    if (length > 0 && length <= decimalDigits) {
                        textField.setText(textField.getText() + StringHelper.repeat("0", decimalDigits));
                    }
                }
        });
    }
    
    
    /**
     * Set max size on TextArea
     */
    public static void maxTextArea(final TextArea textArea, final Integer length) {
    	logger.info("Calling maxTextArea");
    	
        textArea.textProperty().addListener((observableValue, oldValue, newValue) -> {
                if (newValue.length() > length)
                    textArea.setText(oldValue);
        });
    }
    
    
    /**
     * Set max size on TextField
     */
    public static void maxTextField(final TextField textField, final Integer length) {
    	logger.info("Calling maxTextField");
    	
        textField.textProperty().addListener((observableValue, oldValue, newValue) -> {
                if (newValue.length() > length)
                    textField.setText(oldValue);
        });
    }
    
    
    /**
     * Only numeric digits.
     */
    public static void numericField(final TextField textField) {
    	logger.info("Calling numericField");
    	
        textField.lengthProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.intValue() > oldValue.intValue()) {
                    char ch = textField.getText().charAt(oldValue.intValue());
                    if (!(ch >= '0' && ch <= '9')) {
                        textField.setText(textField.getText().substring(0, textField.getText().length() - 1));
                    }
                }
        });
    }

    
    /**
     * Phone Mask.
     */
    public static void phoneField(final TextField textField) {
    	logger.info("Calling phoneField");
    	
		maxTextField(textField, 15);
		
        textField.lengthProperty().addListener((observableValue, number, number2) -> {
                String value = textField.getText();
                value = value.replaceAll("[^0-9]", "");
                value = value.replaceFirst("(\\d{2})(\\d)", "($1) $2");
                if(value.length()>13)
                	value = value.replaceFirst("(\\d{5})(\\d)", "$1-$2");
                else
                	value = value.replaceFirst("(\\d{4})(\\d)", "$1-$2");
                
                textField.setText(value);
                positionCaret(textField);
        });
    }
    
    
    /**
     * Set cursor positions on string ends
     */
    private static void positionCaret(final TextField textField) {
    	logger.info("Calling positionCaret");
    	
        Platform.runLater(()-> textField.positionCaret(textField.getText().length()));
    }
    
    
    /**
     * Remove Mask - Numeral
     */
    public static String removeMaskNumeral(String str) {
    	logger.info("Calling removeMaskNumeral");
    	
		return str.replaceAll("\\D", "");
	}
    
    /**
     * Remove Mask
     */
    public static String removeMask(String str) {
    	logger.info("Calling removeMask");
    	
		return  str.replaceAll("[^A-Za-z0-9]", "");
	}
    
    
    /**
     * Remove Monetary Mask
     */
    public static String removeMonetaryMask(String str) {
    	logger.info("Calling removeMonetaryMask");
    	
		return str.replaceAll("\\.", "");
	}
    
    
    /**
     * Zip Code Mask.
     */
    public static void zipCodeField(final TextField textField) {
    	logger.info("Calling zipCodeField");
    	
        maxTextField(textField, 9);

        textField.lengthProperty().addListener((observableValue, number, number2) ->{
                String value = textField.getText();
                value = value.replaceAll("[^0-9]", "");
                value = value.replaceFirst("(\\d{5})(\\d)", "$1-$2");
                textField.setText(value);
                positionCaret(textField);
        });

    }
    
}
