package br.com.cd.style;

import javafx.scene.control.TextField;

/**
 * Metro Skin for Text Field
 * 
 * @author lmpinheiro
 *
 */
public class MetroTextFieldSkin extends TextFieldWithButtonSkin{
    public MetroTextFieldSkin(TextField textField) {
        super(textField);
    }

    @Override
    protected void rightButtonPressed()
    {
        getSkinnable().setText("");
    }

}