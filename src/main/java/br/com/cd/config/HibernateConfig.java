package br.com.cd.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
 
/**
 * Hibernate Configuration Class
 * 
 * @author lmpinheiro
 */

@Configuration
@EnableTransactionManagement
@ComponentScan({ "br.com.cd.config" })
@PropertySource(value = { "classpath:application.properties" })
public class HibernateConfig {
	private static final Logger logger = Logger.getLogger(HibernateConfig.class);
	
    @Autowired
    private Environment environment;
 
    /**
     * SessionFactory Create Method
     * 
     * @return Session Data Base
     */
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
    	logger.info("Starting Session Factory Bean");
    	
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[] { "br.com.cd.model" });
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
     }
     
    /**
     * Data Source properties set
     * 
     * @return Driver Properties
     */
    @Bean
    public DataSource dataSource() {
    	logger.info("Starting Data Source");
    	
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
        dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
        return dataSource;
    }
     
    
    /**
     * Hibernate properties set
     * 
     * @return Hibernate Properties
     */
    private Properties hibernateProperties() {
    	logger.info("Getting Hibernate Properties");
    	
        Properties properties = new Properties();
        properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
        properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
        properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
        properties.put("connection.autocommit", "true");
        return properties;        
    }
     
    /**
     * TransactionManager create and Session Factory set
     * 
     * @param s
     * @return HibernateTransactionManager
     */
    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory s) {
    	logger.info("Starting Transaction Manager Bean");
    	
    	HibernateTransactionManager txManager = new HibernateTransactionManager();
    	txManager.setSessionFactory(s);
    	return txManager;
    }
}
