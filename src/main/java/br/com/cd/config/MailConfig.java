package br.com.cd.config;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

/**
 * Class set mail sender properties
 * 
 * @author lmpinheiro
 *
 */
@Service("mailService")
@PropertySource(value = { "classpath:application.properties" })
public class MailConfig {
	private static final Logger logger = Logger.getLogger(MailConfig.class);
	
	/**
	 * Object that contains context properties
	 */
	@Autowired
    private Environment environment;

    @Bean
    public JavaMailSender javaMailSender() {
    	logger.info("Starting Java Mail Sender Bean");
    	
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        Properties mailProperties = new Properties();
        mailProperties.put("mail.smtp.auth", Boolean.parseBoolean(environment.getProperty("mail.smtp.auth")));
        mailProperties.put("mail.smtp.starttls.enable", Boolean.parseBoolean(environment.getProperty("mail.smtp.starttls.enable")));
        mailProperties.put("mail.debug", Boolean.parseBoolean(environment.getProperty("mail.debug")));
        mailProperties.put("mail.smtp.socketFactory.port", environment.getProperty("mail.smtp.socketFactory.port"));    
        mailProperties.put("mail.smtp.socketFactory.fallback", Boolean.parseBoolean(environment.getProperty("mail.smtp.socketFactory.fallback")));    
        mailProperties.put("mail.smtp.socketFactory.class", environment.getProperty("mail.smtp.socketFactory.class"));
        mailSender.setJavaMailProperties(mailProperties);
        mailSender.setHost(environment.getProperty("mail.host"));
        mailSender.setPort(Integer.parseInt(environment.getProperty("mail.port")));
        mailSender.setProtocol(environment.getProperty("mail.protocol"));
        mailSender.setUsername(environment.getProperty("mail.username"));
        mailSender.setPassword(environment.getProperty("mail.password"));
        return mailSender;
    }
}