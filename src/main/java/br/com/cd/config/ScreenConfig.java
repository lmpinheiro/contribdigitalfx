package br.com.cd.config;

import java.io.IOException;
import java.util.List;

import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Configuration;

import br.com.cd.control.DialogController;
import br.com.cd.control.InjectionController;
import br.com.cd.control.ScreenController;

/**
 * 
 * Auxiliary Class for view layer
 * 
 * @author lmpinheiro
 * 
 */
@Configuration
public class ScreenConfig {
	private static final Logger logger = Logger.getLogger(ScreenConfig.class);
	
	/**
	 * Dialog Types Enun
	 *
	 */
	public static enum DialogType { 
		ERROR, INFORMATION, QUESTION;
	}
	
	
	/**
	 * Remote return about question's dialog
	 */
	private boolean dialogReturn = false;

	
	/**
	 * 
	 * Alert Java Default Model
	 * 
	 * @param alertType
	 * @param s
	 */
	@Deprecated
	public static void dialog(Alert.AlertType alertType, String s) {
		logger.info("Calling System Dialog");
		
		Alert alert = new Alert(alertType, s);
		alert.initStyle(StageStyle.UNDECORATED);
		alert.setTitle("Info");
		alert.showAndWait();
	}

	
	/**
	 * Stylized Default System's Dialog
	 * 
	 * @param Title dialog
	 * @param Content dialog
	 * @param Dialog Type (ERROR, INFORMATION, QUESTION)
	 * 
	 * @return If was choosed QUESTION type, Yes or No will be returned
	 */
	public boolean dialogDefault(String title, List<String> content, DialogType dialogType) {
		logger.info("Calling Default Dialog: "+dialogType);
		try {
			Stage stage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			if(dialogType == DialogType.ERROR){
				loader.setLocation(getClass().getResource("/br/com/cd/view/fxml/dialog-error.fxml"));
			}else if(dialogType == DialogType.INFORMATION){
				loader.setLocation(getClass().getResource("/br/com/cd/view/fxml/dialog-information.fxml"));
			}else if(dialogType == DialogType.QUESTION){
				loader.setLocation(getClass().getResource("/br/com/cd/view/fxml/dialog-question.fxml"));
			}
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root, Color.TRANSPARENT);
			stage.setScene(scene);
			stage.initStyle(StageStyle.TRANSPARENT);
			stage.initModality(Modality.APPLICATION_MODAL);
			DialogController controller = loader.getController();
			controller.setStageParent(stage);
			controller.setTitle(title);
			controller.setContent(content);
			controller.setScrenConfig(this);
			
			stage.showAndWait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dialogReturn;
	}
	
	
	/**
	 * Set return about question's dialog
	 */
	public void setDialogReturn(boolean dialogReturn){
		logger.info("Setting Dialog Return: "+dialogReturn);
		
		this.dialogReturn = dialogReturn;
	}


	/**
	 * Creates a new Stage
	 * 
	 * @param Stage that called new Stage
	 * @param lb to get stage window properties
	 * @param load FXML Directory to be called
	 * @param title of new Stage
	 * @param resize - If this new stage will be resizable
	 * @param style - Stage Style Properties
	 * @param maximized - If this new stage will start maximized
	 * @param screenParent - ScreenController Parent
	 * @param data - Any data to transfer
	 */
	public void newStage(Stage stage, Label lb, String load, String title,
			boolean resize, StageStyle style, Modality modality, boolean maximized, ScreenController screenParent, Object data, boolean closeParent) {
		logger.info("New Stage: "+load);
		
		try {
			Stage st = new Stage();
			stage = (Stage) lb.getScene().getWindow();
			
			FXMLLoader loader = new FXMLLoader();
			loader.setControllerFactory((clazz) ->{
					return InjectionController.getContext().getBean(clazz);
			});
			
			loader.setLocation(getClass().getResource("/br/com/cd/view/fxml/"+load));
			Parent root = (Parent) loader.load();
			
			ScreenController controller = loader.getController();
			controller.setData(data);
			controller.setStageParent(st);
			Scene scene = new Scene(root, Color.TRANSPARENT);
			if(modality!=null)
				st.initModality(modality);
			st.initStyle(style);
			st.setResizable(resize);
			st.setMaximized(maximized);
			st.setTitle(title);
			st.setScene(scene);
			st.getIcons().add(new Image("/br/com/cd/view/img/app-icon.png"));
				
        	st.show();
        	if(closeParent)stage.close();
        	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Load a new pane
	 * 
	 * @param AnchorPane to incorpore
	 * @param Pane to incorpore
	 */
	public void loadAnchorPane(AnchorPane ap, String a, Object data){
	    try {
	    	
	    	FXMLLoader loader = new FXMLLoader();
	    	loader.setControllerFactory((clazz) ->{
				return InjectionController.getContext().getBean(clazz);
	    	});
	    	
			loader.setLocation(getClass().getResource("/br/com/cd/view/fxml/"+a));
			AnchorPane root = (AnchorPane) loader.load();
			ScreenController controller = loader.getController();
			controller.setData(data);
			
			ap.getChildren().setAll(root);
	    } catch (LoadException e){
	    	e.printStackTrace();
	           System.out.println(e.getMessage());
	    } catch (IOException e) {
	    	System.out.println(e.getMessage());
		}
    }
	
	
	/**
	 * Set model column
	 */
	public void setModelColumn(TableColumn tb,String a, boolean visible){
        tb.setCellValueFactory(new PropertyValueFactory(a));
        tb.setVisible(visible);
    }
	
	
	/**
	 * Embed a external font on App
	 */
//	public void fontLoad(){
//		Font.loadFont(
//			  ScreenConfig.class.getResource("TRON.TTF").toExternalForm(), 
//			  10
//			);
//	}
}
