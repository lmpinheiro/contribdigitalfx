package br.com.cd.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

/**
 * Scan class to incorporate Spring on context application
 * 
 * @author lmpinheiro
 */
@Configuration
@ComponentScan(basePackages = "br.com.cd")
@EnableGlobalMethodSecurity(securedEnabled = true, proxyTargetClass = true)
public class AppConfig {
	
}
