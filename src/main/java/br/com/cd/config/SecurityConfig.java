package br.com.cd.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

/**
 * Configure Security Global Settings
 * 
 * securedEnabled = Spring Security's annotations should be enabled
 * 
 * @author lmpinheiro
 */
@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig{
	
    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder auth) 
      throws Exception {
        auth.inMemoryAuthentication().withUser("default")
          .password("password").roles("DEFAULT");
    }
}